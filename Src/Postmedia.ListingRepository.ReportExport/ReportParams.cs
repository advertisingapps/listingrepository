﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Postmedia.ListingRepository.ReportEmailExport
{
    public enum ReportParams
    {
        Report,
        Division, 
        RunDate
    }
}
