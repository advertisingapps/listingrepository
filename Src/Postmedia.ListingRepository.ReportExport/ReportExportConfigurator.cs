using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Postmedia.ListingRepository.Core.Communication;
using Postmedia.ListingRepository.Core.Configuration;
using Postmedia.ListingRepository.Core.EMail;
using Postmedia.ListingRepository.Core.EnumExtensions.RESTMethodExtensions;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports.Working;
using log4net;

namespace Postmedia.ListingRepository.ReportEmailExport
{
    public class ReportExportConfigurator  
    {
        private bool emailResults = true;
        // set email constants
//        private string workingDefaultEmailAddress = ConfigurationManager.AppSettings[
//                                    ServiceSettings.DefaultWorkingEmailAccount.ToString()];
        private string drivingDefaultEmailAddress = "";
       
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static string _methodParamString = "/Method:";
        private static string[] _exportPrefixes = new[] { _methodParamString, "/Feed:", "/Daily:", "/RunDate:" };
        private static string[] _ymmImportPrefixes = new[] { _methodParamString };

        private Dictionary<string, Type> MethodCriterias { get; set; }

        public void GetCriteria(string[] args)
        {
            if(MethodCriterias.Count == 0)
            {
                InitializeMethodCriterias();
            }
            Log.Info("Entered - GetCriteria()");
            
            if (args.Any())
            {
                var param = args[0].Substring(args[0].IndexOf(':') + 1, args[0].Length - _methodParamString.Length).Trim();
                var method = ((RESTMethod)Enum.Parse(typeof(RESTMethod), param, true));
            }

            Log.Info("Leaving - GetCriteria()");
        }

        private void InitializeMethodCriterias()
        {
            MethodCriterias = new Dictionary<string, Type>();
            MethodCriterias.Add("export", typeof(ExportCriteria));
            MethodCriterias.Add("report", typeof(ExportCriteria));
        }

        public void GetExportCriteria(string[] args)
        {
            Log.Info("Entered - GetExportCriteria()");
            string reportname = "";
            if (args.Count() >= 2)
            {
                Log.Info("Arg count accepted - GetExportCriteria()");
                for (int index = 0; index < args.Length; index++)
                {
                    var arg = args[index];
                    var param = arg.Substring(arg.IndexOf(':') + 1, arg.Length - _exportPrefixes[index].Length).Trim();
                    if (index == 0)
                    {
                        reportname = param;
                        switch (param)
                        {
                            case "WorkingDigitalDisplay":
                                var DigitalDisplayCriteria = new DigitalDisplayCriteria();

                                DigitalDisplayCriteria.EmailResults = emailResults;
                                break;
                            case "WorkingSAMS":
                                var SAMSCriteria = new SAMSCriteria();
                                break;
                            default:
                                Log.Error("Could not locate the passed report name - "+reportname);
                                break;
                        }
                        Log.Info("ReportName found: " + reportname + "- GetExportCriteria()");
                    }
                }
            }
            else
            {
                var message = "You must provide a reportname and at least one additional parameter";
                Console.WriteLine(message);
                throw new ArgumentException(message);
            }
            Log.Info("Returned ExportCriteria Details");
            Log.Info("FeedName: " + reportname + " - GetExportCriteria()");
        }

    }
}