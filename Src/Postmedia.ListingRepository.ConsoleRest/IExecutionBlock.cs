﻿namespace Postmedia.ListingRepository.ConsoleRest
{
    public interface IExecutionBlock
    {
        void Run(string[] args);
    }
}
