﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Configuration;
using System.Net;
using System.Threading;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.Communication;
using Postmedia.ListingRepository.Core.EMail;
using Postmedia.ListingRepository.Core.Translators;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports.Working;
using log4net;

namespace Postmedia.ListingRepository.ConsoleRest
{
    /******************************************************************************************************
    * Class:        RefreshListing
    * Methods:      RefreshListing(ILog), Run(String[]), RefreshListingId(String), LogInfo(String)
    * Description:  Designed to read a text file containing Ids/Numbers (new line delimited) of Listings
    *               created without outbound cash entries.      
    ******************************************************************************************************/
    class RefreshListing : IExecutionBlock
    {
        private ILog Log { get; set; }
        
        public RefreshListing(ILog log)
        {   
            Log = log;
            //grab url to services
            RESTClient.BaseUrl = ConfigurationManager.AppSettings[WebAppSettings.ServiceEndpoint.ToString()];
        }

        /**************************************************************************
         * Method:      Run(String[])
         * Parameter:   String[] args
         *                - args[0] - Filepath to file containing listings
         * Description: Entry point for processing
         **************************************************************************/
        public void Run(string[] args)
        {
            String listingId;

            try
            {
                if (args.Length == 0) throw new Exception();
                if (!File.Exists(args[0])) throw new Exception("File Does Not Exists!");

                //get listing id/number in file
                StreamReader sr = new StreamReader(args[0]);
                while (sr.Peek() != -1)
                {
                    
                    listingId = sr.ReadLine();

                    LogInfo("Now Processing " + listingId);
                    try
                    {
                        listingId = RefreshListingId(listingId);
                    }
                    catch (CustomException ex)
                    {
                        LogInfo("Unsuccessfully Processed [" + ex.Message + "]");
                    }

                    LogInfo("Successfully Processed " + listingId);
                }
                sr.Dispose();
                sr.Close();
                sr = null;
            }
            catch (Exception ex)
            {
                LogInfo("Run(): " + ex.Message);
            }

            while (Console.ReadLine() != "y") ;
        }

        /**************************************************************************
         * Method:      Refresh
         * Parameter:   - String id - Listing id/number key 
         *                  
         * Description: Will determine if Listing exists and will update listing.
         **************************************************************************/
        public String RefreshListingId(String id)
        {
            int convertedId;

            try
            {
                var method = int.TryParse(id, out convertedId) ? RESTMethod.GetListingById : RESTMethod.GetListingByNumber;

                var retrievedListing = RESTClient.Instance.Call<Listing>(id, method);

                if (retrievedListing != null)
                {
                    var method2 = RESTMethod.UpdateListing;
                    var updatedListing = RESTClient.Instance.Call<Listing, Listing>(retrievedListing, retrievedListing.ListingNumber, method2);
                    
                    if (updatedListing == null) throw new Exception("Listing [" + id + "] Listing was not saved");

                    return updatedListing.ListingNumber;
                }
                else
                {
                    throw new Exception("Listing [" + id + "] does not exists");
                }
            }
            catch (Exception ex)
            {
                throw new CustomException("RefreshListingId(String): " + ex.Message);
            }
        }

        private void LogInfo(String info)
        {
            Console.WriteLine("Module RefreshListing: " + info);
            Log.Info("Module RefreshListing: " + info);
        }        
    }

    class CustomException : Exception
    {
        public CustomException() : base() { }
        public CustomException(String str) : base(str) { }
        public override string ToString() { return Message; }
    }
}

/*//public void RefeshListingId(String listingID)
//{
//    string UriRefreshListing = "cache/refresh/", UriGetListing = "listings/";
//    String url;
//    HttpWebRequest request;
//    _resetEvent = new ManualResetEvent(false);

//    url = ConfigurationManager.AppSettings.Get("ServiceEndpoint") + UriGetListing + listingID;
//    request = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
//    request.Method = WebMethod;

//    request.BeginGetResponse(GetResponse, request);

//    _resetEvent.WaitOne();

//    _resetEvent.Close();
//    request = null;
//    _resetEvent = null;
//}

//private void GetResponse(IAsyncResult ar)
//{
//    try
//    {
//        var request = ar.AsyncState as HttpWebRequest;
//        var response = request.EndGetResponse(ar) as HttpWebResponse;

//        if (response != null)
//        {
//            LogInfo(response.Server);
//            LogInfo(response.ResponseUri.ToString());
//            LogInfo(response.StatusCode.ToString());

//            var Answer = response.GetResponseStream() as Stream;
//            var _Answer = new StreamReader(Answer) as StreamReader;

//            LogInfo("Response: " + _Answer.ReadToEnd());

//            _Answer.Close();
//            Answer.Close();
//            response.Close();
//            _resetEvent.Set();
//        }
//        else
//        {
//            throw new Exception("No Response Recieved!");
//        }

//    }
//    catch (Exception ex)
//    {
//        LogInfo("Error: " + ex.Message);
//        _resetEvent.Set();
//    }
//}


//public void RefreshListingId(String listingID)
//{
//    String url;
//    HttpWebRequest request;
//    _resetEvent = new ManualResetEvent(false);

//    url = ConfigurationManager.AppSettings.Get("ServiceEndpoint") + UriTemplate + listingID;
//    request = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
//    request.Method = WebMethod;

//    request.BeginGetResponse(GetResponse, request);

//    _resetEvent.WaitOne();

//    _resetEvent.Close();
//    request = null;
//    _resetEvent = null;
//}

//private void GetResponse(IAsyncResult ar)
//{
//    try
//    {
//        var request = ar.AsyncState as HttpWebRequest;
//        var response = request.EndGetResponse(ar) as HttpWebResponse;

//        if (response != null)
//        {
//            LogInfo(response.Server);
//            LogInfo(response.ResponseUri.ToString());
//            LogInfo(response.StatusCode.ToString());

//            var Answer = response.GetResponseStream() as Stream;
//            var _Answer = new StreamReader(Answer) as StreamReader;

//            LogInfo("Response: " + _Answer.ReadToEnd());

//            _Answer.Close();
//            Answer.Close();
//            response.Close();
//            _resetEvent.Set();
//        }
//        else
//        {
//            throw new Exception("No Response Recieved!");
//        }

//    }
//    catch (Exception ex)
//    {
//        LogInfo("Error: " + ex.Message);
//        _resetEvent.Set();
//    }
//}


//public void Execute(Dictionary<string, string> listingIDs)
//{
//    String url;
//    HttpWebRequest request;

//    foreach (KeyValuePair<string, string> listings in listingIDs)
//    {
//        url = ConfigurationManager.AppSettings.Get("ServiceEndpoint") + UriTemplate + listings.Key;
//        request = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
//        request.Method = WebMethod;

//        request.BeginGetResponse(GetResponse, request);
                                
//        _resetEvent.WaitOne();
//        Console.WriteLine("HIT");
//        request = null;
//    }
//}

//String listId;
//listId = "CAL473326";
//var url = ConfigurationManager.AppSettings.Get("ServiceEndpoint") + UriTemplate + listId;
//var request = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
//request.Method = WebMethod;
//var response = (HttpWebResponse)request.GetResponse();

//Console.WriteLine(response.StatusCode);
//Console.WriteLine(response.Server);

//Stream Answer = response.GetResponseStream();
//StreamReader _Answer = new StreamReader(Answer);
//Console.WriteLine(_Answer.ReadToEnd());
//_Answer.Close();*/