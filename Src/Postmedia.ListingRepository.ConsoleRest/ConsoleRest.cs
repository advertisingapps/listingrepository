﻿using System;
using System.Linq;
using System.Reflection;
using Postmedia.ListingRepository.ConsoleRest.ReportExport.Working.QRCodeUrl;
using log4net;
using Postmedia.ListingRepository.ConsoleRest.ReportExport.Working.SAMS;
using Postmedia.ListingRepository.ConsoleRest.ReportExport.Working.SamsNoFeedback;

namespace Postmedia.ListingRepository.ConsoleRest
{
    public class ConsoleRest
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static string _moduleParamString = "/Module:";

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            if(args.Any())
            {
                IExecutionBlock exe;

                var arg = args[0];
                var module = arg.Substring(arg.IndexOf(':') + 1, arg.Length - _moduleParamString.Length).Trim();

                switch (module)
                {
                    case "BatchExport":
                        exe = new BatchExport.BatchExport(Log);
                        break;
                    case "UpdateYMM":
                        exe = new UpdateYMM.UpdateYMM(Log);
                        break;
                    case "QRCodeUrl":
                        exe = new QRCodeUrl(Log);
                        break;
                    case "WorkingSAMS":
                        exe = new SAMS(Log);
                        break;
                    case "WorkingSamsNoFeedback":
                        exe = new SamsNoFeedback(Log);
                        break;
                    case "RefreshListing":
                        exe = new RefreshListing(Log);
                        break;
                    default:
                        exe = null;
                        break;
                }

                if(exe != null)
                {
                    string[] passedArgs = null;
                    if (args.Count() > 1)
                    {
                        passedArgs = new string[args.Count() - 1];
                        for (int i = 1; i < args.Count(); i++)
                        {
                            passedArgs[i - 1] = args[i];    
                        }
                    }
                    exe.Run(passedArgs);
                }
            }
            else
            {
                Console.WriteLine("You must supply an execution type. Please make your call again.");
            }
        }
    }
}
