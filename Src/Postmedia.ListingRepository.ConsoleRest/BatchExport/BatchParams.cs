namespace Postmedia.ListingRepository.ConsoleRest.BatchExport
{
    public enum BatchExportParams
    {
        FeedName, 
        DailyExport,
        RunDate
    }
}