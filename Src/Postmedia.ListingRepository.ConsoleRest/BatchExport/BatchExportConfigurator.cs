using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Postmedia.ListingRepository.Core.Communication;
using Postmedia.ListingRepository.DTO;
using log4net;

namespace Postmedia.ListingRepository.ConsoleRest.BatchExport
{
    public class BatchExportConfigurator  
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private ExportCriteria Criteria { get; set; }
        private static string _methodParamString = "/Method:";
        private static string[] _exportPrefixes = new[] { "/Feed:", "/Daily:", "/RunDate:" };
        private static string[] _ymmImportPrefixes = new[] { _methodParamString };

        private Dictionary<string, Type> MethodCriterias { get; set; }

        public void GetCriteria(string[] args)
        {
            if(MethodCriterias.Count == 0)
            {
                InitializeMethodCriterias();
            }
            Log.Info("Entered - GetCriteria()");
            
            if (args.Any())
            {
                var param = args[0].Substring(args[0].IndexOf(':') + 1, args[0].Length - _methodParamString.Length).Trim();
                var method = ((RESTMethod)Enum.Parse(typeof(RESTMethod), param, true));
            }

            Log.Info("Leaving - GetCriteria()");
        }

        private void InitializeMethodCriterias()
        {
            MethodCriterias = new Dictionary<string, Type>();
            MethodCriterias.Add("export", typeof(ExportCriteria));
            MethodCriterias.Add("report", typeof(ExportCriteria));
        }

        public ExportCriteria GetExportCriteria(string[] args)
        {
            Log.Info("Entered - GetExportCriteria()");
            if (args.Count() >= 2)
            {
                Log.Info("Arg count accepted - GetExportCriteria()");
                Criteria = new ExportCriteria();
                for (int index = 0; index < args.Length; index++)
                {
                    var arg = args[index];
                    var param = arg.Substring(arg.IndexOf(':') + 1, arg.Length - _exportPrefixes[index].Length).Trim();
                    if (index == (int)BatchExportParams.FeedName)
                    {
                        Criteria.FeedName = param;
                        Log.Info("FeedName found: " + Criteria.FeedName + "- GetExportCriteria()");
                    }
                    if (index == (int)BatchExportParams.DailyExport)
                    {
                        Criteria.DailyExport = false;
                        if ((param.ToLower() == "y") || (param.ToLower() == "yes"))
                        {
                            Criteria.DailyExport = true;
                            Log.Info("DailyExport determined: " + Criteria.DailyExport + "- GetExportCriteria()");
                        }
                    }
                    if (index == (int)BatchExportParams.RunDate)
                    {
                        Log.Info("RunDate provided: " + param + " - GetExportCriteria()");
                        Criteria.RunDate = DateTime.Parse(param);
                        Log.Info("RunDate provided: " + param + ", Parsed As: Year(" + Criteria.RunDate.Value.Date.Year + ") Month (" + Criteria.RunDate.Value.Date.Month + ") Day (" + Criteria.RunDate.Value.Date.Day + ") - GetCritieria()");
                    }
                }
            }
            else
            {
                var message = "You must provide a feedname, a Daily / Non Daily parameter and (optionally) a Run Date parameter";
                Console.WriteLine(message);
                throw new ArgumentException(message);
            }
            Log.Info("Returned ExportCriteria Details");
            Log.Info("RunDate: " + Criteria.RunDate );
            Log.Info("Daily: " + Criteria.DailyExport);
            Log.Info("FeedName: " + Criteria.FeedName + " - GetExportCriteria()");
            return Criteria;
        }

    }
}