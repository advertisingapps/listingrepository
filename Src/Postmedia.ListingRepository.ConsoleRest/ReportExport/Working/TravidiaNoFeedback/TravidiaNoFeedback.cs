﻿using System;
using System.Configuration;
using System.Net;
using System.Threading;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.EMail;
using Postmedia.ListingRepository.Core.Translators;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports.Working;
using log4net;

namespace Postmedia.ListingRepository.ConsoleRest.ReportExport.Working.TravidiaNoFeedback
{
    public class TravidiaNoFeedback : IExecutionBlock
    {
        private ILog Log { get; set; }
        static ManualResetEvent _resetEvent = new ManualResetEvent(false);
        private static TravidiaNoFeedbackCriteria Criteria { get; set; }


        private const string WebMethod = "POST";
        private const string ContentType = "application/xml";
        private const string UriTemplate = "reports/workingtravidianofeedback";

        public TravidiaNoFeedback(ILog log)
        {
            Log = log;
        }


        public void Run(string[] args)
        {
            // get our criteria populated
            Criteria = new TravidiaNoFeedbackCriteria();

            //TODO: get criteria for BatchExport (only)
            Criteria.Date = DateTime.Today;
            Criteria.Division = "";
            Criteria.EmailResults = true;
            Criteria.EmailAddress = ConfigurationManager.AppSettings[
                                        ServiceSettings.DefaultWorkingEmailAccount.ToString()];
            
            //execute as normal
            ExecuteBatchAsync();
        }

        private void ExecuteBatchAsync()
        {
            try
            {
                Log.Info("Retrieving service endpoint");
                var url = ConfigurationManager.AppSettings.Get("ServiceEndpoint") + UriTemplate;
                Log.Info("Service endpoint retrieved");
                Console.WriteLine("Service endpoint retrieved");
                var rest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
                Log.Info("Web request created at: " + url);
                Console.WriteLine("Web Request Created at" + url);
                if (rest != null)
                {
                    rest.Method = WebMethod;
                    rest.ContentType = ContentType;
                    Log.Info("Begining request stream");
                    Console.WriteLine("Begining request stream");

                    rest.BeginGetRequestStream(GetRequestStreamCallBack, rest);
                }
                _resetEvent.WaitOne();
            }

            catch (Exception e)
            {
                SendExportWarningEmail(e);
                Console.WriteLine("Error: " + e.Message);
                _resetEvent.Set();
            }
        }

        private void GetRequestStreamCallBack(IAsyncResult ar)
        {
            try
            {
                Log.Info("Request stream call back entered");
                Console.WriteLine("Request stream call back entered");
                var request = (HttpWebRequest)ar.AsyncState;
                Log.Info("Creating stream from request for writing");
                Console.WriteLine("Creating stream from request for writing");
                var postStream = request.EndGetRequestStream(ar);

                Log.Info("Generating criteria byte array from criteria");
                Console.WriteLine("Generating criteria byte array from criteria");
                var criteriaBytes = StreamTranslator<TravidiaNoFeedbackCriteria>.DtoToByteXml(Criteria);
                Log.Info("Writing the byte array to the request");
                Console.WriteLine("Writing the byte array to the request");
                postStream.Write(criteriaBytes, 0, criteriaBytes.Length);
                postStream.Flush();
                postStream.Close();

                Log.Info("Begining async request to LR Services");
                Console.WriteLine("Begining async request to LR Services");
                request.BeginGetResponse(BatchAsyncCallBack, request);
            }

            catch (Exception e)
            {
                SendExportWarningEmail(e);
                Console.WriteLine("Error: " + e.Message);
                _resetEvent.Set();
            }
        }

        private void BatchAsyncCallBack(IAsyncResult iar)
        {
            try
            {
                Log.Info("Returned from Async call");
                Console.WriteLine("Returned from Async call");
                var rest = (HttpWebRequest)iar.AsyncState;
                Log.Info("Ending the response");
                Console.WriteLine("Ending the response");
                var response = rest.EndGetResponse(iar) as HttpWebResponse;
                if (response != null)
                {
                    Log.Info("Response is not null --> retrieving Export Summary");
                    Console.WriteLine("Response is not null --> retrieving Export Summary");
                    //var summary = StreamTranslator<ExportSummary>.StreamToDto(response.GetResponseStream());

                    //// Write summary information to Log? Console?
                    //var dailyIncremental = summary.DailyExport ? "daily" : "incremental";
                    //Log.Info("Completed running " + dailyIncremental + " batch for " + summary.FeedName);
                    //Console.WriteLine("Completed running " + dailyIncremental + " batch for " + summary.FeedName);
                    //Log.Info("Number of records exported: " + summary.SummaryCount);
                    //Console.WriteLine("Number of records exported: " + summary.SummaryCount);

                    //// Send an email if we get a daily summary object with no entries
                    //if (summary.DailyExport && (summary.SummaryCount == 0))
                    //{
                    //    Log.Info("Empty daily feed exported - no summary records created");
                    //    //SendExportWarningEmail(
                    //    //    new Exception("Daily export was run with no entries exported from outbound cache"));
                    //}

                }

                Log.Info("Releasing the 'resource lock' program should exit");
                Console.WriteLine("Releasing the 'resource lock' program should exit");
                _resetEvent.Set();
            }

            catch (Exception e)
            {
                SendExportWarningEmail(e);
                Console.WriteLine("Error: " + e.Message);
                _resetEvent.Set();
            }
        }

        private void SendExportWarningEmail(Exception e)
        {
            Log.Info("Sending Email");
            var subject = "ExportBatchApplication - Error during batch export";
            var body = "There was an error during the batch export process." + Environment.NewLine
                       + "Error was: " + (e != null ? e.Message : string.Empty) + Environment.NewLine
                       + Environment.NewLine
                       + "Host Machine: " + Environment.MachineName + Environment.NewLine
                       + "Report was: DigitalDisplay" + Environment.NewLine
                       + "Date was: " + (Criteria != null ? Criteria.Date.ToString() : string.Empty) + Environment.NewLine
                       + "Report email address was: " + (Criteria != null ? Criteria.EmailAddress.ToString() : string.Empty) + Environment.NewLine;

            var innerExceptionInfo = "Additional Information"
                + Environment.NewLine + "=-=-=-=-=-=-=-=-=-=-=-" + Environment.NewLine
                + ((e != null && e.InnerException != null) ? GetInnerExceptionInfo(e) : "-- None --");

            body += innerExceptionInfo;

            var message = MailService.CreateMessage(
                ConfigurationManager.AppSettings[ServiceSettings.Recipients.ToString()],
                ConfigurationManager.AppSettings[ServiceSettings.EmailAccount.ToString()],
                body,
                subject);

            MailService.SendNotificationEmail(message);
        }

        private static string GetInnerExceptionInfo(Exception exception)
        {
            var exceptionInfo = string.Empty;
            if (exception != null)
            {
                if (exception.InnerException != null)
                {
                    exceptionInfo += Environment.NewLine +
                        "InnerException" + Environment.NewLine +
                        "Message: " + exception.InnerException.Message + Environment.NewLine +
                        "StackTrace: " + exception.InnerException.StackTrace + Environment.NewLine +
                        GetInnerExceptionInfo(exception.InnerException);
                }
            }
            return exceptionInfo;
        }
    }
}
