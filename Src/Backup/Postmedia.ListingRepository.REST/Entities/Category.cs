﻿using System.Collections.Generic;
using Postmedia.ListingRepository.REST.Entities.BaseClasses;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class Category : CodeNameBase
    {
        public virtual Publication Publication { get; set; }
        public virtual ListingPublication ListingPublication { get; set; }
    }
}