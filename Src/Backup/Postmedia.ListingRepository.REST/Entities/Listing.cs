﻿using System;
using System.Collections.Generic;
using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class Listing : BaseDomain
    {
        public virtual string ListingNumber { get; set; }
        public virtual string Description { get; set; }
        public virtual string AdType { get; set; }
        public virtual string Title { get; set; }
        public virtual string Source { get; set; }
        public virtual int HasAdminLock { get; set; }

        public virtual AdBooking AdBooking { get; set; }
        public virtual Advertiser Advertiser { get; set; }

        public virtual IList<Media> MediaItems { get; set; }
        public virtual IList<Property> Properties { get; set; }
        public virtual IList<ListingPublication> ListingPublications { get; set; }
        //public virtual IList<CacheEntry> CacheEntries { get; set; }
       
        public Listing()
        {
            ListingPublications = new List<ListingPublication>();
            Properties = new List<Property>();
            MediaItems = new List<Media>();
        }

        public virtual void AddListingPublication(ListingPublication listingPublication)
        {
            listingPublication.Listing = this;
            ListingPublications.Add(listingPublication);
        }

        public virtual void AddProperty(Property property)
        {
            property.Listing = this;
            Properties.Add(property);
        }

        public virtual void AddMedia(Media media)
        {
            media.Listing = this;
            MediaItems.Add(media);
        }
    }
}
