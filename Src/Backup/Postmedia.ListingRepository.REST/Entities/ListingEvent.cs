﻿using System;
using System.Collections.Generic;
using IglooCoder.Commons.WcfNhibernate;


namespace Postmedia.ListingRepository.REST.Entities
{
    public class ListingEvent: BaseDomain
    {
        public virtual string ListingNumber { get; set; }
        public virtual string FeedName { get; set; }
        public virtual string EventDescription { get; set; }
        public virtual string EventType { get; set; }
        public virtual int RowsAffected { get; set; }
        public virtual DateTime EventTime { get; set; }
    }
}