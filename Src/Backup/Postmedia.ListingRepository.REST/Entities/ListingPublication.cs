﻿using System;
using System.Collections.Generic;
using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class ListingPublication : BaseDomain
    {
        //Members
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? ExpireDate { get; set; }
        public virtual string Status { get; set; }

        //Collections
        public virtual IList<Upsell> Upsells { get; set; }
        public virtual IList<Category> Categories { get; set; }
        
        //References
        public virtual Listing Listing { get; set; }
        public virtual Publication Publication { get; set; }

        public ListingPublication()
        {
            Upsells = new List<Upsell>();
            Categories = new List<Category>();
        }


        public virtual void AddUpsell(Upsell upsell)
        {
            upsell.ListingPublication = this;
            Upsells.Add(upsell);
        }

        public virtual void AddCategory(Category category)
        {
            category.ListingPublication = this;
            Categories.Add(category);
        }

        //public virtual void AddCategory(Category category)
        //{
        //    category.ListingPublication = this;
        //    Categories.Add(category);
        //}
    }
}
