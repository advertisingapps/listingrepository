﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class YearMakeModel : BaseDomain
    {
        public virtual int Year { get; set; }
        public virtual string Make { get; set; }
        public virtual string Model { get; set; }
    }
}