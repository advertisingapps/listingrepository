﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class AdBooking : BaseDomain
    {
        public virtual string AccountNumber { get; set; }
        public virtual string AdOrderNumber { get; set; }
        public virtual string AgencyAccountNumber { get; set; }
        public virtual string ExternalAccountNumber { get; set; }
        public virtual string ExternalAdOrderNumber { get; set; }

        public virtual Division Division { get; set; }
        
    }
}