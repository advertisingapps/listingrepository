﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class Contact : BaseDomain
    {
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Email { get; set; }
        public virtual string Phone { get; set; }

        public virtual Advertiser Advertiser { get; set; }
    }
}