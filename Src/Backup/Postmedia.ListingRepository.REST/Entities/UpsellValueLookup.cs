﻿using Postmedia.ListingRepository.REST.Entities.BaseClasses;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class UpsellValueLookup : LookupBase
    {
        public virtual string Code { get; set; }
    }
}