﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.REST.Entities.BaseClasses
{
    public abstract class CodeNameBase : BaseDomain
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }

    
    }
}