﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.REST.Entities.BaseClasses
{
    public class LookupBase : BaseDomain
    {
        public virtual string Value { get; set; }
        public virtual int SortSequence { get; set; }
    }
}