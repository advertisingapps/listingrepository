﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class Image : BaseDomain
    {
        public virtual string Url { get; set; }
        public virtual Listing Listing { get; set; }
    }
}