﻿using IglooCoder.Commons.WcfNhibernate;
using System;
using System.Collections.Generic;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class DetailedUpsellReport: BaseDomain
    {
        public virtual long ListingId { get; set; }
        public virtual long UpsellId { get; set; }
        public virtual string DivisionName { get; set; }
        public virtual string Source { get; set; }
        public virtual string ListingNumber { get; set; }
        public virtual string PublicationName { get; set; }
        public virtual string Title { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? ExpireDate { get; set; }
        public virtual string Status { get; set; }
        public virtual string UpsellName { get; set; }
        public virtual long PublishingDays { get; set; }
    }
}