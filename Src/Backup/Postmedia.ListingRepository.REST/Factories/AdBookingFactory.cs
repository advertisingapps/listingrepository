﻿using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Repositories;

namespace Postmedia.ListingRepository.REST.Factories
{
    public class AdBookingFactory : IAdBookingFactory
    {
        private IDivisionRepository<Division> DivisionRepository { get; set; }
        
        public AdBookingFactory() : this(new DivisionRepository())
        {
        }

        public AdBookingFactory(IDivisionRepository<Division> divisionRepository )
        {
            DivisionRepository = divisionRepository;
        }

        public AdBooking  Create(DTO.AdBooking source)
        {
            AdBooking dest = null;

            if (source != null)
            {
                dest = new AdBooking();
                dest = Create(source, dest);
            }

            return dest;
        }

        public AdBooking Create(DTO.AdBooking source , AdBooking dest)
        {
            if (source != null)
            {
                if(dest == null)
                    dest = new AdBooking();

                dest = PerformMapping(source, dest);
            }
            return dest;
        }

        private AdBooking PerformMapping(DTO.AdBooking source, AdBooking dest )
        {
            if(source != null)
            {
                dest.AccountNumber = source.AccountNumber;
                dest.AdOrderNumber = source.AdOrderNumber;
                dest.AgencyAccountNumber = source.AgencyAccountNumber;
                dest.ExternalAccountNumber = source.ExternalAccountNumber;
                dest.ExternalAdOrderNumber = source.ExternalAdOrderNumber;

                if (source.Division != null)
                {
                    dest.Division = DivisionRepository.FetchByCode(source.Division.Code);
                }
            }

            return dest;
        }
    }
}