﻿using System.Linq;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Repositories;

namespace Postmedia.ListingRepository.REST.Factories
{
    public class AdvertiserFactory : IAdvertiserFactory
    {
        private IContactFactory ContactFactory { get; set; }
        private IAddressFactory AddressFactory { get; set; }

        public AdvertiserFactory() : this(new ContactFactory(), new AddressFactory())
        {

        }

        public AdvertiserFactory(IContactFactory contactFactory, IAddressFactory addressFactory)
        {
            ContactFactory = contactFactory;
            AddressFactory = addressFactory;
        }

        public Advertiser Create(DTO.Advertiser dto)
        {
            Advertiser dest = null;
            if (dto != null)
            {
                dest = Create(dto, dest);
            }

            return dest;
        }

        public Advertiser Create(DTO.Advertiser dto , Advertiser entity)
        {
            if (dto != null)
            {
                if(entity == null)
                    entity = new Advertiser();

                entity = PerformMapping(dto, entity);
            }
            return entity;
        }

        private Advertiser PerformMapping(DTO.Advertiser dto, Advertiser entity)
        {
            entity.CompanyName = dto.CompanyName;
            entity.Name = dto.Name;
            entity.Url= dto.Url;
            entity.ExternalId = dto.ExternalId;

            if (dto.Contact != null)
            {
                if(entity.Contact == null)
                    entity.Contact = new Contact();

                entity.Contact.Advertiser = entity;
                entity.Contact = ContactFactory.Create(dto.Contact, entity.Contact);
            }
       

            if (dto.Address != null)
            {
                if (entity.Address== null)
                    entity.Address= new Address();

                entity.Address.Advertiser = entity;
                entity.Address = AddressFactory.Create(dto.Address, entity.Address);
            }
        
            return entity;
        }
    }
}