using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Factories
{
    public interface IContactFactory
    {
        Contact Create(DTO.Contact source);
        Contact Create(DTO.Contact source, Contact dest);
    }
}