﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Repositories;
using log4net;

namespace Postmedia.ListingRepository.REST.Factories
{
    public interface IListingFactory
    {
        Listing Create(DTO.Listing dto);
        Listing Create(DTO.Listing dto, Listing entity);
    }

    public class ListingFactory : IListingFactory
    {

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IListingPublicationFactory ListingPublicationFactory { get; set; }
        private IAdvertiserFactory AdvertiserFactory { get; set; }
        private IAdBookingFactory AdBookingFactory { get; set; }
        private ICategoryRepository<Category> CategoryRepository { get; set; }

        public ListingFactory(IListingPublicationFactory listingPublicationFactory
            ,IAdvertiserFactory advertiserFactory
            ,IAdBookingFactory adBookingFactory
            , ICategoryRepository<Category> categoryRepository )
        {
            AdBookingFactory = adBookingFactory;
            AdvertiserFactory = advertiserFactory;
            ListingPublicationFactory = listingPublicationFactory;
            CategoryRepository = categoryRepository;
        }

        public ListingFactory() : this(new ListingPublicationFactory()
            , new AdvertiserFactory()
            , new AdBookingFactory()
            , new CategoryRepository()) {}


        public Listing Create(DTO.Listing dto)
        {
            Listing dest = null;

            if (dto != null)
            {
                dest = Create(dto, dest);
            }

            return dest;
        }

        public Listing Create(DTO.Listing dto, Listing entity)
        {   
            if (dto != null)
            {
                if (entity == null)
                    entity = new Listing();

                entity = PerformMapping(dto, entity);
            }
            return entity;
        }

        private Listing PerformMapping(DTO.Listing dto, Listing entity)
        {
            Log.Info("Entering ListingFactory - PerformMapping()");
            Log.Info("Mapping base properties - PerformMapping()");
            //Copy base properties
            entity.AdType = dto.AdType;
            entity.Description = dto.Description;
            entity.ListingNumber = dto.ListingNumber;
            entity.Source = dto.Source;
            entity.Title = dto.Title;
            entity.HasAdminLock = (dto.HasAdminLock ? 1 : 0);

            Log.Debug("Completed ... - PerformMapping()");
            Log.Debug("Creating Listing Publications - PerformMapping()");
            //Create / update the listing publications
            if (dto.ListingPublications != null)
            {
                int maxCount = entity.ListingPublications.Count;
                for (int index = 0; index < maxCount; index++)
                {
                    entity.ListingPublications.RemoveAt(0);
                }
                
                foreach (var listingPublication in dto.ListingPublications)
                {
                    var lp = ListingPublicationFactory.Create(listingPublication);
                    entity.AddListingPublication(lp);
                }
            }
            Log.Debug("Completed ... - PerformMapping()");

            //Create / Update the AdBooking
            Log.Debug("Mapping AdBooking - PerformMapping()");
            var destBooking = entity.AdBooking ?? new AdBooking();
            entity.AdBooking = AdBookingFactory.Create(dto.AdBooking, destBooking);
            Log.Debug("Completed ... - PerformMapping()");

            //Create / Update the Advertiser
            Log.Debug("Mapping Advertiser - PerformMapping()");
            var destAdvertiser = entity.Advertiser ?? new Advertiser();
            entity.Advertiser = AdvertiserFactory.Create(dto.Advertiser, destAdvertiser);
            Log.Debug("Completed ...  - PerformMapping()");

            Log.Debug("Mapping Properties - PerformMapping()");
            if (dto.Properties != null)
            {
                int maxCount = entity.Properties.Count;
                for (int index = 0; index < maxCount; index++)
                {
                    entity.Properties.RemoveAt(0);
                }

                foreach (var property in dto.Properties)
                {
                    var prop = new Property
                    {
                        Code = property.Code,
                        Value = property.Value,
                        Listing = entity
                    };
                    entity.AddProperty(prop);
                }
            }
            Log.Debug("Completed ...  - PerformMapping()");


            Log.Debug("Mapping Media Items - PerformMapping()");
            if (dto.MediaItems != null)
            {
                int maxCount = entity.MediaItems.Count;
                for (int index = 0; index < maxCount; index++)
                {
                    entity.MediaItems.RemoveAt(0);
                }

                foreach (var mediaItem in dto.MediaItems)
                {
                    var media = new Media
                    {
                        Url = mediaItem.Url
                        , MimeType = mediaItem.MimeType
                        , Role = mediaItem.Role
                    };
                    entity.AddMedia(media);
                }
            }
            Log.Debug("Completed ... - PerformMapping()");

            Log.Info("Exiting PerformMapping()");
            return entity;

        }
    }
}