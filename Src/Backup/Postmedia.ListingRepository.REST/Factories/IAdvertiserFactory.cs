using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Factories
{
    public interface IAdvertiserFactory
    {
        Advertiser Create(DTO.Advertiser dto);
        Advertiser Create(DTO.Advertiser dto, Advertiser entity);
    }
}