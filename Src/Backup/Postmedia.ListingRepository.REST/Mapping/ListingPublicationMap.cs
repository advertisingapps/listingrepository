﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class ListingPublicationMap : ClassMap<ListingPublication>
    {
        public ListingPublicationMap()
        {
            Table("ListingPublication");
            Id(x => x.Id, "ListingPublicationID")
                .GeneratedBy
                .Native();

            Map(x => x.ExpireDate);
            Map(x => x.StartDate);
            Map(x => x.Status);

            References(x => x.Listing)
                .Column("ListingID")
                .Nullable()
                .Cascade.None();

            References(x => x.Publication)
                .Column("PublicationID")
                .Nullable()
                .Cascade.None();

            HasMany(x => x.Upsells)
                .KeyColumn("ListingPublicationID")
                .Table("Upsell")
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyNullable();

            HasMany(x => x.Categories)
                .KeyColumn("ListingPublicationID")
                .Table("Category")
                .Inverse()
                .Cascade.AllDeleteOrphan()
                .KeyNullable();

            //HasManyToMany(x => x.Categories)
            //    .Cascade.SaveUpdate()
            //    .Table("JoinListingPublicationToCategory")
            //    .ParentKeyColumn("ListingPublicationID")
            //    .ChildKeyColumn("CategoryID").AsSet(); 


        }
    }
}