﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class ListingMap : ClassMap<Listing>
    {
        public ListingMap()
        {
            Table("Listing");
            
            Id(x => x.Id, "ListingID")
                .GeneratedBy
                .Native();

            Map(x => x.ListingNumber);
            Map(x => x.Source);
            Map(x => x.Title);
            Map(x => x.Description).Length(4001);
            Map(x => x.AdType);

            References(x => x.AdBooking)
                .Column("AdBookingID")
                .Nullable()
                .Cascade.All();

            References(x => x.Advertiser)
                .Column("AdvertiserID")
                .Nullable()
                .Cascade.All();
            
            HasMany(x => x.MediaItems)
              .KeyColumn("ListingID")
              .Table("Media")
              .Inverse()
              .Cascade.AllDeleteOrphan()
              .KeyNullable();
            
            HasMany(x => x.Properties)
              .KeyColumn("ListingID")
              .Table("Property")
              .Inverse()
              .Cascade.AllDeleteOrphan()
              .KeyNullable();

            HasMany(x => x.ListingPublications)
                .KeyColumn("ListingID")
                .Inverse()
                .Table("ListingPublication")
                .Cascade.AllDeleteOrphan();

            Map(x => x.HasAdminLock, "HasAdminLock");

        }
    }
}