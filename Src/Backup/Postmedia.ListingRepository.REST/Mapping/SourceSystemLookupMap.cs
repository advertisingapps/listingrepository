﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class SourceSystemLookupMap : ClassMap<SourceSystemLookup>
    {
        public SourceSystemLookupMap()
        {
            Table("SourceSystemValue");

            Id(x => x.Id, "SourceSystemValueID")
                .GeneratedBy
                .Native();

            Map(x => x.Value, "SourceSystem");
            Map(x => x.SortSequence);
        }
    }
}