﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class AddressLineMap : ClassMap<AddressLine>
    {
        public AddressLineMap()
        {
            Table("AddressLine");

            Id(x => x.Id, "AddressLineID")
                .GeneratedBy
                .Native();

            Map(x => x.LineNumber);
            Map(x => x.LineText);

            References(x => x.Address)
                .Column("AddressID")
                .Nullable()
                .Cascade.None();
        }
    }
}