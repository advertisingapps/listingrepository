﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class AdvertiserMap : ClassMap<Advertiser>
    {
        public AdvertiserMap()
        {
            Table("Advertiser");

            Id(x => x.Id, "AdvertiserID")
                .GeneratedBy
                .Native();

            Map(x => x.CompanyName );
            Map(x => x.Name);
            Map(x => x.Url);
            Map(x => x.ExternalId, "externalId");

            HasOne(x => x.Address)
                .PropertyRef(a => a.Advertiser)
                .Cascade.All();

            HasOne(x => x.Contact)
                .PropertyRef(c => c.Advertiser)
                .Cascade.All();
        }
    }
}