﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class DivisionMap : ClassMap<Division>
    {
        public DivisionMap()
        {
            Table("Division");

            Id(x => x.Id, "DivisionID")
                .GeneratedBy
                .Native();

            Map(x => x.Code);
            Map(x => x.Name);

        }
    }
}