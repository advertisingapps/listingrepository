﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class UpsellMap : ClassMap<Upsell>
    {
        public UpsellMap()
        {
            Table("Upsell");

            Id(x => x.Id, "UpsellID")
                .GeneratedBy
                .Native();

            Map(x => x.Code);
            Map(x => x.Name);

            References(x => x.ListingPublication)
                .Column("ListingPublicationID")
                .Cascade.None();

        }
    }
}