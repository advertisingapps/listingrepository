﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class LogoMap : ClassMap<Logo>
    {
        public LogoMap()
        {
            Table("Logo");

            Id(x => x.Id, "LogoID")
                .GeneratedBy
                .Native();

            Map(x => x.Url);

            References(x => x.Listing)
                .Column("ListingID");

        }
    }
}