﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;


namespace Postmedia.ListingRepository.REST.Mapping
{
    public class MediaMap : ClassMap<Media>
    {
        public MediaMap()
        {
            Table("Media");

            Id(x => x.Id, "MediaID")
                .GeneratedBy
                .Native();

            Map(x => x.Url);
            Map(x => x.MimeType);
            Map(x => x.Role);

            References(x => x.Listing)
                .Column("ListingID")
                .Cascade.None();
        }
    }
}