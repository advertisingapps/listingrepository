﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class CategoryMap : ClassMap<Category>
    {
        public CategoryMap()
        {
            Table("ListingPublicationClassification");

            Id(x => x.Id, "ListingPublicationClassificationID")
                .GeneratedBy
                .Native();

            Map(x => x.Code);
            Map(x => x.Name);

            References(x => x.ListingPublication)
                .Column("ListingPublicationID");

            //HasManyToMany(x => x.ListingPublications)
            //    .Cascade.SaveUpdate()
            //    .Inverse().AsBag()
            //    .Table("JoinListingPublicationToCategory")
            //    .ParentKeyColumn("CategoryID")
            //    .ChildKeyColumn("ListingPublicationID");
        }
    }
}