﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;
namespace Postmedia.ListingRepository.REST.Mapping
{
    public sealed class ListingPubResultMap : ClassMap<ListingPubResult>
    {
        public ListingPubResultMap()
        {
            ReadOnly();
            Id(x => x.ListingId);
            CompositeId()
            .KeyProperty(x => x.ListingId)
            .KeyProperty(x => x.ListingPublicationId);
            Map(x => x.Division);
            Map(x => x.AccountNumber);
            Map(x => x.ListingNumber);
            Map(x => x.AdOrderNumber);
            Map(x => x.FeedName);
            Map(x => x.Publication);
            Map(x => x.StartDate);
            Map(x => x.ExpireDate);
            Map(x => x.Status);
            Map(x => x.Category);
            Map(x => x.Caption);
            Map(x => x.Source);
        }
    }
}