﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class PropertyMap : ClassMap<Property>
    {
        public PropertyMap()
        {
            Table("Property");

            Id(x => x.Id, "PropertyID")
                .GeneratedBy
                .Native();

            Map(x => x.Code);
            Map(x => x.Value);

            References(x => x.Listing)
                .Column("ListingID")
                .Cascade.None();
        }
    }
}