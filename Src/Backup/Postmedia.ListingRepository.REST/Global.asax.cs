﻿using System;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Tool.hbm2ddl;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Mapping;
using Postmedia.ListingRepository.REST.Services;

namespace Postmedia.ListingRepository.REST
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes();
            InitLog4Net();
            InitAutomapper();
            InitNHibernate();
        }

        private void RegisterRoutes()
        {
            // Edit the base address replacing the "ListingRepositoryService" string below
            RouteTable.Routes.Add(new ServiceRoute("ListingRepositoryService",
                new WebServiceHostFactory(), typeof(ListingRepositoryService)));
        }

        private void InitAutomapper()
        {
            AutomapperConfiguration.Config();
        }

        private void InitLog4Net()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        private void InitNHibernate()
        {
            //NHibernateFactory.Initialize(
            //    Fluently.Configure()
            //        .Database(MsSqlConfiguration.MsSql2008
            //                      .ConnectionString(
            //                          c => c.FromConnectionStringWithKey("DbConn"))
            //                      .ShowSql())
            //        .Mappings(m => m.FluentMappings.AddFromAssemblyOf<AdBooking>())
            //        .ExposeConfiguration(cfg => new SchemaExport(cfg).Create(false, false))
            //        .BuildSessionFactory());

            var fluentConfig = Fluently.Configure();

            fluentConfig.Database(MsSqlConfiguration.MsSql2008
                           .ConnectionString(
                               c => c.FromConnectionStringWithKey("DbConn"))
                           .ShowSql());
            fluentConfig.Mappings(m => m.FluentMappings.AddFromAssemblyOf<AdBooking>());
            fluentConfig.BuildConfiguration().SetProperty("command_timeout", "600");
            fluentConfig.ExposeConfiguration(
                cfg => new SchemaExport(cfg).Create(false, false));
            NHibernateFactory.Initialize(fluentConfig.BuildSessionFactory());


            ////#IFDEBUG
            //HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
            ////#ENDIF
        }
    }
}
