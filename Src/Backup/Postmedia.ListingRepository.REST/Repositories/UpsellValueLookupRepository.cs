﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class UpsellValueLookupRepository : BaseNHibernateRepository<UpsellValueLookup>, IUpsellValueLookupRepository<UpsellValueLookup>
    {
        public UpsellValueLookupRepository(ISessionStorage session) : base (session)
        {
        }

        public UpsellValueLookupRepository()
            : base(new WcfSessionStorage())
        {
        }
    }
}