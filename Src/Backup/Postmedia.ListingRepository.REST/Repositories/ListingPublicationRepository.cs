﻿using IglooCoder.Commons.WcfNhibernate;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class ListingPublicationRepository : BaseNHibernateRepository<ListingPublication>, IListingPublicationRepository<ListingPublication>
    {
        public ListingPublicationRepository()
            : base(new WcfSessionStorage())
        {

        }
    }
}