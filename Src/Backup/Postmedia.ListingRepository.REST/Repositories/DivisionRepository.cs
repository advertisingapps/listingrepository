﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class DivisionRepository : BaseNHibernateRepository<Division>, IDivisionRepository<Division>
    {
        public DivisionRepository(ISessionStorage session) : base (session)
        {
        }

        public DivisionRepository() : base(new WcfSessionStorage())
        {

        }

        public Division FetchByCode(string code)
        {
            var division = (from div in Session.Query<Division>()
                            where div.Code == code
                            select div).FirstOrDefault();

            return division;
        }
    }
}