﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public interface IListingEventRepository<T>
    {
        void Save(T listingEvent);

        List<GeneralFailedListingEvent> GetGeneralFailedListing(
            GeneralFailedListingEventCriteria criteria);
    }
}
