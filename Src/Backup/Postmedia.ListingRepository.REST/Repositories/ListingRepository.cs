﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Linq;
using System.Reflection;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using System.Collections.Generic;
using NHibernate.SqlCommand;
using NHibernate.Transform;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports;
using Postmedia.ListingRepository.DTO.Reports.Working;
using Listing = Postmedia.ListingRepository.REST.Entities.Listing;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class ListingRepository : BaseNHibernateRepository<Listing>, IListingRepository<Listing>, IComparer<Listing>
    {
        public ListingRepository()
            : this(new WcfSessionStorage())
        {
        }

        public ListingRepository(ISessionStorage sessionStorage)
            : base(sessionStorage)
        {
        }


        public Listing FetchByNumber(string listingNumber)
        {
            //var foundListing = (from listing in Session.Query<Listing>()
            //                    where listing.ListingNumber == listingNumber
            //                    select listing).FirstOrDefault();

            //return foundListing;

            var listingCriteria = DetachedCriteria.For<Listing>();
            if (!string.IsNullOrEmpty(listingNumber))
            {
                listingCriteria.Add(Restrictions.Eq("ListingNumber", listingNumber));
            }
            listingCriteria.SetResultTransformer(new DistinctRootEntityResultTransformer());

            listingCriteria.SetLockMode(LockMode.None);

            var executableCriteria = listingCriteria.GetExecutableCriteria(Session);
            var criteriaListing = executableCriteria.UniqueResult<Listing>();

            return criteriaListing;
        }

        public IEnumerable<Listing> FetchAllByNumber(string listingString)
        {
            var listingCriteria = DetachedCriteria.For<Listing>();
            if (!string.IsNullOrEmpty(listingString))
            {
                listingCriteria.Add(Restrictions.InsensitiveLike("ListingNumber", "%" + listingString + "%"));
            }
            listingCriteria.SetResultTransformer(new DistinctRootEntityResultTransformer());
            listingCriteria.SetLockMode(LockMode.None);
            var executableCriteria = listingCriteria.GetExecutableCriteria(Session);
            var criteriaListings = executableCriteria.List<Listing>();
            return criteriaListings;
        }


        public void Save(Listing listing)
        {
            Session.Save(listing);
        }

        public void DeleteListing(string listingNumber)
        {
            var foundListing = FetchByNumber(listingNumber);
            if (foundListing != null)
            {
                Session.Delete(foundListing);
            }
        }

        public void DeleteListing(Listing listing)
        {
            if (listing != null)
            {
                Session.Delete(listing);
            }
        }

        public IEnumerable<Listing> GetListingsUsingCriteria(ListingCriteria criteria)
        {
            var listingCriteria = DetachedCriteria.For<Listing>();

            if (!string.IsNullOrEmpty(criteria.Division))
            {
                listingCriteria.CreateCriteria("AdBooking", "AB", JoinType.InnerJoin)
                    .CreateCriteria("Division", "D", JoinType.InnerJoin)
                    .Add(Restrictions.Eq("D.Code", criteria.Division));
            }

            if (!string.IsNullOrEmpty(criteria.Account))
            {
                listingCriteria.CreateCriteria("AdBooking", "AB", JoinType.InnerJoin)
                    .Add(Restrictions.InsensitiveLike("AB.AccountNumber", "%" + criteria.Account + "%"));
            }
            if (!string.IsNullOrEmpty(criteria.ListingNumber))
            {
                listingCriteria.Add(Restrictions.InsensitiveLike("ListingNumber", "%" + criteria.ListingNumber + "%"));
            }
            if (criteria.Date != null)
            {
                listingCriteria.CreateCriteria("ListingPublications", "LP", JoinType.InnerJoin)
                    .Add(Restrictions.Le("StartDate", criteria.Date))
                    .Add(Restrictions.Ge("ExpireDate", criteria.Date));
            }
            if (!string.IsNullOrEmpty(criteria.Source))
            {
                listingCriteria.Add(Restrictions.Eq("Source", criteria.Source));
            }
            if (!string.IsNullOrEmpty(criteria.Title))
            {
                listingCriteria.Add(Restrictions.InsensitiveLike("Title", "%" + criteria.Title + "%"));
            }

            listingCriteria.SetResultTransformer(new DistinctRootEntityResultTransformer());
            var executableCriteria = listingCriteria.GetExecutableCriteria(Session);
            var criteriaListings = executableCriteria.List<Listing>();
            return criteriaListings;
        }

        public PagedData<Listing> GetPagedListingsUsingCriteria(ListingCriteriaPaged criteria)
        {
            var results = CreatePagedListingCriteriaResultSet(criteria);

            var listings = (List<Listing>)results[0];
            var criteriaCount = (int)results[1];

            var pagedData = new PagedData<Listing>
            {
                Page = criteria.Page,
                Sort = criteria.Sort,
                SortDirection =
                    criteria.Ascending
                        ? Enumerations.SortDirection.Ascending
                        : Enumerations.SortDirection.Descending,
                RowCount = criteriaCount,
                Items = listings
            };
            return pagedData;
        }

        public IList CreatePagedAdvancedListingCriteriaResultSet(AdvancedPagedSearchCriteria criteria)
        {
            var adjustedPage = criteria.Page > 0 ? criteria.Page - 1 : 0;

            var multiCriteria = Session.CreateMultiCriteria();
            var listingCriteria = DetachedCriteria.For<Listing>();

            AddAdvancedCriteriaFilters(listingCriteria, criteria);

            listingCriteria.SetResultTransformer(new DistinctRootEntityResultTransformer());
            listingCriteria.SetProjection(Projections.Distinct(Projections.Id()));
            listingCriteria.SetMaxResults(int.MaxValue);

            var outerCriteria = Session.CreateCriteria(typeof(Listing));
            outerCriteria.Add(Subqueries.PropertyIn("Id", listingCriteria));

            var clonedCriteria = CriteriaTransformer.Clone(outerCriteria);
            clonedCriteria.ClearOrders();
            clonedCriteria.SetProjection(Projections.RowCount()).UniqueResult();

            outerCriteria.SetFirstResult(((adjustedPage) * criteria.PageSize));
            outerCriteria.SetMaxResults(criteria.PageSize);

            AddSortingOrder(outerCriteria, criteria.Sort, criteria.Ascending);

            //TODO: Andrew see if this line fixes the duplicate issue
            multiCriteria.SetResultTransformer(new DistinctRootEntityResultTransformer());
            multiCriteria.Add(outerCriteria);
            multiCriteria.Add(clonedCriteria);

            var results = multiCriteria.List();

            return results;
        }

        private IList CreatePagedListingCriteriaResultSet(ListingCriteriaPaged filters)
        {
            //var adjustedPage = filters.Page > 0 ? filters.Page - 1 : 0;

            //var multiCriteria = Session.CreateMultiCriteria();
            //var listingCriteria = DetachedCriteria.For<Listing>();

            //AddCriteriaFilters(listingCriteria, filters);

            //listingCriteria.SetResultTransformer(new DistinctRootEntityResultTransformer());
            //listingCriteria.SetProjection(Projections.Distinct(Projections.Id()));
            //listingCriteria.SetMaxResults(int.MaxValue);

            //var outerCriteria = Session.CreateCriteria(typeof(Listing));
            //outerCriteria.Add(Subqueries.PropertyIn("Id", listingCriteria));

            //var clonedCriteria = CriteriaTransformer.Clone(outerCriteria);
            //clonedCriteria.ClearOrders();
            //clonedCriteria.SetProjection(Projections.RowCount()).UniqueResult();

            //outerCriteria.SetFirstResult(((adjustedPage) * filters.PageSize));
            //outerCriteria.SetMaxResults(filters.PageSize);

            //AddSortingOrder(outerCriteria, filters.Sort, filters.Ascending);

            ////TODO: Andrew see if this line fixes the duplicate issue
            //multiCriteria.SetResultTransformer(new DistinctRootEntityResultTransformer());
            //multiCriteria.Add(outerCriteria);
            //multiCriteria.Add(clonedCriteria);

            //var results2 = multiCriteria.List();

            //return results2;

            var rowCountCriteria = Session.CreateCriteria<Listing>();
            AddCriteriaFilters(rowCountCriteria, filters);
            AddSortingOrder(rowCountCriteria, filters.Sort, filters.Ascending);
            rowCountCriteria.SetResultTransformer(CriteriaSpecification.DistinctRootEntity);



            var listingCriteria = Session.CreateCriteria<Listing>();
            AddCriteriaFilters(listingCriteria, filters);
            AddSortingOrder(listingCriteria, filters.Sort, filters.Ascending);
            listingCriteria.SetResultTransformer(CriteriaSpecification.DistinctRootEntity);
            listingCriteria.SetFirstResult(filters.Page * filters.PageSize)
                .SetMaxResults(filters.PageSize)
                .Future<Listing>();


            var rowCount = listingCriteria.List<Listing>().Count;
            var resultListings = listingCriteria.List<Listing>();

            IList results = new ArrayList();
            results.Add(resultListings);
            results.Add(rowCount);
            return results;
        }

        private void AddCriteriaFilters(DetachedCriteria listingCriteria, ListingCriteriaPaged criteria)
        {
            AddDivisionFilters(listingCriteria, criteria.Division);
            AddAccountFilters(listingCriteria, criteria.Account);
            AddListingNumberFilters(listingCriteria, criteria.ListingNumber);
            AddDateFilters(listingCriteria, criteria.Date);
            AddSourceFilters(listingCriteria, criteria.Source);
            AddTitleFilters(listingCriteria, criteria.Title);
        }
        private void AddCriteriaFilters(ICriteria listingCriteria, ListingCriteriaPaged criteria)
        {
            AddDivisionFilters(listingCriteria, criteria.Division);
            AddAccountFilters(listingCriteria, criteria.Account);
            AddListingNumberFilters(listingCriteria, criteria.ListingNumber);
            AddDateFilters(listingCriteria, criteria.Date);
            AddSourceFilters(listingCriteria, criteria.Source);
            AddTitleFilters(listingCriteria, criteria.Title);
        }

        private void AddAdvancedCriteriaFilters(DetachedCriteria listingCriteria, AdvancedPagedSearchCriteria criteria)
        {
            AddDivisionFilters(listingCriteria, criteria.Division);
            AddAccountFilters(listingCriteria, criteria.Account);
            AddListingNumberFilters(listingCriteria, criteria.ListingNumber);
            AddDateFilters(listingCriteria, criteria.Date);
            AddSourceFilters(listingCriteria, criteria.Source);
            AddTitleFilters(listingCriteria, criteria.Title);
            AddDescriptionFilters(listingCriteria, criteria.Description);
            AddMetadataFilters(listingCriteria, criteria.Metadata);
            AddUpsellFilters(listingCriteria, criteria.Upsell);
            AddClassificationFilters(listingCriteria, criteria.Classification);
            AddPublicationFilters(listingCriteria, criteria.Publication);
            AddFeedNameFilters(listingCriteria, criteria.FeedName);
        }
        private void AddAdvancedCriteriaFilters(ICriteria listingCriteria, AdvancedPagedSearchCriteria criteria)
        {
            AddDivisionFilters(listingCriteria, criteria.Division);
            AddAccountFilters(listingCriteria, criteria.Account);
            AddListingNumberFilters(listingCriteria, criteria.ListingNumber);
            AddDateFilters(listingCriteria, criteria.Date);
            AddSourceFilters(listingCriteria, criteria.Source);
            AddTitleFilters(listingCriteria, criteria.Title);
            AddDescriptionFilters(listingCriteria, criteria.Description);
            AddMetadataFilters(listingCriteria, criteria.Metadata);
            AddUpsellFilters(listingCriteria, criteria.Upsell);
            AddClassificationFilters(listingCriteria, criteria.Classification);
            AddPublicationFilters(listingCriteria, criteria.Publication);
            AddFeedNameFilters(listingCriteria, criteria.FeedName);
        }

        private void AddDivisionFilters(DetachedCriteria criteria, string division)
        {
            if (!string.IsNullOrEmpty(division))
            {
                var criteriaPath = criteria.GetCriteriaByAlias("D");

                if (criteriaPath != null)
                    criteriaPath.Add(Restrictions.Eq("D.Code", division));
                else
                    criteria.CreateCriteria("AdBooking", "AB", JoinType.InnerJoin)
                        .CreateCriteria("Division", "D", JoinType.InnerJoin)
                        .Add(Restrictions.Eq("D.Code", division));
            }
        }
        private void AddAccountFilters(DetachedCriteria criteria, string account)
        {
            if (!string.IsNullOrEmpty(account))
            {
                var criteriaPath = criteria.GetCriteriaByAlias("AB");

                if (criteriaPath != null)
                    criteria.Add(Restrictions.InsensitiveLike("AB.AccountNumber", "%" + account + "%"));
                else
                    criteria.CreateCriteria("AdBooking", "AB", JoinType.InnerJoin)
                        .Add(Restrictions.InsensitiveLike("AB.AccountNumber", "%" + account + "%"));
            }
        }
        private void AddListingNumberFilters(DetachedCriteria listingCriteria, string listingNumber)
        {
            if (!string.IsNullOrEmpty(listingNumber))
            {
                listingCriteria.Add(Restrictions.InsensitiveLike("ListingNumber", "%" + listingNumber + "%"));
            }
        }
        private void AddDateFilters(DetachedCriteria criteria, DateTime? date)
        {
            if (date != null)
            {
                var criteriaPath = criteria.GetCriteriaByAlias("LP");

                if (criteriaPath != null)
                    criteriaPath
                        .Add(Restrictions.Le("StartDate", date.Value))
                        .Add(Restrictions.Ge("ExpireDate", date.Value));
                else
                    criteria.CreateCriteria("ListingPublications", "LP", JoinType.InnerJoin)
                        .Add(Restrictions.Le("StartDate", date.Value))
                        .Add(Restrictions.Ge("ExpireDate", date.Value));
            }
        }
        private void AddSourceFilters(DetachedCriteria listingCriteria, string source)
        {
            if (!string.IsNullOrEmpty(source))
            {
                listingCriteria.Add(Restrictions.Eq("Source", source));
            }
        }
        private void AddTitleFilters(DetachedCriteria listingCriteria, string title)
        {
            if (!string.IsNullOrEmpty(title))
            {
                listingCriteria.Add(Restrictions.InsensitiveLike("Title", "%" + title + "%"));
            }
        }
        private void AddDescriptionFilters(DetachedCriteria listingCriteria, string description)
        {
            if (!string.IsNullOrEmpty(description))
            {
                listingCriteria.Add(Restrictions.InsensitiveLike("Description", "%" + description + "%"));
            }
        }
        private void AddMetadataFilters(DetachedCriteria criteria, string metadata)
        {
            if (metadata != null)
            {
                criteria.CreateCriteria("Property", "P", JoinType.InnerJoin)
                    .Add(Restrictions.InsensitiveLike("value", metadata, MatchMode.Anywhere));
            }
        }
        private void AddUpsellFilters(DetachedCriteria criteria, string upsell)
        {
            if (upsell != null)
            {
                var criteriaPath = criteria.GetCriteriaByAlias("LP");
                if (criteriaPath != null)
                {
                    var innerCriteriaPath = criteriaPath.GetCriteriaByAlias("U");
                    if (innerCriteriaPath != null)
                    {
                        innerCriteriaPath.Add(Restrictions.InsensitiveLike("value", upsell, MatchMode.Anywhere));
                    }
                    else
                    {
                        criteriaPath.CreateCriteria("Upsell", "U", JoinType.InnerJoin)
                            .Add(Restrictions.InsensitiveLike("value", upsell, MatchMode.Anywhere));
                    }
                }

                else
                {
                    criteria.CreateCriteria("ListingPublications", "LP", JoinType.InnerJoin)
                        .CreateCriteria("Upsell", "U", JoinType.InnerJoin)
                        .Add(Restrictions.InsensitiveLike("value", upsell, MatchMode.Anywhere));
                }
            }
        }
        private void AddClassificationFilters(DetachedCriteria criteria, string classification)
        {
            if (classification != null)
            {
                var criteriaPath = criteria.GetCriteriaByAlias("LP");
                if (criteriaPath != null)
                {
                    var innerCriteriaPath = criteriaPath.GetCriteriaByAlias("C");
                    if (innerCriteriaPath != null)
                    {
                        innerCriteriaPath.Add(Restrictions.InsensitiveLike("name", classification, MatchMode.Anywhere));
                    }
                    else
                    {
                        criteriaPath.CreateCriteria("Categories", "C", JoinType.InnerJoin)
                            .Add(Restrictions.InsensitiveLike("name", classification, MatchMode.Anywhere));
                    }
                }

                else
                {
                    criteria.CreateCriteria("ListingPublications", "LP", JoinType.InnerJoin)
                        .CreateCriteria("Categories", "C", JoinType.InnerJoin)
                        .Add(Restrictions.InsensitiveLike("name", classification, MatchMode.Anywhere));
                }
            }
        }
        private void AddPublicationFilters(DetachedCriteria criteria, string publication)
        {
            if (publication != null)
            {
                var criteriaPath = criteria.GetCriteriaByAlias("LP");
                if (criteriaPath != null)
                {
                    var innerCriteriaPath = criteriaPath.GetCriteriaByAlias("P");
                    if (innerCriteriaPath != null)
                    {
                        innerCriteriaPath.Add(Restrictions.InsensitiveLike("code", publication, MatchMode.Exact));
                    }
                    else
                    {
                        criteriaPath.CreateCriteria("Publication", "P", JoinType.InnerJoin)
                            .Add(Restrictions.InsensitiveLike("code", publication, MatchMode.Exact));
                    }
                }

                else
                {
                    criteria.CreateCriteria("ListingPublications", "LP", JoinType.InnerJoin)
                        .CreateCriteria("Publication", "P", JoinType.InnerJoin)
                        .Add(Restrictions.InsensitiveLike("code", publication, MatchMode.Exact));
                }
            }
        }
        private void AddFeedNameFilters(DetachedCriteria criteria, string feedName)
        {
            if (criteria != null)
            {
                criteria.CreateCriteria("CacheEntries", "CE", JoinType.InnerJoin)
                    .Add(Restrictions.Eq("FeedName", feedName));
            }
        }
        private void AddDivisionFilters(ICriteria criteria, string division)
        {
            if (!string.IsNullOrEmpty(division))
            {
                var criteriaPath = criteria.GetCriteriaByAlias("D");

                if (criteriaPath != null)
                    criteriaPath.Add(Restrictions.Eq("D.Code", division));
                else
                    criteria.CreateCriteria("AdBooking", "AB", JoinType.InnerJoin)
                        .CreateCriteria("Division", "D", JoinType.InnerJoin)
                        .Add(Restrictions.Eq("D.Code", division));
            }
        }
        private void AddAccountFilters(ICriteria criteria, string account)
        {
            if (!string.IsNullOrEmpty(account))
            {
                var criteriaPath = criteria.GetCriteriaByAlias("AB");

                if (criteriaPath != null)
                    criteria.Add(Restrictions.InsensitiveLike("AB.AccountNumber", "%" + account + "%"));
                else
                    criteria.CreateCriteria("AdBooking", "AB", JoinType.InnerJoin)
                        .Add(Restrictions.InsensitiveLike("AB.AccountNumber", "%" + account + "%"));
            }
        }
        private void AddListingNumberFilters(ICriteria listingCriteria, string listingNumber)
        {
            if (!string.IsNullOrEmpty(listingNumber))
            {
                listingCriteria.Add(Restrictions.InsensitiveLike("ListingNumber", "%" + listingNumber + "%"));
            }
        }
        private void AddDateFilters(ICriteria criteria, DateTime? date)
        {
            if (date != null)
            {
                var criteriaPath = criteria.GetCriteriaByAlias("LP");

                if (criteriaPath != null)
                    criteriaPath
                        .Add(Restrictions.Le("StartDate", date.Value))
                        .Add(Restrictions.Ge("ExpireDate", date.Value));
                else
                    criteria.CreateCriteria("ListingPublications", "LP", JoinType.InnerJoin)
                        .Add(Restrictions.Le("StartDate", date.Value))
                        .Add(Restrictions.Ge("ExpireDate", date.Value));
            }
        }
        private void AddSourceFilters(ICriteria listingCriteria, string source)
        {
            if (!string.IsNullOrEmpty(source))
            {
                listingCriteria.Add(Restrictions.Eq("Source", source));
            }
        }
        private void AddTitleFilters(ICriteria listingCriteria, string title)
        {
            if (!string.IsNullOrEmpty(title))
            {
                listingCriteria.Add(Restrictions.InsensitiveLike("Title", "%" + title + "%"));
            }
        }
        private void AddDescriptionFilters(ICriteria listingCriteria, string description)
        {
            if (!string.IsNullOrEmpty(description))
            {
                listingCriteria.Add(Restrictions.InsensitiveLike("Description", "%" + description + "%"));
            }
        }
        private void AddMetadataFilters(ICriteria criteria, string metadata)
        {
            if (metadata != null)
            {
                criteria.CreateCriteria("Property", "P", JoinType.InnerJoin)
                    .Add(Restrictions.InsensitiveLike("value", metadata, MatchMode.Anywhere));
            }
        }
        private void AddUpsellFilters(ICriteria criteria, string upsell)
        {
            if (upsell != null)
            {
                var criteriaPath = criteria.GetCriteriaByAlias("LP");
                if (criteriaPath != null)
                {
                    var innerCriteriaPath = criteriaPath.GetCriteriaByAlias("U");
                    if (innerCriteriaPath != null)
                    {
                        innerCriteriaPath.Add(Restrictions.InsensitiveLike("value", upsell, MatchMode.Anywhere));
                    }
                    else
                    {
                        criteriaPath.CreateCriteria("Upsell", "U", JoinType.InnerJoin)
                            .Add(Restrictions.InsensitiveLike("value", upsell, MatchMode.Anywhere));
                    }
                }

                else
                {
                    criteria.CreateCriteria("ListingPublications", "LP", JoinType.InnerJoin)
                        .CreateCriteria("Upsell", "U", JoinType.InnerJoin)
                        .Add(Restrictions.InsensitiveLike("value", upsell, MatchMode.Anywhere));
                }
            }
        }
        private void AddClassificationFilters(ICriteria criteria, string classification)
        {
            if (classification != null)
            {
                var criteriaPath = criteria.GetCriteriaByAlias("LP");
                if (criteriaPath != null)
                {
                    var innerCriteriaPath = criteriaPath.GetCriteriaByAlias("C");
                    if (innerCriteriaPath != null)
                    {
                        innerCriteriaPath.Add(Restrictions.InsensitiveLike("name", classification, MatchMode.Anywhere));
                    }
                    else
                    {
                        criteriaPath.CreateCriteria("Categories", "C", JoinType.InnerJoin)
                            .Add(Restrictions.InsensitiveLike("name", classification, MatchMode.Anywhere));
                    }
                }

                else
                {
                    criteria.CreateCriteria("ListingPublications", "LP", JoinType.InnerJoin)
                        .CreateCriteria("Categories", "C", JoinType.InnerJoin)
                        .Add(Restrictions.InsensitiveLike("name", classification, MatchMode.Anywhere));
                }
            }
        }
        private void AddPublicationFilters(ICriteria criteria, string publication)
        {
            if (publication != null)
            {
                var criteriaPath = criteria.GetCriteriaByAlias("LP");
                if (criteriaPath != null)
                {
                    var innerCriteriaPath = criteriaPath.GetCriteriaByAlias("P");
                    if (innerCriteriaPath != null)
                    {
                        innerCriteriaPath.Add(Restrictions.InsensitiveLike("code", publication, MatchMode.Exact));
                    }
                    else
                    {
                        criteriaPath.CreateCriteria("Publication", "P", JoinType.InnerJoin)
                            .Add(Restrictions.InsensitiveLike("code", publication, MatchMode.Exact));
                    }
                }

                else
                {
                    criteria.CreateCriteria("ListingPublications", "LP", JoinType.InnerJoin)
                        .CreateCriteria("Publication", "P", JoinType.InnerJoin)
                        .Add(Restrictions.InsensitiveLike("code", publication, MatchMode.Exact));
                }
            }
        }
        private void AddFeedNameFilters(ICriteria criteria, string feedName)
        {
            if (criteria != null)
            {
                criteria.CreateCriteria("CacheEntries", "CE", JoinType.InnerJoin)
                    .Add(Restrictions.Eq("FeedName", feedName));
            }
        }

        private void AddSortingOrder(ICriteria criteria, string sortColumn, bool ascending)
        {
            if (sortColumn == "Division")
            {
                AddDivisionSort(criteria, ascending);
            }

            if (sortColumn == "Account")
            {
                AddAccountSort(criteria, ascending);
            }

            if (sortColumn == "ListingNumber")
            {
                AddListingNumberSort(criteria, ascending);
            }

            if (sortColumn.StartsWith("ExpireDate"))
            {
                AddDateSort(criteria, false, ascending);
            }

            if (sortColumn.StartsWith("StartDate"))
            {
                AddDateSort(criteria, true, ascending);
            }

            if (sortColumn == "Source")
            {
                AddSourceSort(criteria, ascending);
            }

            if (sortColumn == "Title")
            {
                AddTitleSort(criteria, ascending);
            }

            if (sortColumn == "Category")
            {
                AddCategorySort(criteria, ascending);
            }


        }
        private void AddDivisionSort(ICriteria criteria, bool ascending)
        {
            var criteriaPath = criteria.GetCriteriaByAlias("D");

            if (criteriaPath != null)
                criteriaPath.AddOrder(ascending ? Order.Asc("D.Code") : Order.Desc("D.Code"));
            else
                criteria.CreateCriteria("AdBooking", "AB", JoinType.InnerJoin)
                    .CreateCriteria("Division", "D", JoinType.InnerJoin)
                    .AddOrder(ascending ? Order.Asc("D.Code") : Order.Desc("D.Code"));
        }
        private void AddAccountSort(ICriteria criteria, bool ascending)
        {
            var criteriaPath = criteria.GetCriteriaByAlias("AB");

            if (criteriaPath != null)
                criteriaPath.AddOrder(ascending ? Order.Asc("AB.AccountNumber") : Order.Desc("AB.AccountNumber"));
            else
                criteria.CreateCriteria("AdBooking", "AB", JoinType.InnerJoin)
                    .AddOrder(ascending ? Order.Asc("AB.AccountNumber") : Order.Desc("AB.AccountNumber"));
        }
        private void AddListingNumberSort(ICriteria criteria, bool ascending)
        {
            criteria.AddOrder(ascending ? Order.Asc("ListingNumber") : Order.Desc("ListingNumber"));
        }
        private void AddDateSort(ICriteria criteria, bool sortStart, bool ascending)
        {
            var criteriaPath = criteria.GetCriteriaByAlias("LP");
            var sortOrder = sortStart
                                  ? (ascending ? Order.Asc("LP.StartDate") : Order.Desc("LP.StarDate"))
                                  : (ascending ? Order.Asc("LP.ExpireDate") : Order.Desc("LP.ExpireDate"));

            if (criteriaPath != null)
            {
                criteria.AddOrder(sortOrder);
            }
            else
            {
                criteria.CreateCriteria("ListingPublications", "LP", JoinType.InnerJoin)
                    .AddOrder(sortOrder);
            }
        }
        private void AddSourceSort(ICriteria criteria, bool ascending)
        {
            criteria.AddOrder(ascending ? Order.Asc("Source") : Order.Desc("Source"));
        }
        private void AddTitleSort(ICriteria criteria, bool ascending)
        {
            criteria.AddOrder(ascending ? Order.Asc("Title") : Order.Desc("Title"));
        }
        private void AddCategorySort(ICriteria criteria, bool ascending)
        {
            var criteriaPath = criteria.GetCriteriaByAlias("LP");

            if (criteriaPath != null)
                criteriaPath.AddOrder(ascending ? Order.Asc("LP.Categories") : Order.Desc("LP.Categories"));
            else
                criteria.CreateCriteria("ListingPublications", "LP", JoinType.InnerJoin)
                    .AddOrder(ascending ? Order.Asc("LP.Categories") : Order.Desc("LP.Categories"));
        }

        public IEnumerable<Listing> GetListingsUsingCriteria(QRCodeUrlCriteria criteria)
        {
            // if criteria date is null default to "today".  This should not happen.
            DateTime checkdate = criteria.Date ?? DateTime.Now;

            // not sure if source matters.  This may need to only be Genera bookings
            var sources = new List<string>() { "NationalGenera", "SAMS" };
            var publications = new List<string>() { "Working", "Workopolis" };

            var listingCriteria = DetachedCriteria.For<Listing>("L")
                .Add(Restrictions.In("Source", sources))
                .CreateCriteria("ListingPublications", "LP", JoinType.InnerJoin)
                    .Add(Restrictions.Ge("StartDate", checkdate.AddDays(-7)))
                    .Add(Restrictions.Le("StartDate", checkdate))
                    .Add(Restrictions.Eq("Status", "ready"))
                .CreateCriteria("Publication", "P", JoinType.InnerJoin)
                    .Add(Restrictions.In("P.Name", publications));

            listingCriteria.GetCriteriaByAlias("L")
                .CreateCriteria("MediaItems", "M", JoinType.InnerJoin)
                .Add(Restrictions.Eq("Role", "displayUrl"));

            if (!string.IsNullOrEmpty(criteria.Division))
            {
                listingCriteria.GetCriteriaByAlias("L")
                    .CreateCriteria("AdBooking", "AB", JoinType.InnerJoin)
                    .CreateCriteria("Division", "D", JoinType.InnerJoin)
                    .Add(Restrictions.Eq("D.Code", criteria.Division));
            }
            //else
            //{
            //    var defaultDivisions = new List<string>() { "SAS", "REG" };

            //    listingCriteria.GetCriteriaByAlias("L")
            //        .CreateCriteria("AdBooking", "AB", JoinType.InnerJoin)
            //        .CreateCriteria("Division", "D", JoinType.InnerJoin)
            //        .Add(Restrictions.In("D.Code", defaultDivisions));
            //}

            listingCriteria.SetResultTransformer(new DistinctRootEntityResultTransformer());
            var executableCriteria = listingCriteria.GetExecutableCriteria(Session);
            var criteriaListings = executableCriteria.List<Listing>();

            return criteriaListings.ToList();
        }

        public IEnumerable<SamsResult> GetSamsResultsUsingCriteria(SamsCriteria criteria)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings[0].ConnectionString;
            conn.Open();

            // create a command object identifying the stored procedure
            SqlCommand cmd = new SqlCommand("LRSamsWorkingReport", conn);
            cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings[Core.ServiceSettings.QueryTimeout.ToString()]);

            // set the command object so it knows to execute a stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            // add parameters to the command to be passed to the stored procedure
            if (!String.IsNullOrEmpty(Convert.ToString(criteria.Date)))
            {
                cmd.Parameters.Add(new SqlParameter("@argListingPubStartDate", criteria.Date));
            }
            if (!String.IsNullOrEmpty(criteria.Division))
            {
                cmd.Parameters.Add(new SqlParameter("@argDivision", criteria.Division));
            }

            // create the list to be populated
            var samsResults = new List<SamsResult>();

            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var samsResult = new SamsResult();

                int maxCols = reader.FieldCount;
                for (int i = 0; i < maxCols; i++)
                {
                    var type = reader.GetFieldType(i).FullName;

                    var stringvalue = "";
                    int intvalue = 0;
                    var datetimevalue = new DateTime();

                    switch (type)
                    {
                        case "System.String":
                            if (!reader.IsDBNull(i))
                            {
                                stringvalue = reader.GetString(i);
                            }
                            break;
                        case "System.DateTime":
                            if (!reader.IsDBNull(i))
                            {
                                datetimevalue = reader.GetDateTime(i);
                            }
                            break;
                        case "System.Int16":
                            if (!reader.IsDBNull(i))
                            {
                                intvalue = reader.GetInt16(i);
                            }
                            break;
                        case "System.Int32":
                            if (!reader.IsDBNull(i))
                            {
                                intvalue = reader.GetInt32(i);
                            }
                            break;
                        case "System.Int64":
                            if (!reader.IsDBNull(i))
                            {
                                var longvalue = reader.GetInt64(i);
                                if (longvalue <= Int32.MaxValue)
                                {
                                    intvalue = Convert.ToInt32(longvalue);
                                }
                            }
                            break;
                        default:
                            // TODO: write error to log
                            break;
                    }

                    // set values in result record
                    if (i == (int)SamsWorkingReportEnum.ListingId)
                    {
                        samsResult.ListingId = intvalue;
                    }
                    if (i == (int)SamsWorkingReportEnum.Division)
                    {
                        samsResult.Division = stringvalue;
                    }
                    if (i == (int)SamsWorkingReportEnum.ListingNumber)
                    {
                        samsResult.ListingNumber = stringvalue;
                    }
                    if (i == (int)SamsWorkingReportEnum.AdOrderNumber)
                    {
                        samsResult.AdOrderNumber = stringvalue;
                    }
                    if (i == (int)SamsWorkingReportEnum.AdvertiserName)
                    {
                        samsResult.AdvertiserName = stringvalue;
                    }
                    if (i == (int)SamsWorkingReportEnum.Title)
                    {
                        samsResult.Title = stringvalue;
                    }
                    if (i == (int)SamsWorkingReportEnum.Category)
                    {
                        samsResult.Category = stringvalue;
                    }
                    if (i == (int)SamsWorkingReportEnum.StartDate)
                    {
                        samsResult.StartDate = datetimevalue;
                    }
                    if (i == (int)SamsWorkingReportEnum.ExpireDate)
                    {
                        samsResult.ExpireDate = datetimevalue;
                    }
                    if (i == (int)SamsWorkingReportEnum.AdLength)
                    {
                        samsResult.AdLength = intvalue;
                    }
                    if (i == (int)SamsWorkingReportEnum.FeedName)
                    {
                        samsResult.FeedName = stringvalue;
                    }
                }

                // add the line item to the collection
                samsResults.Add(samsResult);
            }

            // clean up connection
            conn.Close();
            conn.Dispose();

            return samsResults;
        }

        public IEnumerable<SamsNoFeedbackResult> GetSamsNoFeedbackResultsUsingCriteria(SamsNoFeedbackCriteria criteria)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings[0].ConnectionString;
            conn.Open();

            // create a command object identifying the stored procedure
            SqlCommand cmd = new SqlCommand("LRSamsWorkingNoFeedbackReport", conn);
            cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings[Core.ServiceSettings.QueryTimeout.ToString()]);

            // set the command object so it knows to execute a stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            // add parameters to the command to be passed to the stored procedure
            if (!String.IsNullOrEmpty(Convert.ToString(criteria.Date)))
            {
                cmd.Parameters.Add(new SqlParameter("@argListingPubActiveDate", criteria.Date));
            }
            if (!String.IsNullOrEmpty(criteria.Division))
            {
                cmd.Parameters.Add(new SqlParameter("@argDivision", criteria.Division));
            }

            // create the list to be populated
            var samsNoFeedbackResults = new List<SamsNoFeedbackResult>();

            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var samsNoFeedbackResult = new SamsNoFeedbackResult();

                int maxCols = reader.FieldCount;
                for (int i = 0; i < maxCols; i++)
                {
                    var type = reader.GetFieldType(i).FullName;

                    var stringvalue = "";
                    int intvalue = 0;
                    var datetimevalue = new DateTime();

                    switch (type)
                    {
                        case "System.String":
                            if (!reader.IsDBNull(i))
                            {
                                stringvalue = reader.GetString(i);
                            }
                            break;
                        case "System.DateTime":
                            if (!reader.IsDBNull(i))
                            {
                                datetimevalue = reader.GetDateTime(i);
                            }
                            break;
                        case "System.Int16":
                            if (!reader.IsDBNull(i))
                            {
                                intvalue = reader.GetInt16(i);
                            }
                            break;
                        case "System.Int32":
                            if (!reader.IsDBNull(i))
                            {
                                intvalue = reader.GetInt32(i);
                            }
                            break;
                        case "System.Int64":
                            if (!reader.IsDBNull(i))
                            {
                                var longvalue = reader.GetInt64(i);
                                if (longvalue <= Int32.MaxValue)
                                {
                                    intvalue = Convert.ToInt32(longvalue);
                                }
                            }
                            break;
                        default:
                            // TODO: write error to log
                            break;
                    }

                    // set values in result record
                    if (i == (int)SamsWorkingNoFeedbackReportEnum.ListingId)
                    {
                        samsNoFeedbackResult.ListingId = intvalue;
                    }
                    if (i == (int)SamsWorkingNoFeedbackReportEnum.Division)
                    {
                        samsNoFeedbackResult.Division = stringvalue;
                    }
                    if (i == (int)SamsWorkingNoFeedbackReportEnum.ListingNumber)
                    {
                        samsNoFeedbackResult.ListingNumber = stringvalue;
                    }
                    if (i == (int)SamsWorkingNoFeedbackReportEnum.AdOrderNumber)
                    {
                        samsNoFeedbackResult.AdOrderNumber = stringvalue;
                    }
                    if (i == (int)SamsWorkingNoFeedbackReportEnum.AdvertiserName)
                    {
                        samsNoFeedbackResult.AdvertiserName = stringvalue;
                    }
                    if (i == (int)SamsWorkingNoFeedbackReportEnum.Title)
                    {
                        samsNoFeedbackResult.Title = stringvalue;
                    }
                    if (i == (int)SamsWorkingNoFeedbackReportEnum.Category)
                    {
                        samsNoFeedbackResult.Category = stringvalue;
                    }
                    if (i == (int)SamsWorkingNoFeedbackReportEnum.StartDate)
                    {
                        samsNoFeedbackResult.StartDate = datetimevalue;
                    }
                    if (i == (int)SamsWorkingNoFeedbackReportEnum.ExpireDate)
                    {
                        samsNoFeedbackResult.ExpireDate = datetimevalue;
                    }
                    if (i == (int)SamsWorkingNoFeedbackReportEnum.AdLength)
                    {
                        samsNoFeedbackResult.AdLength = intvalue;
                    }
                    if (i == (int)SamsWorkingNoFeedbackReportEnum.FeedName)
                    {
                        samsNoFeedbackResult.FeedName = stringvalue;
                    }
                }

                // add the line item to the collection
                samsNoFeedbackResults.Add(samsNoFeedbackResult);
            }

            // clean up connection
            conn.Close();
            conn.Dispose();

            return samsNoFeedbackResults;
        }

        public IEnumerable<DetailedUpsellResult> GetDetailedUpsellUsingCriteria(DetailedUpsellCriteria criteria)
        {
            const string query = "exec [dbo].[LRDetailedUpsellReport] " +
                           "@argReportStartDate1 = :StartDate1" +
                           ",@argReportStartDate2 = :StartDate2" +
                           ",@argReportExpireDate1 = :ExpireDate1" +
                           ",@argReportExpireDate2 = :ExpireDate2" +
                           ",@argUpsellCode = :UpsellCode" +
                           ",@argListingPubStatus = :ListingPubStatus" +
                           ",@argDivisionCode = :DivisionCode" +
                           ",@argPublicationCode = :PublicationCode" +
                           ",@argSource = :Source" +
                           ",@argPublishingDays = :PublishingDays"
                           ;
            
            var retResults = Session.CreateSQLQuery(query)
                .AddEntity(typeof (Entities.DetailedUpsellReport))
                .SetParameter("StartDate1", criteria.StartDate1)
                .SetParameter("StartDate2", criteria.StartDate2)
                .SetParameter("ExpireDate1", criteria.ExpireDate1)
                .SetParameter("ExpireDate2", criteria.ExpireDate2)
                .SetParameter("UpsellCode", criteria.UpsellCode)
                .SetParameter("ListingPubStatus", criteria.ListingPubStatus)
                .SetParameter("DivisionCode", criteria.DivisionCode)
                .SetParameter("PublicationCode", criteria.PublicationCode)
                .SetParameter("Source", criteria.ListingSource)
                .SetParameter("PublishingDays", criteria.PublishingDays)
                .List<Entities.DetailedUpsellReport>()
                .Select(x => new DetailedUpsellResult
                    {
                        ListingId = x.ListingId,
                        UpsellId = x.UpsellId,
                        Source = x.Source,
                        DivisionName = x.DivisionName,
                        ListingNumber = x.ListingNumber,
                        PublicationName = x.PublicationName,
                        Title = x.Title,
                        StartDate = x.StartDate,
                        ExpireDate = x.ExpireDate,
                        Status = x.Status,
                        UpsellName = x.UpsellName,
                        PublishingDays = x.PublishingDays
                    })
                .ToList();

            //Session.Dispose();

            return retResults;
        }

        public IEnumerable<ListingPubResult> NewGetListingPublicationsUsingCritera(ListingPubCriteria criteria)
        {
            const string query = "exec [dbo].[LRListingPubByFullCriteria] " +
                                " @argDivision = :Division" +
                                ", @argAcctNum = :AccountNumber" +
                                ", @argListingNum = :ListingNumber" +
                                ", @argListingSource = :ListingSource" +
                                ", @argListingCaption = :ListingCaption" +
                                ", @argListingText = :ListingText" +
                                ", @argAdOrderNum = :AdOrderNumber" +
                                ", @argListingPubDate = :ListingPubDate" +
                                ", @argListingPubStatus = :ListingPubStatus" +
                                ", @argListingPubCategory = :ListingPubCategory" +
                                ", @argPubName = :PublicationName" +
                                ", @argFeedname = :FeedName" +
                                ", @argUpsellName = :UpsellName" +
                                ", @argUpsellCode = :UpsellCode" +
                                ", @argPropertyCode = :PropertyCode" +
                                ", @argPropertyValue = :PropertyValue" +
                                ", @argListingPubStartDate = :ListingPubStartDate" +
                                ", @argListingPubExpireDate = :ListingPubExpireDate"
                                ;

            var retResults = Session.CreateSQLQuery(query)
                .AddEntity(typeof (Entities.ListingPubResult))
                .SetParameter("Division", criteria.Division)
                .SetParameter("AccountNumber", criteria.AccountNumber)
                .SetParameter("ListingNumber", criteria.ListingNumber)
                .SetParameter("ListingSource", criteria.ListingSource)
                .SetParameter("ListingCaption", criteria.ListingCaption)
                .SetParameter("ListingText", criteria.ListingText)
                .SetParameter("AdOrderNumber", criteria.AdOrderNumber)
                .SetParameter("ListingPubDate", criteria.ListingPubDate)
                .SetParameter("ListingPubStatus", criteria.ListingPubStatus)
                .SetParameter("ListingPubCategory", criteria.ListingPubCategory)
                .SetParameter("PublicationName", criteria.PublicationName)
                .SetParameter("FeedName", criteria.FeedName)
                .SetParameter("UpsellName", criteria.UpsellName)
                .SetParameter("UpsellCode", criteria.UpsellCode)
                .SetParameter("PropertyCode", criteria.PropertyCode)
                .SetParameter("PropertyValue", criteria.PropertyValue)
                .SetParameter("ListingPubStartDate", criteria.ListingPubStartDate)
                .SetParameter("ListingPubExpireDate", criteria.ListingPubExpireDate)
                .List<Entities.ListingPubResult>()
                .Select(x => new ListingPubResult
                    {
                        Id = x.ListingId,
                        ListingPublicationId = x.ListingPublicationId,
                        Division = x.Division,
                        AccountNumber = x.AccountNumber,
                        ListingNumber = x.ListingNumber,
                        AdOrderNumber = x.AdOrderNumber,
                        FeedName = x.FeedName,
                        Publication = x.Publication,
                        StartDate = x.StartDate,
                        ExpireDate = x.ExpireDate,
                        Status = x.Status,
                        Category = x.Category,
                        Title = x.Caption,
                        Source = x.Source
                    })
                .ToList();

            //Session.Dispose();

            return retResults;
        }

        public IEnumerable<UpsellResult> GetWorkingUpsellUsingCritera(UpsellCriteria criteria)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings[0].ConnectionString;
            conn.Open();

            // create a command object identifying the stored procedure
            SqlCommand cmd = new SqlCommand("LRWorkingUpsellReport", conn);

            // set the command object so it knows to execute a stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            // add parameters to the command to be passed to the stored procedure
            cmd.Parameters.Add(new SqlParameter("@startDate", criteria.StartDate));
            cmd.Parameters.Add(new SqlParameter("@endDate", criteria.EndDate));
            string division = "";
            if (!string.IsNullOrEmpty(criteria.Division))
            {
                division = criteria.Division;
            }
            cmd.Parameters.Add(new SqlParameter("@division", division));

            // create the list to be populated
            var upsellResults = new List<UpsellResult>();

            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var UpsellResult = new UpsellResult();
                var upsellCounts = new List<UpsellCount>();

                string upselldivision = "";

                int maxCols = reader.FieldCount;
                for (int i = 0; i < maxCols; i++)
                {
                    var name = reader.GetName(i);
                    if (i == 0)
                    {
                        var value = reader.GetString(i);
                        upselldivision = value;
                    }
                    else
                    {
                        var count = new UpsellCount();
                        var value = reader.GetInt32(i);

                        count.Name = name;
                        count.Value = value;
                        upsellCounts.Add(count);
                    }
                }

                // set the values on the upsellresult
                UpsellResult.Division = upselldivision;
                UpsellResult.UpsellCounts = upsellCounts;

                // add the line item to the collection
                upsellResults.Add(UpsellResult);
            }

            // clean up connection
            conn.Close();
            conn.Dispose();

            // return results
            return upsellResults;
        }

        public IEnumerable<ListingPubResult> GetListingPublicationsUsingCritera(ListingPubCriteria criteria)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = ConfigurationManager.ConnectionStrings[0].ConnectionString;
            conn.Open();

            // create a command object identifying the stored procedure
            SqlCommand cmd = new SqlCommand("LRListingPubByFullCriteria", conn);
            cmd.CommandTimeout = int.Parse(ConfigurationManager.AppSettings[Core.ServiceSettings.QueryTimeout.ToString()]);

            // set the command object so it knows to execute a stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            // add parameters to the command to be passed to the stored procedure
            if (!String.IsNullOrEmpty(criteria.Division))
            {
                cmd.Parameters.Add(new SqlParameter("@argDivision", criteria.Division));
            }
            if (!String.IsNullOrEmpty(criteria.AccountNumber))
            {
                cmd.Parameters.Add(new SqlParameter("@argAcctNum", criteria.AccountNumber));
            }
            if (!String.IsNullOrEmpty(criteria.ListingNumber))
            {
                cmd.Parameters.Add(new SqlParameter("@argListingNum", criteria.ListingNumber));
            }
            if (!String.IsNullOrEmpty(criteria.ListingSource))
            {
                cmd.Parameters.Add(new SqlParameter("@argListingSource", criteria.ListingSource));
            }
            if (!String.IsNullOrEmpty(criteria.ListingCaption))
            {
                cmd.Parameters.Add(new SqlParameter("@argListingCaption", criteria.ListingCaption));
            }
            if (!String.IsNullOrEmpty(criteria.ListingText))
            {
                cmd.Parameters.Add(new SqlParameter("@argListingText", criteria.ListingText));
            }
            if (!String.IsNullOrEmpty(criteria.AdOrderNumber))
            {
                cmd.Parameters.Add(new SqlParameter("@argAdOrderNum", criteria.AdOrderNumber));
            }
            if (!String.IsNullOrEmpty(Convert.ToString(criteria.ListingPubDate)))
            {
                cmd.Parameters.Add(new SqlParameter("@argListingPubDate", criteria.ListingPubDate));
            }
            if (!String.IsNullOrEmpty(criteria.ListingPubStatus))
            {
                cmd.Parameters.Add(new SqlParameter("@argListingPubStatus", criteria.ListingPubStatus));
            }
            if (!String.IsNullOrEmpty(criteria.ListingPubCategory))
            {
                cmd.Parameters.Add(new SqlParameter("@argListingPubCategory", criteria.ListingPubCategory));
            }
            if (!String.IsNullOrEmpty(criteria.PublicationName))
            {
                cmd.Parameters.Add(new SqlParameter("@argPubName", criteria.PublicationName));
            }
            if (!String.IsNullOrEmpty(criteria.FeedName))
            {
                cmd.Parameters.Add(new SqlParameter("@argFeedname", criteria.FeedName));
            }
            if (!String.IsNullOrEmpty(criteria.UpsellName))
            {
                cmd.Parameters.Add(new SqlParameter("@argUpsellName", criteria.UpsellName));
            }
            if (!String.IsNullOrEmpty(criteria.UpsellCode))
            {
                cmd.Parameters.Add(new SqlParameter("@argUpsellCode", criteria.UpsellCode));
            }
            if (!String.IsNullOrEmpty(criteria.PropertyCode))
            {
                cmd.Parameters.Add(new SqlParameter("@argPropertyCode", criteria.PropertyCode));
            }
            if (!String.IsNullOrEmpty(criteria.PropertyValue))
            {
                cmd.Parameters.Add(new SqlParameter("@argPropertyValue", criteria.PropertyValue));
            }

            if (!String.IsNullOrEmpty(Convert.ToString(criteria.ListingPubStartDate)))
            {
                cmd.Parameters.Add(new SqlParameter("@argListingPubStartDate", criteria.ListingPubStartDate));
            }
            if (!String.IsNullOrEmpty(Convert.ToString(criteria.ListingPubExpireDate)))
            {
                cmd.Parameters.Add(new SqlParameter("@argListingPubExpireDate", criteria.ListingPubExpireDate));
            }


            // create the list to be populated
            var listingPubResults = new List<ListingPubResult>();

            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var listingPubResult = new ListingPubResult();

                /*
                listingPubResult.Id = (ListingPubEnum.Id.Type())reader.GetExtension(ListingPubEnum.Id);
                listingPubResult.ExpireDate = (ListingPubEnum.ColumnName.Type)reader.GetExtension(ListingPubEnum.ColumnName);
                listingPubResult.FeedName = (ListingPubEnum.ColumnName.Type)reader.GetExtension(ListingPubEnum.ColumnName);
                listingPubResult.Id = (ListingPubEnum.ColumnName.Type)reader.GetExtension(ListingPubEnum.ColumnName);
                listingPubResult.Id = (ListingPubEnum.ColumnName.Type)reader.GetExtension(ListingPubEnum.ColumnName);
                listingPubResult.Id = (ListingPubEnum.ColumnName.Type)reader.GetExtension(ListingPubEnum.ColumnName);
                listingPubResult.Id = (ListingPubEnum.ColumnName.Type)reader.GetExtension(ListingPubEnum.ColumnName);
                listingPubResult.Id = (ListingPubEnum.ColumnName.Type)reader.GetExtension(ListingPubEnum.ColumnName);
                listingPubResult.Id = (ListingPubEnum.ColumnName.Type)reader.GetExtension(ListingPubEnum.ColumnName);
                listingPubResult.Id = (ListingPubEnum.ColumnName.Type)reader.GetExtension(ListingPubEnum.ColumnName);
                listingPubResult.Id = (ListingPubEnum.ColumnName.Type)reader.GetExtension(ListingPubEnum.ColumnName);
                */

                // set the values on the listingpubresult
                // TODO: need to complete this.  Possibly use enumeration?

                /* results coming from proc in this order:
                    listingid, division, accountnumber, listingnumber, adordernumber,feedname, 
                    publication, startdate, expiredate, status, category, caption, source
                */
                int maxCols = reader.FieldCount;
                for (int i = 0; i < maxCols; i++)
                {
                    var name = reader.GetName(i);
                    string stringvalue = "";

                    if (i == 0)
                    {
                        var value = reader.GetInt32(i);
                        listingPubResult.Id = value;
                    }
                    if (i == 1)
                    {
                        var value = reader.GetString(i);
                        listingPubResult.Division = value;
                    }
                    if (i == 2)
                    {
                        var value = reader.GetString(i);
                        listingPubResult.AccountNumber = value;
                    }
                    if (i == 3)
                    {
                        var value = reader.GetString(i);
                        listingPubResult.ListingNumber = value;
                    }
                    if (i == 4)
                    {
                        if (!reader.IsDBNull(i))
                        {
                            stringvalue = reader.GetString(i);
                        }
                        listingPubResult.AdOrderNumber = stringvalue;
                    }
                    if (i == 5)
                    {
                        if (!reader.IsDBNull(i))
                        {
                            stringvalue = reader.GetString(i);
                        }
                        listingPubResult.FeedName = stringvalue;
                    }
                    if (i == 6)
                    {
                        if (!reader.IsDBNull(i))
                        {
                            stringvalue = reader.GetString(i);
                        }
                        listingPubResult.Publication = stringvalue;
                    }
                    if (i == 7)
                    {
                        var value = reader.GetDateTime(i);
                        listingPubResult.StartDate = value;
                    }
                    if (i == 8)
                    {
                        var value = reader.GetDateTime(i);
                        listingPubResult.ExpireDate = value;
                    }
                    if (i == 9)
                    {
                        if (!reader.IsDBNull(i))
                        {
                            stringvalue = reader.GetString(i);
                        }
                        listingPubResult.Status = stringvalue;
                    }
                    if (i == 10)
                    {
                        if (!reader.IsDBNull(i))
                        {
                            stringvalue = reader.GetString(i);
                        }
                        listingPubResult.Category = stringvalue;
                    }
                    if (i == 11)
                    {
                        if (!reader.IsDBNull(i))
                        {
                            stringvalue = reader.GetString(i);
                        }
                        listingPubResult.Title = stringvalue;
                    }
                    if (i == 12)
                    {
                        if (!reader.IsDBNull(i))
                        {
                            stringvalue = reader.GetString(i);
                        }
                        listingPubResult.Source = stringvalue;
                    }
                }

                // add the line item to the collection
                listingPubResults.Add(listingPubResult);
            }

            // clean up connection
            conn.Close();
            conn.Dispose();

            return listingPubResults;
        }

        public int Compare(Listing x, Listing y)
        {
            return x.AdBooking.Division.Name.CompareTo(y.AdBooking.Division.Name);
        }

        public IEnumerable<int> FetchAllLikeNumber(string listingString)
        {
            var c = Session.CreateSQLQuery("select ListingId from Listing with (nolock)" +
                                           "where ListingNumber like :listingNumber")
                                           .SetParameter("listingNumber", "%" + listingString + "%")
                                           ;

            var query = (IQuery) Session.GetNamedQuery("WhateverYourProcNamed");
            query.SetParameter("asdf", "");
            var result = query.List();
            return c.List<int>();
        }
    }

    public class AdvancedPagedSearchCriteria
    {
        public string Division { get; set; }
        public string Account { get; set; }
        public string ListingNumber { get; set; }
        public string Source { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? Date { get; set; }

        public string Metadata { get; set; }
        public string Upsell { get; set; }
        public string Classification { get; set; }
        public string Publication { get; set; }
        public string FeedName { get; set; }
        public string Media { get; set; }

        public int Page { get; set; }
        public int PageSize { get; set; }

        public bool Ascending { get; set; }
        public string Sort { get; set; }
    }
}
