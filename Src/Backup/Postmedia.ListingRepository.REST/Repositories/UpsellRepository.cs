﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class UpsellRepository : BaseNHibernateRepository<Upsell>, IUpsellRepository<Upsell>
    {
        public UpsellRepository() : base(new WcfSessionStorage())
        {

        }

        public Upsell FetchByCode(string code)
        {
            var division = (from upsell in Session.Query<Upsell>()
                            where upsell.Code == code
                            select upsell).FirstOrDefault();

            return division;
        }
    }
}