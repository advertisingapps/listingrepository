﻿using System;
using System.Collections.Generic;
using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Transform;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;
using CacheEntry = Postmedia.ListingRepository.REST.Entities.CacheEntry;


namespace Postmedia.ListingRepository.REST.Repositories
{
    public class CacheEntryRepository : BaseNHibernateRepository<CacheEntry>, ICacheEntryRepository<CacheEntry>
    {
        public CacheEntryRepository() : base(new WcfSessionStorage())  {}

        public IEnumerable<CacheEntry> FetchUsingCriteriaOriginal(ExportTarget target, ExportCriteria criteria)
        {
            var cacheEntries = Session.Query<CacheEntry>()
                .Where(ce => criteria.RunDate.Value.Date >= ce.StartDate &&
                             criteria.RunDate.Value.Date <= ce.ExpireDate &&
                             target.ToString() == ce.FeedName);

            if (criteria.LastRunDate != null)
            {
                cacheEntries = cacheEntries.Where(entry =>
                                                  entry.LastModifiedDate.Value
                                                  >= criteria.LastRunDate.Value);
            }

            return cacheEntries.OrderByDescending(m => m.StartDate);
        }

        public IEnumerable<CacheEntry> FetchUsingCriteria(ExportTarget target, ExportCriteria criteria)
        {
            
            var cacheEntries = Session.CreateSQLQuery("select obc.FeedName As \"FeedName\", " +
                                          "obc.ListingNumber as \"ListingNumber\", " +
                                          "obc.StartDate as \"StartDate\", " +
                                          "obc.ExpireDate as \"ExpireDate\", " +
                                          "obc.LastModifiedDate as \"LastModifiedDate\", " +
                                          "obc.CacheData as \"CacheData\", " +
                                          "obc.OutboundCacheId as \"Id\" " +
                                          "from OutboundCache obc with (nolock) " +
                                          "inner join Listing l with (nolock) on obc.ListingNumber = l.ListingNumber " +
                                          "where obc.StartDate <= :runDate " +
                                          "and obc.ExpireDate >= :runDate " +
                                          "and obc.FeedName like :targetFeed " +
                                          "and (l.HasAdminLock is null or l.HasAdminLock = 0)")
                                          .AddScalar("FeedName", NHibernateUtil.String)
                                          .AddScalar("ListingNumber", NHibernateUtil.String)
                                          .AddScalar("StartDate", NHibernateUtil.DateTime)
                                          .AddScalar("ExpireDate", NHibernateUtil.DateTime)
                                          .AddScalar("LastModifiedDate", NHibernateUtil.DateTime)
                                          .AddScalar("CacheData", NHibernateUtil.String)
                                          .AddScalar("Id", NHibernateUtil.Int64)
                                          .SetParameter("runDate", criteria.RunDate.Value.Date)
                                          .SetParameter("targetFeed", target.ToString())
                                          .SetResultTransformer(new AliasToBeanResultTransformer(typeof(CacheEntry)))
                                          .List<CacheEntry>();

            if (criteria.LastRunDate != null)
            {
                cacheEntries = cacheEntries.Where(entry =>
                                                  entry.LastModifiedDate.Value
                                                  >= criteria.LastRunDate.Value).ToList();
            }

            return cacheEntries.OrderByDescending(m => m.StartDate);
        }

        public IEnumerable<CacheEntry> FetchUsingListingNumber(string listingNumber)
        {
            var cacheEntries = Session.Query<CacheEntry>()
                .Where(ce => ce.ListingNumber.Contains(listingNumber));

            return cacheEntries;
        }


    }
}