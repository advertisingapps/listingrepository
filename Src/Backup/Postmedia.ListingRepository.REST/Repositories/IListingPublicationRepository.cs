﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public interface IListingPublicationRepository<T>
    {
        IEnumerable<T> FetchAll();
        T FetchById(long id);
        void SaveOrUpdate(T listing);
    }
}
