﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public interface IListingStatusLookupRepository<T>
    {
        IEnumerable<T> FetchAll();
    }
}