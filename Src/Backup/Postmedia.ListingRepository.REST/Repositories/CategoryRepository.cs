﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class CategoryRepository : BaseNHibernateRepository<Category>, ICategoryRepository<Category>
    {
        public CategoryRepository(): base(new WcfSessionStorage()) { }

        public Category FetchByCode(string code)
        {
            var category = (from cat in Session.Query<Category>()
                            where cat.Code == code
                            select cat).FirstOrDefault();

            return category;

        }
    }
}