﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class PublicationRepository : BaseNHibernateRepository<Publication>, IPublicationRepository<Publication>
    {
        public PublicationRepository()
            : base(new WcfSessionStorage())
        {

        }

        public Publication FetchByCode(string code)
        {
            var publication = (from pub in Session.Query<Publication>()
                               where pub.Code == code
                               select pub).FirstOrDefault();

            return publication;
        }
    }
}
    
    