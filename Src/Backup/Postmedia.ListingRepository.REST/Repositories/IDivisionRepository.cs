﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public interface IDivisionRepository<T>
    {
        IEnumerable<T> FetchAll();
        T FetchById(long id);
        T FetchByCode(string code);
        void SaveOrUpdate(T division);
    }
}