﻿using System;
using System.Collections.Generic;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public interface IListingRepository<T>
    {
        IEnumerable<T> FetchAll();
        T FetchById(long id);
        T FetchByNumber(string listingNumber);
        IEnumerable<T> FetchAllByNumber(string listingString);
        void SaveOrUpdate(T listing);
        void DeleteListing(string listingNumber);
        void DeleteListing(T listing);

        IEnumerable<T> GetListingsUsingCriteria(ListingCriteria criteria);
        PagedData<T> GetPagedListingsUsingCriteria(ListingCriteriaPaged criteria);
        IEnumerable<T> GetListingsUsingCriteria(QRCodeUrlCriteria criteria);
        IEnumerable<UpsellResult> GetWorkingUpsellUsingCritera(UpsellCriteria criteria);
        IEnumerable<ListingPubResult> GetListingPublicationsUsingCritera(ListingPubCriteria criteria);
        IEnumerable<ListingPubResult> NewGetListingPublicationsUsingCritera(ListingPubCriteria criteria);
        IEnumerable<SamsResult> GetSamsResultsUsingCriteria(SamsCriteria criteria);
        IEnumerable<SamsNoFeedbackResult> GetSamsNoFeedbackResultsUsingCriteria(SamsNoFeedbackCriteria criteria);
        IEnumerable<DetailedUpsellResult> GetDetailedUpsellUsingCriteria(DetailedUpsellCriteria criteria);
    }
}
