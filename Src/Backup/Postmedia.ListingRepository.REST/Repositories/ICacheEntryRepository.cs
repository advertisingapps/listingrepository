﻿using System.Collections.Generic;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public interface ICacheEntryRepository<T>
    {
        IEnumerable<T> FetchAll();
        IEnumerable<T> FetchUsingCriteria(ExportTarget target, ExportCriteria criteria);
        IEnumerable<T> FetchUsingListingNumber(string listingNumber);
        T FetchById(long id);
        void SaveOrUpdate(T listing);
    }
}