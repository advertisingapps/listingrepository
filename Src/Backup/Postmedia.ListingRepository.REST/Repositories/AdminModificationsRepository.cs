﻿using System.Collections.Generic;
using IglooCoder.Commons.WcfNhibernate;
using Postmedia.ListingRepository.REST.Entities;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.SqlCommand;
using NHibernate.Transform;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class AdminModificationsRepository: BaseNHibernateRepository<AdminModifications>, IAdminModificationsRepository<AdminModifications>
    {
        public AdminModificationsRepository():base(new WcfSessionStorage())
        {
            
        }

        public IEnumerable<AdminModifications> FetchAllByNumber(string listingNumber)
        {
            var criteria = DetachedCriteria.For<AdminModifications>();

            criteria.Add(Restrictions.InsensitiveLike("ListingNumber", listingNumber));

            criteria.SetResultTransformer(new DistinctRootEntityResultTransformer());

            var executeCriteria = criteria.GetExecutableCriteria(Session);
            var retModifications = executeCriteria.List<AdminModifications>();

            return retModifications;
        }

        public List<AdminModifications> FetchExisting(List<AdminModifications> adminModifications)
        {
            var adMods = new List<AdminModifications>();

            adminModifications.ForEach(x =>
                {
                    var criteria = DetachedCriteria.For<AdminModifications>();
                    criteria.Add(Restrictions.InsensitiveLike("ListingNumber", x.ListingNumber));
                    criteria.Add(Restrictions.InsensitiveLike("Field", x.Field));
                    criteria.SetResultTransformer(new DistinctRootEntityResultTransformer());

                    var executeCriteria = criteria.GetExecutableCriteria(Session);

                    var retModification = executeCriteria.UniqueResult<AdminModifications>();

                    if (retModification == null)
                    {
                        adMods.Add(x);
                    }
                    else
                    {
                        retModification.DateModified = x.DateModified;
                        adMods.Add(retModification);
                    }
                }
            );

            return adMods;
        }

        void IAdminModificationsRepository<AdminModifications>.Delete(List<AdminModifications> adminModifications)
        {
            adminModifications.ForEach(x =>
            {
                Session.Delete(x);
                Session.Flush();
            }
            );
        }

        void IAdminModificationsRepository<AdminModifications>.SaveOrUpdate(List<AdminModifications> adminModifications)
        {
            adminModifications.ForEach(x =>
                {
                    Session.SaveOrUpdate(x);
                    Session.Flush();
                }
            );
        }
    }
}