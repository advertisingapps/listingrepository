﻿using System.Collections.Generic;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.REST.Services
{
    public interface IListingService
    {
        Listing CreateListing(Listing listing);
        void DeleteListing(string listingNumber);
        Listing UpdateListing(string listingNumber, Listing listing);
        Listing GetListingById(int listingId);
        Listing GetListingByNumber(string listingId);

        List<Listing> GetListingsLikeNumber(string listingNumber);
        
        List<ListingSummary> GetAllListingSummaries();
        List<ListingSummary> GetListingSummariesUsingCriteria(ListingCriteria criteria);
        PagedData<ListingSummary> GetPagedListingSummariesUsingCriteria(ListingCriteriaPaged criteria);

        ExportSummary ExportListingBatch(ExportCriteria criteria);
    }
}
