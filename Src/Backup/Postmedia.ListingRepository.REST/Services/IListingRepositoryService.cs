﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.REST.Services
{
    // Start the service and browse to http://<machine_name>:<port>/ListingRepositoryService/help to view the service's generated help page
    [ServiceContract(Namespace = "" )]
    [XmlSerializerFormat]
    public interface IListingRepositoryService
    {
        [WebInvoke(UriTemplate = "listings/search", Method = "POST")]
        [ServiceKnownType(typeof(List<ListingSummary>))]
        List<ListingSummary> GetListingSummaries(ListingCriteria criteria);

        [WebInvoke(UriTemplate = "listings/pagedsearch", Method = "POST")]
        [ServiceKnownType(typeof(PagedData<ListingSummary>))]
        PagedData<ListingSummary> GetPagedListingSummaries(ListingCriteriaPaged criteria);

        [WebGet(UriTemplate = "listings/{id}")]
        Listing GetListing(string id);

        [WebInvoke(UriTemplate = "listings", Method = "POST")]
        Listing CreateListing(Listing instance);
        
        [WebInvoke(UriTemplate = "listings/{id}", Method = "PUT")]
        Listing UpdateListing(string id, Listing instance);

        [WebInvoke(UriTemplate = "listings/{id}", Method = "DELETE")]
        void DeleteListing(string id);

        [WebGet(UriTemplate = "listings/likenumber/{id}")]
        List<Listing> GetListingsLikeNumber(string id);


        //Lookup Operations
        [OperationContract]
        [WebGet(UriTemplate = "/lookup/divisions")]
        List<Division> GetDivisions();

        //[OperationContract]
        //[WebGet(UriTemplate = "/lookup/categories")]
        //List<Category> GetCategories();

        [OperationContract]
        [WebGet(UriTemplate = "/lookup/publications")]
        List<Publication> GetPublications();

        [OperationContract]
        [WebGet(UriTemplate = "/lookup/sources")]
        List<SourceSystemLookup> GetSourceSystems();

        [OperationContract]
        [WebGet(UriTemplate = "/lookup/statuses")]
        List<ListingStatusLookup> GetListingStatuses();

        [OperationContract]
        [WebGet(UriTemplate = "/lookup/upsellvalues")]
        List<UpsellValueLookup> GetUpsellValues();


        //Batch / Imp/Exp Operations
        [WebGet(UriTemplate = "metadata/updatecategories")]
        OodleImportSummary UpdateCategories();

        [WebGet(UriTemplate = "metadata/updateymm")]
        YearMakeModelImportSummary UpdateYearMakeModels();

        [WebInvoke(UriTemplate = "exportcache", Method = "POST")]
        ExportSummary BatchExport(ExportCriteria criteria);

        [WebInvoke(UriTemplate = "cache/search/{listingNumber}")]
        List<CacheEntry> GetCacheEntries(string listingNumber);

        [WebGet(UriTemplate = "cache/refresh/{listingNumber}")]
        void RefreshCacheForListingNumber(string listingNumber);

        [WebGet(UriTemplate = "cache/{id}")]
        CacheEntry GetCacheEntry(string id);


        //Reports
        [WebInvoke(UriTemplate = "reports/workingsams", Method = "POST")]
        List<SamsResult> GetWorkingSamsReport(SamsCriteria criteria);

        [WebInvoke(UriTemplate = "reports/workingsamsnofeedback", Method = "POST")]
        List<SamsNoFeedbackResult> GetWorkingSamsNoFeedbackReport(SamsNoFeedbackCriteria criteria );

        [WebInvoke(UriTemplate = "reports/qrcodeurl", Method = "POST")]
        List<QRCodeUrlResult> GetQRCodeUrlReport(QRCodeUrlCriteria criteria);

        [WebInvoke(UriTemplate = "reports/workingupsells", Method = "POST")]
        List<UpsellResult> GetWorkingUpsellReport(UpsellCriteria criteria);

        [WebInvoke(UriTemplate = "reports/generallistingpublication", Method = "POST")]
        List<ListingPubResult> GetListingPubReportUsingCriteria(ListingPubCriteria criteria);

        [WebInvoke(UriTemplate = "reports/getgeneralfailedlisting", Method = "POST")]
        List<GeneralFailedListingEvent> GetGeneralFailedListing(GeneralFailedListingEventCriteria criteria);

        [WebInvoke(UriTemplate = "reports/getdetailedupsellreport", Method = "POST")]
        List<DetailedUpsellResult> GetDetailedUpsellReport(DetailedUpsellCriteria critera);

        //Misc
        [OperationContract]
        [WebGet(UriTemplate = "/ping")]
        void Ping();

    }
}