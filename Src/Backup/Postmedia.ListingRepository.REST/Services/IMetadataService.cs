﻿using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.REST.Services
{
    public interface IMetadataService
    {
        OodleImportSummary UpdateCategories();
        YearMakeModelImportSummary UpdateYearMakeModel();
    }
}
