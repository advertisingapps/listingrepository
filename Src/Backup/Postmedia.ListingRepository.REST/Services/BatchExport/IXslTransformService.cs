using System;
using System.Xml;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Services.BatchExport
{
    public interface IXslTransformService
    {
        string ExportDir { get; set; }
        string Transform(XmlDocument doc, ExportTarget target, DateTime timeStamp, Division division);
    }
}