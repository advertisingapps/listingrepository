﻿using AutoMapper;

using log4net;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.REST.Factories;
using Postmedia.ListingRepository.REST.Repositories;

namespace Postmedia.ListingRepository.REST.Services
{
    class AdminModificationsService: IAdminModificationsService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private IAdminModificationsRepository<Entities.AdminModifications> AdminModificationsRepository { get; set; }

        public AdminModificationsService() : this(new AdminModificationsRepository()) { }

        public AdminModificationsService(IAdminModificationsRepository<Entities.AdminModifications> adminModificationsRepository)
        {
            AdminModificationsRepository = adminModificationsRepository;
        }

        public void CheckListingForChanges(Listing currentListing, Listing newListing)
        {
            Log.Debug("(AdminModifications) FindingChanges");

            var adminModFactory = new AdminModificationsFactory();
            var updatedAdMods = adminModFactory.FindChanges(newListing.ListingNumber, currentListing, newListing);

            if (updatedAdMods.Count > 0)
            {
                Log.Debug("(AdminModifications) Admin Change Count: [" + updatedAdMods.Count() + "]");
                Log.Debug("(AdminModifications) Mapping List<DTO.AdminModifications> ---> List<Entities.AdminModifications>");

                var entAdDeletes = Mapper.Map<List<DTO.AdminModifications>,
                    List<Entities.AdminModifications>>(updatedAdMods.FindAll(x => x.ModType == ModType.Delete.ToString())); ;
                var entAdUpdates = Mapper.Map<List<DTO.AdminModifications>,
                    List<Entities.AdminModifications>>(updatedAdMods.FindAll(x => x.ModType == ModType.Update.ToString())); ; ;

                Log.Debug("(AdminModifications) FetchExisting AdminModifications");
                var existEntDelete = AdminModificationsRepository.FetchExisting(entAdDeletes).FindAll(x => x.Id != 0);
                var existEntUpdate = AdminModificationsRepository.FetchExisting(entAdUpdates);

                Log.Debug("(AdminModifications) Delete on List<Entities.AdminModifications>");
                if (existEntDelete.Count > 0) AdminModificationsRepository.Delete(existEntDelete);
                Log.Debug("(AdminModifications) SaveOrUpdate on List<Entities.AdminModifications>");
                if (existEntUpdate.Count > 0) AdminModificationsRepository.SaveOrUpdate(existEntUpdate);
            }
        }

        public Listing ResolveListingWithMasterChanges(Listing currentListing, Listing newListing)
        {

            Log.Debug("(AdminModifications) Retrieving any current Admin Changes");
            var retAdMods = AdminModificationsRepository.FetchAllByNumber(currentListing.ListingNumber);
            
            if (retAdMods.Count() > 0)
            {
                var adminModFactory = new AdminModificationsFactory();
                newListing = adminModFactory.ResolveAdminMasterFields(retAdMods.ToList(), currentListing, newListing);    
            }

            return newListing;
        }
    }
}
