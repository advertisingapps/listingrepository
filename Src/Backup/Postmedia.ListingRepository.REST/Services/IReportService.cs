﻿using System.Collections.Generic;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.REST.Services
{
    public interface IReportService
    {
        // Report methods
        List<SamsResult> GetWorkingSamsReport(SamsCriteria criteria);
        List<SamsNoFeedbackResult> GetWorkingSamsNoFeedbackReport(SamsNoFeedbackCriteria criteria);
        List<QRCodeUrlResult> GetQRCodeUrlReport(QRCodeUrlCriteria criteria);
        List<UpsellResult> GetUpsellReport(UpsellCriteria criteria);
        List<ListingPubResult> GetListingPubReportUsingCriteria(ListingPubCriteria criteria);
        List<GeneralFailedListingEvent> GetGeneralFailedListing(
            GeneralFailedListingEventCriteria criteria);

        // Methods for formatting reports for email
        string BuildCriteriaString(SamsCriteria criteria);
        string BuildCriteriaString(SamsNoFeedbackCriteria criteria);
        string BuildCriteriaString(QRCodeUrlCriteria criteria);
        string BuildCriteriaString(UpsellCriteria criteria);
        string BuildCriteriaString(GeneralFailedListingEventCriteria criteria);

        string FormatReportForHtml(SamsCriteria criteria, List<SamsResult> results);
        string FormatReportForHtml(SamsNoFeedbackCriteria criteria, List<SamsNoFeedbackResult> results);
        string FormatReportForHtml(QRCodeUrlCriteria criteria, List<QRCodeUrlResult> results);
        string FormatReportForHtml(UpsellCriteria criteria, List<UpsellResult> results);
        string FormatReportForHtml(GeneralFailedListingEventCriteria criteria, List<GeneralFailedListingEvent> results);
    }
}