﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.EMail;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.REST.Factories;
using Postmedia.ListingRepository.REST.Repositories;
using Postmedia.ListingRepository.REST.Services.BatchExport;
using log4net;
using CacheEntry = Postmedia.ListingRepository.DTO.CacheEntry;
using Listing = Postmedia.ListingRepository.DTO.Listing;

namespace Postmedia.ListingRepository.REST.Services
{
    public class ListingService : IListingService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IListingRepository<Entities.Listing> ListingRepository { get; set; }
        private IDivisionRepository<Entities.Division> DivisionRepository { get; set; }
        private ICacheEntryRepository<Entities.CacheEntry> OutboundCacheRepository { get; set; }
        private IBatchLogRepository<Entities.BatchLog> BatchLogRepository { get; set; }
        private IXmlBatchExportService XmlBatchExportService { get; set; }
        
        
        public ListingService() : this(new Repositories.ListingRepository(), new DivisionRepository(), new CacheEntryRepository(), new BatchLogRepository(), new XmlBatchExportService()) { }
        public ListingService(IListingRepository<Entities.Listing> listingRepository,
            IDivisionRepository<Entities.Division> divisionRepository,
            ICacheEntryRepository<Entities.CacheEntry> cacheEntryRepository,
            IBatchLogRepository<Entities.BatchLog> batchLogRepository,
            IXmlBatchExportService xmlBatchExportService)
        {
            DivisionRepository = divisionRepository;
            ListingRepository = listingRepository;
            OutboundCacheRepository = cacheEntryRepository;
            BatchLogRepository = batchLogRepository;
            XmlBatchExportService = xmlBatchExportService;
        }
        
        public Listing CreateListing(Listing listing)
        {
            return null;
        }

        public void DeleteListing(string listingNumber)
        {
            Log.Debug("Entering - " + GetType() + "(" + listingNumber + ")");
            ListingRepository.DeleteListing(listingNumber);
            Log.Debug("Exiting " + GetType());
        }
        
        public Listing UpdateListing(string id, Listing listing)
        {
            var listingEvent = new ListingEventFactory().GetListingEvent(listing.ListingNumber, ListingEventType.NONE);

            try
            {
                Log.Debug("Entering - " + GetType());
                Log.Debug("Listing received was: " + Core.Translators.StreamTranslator<Listing>.DtoToXml(listing));
                Log.Debug("Update previously stored Listing using listingID (" + id + ") - UpdateListing");

                var entityListing = ListingRepository.FetchByNumber(id);
                Log.Debug("Retrieved previously stored Listing " + (entityListing == null ? "which was null " : string.Empty) + "- UpdateListing");
                
                var updatedFrom = UpdateFrom.None;
                if (!Enum.TryParse(listing.Updater, true, out updatedFrom)) updatedFrom = UpdateFrom.None;
                
                listingEvent.EventType = (entityListing != null) ? ListingEventType.UPDATE.ToString() : ListingEventType.CREATE.ToString();
                
                if (updatedFrom == UpdateFrom.None)
                {
                    Log.Debug("UpdateFrom Not Specified. Setting to ESB");
                    updatedFrom = UpdateFrom.ESB;
                }

                Log.Debug("UpdateFrom Specified: [" + updatedFrom.ToString() + "]");
                
                if (entityListing != null)
                {
                    Log.Debug("Checking - AdminModifications");
                    var currentListing = Mapper.Map<Entities.Listing, Listing>(entityListing);

                    var adminModService = new AdminModificationsService();

                    switch (updatedFrom)
                    {
                        case UpdateFrom.ESB:
                            listing.HasAdminLock = currentListing.HasAdminLock;
                            listing = adminModService.ResolveListingWithMasterChanges(currentListing, listing);
                            break;

                        case UpdateFrom.ListingRepository:
                            adminModService.CheckListingForChanges(currentListing, listing);
                            break;
                    }
                }

                if (entityListing != null)
                {
                    Log.Debug("Deleting previously stored listing (was not null) - UpdateListing");
                    ListingRepository.DeleteListing(entityListing);
                }

                var factory = new ListingFactory();
                Log.Debug("Creating listing using factory - UpdateListing");
                entityListing = factory.Create(listing, null);

                Log.Debug("Calling Repository.SaveOrUpdate() - UpdateListing");
                ListingRepository.SaveOrUpdate(entityListing);

                Log.Debug("Mapping entity to DTO - UpdateListing");
                var retListing = Mapper.Map<Entities.Listing, Listing>(entityListing);

                //since Updater is not saved on database, set the saved listing's updater
                retListing.Updater = updatedFrom.ToString();

                listingEvent.EventDescription = "[Called From: " + updatedFrom.ToString() + "] [Status: SUCCESS]";
                listingEvent.RowsAffected = 1;

                Log.Debug("Exiting " + GetType());
                return retListing;
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("The following problem has occurred: {0}", ex);
                listingEvent.EventDescription = "[Called From: " + listing.Updater + "] [Status: FAILURE] [Message:" + ex.Message + "]";
                listingEvent.RowsAffected = 0;
                SendGeneralErrorEmail2(ex, GetType().ToString());
                throw;
            }
            finally
            {
                var leService = new ListingEventService();
                leService.LogEvent(listingEvent);
            }

        }

        public Listing GetListingById(int listingId)
        {
            try
            {
                Log.Debug("Entering - " + GetType() + "(" + listingId + ")");
                var modelListing = ListingRepository.FetchById(listingId);

                Log.Debug("Mapping Entities.Listing --> GetListingById");
                var listing = Mapper.Map<Entities.Listing, Listing>(modelListing);

                Log.Debug("Exiting " + GetType());
                return listing;
            }
            catch (AutoMapperMappingException amme)
            {
                Log.Info("Mapping Exception", amme);
                SendGeneralErrorEmail2(amme, GetType().ToString());
                throw;
            }
        }

        public Listing GetListingByNumber(string listingNumber)
        {
            try
            {
                Log.Debug("Entering - " + GetType() + "(" + listingNumber + ")");
                var modelListing = ListingRepository.FetchByNumber(listingNumber);

                Log.Debug("Mapping Entities.Listing --> Listing" + GetType());
                var listing = Mapper.Map<Entities.Listing, Listing>(modelListing);

                Log.Debug("Exiting " + GetType());
                return listing;
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(GetType() + ": The following problem has occurred: {0}", ex);
                SendGeneralErrorEmail2(ex, GetType().ToString());
                throw;
            }
        }
        
        public List<Listing> GetListingsLikeNumber(string listingNumber)
        {
            try
            {
                Log.Debug("Entering - " + GetType() + "("+listingNumber+")");
                var matchListings = ListingRepository.FetchAllByNumber(listingNumber);

                var listings = new List<Listing>();

                if (matchListings.Count() != 0)
                {
                    Log.Debug("Found " + matchListings.Count() + " Listings");
                    foreach (var listing in matchListings)
                    {
                        Log.Debug("ListingId: [" + listing.Id + "] ListingNumber: [" + listing.ListingNumber + "]");
                        listings.Add(Mapper.Map<Entities.Listing, Listing>(listing));
                    }
                }
                else
                {
                    Log.Debug("Found no listings that matched [" + listingNumber + "]");
                }
                

                Log.Debug("Exiting " + GetType());
                return listings; 
            }
            catch (Exception ex)
            {
                Log.ErrorFormat(GetType() + ": The following problem has occurred: {0}", ex);
                SendGeneralErrorEmail2(ex, GetType().ToString());
                throw;
            }
        }


        public List<ListingSummary> GetAllListingSummaries()
        {
            Log.Debug("Entering - GetListingSummaries");
            Log.Debug("Calling NHRepository.GetAllListingsForSummary() ");
            
            var listings = ListingRepository.FetchAll();
           
            var summaries = new List<ListingSummary>();
            foreach (var listing in listings)
            {
                summaries.Add(Mapper.Map<Entities.Listing, ListingSummary>(listing));    
            }

            Log.Debug("Exiting - GetListingSummaries");
            return summaries;
        }
        
        public List<ListingSummary> GetListingSummariesUsingCriteria(ListingCriteria criteria)
        {
            Log.Debug("Entering - GetListingSummariesUsingCriteria" );
            Log.Debug("Calling Repository.GetAllListingsForSummaryUsingCriteria() ");
            
            //get DetachedCriteria from criteria object
            var listings = ListingRepository.GetListingsUsingCriteria(criteria).ToList();

            Log.Debug("Creating List<ListingSummary>");

            var summaries = listings.Select(Mapper.Map<Entities.Listing, ListingSummary>).ToList();
            
            Log.Debug("Exiting - GetListingSummariesUsingCriteria");
            return summaries;
        }

        public PagedData<ListingSummary> GetPagedListingSummariesUsingCriteria(ListingCriteriaPaged criteria)
        {
            Log.Debug("Entering - GetListingSummariesUsingCriteria");
            Log.Debug("Calling Repository.GetAllListingsForSummaryUsingCriteria() ");

            //get DetachedCriteria from criteria object
            var pagedlistings = ListingRepository.GetPagedListingsUsingCriteria(criteria);

            Log.Debug("Creating List<ListingSummary>");
            var pagedData = new PagedData<ListingSummary>
                                {
                                    Page = pagedlistings.Page,
                                    RowCount = pagedlistings.RowCount,
                                    Sort = pagedlistings.Sort,
                                    SortDirection = pagedlistings.SortDirection,
                                    Items =
                                        pagedlistings.Items.Select(Mapper.Map<Entities.Listing, ListingSummary>).ToList()
                                };

            Log.Debug("Exiting - GetListingSummariesUsingCriteria");
            return pagedData;
        }

        public ExportSummary ExportListingBatch(ExportCriteria criteria)
        {
            Log.Debug("Entered - ExportListingBatch");
            Log.Debug("Deriving Date Critieria - ExportListingBatch");
            try
            {
                var target = ((ExportTarget)Enum.Parse(typeof(ExportTarget), criteria.FeedName, true));
                Log.Debug("Target determined to be - " + target + " - ExportListingBatch");
                DeriveDateCriteriaForFilter(criteria);

                Log.Debug("Calling outboundCacheRepository.FetchUsingCriteria() - ExportListingBatch");
                var filteredCacheEntries = OutboundCacheRepository.FetchUsingCriteria(target, criteria).ToList();

                Log.Debug("Calling Export Service to export the XML - ExportListingBatch");
                var summary = XmlBatchExportService.ExportBatch(target, filteredCacheEntries);
                summary.DailyExport = criteria.DailyExport;
                summary.FeedName = criteria.FeedName;

                Log.Debug("Exiting - ExportListingBatch");
                return summary;
            }

            catch (ArgumentException ae)
            {
                SendExportWarningEmail(ae, criteria);
                throw;
            }
            catch (Exception e)
            {
                SendExportWarningEmail(e, criteria);
                throw;
            }
        }

        public List<CacheEntry> GetCacheEntries(string listingNumber)
        {
            Log.Debug("Entered - GetCacheEntries");
                
            Log.Debug("Calling outboundCacheRepository.FetchUsingListingNumber() - GetCacheEntries()");
            var cacheEntities = OutboundCacheRepository.FetchUsingListingNumber(listingNumber).ToList();
            Log.Debug("Mapping entities --> DTO - GetCacheEntries");
            var cacheEntries = Mapper.Map<List<Entities.CacheEntry>, List<CacheEntry> > (cacheEntities);
            Log.Debug("Exiting - GetCacheEntries");
            return cacheEntries;
        }

        public CacheEntry GetCacheEntry(int id)
        {
            Log.Debug("Entered - GetCacheEntries");

            Log.Debug("Calling outboundCacheRepository.FetchUsingListingNumber() - GetCacheEntry()");
            var cacheEntity = OutboundCacheRepository.FetchById(id);
            Log.Debug("Mapping entity --> DTO - GetCacheEntry");
            var cacheEntry = Mapper.Map<Entities.CacheEntry, CacheEntry>(cacheEntity);
            Log.Debug("Exiting - GetCacheEntry");
            return cacheEntry;
        }



        private void DeriveDateCriteriaForFilter(ExportCriteria criteria)
        {
            Log.Debug("Entering - DeriveDateCriteriaForFilter");
            if (criteria != null)
            {
                Log.Debug("Calling BatchLogRepository.FetchAll() Where - DeriveDateCriteriaForFilter");
                //get the last batch runtime for the selected ING
                if (criteria.DailyExport == false)
                {
                    var lastBatchRuntime = (from batch in BatchLogRepository.FetchAll()
                                            where batch.FeedName.ToLower() == criteria.FeedName.ToLower()
                                            select batch).OrderByDescending(x => x.TimeStamp).FirstOrDefault();

                    Log.Debug("Setting the last rundate - ");
                    if (lastBatchRuntime != null)
                        criteria.LastRunDate = lastBatchRuntime.TimeStamp;
                }
                else
                {
                    criteria.LastRunDate = null;
                }
                Log.Debug("Setting the rundate - DeriveDateCriteriaForFilter");
                //ensure datetime in criteria is populated
                criteria.RunDate = criteria.RunDate ?? DateTime.Now;
            }
        }

        private void SendExportWarningEmail(Exception e, ExportCriteria criteria)
        {
            Log.Info("Sending Email");
            const string subject = "ListingService - Error during batch export";
            var body = "There was an error during the batch export process." + Environment.NewLine
                           + "Error was: " + (e != null ? e.Message : String.Empty) + Environment.NewLine
                           + Environment.NewLine
                           + "Host Machine: " + Environment.MachineName + Environment.NewLine
                           + "Criteria posted contained: " + Environment.NewLine
                           + "RunDate was: " + criteria.RunDate + Environment.NewLine
                           + "LastRunDate was: " + criteria.LastRunDate + Environment.NewLine
                           + "FeedName was: " + criteria.FeedName + Environment.NewLine
                           + "DailyExport was: " + criteria.DailyExport + Environment.NewLine 
                           + Environment.NewLine;

            var innerExceptionInfo = "Additional Information" 
                + Environment.NewLine + "=-=-=-=-=-=-=-=-=-=-=-" 
                + Environment.NewLine + ((e != null && e.InnerException != null) ? GetInnerExceptionInfo(e) : "-- None --");

            body += innerExceptionInfo;

            var message = MailService.CreateMessage(
                ConfigurationManager.AppSettings[ServiceSettings.Recipients.ToString()],
                ConfigurationManager.AppSettings[ServiceSettings.EmailAccount.ToString()],
                body,
                GetSubjectLine(subject));

            MailService.SendNotificationEmail(message);
        }

        private void SendGeneralErrorEmail(Exception e, String callingMethod)
        {
            Log.Info("Sending Email");
            const string subject = "ListingService - General Error";
            var body = "The following error has occurred in "+callingMethod+"(): " + e.ToString();

            var message = MailService.CreateMessage(
                ConfigurationManager.AppSettings[ServiceSettings.Recipients.ToString()],
                ConfigurationManager.AppSettings[ServiceSettings.EmailAccount.ToString()],
                body,
                GetSubjectLine(subject));

            MailService.SendNotificationEmail(message);
        }

        private void SendGeneralErrorEmail2(Exception e, String callingMethod)
        {
            Log.Info("Sending Email");
            const string subject = "ListingService - General Error";
            var body = "The following error has occurred in " + callingMethod + "(): " + e.ToString();

            var message = MailService.CreateMessage(
                "alomibao@postmedia.com",
                ConfigurationManager.AppSettings[ServiceSettings.EmailAccount.ToString()],
                body,
                GetSubjectLine(subject));

            MailService.SendNotificationEmail(message);
        }


        // Get the subjectline for the email
        private string GetSubjectLine(string baseSubject)
        {
            var serviceEnviron = ConfigurationManager.AppSettings[ServiceSettings.ServiceEnvironment.ToString()];
            var subjectLine = (!String.IsNullOrEmpty(serviceEnviron) && serviceEnviron != "prod" ?
                                "Environment: " + serviceEnviron + " - " + baseSubject : baseSubject);
            return subjectLine;
        }

        private string GetInnerExceptionInfo(Exception exception)
        {
            var exceptionInfo = string.Empty; 
            if(exception != null)
            {
                if(exception.InnerException != null)
                {
                    exceptionInfo += Environment.NewLine + 
                        "InnerException" + Environment.NewLine +
                        "Message: " + exception.InnerException.Message + Environment.NewLine +
                        "StackTrace: " + exception.InnerException.StackTrace + Environment.NewLine +
                        GetInnerExceptionInfo(exception.InnerException);
                }
            }
            return exceptionInfo;
        }
    }
}