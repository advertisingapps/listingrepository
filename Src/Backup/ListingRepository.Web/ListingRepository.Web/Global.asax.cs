﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.Communication;

namespace Postmedia.ListingRepository.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            
                filters.Add(new HandleErrorAttribute
                                {
                                    ExceptionType = typeof(AutoMapper.AutoMapperMappingException),
                                    View = "AutomapperException",
                                    Order = 4
                                });

                filters.Add(new HandleErrorAttribute
                                {
                                    ExceptionType = typeof (System.Net.WebException),
                                    View = "WebException",
                                    Order = 3
                                });

                filters.Add(new HandleErrorAttribute
                                {
                                    ExceptionType = typeof (Exception),
                                    View = "Error",
                                    Order = 2
                                });
                filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new {controller = "Home", action = "Index", id = UrlParameter.Optional} // Parameter defaults
                );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            
            RESTClient.BaseUrl = ConfigurationManager.AppSettings[WebAppSettings.ServiceEndpoint.ToString()];
        }

    }
}