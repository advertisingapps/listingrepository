﻿using System.Collections.Generic;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Web.ViewModels
{
    public class ListingPublicationSearchViewModel
    {
        public ListingPublicationCriteriaViewModel ListingPublicationCriteria { get; set; }
        public List<ListingPublicationDetailViewModel> ListingPublicationResults { get; set; }

        // lookups
        public List<Division> Divisions { get; set; }
        public List<SourceSystemLookup> Sources { get; set; }
        public List<Publication> Publications { get; set; }
        public List<UpsellValueLookup> UpsellValues { get; set; }
        public List<ListingStatusLookup> ListingPubStatuses { get; set; }
    }
}