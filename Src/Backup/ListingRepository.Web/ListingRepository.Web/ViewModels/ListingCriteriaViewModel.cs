﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Postmedia.ListingRepository.DTO;


namespace Postmedia.ListingRepository.Web.ViewModels
{
    public class ListingCriteriaViewModel
    {

        public ListingCriteria Criteria { get; internal set; }
        
        public ListingCriteriaViewModel()
        {
            Criteria = new ListingCriteria();
        }

        public ListingCriteriaViewModel(ListingCriteria criteria)
        {
            if (criteria == null)
            {
                Criteria = new ListingCriteria();
            }
            else
                Criteria = criteria;
        }

        public string Division
        {
            get
            {
                return Criteria.Division;
            }
            set
            {
                Criteria.Division = value;
            }
        }

        [DisplayName("Listing #")]
        [StringLength(25)]
        public string ListingNumber
        {
            get { return Criteria.ListingNumber; }
            set { Criteria.ListingNumber = value; }
        }

        public string Title
        {
            get { return Criteria.Title; }
            set { Criteria.Title = value; }
        }
        
        [StringLength(25)]
        public string Source
        {
            get { return Criteria.Source; }
            set { Criteria.Source = value; }
        }
        
        public string Account
        {
            get { return Criteria.Account; }
            set { Criteria.Account = value; }
        }

        [DataType(DataType.Date)]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? Date
        {
            get { return Criteria.Date; }
            set { Criteria.Date = value;}
        }
    }

}
