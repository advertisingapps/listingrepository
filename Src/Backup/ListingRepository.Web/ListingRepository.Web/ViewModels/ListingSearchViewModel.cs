﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Web.Mvc;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Web.ViewModels
{
    [DataContract]
    public class ListingSearchViewModel
    {
        public ListingCriteriaViewModel ListingCriteria { get; set; }

        [DataMember]
        public List<ListingSummaryViewModel> Listings { get; set; }

        // lookups
        public List<Division> Divisions { get; set; }
        public List<SourceSystemLookup> Sources { get; set; }

        //paging variables
        public int PageIndex { get; set; }
        public int GridPageIndex
        {
            get
            {
                var retval = ((PageIndex - 1) < 0) ? 0 : (PageIndex - 1);
                return retval;
            }
        }
        public int TotalRows { get; set; }
        public int PageSize { get; set; }
        public string SortColumn { get; set; }
        public string SortDirection { get; set; }

        public string PagingInformationString
        {
            get
            {
                string pageInformationString;

                if (Listings != null && Listings.Count > 0)
                {
                    var minBound = PageIndex > 0 ? PageIndex - 1 : 0;
                    var minRecord = minBound*PageSize + 1;
                    var maxRecord = (minBound*PageSize) + Listings.Count;
                    pageInformationString = "Viewing rows " + minRecord + " to " + maxRecord + " of " + TotalRows;
                }
                else
                {
                    pageInformationString = "No records were returned using the criteria provided";
                }

                return pageInformationString;
            }
        }

    }
}
