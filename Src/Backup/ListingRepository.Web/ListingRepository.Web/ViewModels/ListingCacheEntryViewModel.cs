﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Web.ViewModels
{
    public class ListingCacheEntryViewModel
    {
                // empty constructor (required?)
        public ListingCacheEntryViewModel() 
        {
            CacheEntry = new CacheEntry();
        }

        //Create constructor that accepts a listing publication 
        public ListingCacheEntryViewModel(CacheEntry entry)
        {
            CacheEntry = entry ?? new CacheEntry();
        }

        private CacheEntry CacheEntry { get; set; }

        public string FeedName 
        {
            get { return CacheEntry.FeedName; }
        }
        public string StartDate
        {
            get { return CacheEntry.StartDate.GetValueOrDefault().ToString("yyyy-MM-dd"); } 
        }
        public string ExpireDate
        {
            get { return CacheEntry.ExpireDate.GetValueOrDefault().ToString("yyyy-MM-dd"); } 
        }
        public string LastModifiedDate
        {
            get { return CacheEntry.LastModifiedDate.GetValueOrDefault().ToString("yyyy-MM-dd"); } 
        }
        public string CacheData
        {
            get { return CacheEntry.CacheData.Substring(0, 15) + " ..."; } 
        }

        public int Id
        {
            get { return (int)CacheEntry.Id; }
        }

        public string ListingNumber
        {
            get { return CacheEntry.ListingNumber; }
        }

        public CacheEntry GetCacheEntry()
        {
            return CacheEntry;
        }
    }
}