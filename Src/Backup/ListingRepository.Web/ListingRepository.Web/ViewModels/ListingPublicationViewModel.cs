﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Postmedia.ListingRepository.DTO;
using System.Collections.Generic;


namespace Postmedia.ListingRepository.Web.ViewModels
{
    public class ListingPublicationViewModel
    {
        private const string NOCAT = "--Category Unavailable--";

        // empty constructor (required?)
        public ListingPublicationViewModel() 
        {
            ListingPublication = new ListingPublication();
        }

        //Create constructor that accepts a listing publication 
        public ListingPublicationViewModel(ListingPublication listingPublication)
        {
            ListingPublication = listingPublication ?? new ListingPublication();

            if (listingPublication != null)
            {
                if (listingPublication.Upsells.Any())
                {
                    foreach (var listPubUpsell in ListingPublication.Upsells)
                    {
                        ListingPubUpsellViewModels.Add(new ListingPubUpsellViewModel(listPubUpsell));
                    }
                }
                else
                { 
                    ListingPubUpsellViewModels = new List<ListingPubUpsellViewModel>();
                }
            }
        }


        private ListingPublication ListingPublication { get; set; }

        public long Id
        {
            get { return ListingPublication.Id; }
        }

        //create properties to display dates with your desired format 
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate
        {
            get { return ListingPublication.StartDate; }
            set { ListingPublication.StartDate = value; } 
        }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? ExpireDate
        {
            get { return ListingPublication.ExpireDate; }
            set { ListingPublication.ExpireDate = value; }
        }

        
        public string Status
        {
            get { return ListingPublication.Status; }
            set { ListingPublication.Status = value; }
        }

        public Publication Publication
        {
            get { return ListingPublication.Publication; }
            set { ListingPublication.Publication = value; }
        }

        public List<Upsell> Upsells
        {
            get { return ListingPublication.Upsells; }
            set { ListingPublication.Upsells = value; } // probably don't need a set, as this is display only
        }

        //public Category Category
        //{
        //    get 
        //    {
        //        var cat = new Category()
        //        {
        //            Name = NOCAT
        //        };
        //        if (ListingPublication.Categories.Any())
        //        {
        //            cat =  ListingPublication.Categories[0];
        //        }
        //        return cat;
        //    }
        //    set 
        //    {
        //        ListingPublication.Categories = new List<Category>();
        //        if (value.Name != NOCAT)
        //        { 
        //            ListingPublication.Categories.Add(value); 
        //        }
        //    } // probably don't need a set, as this is display only
        //}

        private List<ListingPubUpsellViewModel> _listingPubUpsellViewModels;
        public List<ListingPubUpsellViewModel> ListingPubUpsellViewModels
        {
            get { return _listingPubUpsellViewModels ?? (_listingPubUpsellViewModels = new List<ListingPubUpsellViewModel>()); }
            set { _listingPubUpsellViewModels = value; }
        }

        //The datamodel was designed for a list of categories, but the current business usage is that there is a 
        //1 to 1 mapping between listing publication and category.  Because this is 1 to 1 currently the UI is 
        //designed to show this at the listing publication level.  If this business need changes in the future
        //then the UI must be modified to show category as a collection.
        private string _displayCategory { get; set; }
        public string DisplayCategory
        {
            get { 
                string retVal = "--Category Unavailable--";

                if ((ListingPublication.Categories != null) && (ListingPublication.Categories.Count > 0))
                {
                    //return the first value only (see note above)
                    _displayCategory = ListingPublication.Categories[0].Name;
                }
                return _displayCategory;
            }
            set {
                _displayCategory = value;
            }
        }

        public ListingPublication GetListingPublicationDTOFromViewModel()
        {
            // code is not currently being used.
            ListingPublication.Upsells = ListingPubUpsellViewModels.Select(upsell => upsell.GetUpsellDTOFromViewModel()).ToList();
            return ListingPublication;
        }


    }
}