﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Web.ViewModels
{
    public class ListingPublicationCriteriaViewModel
    {
        private ListingPubCriteria Criteria { get; set; }

        // Constructors
        public ListingPublicationCriteriaViewModel()
        {
            Criteria = new ListingPubCriteria();
        }
        public ListingPublicationCriteriaViewModel(ListingPubCriteria criteria)
        {
            if (criteria == null)
            {
                Criteria = new ListingPubCriteria();
            }
            else
                Criteria = criteria;
        }

        
        public string Division
        {
            get { return Criteria.Division; }
            set { Criteria.Division = value; }
        }

        [StringLength(20)]
        [DisplayName("Account #")]
        public string AccountNumber 
        {
            get { return Criteria.AccountNumber; }
            set { Criteria.AccountNumber = value; }
        }

        [StringLength(20)]
        [DisplayName("Listing #")]
        public string ListingNumber
        {
            get { return Criteria.ListingNumber; }
            set { Criteria.ListingNumber = value; }
        }

        [StringLength(25)]
        [DisplayName("Listing Source")]
        public string ListingSource
        {
            get { return Criteria.ListingSource; }
            set { Criteria.ListingSource = value; }
        }

        [StringLength(25)]
        [DisplayName("Caption")]
        public string ListingCaption
        {
            get { return Criteria.ListingCaption; }
            set { Criteria.ListingCaption = value; }
        }

        [StringLength(25)]
        [DisplayName("Ad Text")]
        public string ListingText
        {
            get { return Criteria.ListingText; }
            set { Criteria.ListingText = value; }
        }

        [StringLength(20)]
        [DisplayName("Ad Order #")]
        public string AdOrderNumber
        {
            get { return Criteria.AdOrderNumber; }
            set { Criteria.AdOrderNumber = value; }
        }

        [DataType(DataType.Date)]
        [DisplayName("Publication Date")]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? ListingPubDate
        {
            get { return Criteria.ListingPubDate; }
            set { Criteria.ListingPubDate = value; }
        }


        [DataType(DataType.Date)]
        [DisplayName("Start Date")]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? ListingPubStartDate
        {
            get { return Criteria.ListingPubStartDate; }
            set { Criteria.ListingPubStartDate = value; }
        }

        [DataType(DataType.Date)]
        [DisplayName("Expire Date")]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? ListingPubExpireDate
        {
            get { return Criteria.ListingPubExpireDate; }
            set { Criteria.ListingPubExpireDate = value; }
        }

        [StringLength(20)]
        [DisplayName("Status")]
        public string ListingPubStatus
        {
            get { return Criteria.ListingPubStatus; }
            set { Criteria.ListingPubStatus = value; }
        }

        [StringLength(20)]
        [DisplayName("Category")]
        public string ListingPubCategory
        {
            get { return Criteria.ListingPubCategory; }
            set { Criteria.ListingPubCategory = value; }
        }

        //[StringLength(20)]
        [DisplayName("Publication")]
        public string PublicationName
        {
            get { return Criteria.PublicationName; }
            set { Criteria.PublicationName = value; }
        }

        [StringLength(15)]
        [DisplayName("Feed Name")]
        public string FeedName
        {
            get { return Criteria.FeedName; }
            set { Criteria.FeedName = value; }
        }

        [StringLength(100)]
        [DisplayName("Upsell Name")]
        public string UpsellName
        {
            get { return Criteria.UpsellName; }
            set { Criteria.UpsellName = value; }
        }

        [StringLength(100)]
        [DisplayName("Upsell Code")]
        public string UpsellCode
        {
            get { return Criteria.UpsellCode; }
            set { Criteria.UpsellCode = value; }
        }

        [StringLength(20)]
        [DisplayName("Property Code")]
        public string PropertyCode
        {
            get { return Criteria.PropertyCode; }
            set { Criteria.PropertyCode = value; }
        }

        [StringLength(50)]
        [DisplayName("Metadata")]
        public string PropertyValue
        {
            get { return Criteria.PropertyValue; }
            set { Criteria.PropertyValue = value; }
        }

        [DisplayName("Email Results")]
        public bool EmailResults
        {
            get { return Criteria.EmailResults; }
            set { Criteria.EmailResults = value; }
        }

        [DisplayName("Email Address")]
        public string EmailAddress
        {
            get { return Criteria.EmailAddress; }
            set { Criteria.EmailAddress = value; }
        }

        public ListingPubCriteria getDTOForReport()
        {
            return Criteria;
        }
    }
}