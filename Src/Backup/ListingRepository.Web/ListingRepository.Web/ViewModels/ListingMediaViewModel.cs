﻿using System;
using System.ComponentModel.DataAnnotations;
using Postmedia.ListingRepository.DTO;
using System.Collections.Generic;

namespace Postmedia.ListingRepository.Web.ViewModels
{
    public class ListingMediaViewModel
    {
        private string _source = string.Empty;

        // empty constructor (required?)
        public ListingMediaViewModel() 
        {
            ListingMedia = new Media();
        }

        //Create constructor that accepts a listing publication 
        public ListingMediaViewModel(Media listingMedia, string listingSource)
        {
            _source = listingSource;

            if (listingMedia == null)
            {
                ListingMedia = new Media(); // TODO: should never happen.  Should I have this or should I somehow error?
            }
            else
                ListingMedia = listingMedia;
        }

        private Media ListingMedia { get; set; }

        // display properties
        public long Id {
            get { return ListingMedia.Id; }
        }

        public string Url {
            get { return ListingMedia.Url; } 
        }
        public string MimeType {
            get { return ListingMedia.MimeType; } 
        }
        public string Role {
            get { return ListingMedia.Role; } 
        }

        // used for the display link, since the Url is not correct
        public string FormattedUrl 
        { 
            get {
                string retVal = "";

                if (!string.IsNullOrEmpty(ListingMedia.Url))
                {
                    retVal = ListingMedia.Url;
                }
                return retVal;
            } 
        }

        public bool DisplayPreview
        {
            get {
                bool retVal = false;

                // preview wil be based on the listing source
                if (_source == "SAMS" || _source == "Travidia" || (_source == "NationalGenera" && (Role == "displayUrl" || Role == "thumbnailUrl")))
                {
                    retVal = true;
                }
                return retVal;
            }
        }
    }
}