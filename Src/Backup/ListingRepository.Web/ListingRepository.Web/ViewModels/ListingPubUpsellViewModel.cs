﻿using System;
using System.ComponentModel.DataAnnotations;
using Postmedia.ListingRepository.DTO;
using System.Collections.Generic;

namespace Postmedia.ListingRepository.Web.ViewModels
{
    public class ListingPubUpsellViewModel
    {
        // empty constructor (required?)
        public ListingPubUpsellViewModel() 
        {
            ListingPubUpsell = new Upsell();
        }

        //Create constructor that accepts a listing publication 
        public ListingPubUpsellViewModel(Upsell listingPubUpsell)
        {
            if (listingPubUpsell == null)
            {
                ListingPubUpsell = new Upsell(); 
            }
            else
                ListingPubUpsell = listingPubUpsell;
        }

        private Upsell ListingPubUpsell { get; set; }

        // display properties
        public long Id {
            get { return ListingPubUpsell.Id; }
        }

        public string Code {
            get { return ListingPubUpsell.Code; } 
        }
        public string Name {
            get { return ListingPubUpsell.Name; } 
        }

        public Upsell GetUpsellDTOFromViewModel()
        {
            return ListingPubUpsell;
        }
    }
}