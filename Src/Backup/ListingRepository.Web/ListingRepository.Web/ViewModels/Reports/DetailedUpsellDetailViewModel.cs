﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Postmedia.ListingRepository.DTO.Reports;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports
{
    public class DetailedUpsellDetailViewModel
    {
        private DetailedUpsellResult UpsellReport;

        public DetailedUpsellDetailViewModel()
        {
            UpsellReport = new DetailedUpsellResult();
        }

        public DetailedUpsellDetailViewModel(DetailedUpsellResult rptResult)
        {
            UpsellReport = rptResult ?? new DetailedUpsellResult();
        }

        //[DisplayName("Listing Id")]
        [DataType(DataType.Text)]
        public long ListingId
        {
            get { return UpsellReport.ListingId; }
        }

        [DisplayName("Source")]
        [DataType(DataType.Text)]
        public string Source
        {
            get { return UpsellReport.Source; }
        }

        [DisplayName("Division Name")]
        [DataType(DataType.Text)]
        public string DivisionName
        {
            get { return UpsellReport.DivisionName; }
        }

        [DisplayName("Listing Number")]
        [DataType(DataType.Text)]
        public string ListingNumber
        {
            get { return UpsellReport.ListingNumber; }
        }

        [DisplayName("Publication Name")]
        [DataType(DataType.Text)]
        public string PublicationName
        {
            get { return UpsellReport.PublicationName; }
        }

        [DisplayName("Title")]
        [DataType(DataType.Text)]
        public string Title
        {
            get { return UpsellReport.Title; }
        }

        [DisplayName("Upsell Name")]
        [DataType(DataType.Text)]
        public String UpsellName
        {
            get { return UpsellReport.UpsellName; }
        }

        [DisplayName("Status")]
        [DataType(DataType.Text)]
        public string Status
        {
            get { return UpsellReport.Status; }
        }

        [DisplayName("Start Date")]
        [DataType(DataType.Text)]
        public string StartDate
        {
            get { return UpsellReport.StartDate != null ? UpsellReport.StartDate.Value.ToString("yyyy/MM/dd") : ""; }
        }

        [DisplayName("Expire Date")]
        [DataType(DataType.Text)]
        public string ExpireDate
        {
            get { return UpsellReport.ExpireDate != null ? UpsellReport.ExpireDate.Value.ToString("yyyy/MM/dd") : ""; }
        }

        [DisplayName("Publishing Days")]
        [DataType(DataType.Custom)]
        public long PublishingDays
        {
            //get { return UpsellReport.ConsecutiveRunDays.ToString(CultureInfo.InvariantCulture); }
            get { return UpsellReport.PublishingDays; }
        }
    }
}