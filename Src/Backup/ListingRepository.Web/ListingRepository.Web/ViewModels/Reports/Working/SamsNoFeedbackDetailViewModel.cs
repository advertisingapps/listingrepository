﻿using System.ComponentModel;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class SamsNoFeedbackDetailViewModel
    {
        // empty constructor (required?)
        public SamsNoFeedbackDetailViewModel() 
        {
            SamsNoFeedbackReport = new SamsNoFeedbackResult();
        }

        //Create constructor that accepts a SAMSResult DTO object
        public SamsNoFeedbackDetailViewModel(SamsNoFeedbackResult rptResult)
        {
            if (rptResult == null)
            {
                SamsNoFeedbackReport = new SamsNoFeedbackResult();
            }
            else
                SamsNoFeedbackReport = rptResult;
        }

        private SamsNoFeedbackResult SamsNoFeedbackReport { get; set; }

        public string Division { 
            get { return SamsNoFeedbackReport.Division; }
        }

        [DisplayName("Listing Number")]
        public string ListingNumber
        {
            get { return SamsNoFeedbackReport.ListingNumber; }
        }

        [DisplayName("Account")]
        public string Advertiser
        {
            get { return SamsNoFeedbackReport.AdvertiserName; }
        }

        public string Title
        {
            get { return SamsNoFeedbackReport.Title; }         
        }

        public string Category {
            get { return SamsNoFeedbackReport.Category; }
        }

        public string StartDate
        {
            get { return SamsNoFeedbackReport.StartDate.ToString("yyyy/MM/dd"); }
        }

        public string ExpireDate
        {
            get { return SamsNoFeedbackReport.ExpireDate.ToString("yyyy/MM/dd"); }
        }

        [DisplayName("Ad Length")]
        public int AdLength
        {
            get { return SamsNoFeedbackReport.AdLength; }
        }

        public string AdLengthStyle
        {
            get 
            { 
                string retVal = "numeric";
                if (SamsNoFeedbackReport.AdLength == 0)
                {
                    retVal += " redtext";
                }
                return retVal;
            }
        }

        public string FeedName
        {
            get { return SamsNoFeedbackReport.FeedName; }
        }

        public string FeedNameStyle
        {
            get
            {
                string retVal = "";
                if (SamsNoFeedbackReport.FeedName == "N/A")
                {
                    retVal = "redtext";
                }
                return retVal;
            }
        }

        public string SamsAdNumber
        {
            get { return SamsNoFeedbackReport.AdOrderNumber; }
        }
    }
}