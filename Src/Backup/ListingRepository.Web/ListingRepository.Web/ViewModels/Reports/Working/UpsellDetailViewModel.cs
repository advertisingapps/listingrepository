﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class UpsellDetailViewModel
    {
        // empty constructor (required?)
        public UpsellDetailViewModel() 
        {
            UpsellReport = new UpsellResult();
        }

        //Create constructor that accepts a SAMSResult DTO object
        public UpsellDetailViewModel(UpsellResult rptResult)
        {
            if (rptResult == null)
            {
                UpsellReport = new UpsellResult(); // TODO: should never happen.  Should I have this or should I somehow error?
            }
            else
                UpsellReport = rptResult;
        }

        private UpsellResult UpsellReport { get; set; }


        public string Division
        {
            get { return UpsellReport.Division; }
        }

        public List<UpsellCount> UpsellCounts 
        {
            get { return UpsellReport.UpsellCounts; }
        }

        public int DivUpsellTotal
        {
            get {
                int retVal = 0;
                foreach (var item in UpsellCounts)
                {
                    retVal += item.Value;
                }
                return retVal;
            }
        }
    }
}