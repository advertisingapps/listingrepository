﻿using System.Collections.Generic;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class SAMSSearchViewModel
    {
        public SAMSCriteriaViewModel SamsCriteria { get; set; }
        public List<SamsDetailViewModel> SamsResults { get; set; }

        // lookups
        public List<Division> Divisions { get; set; }
    }
}