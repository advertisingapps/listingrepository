﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports.Working;


namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class UpsellSearchViewModel
    {
        public UpsellCriteriaViewModel UpsellCriteria { get; set; }
        public List<UpsellDetailViewModel> UpsellResults { get; set; }

        // lookups
        public List<Division> Divisions { get; set; }

        // 
        public UpsellDetailViewModel GetTotalRow()
        {
            // create a new upsell result
            var UpsellTotalRow = new UpsellResult();

            // insert data
            UpsellTotalRow.Division = "Total";

            // need to add other data
            //foreach (var item in UpsellResults)
            //{
            //   item. 
            //}

            // create new upselldetailviewmodel using this result
            var UpsellTotalRowViewModel = new UpsellDetailViewModel(UpsellTotalRow);
            return UpsellTotalRowViewModel;
        }
    }
}