﻿using System;
using System.ComponentModel;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class QRCodeUrlDetailViewModel
    {
        // empty constructor (required?)
        public QRCodeUrlDetailViewModel() 
        {
            QRCodeUrlReportDetail = new QRCodeUrlResult();
        }

        //Create constructor that accepts a DigitalDisplayResult DTO object
        public QRCodeUrlDetailViewModel(QRCodeUrlResult rptResult)
        {
            if (rptResult == null)
            {
                QRCodeUrlReportDetail = new QRCodeUrlResult(); 
            }
            else
                QRCodeUrlReportDetail = rptResult;
        }

        private QRCodeUrlResult QRCodeUrlReportDetail { get; set; }

        [DisplayName("Division")]
        public string DivisionName 
        { 
            get { return QRCodeUrlReportDetail.Division; }
        }

        [DisplayName("Publication")]
        public string PublicationName
        {
            get { return QRCodeUrlReportDetail.Publication; }
        }

        [DisplayName("Listing Number")]
        public string ListingNumber
        {
            get { return QRCodeUrlReportDetail.ListingNumber; }
        }

        [DisplayName("Account #")]
        public string Account
        {
            get { return QRCodeUrlReportDetail.Account; }
        }

        public string Advertiser
        {
            get { return QRCodeUrlReportDetail.AdvertiserName; }
        }

        public string Title
        {
            get {
                string retval = string.Empty;

                //title is not null
                if (!string.IsNullOrEmpty(QRCodeUrlReportDetail.Title))
                {
                    if (QRCodeUrlReportDetail.Title.Length > 50)
                    {
                        retval = QRCodeUrlReportDetail.Title.Substring(0, 50) + "...";
                    }
                    else
                    {
                        retval = QRCodeUrlReportDetail.Title;
                    }
                }
                return retval;
            }         
        }

        public string Category {
            get { return QRCodeUrlReportDetail.Category; }
        }

        public string StartDate
        {
            get { return QRCodeUrlReportDetail.StartDate.ToString("yyyy/MM/dd"); }
        }

        public string ExpireDate
        {
            get { return QRCodeUrlReportDetail.ExpireDate.ToString("yyyy/MM/dd"); }
        }

        [DisplayName("Ad Length")]
        public int AdLength
        {
            get { return QRCodeUrlReportDetail.AdLength; }
        }

        [DisplayName("Display URL")]
        public string DisplayURL
        {
            get
            {
                return getDisplayURL;
            }
        }

        public string AdLengthStyle
        {
            get
            {
                string retVal = "numeric";
                if (QRCodeUrlReportDetail.AdLength == 0)
                {
                    retVal += " redtext";
                }
                return retVal;
            }
        }

        public string DisplayURLStyle
        {
            get
            {
                string retval = string.Empty;
                if (getDisplayURL == "N/A")
                {
                    retval = "redtext";
                }
                return retval;
            }
        }

        private string getDisplayURL
        {
            get
            {
                string retval = "N/A";
                if ((QRCodeUrlReportDetail.MediaItems != null) && (QRCodeUrlReportDetail.MediaItems.Count > 0))
                {
                    foreach (var item in QRCodeUrlReportDetail.MediaItems)
                    {
                        if ((item.Role == "displayUrl") && (!String.IsNullOrEmpty(item.Url)))
                        {
                            retval = item.Url;
                        }
                    }
                }
                return retval;
            }
        }
    }
}