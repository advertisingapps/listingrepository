﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class UpsellCriteriaViewModel
    {
        private UpsellCriteria Criteria { get; set; }

        // empty constructor
        public UpsellCriteriaViewModel()
        {
            Criteria = new UpsellCriteria();
        }

        // constructor that accepts ReportCriteria
        public UpsellCriteriaViewModel(UpsellCriteria criteria)
        {
            if (criteria == null)
            {
                Criteria = new UpsellCriteria();
            }
            else
                Criteria = criteria;
        }
        
        public string Division
        {
            get
            {
                return Criteria.Division;
            }
            set
            {
                Criteria.Division = value;
            }
        }

        [DataType(DataType.Date)]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate
        {
            get { return Criteria.StartDate; }
            set { Criteria.StartDate = value; }
        }

        [DataType(DataType.Date)]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? EndDate
        {
            get { return Criteria.EndDate; }
            set { Criteria.EndDate = value; }
        }

        [DisplayName("Email Results")]
        public bool EmailResults
        {
            get { return Criteria.EmailResults; }
            set { Criteria.EmailResults = value; }
        }

        [DisplayName("Email Address")]
        public string EmailAddress
        {
            get { return Criteria.EmailAddress; }
            set { Criteria.EmailAddress = value; }
        }

        public UpsellCriteria getDTOForReport()
        {
            return Criteria;
        }
    }
}