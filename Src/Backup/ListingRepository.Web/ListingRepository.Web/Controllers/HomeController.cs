﻿using System;
using System.Configuration;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.Web.ViewModels;
using Postmedia.ListingRepository.Core.Communication;


namespace Postmedia.ListingRepository.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: /ReportList/GeneralListingPublication/
        public ActionResult Index(ListingPublicationSearchViewModel lpvm)
        {
            // populate page size based of configuration
            int pageSize;
            ViewData["GetPageSize"] = int.TryParse(ConfigurationManager.AppSettings[WebAppSettings.PageSize.ToString()], out pageSize) ? pageSize : 25;

            // populate the lookups
            var divisions = RESTClient.Instance.Call<List<Division>>(RESTMethod.GetDivisions);
            lpvm.Divisions = divisions;

            var sources = RESTClient.Instance.Call<List<SourceSystemLookup>>(RESTMethod.GetSourceSystems);
            lpvm.Sources = sources;

            var publications = RESTClient.Instance.Call<List<Publication>>(RESTMethod.GetPublications);
            lpvm.Publications = publications;

            var upsellvalues = RESTClient.Instance.Call<List<UpsellValueLookup>>(RESTMethod.GetUpsellValues);
            lpvm.UpsellValues = upsellvalues;

            var statuses = RESTClient.Instance.Call<List<ListingStatusLookup>>(RESTMethod.GetListingStatuses);
            lpvm.ListingPubStatuses = statuses;

            // check if the view model is null (first time hitting page)
            if (lpvm.ListingPublicationCriteria == null)
            {
                // if true then create a new viewmodel
                lpvm.ListingPublicationCriteria = new ListingPublicationCriteriaViewModel(null);

                // set up some default summaries values
                lpvm.ListingPublicationCriteria.ListingPubDate = DateTime.Today;
                lpvm.ListingPublicationCriteria.ListingPubStatus = "ready"; //todo: this should be an enumeration
            }

            if (ModelState.IsValid)
            {
                var results = RESTClient.Instance.Call<ListingPubCriteria, List<ListingPubResult>>(lpvm.ListingPublicationCriteria.getDTOForReport(), RESTMethod.GetListingPubReportUsingCriteria);
                lpvm.ListingPublicationResults = new List<ListingPublicationDetailViewModel>();
                foreach (var listingpubdetail in results)
                {
                    lpvm.ListingPublicationResults.Add(new ListingPublicationDetailViewModel(listingpubdetail));
                }
            }

            return View(lpvm);
        }

        public ActionResult ViewDetail(string id)
        {
            int convertedId;
            var method = int.TryParse(id, out convertedId) ? RESTMethod.GetListingById : RESTMethod.GetListingByNumber;

            var retrievedListing = RESTClient.Instance.Call<Listing>(id, method );
            var ldvm = new ListingDetailViewModel(retrievedListing);
            
            GetListingDetailViewModelLookups(ldvm);

            return View(ldvm);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ViewDetail(ListingDetailViewModel listingDetailViewModel)
        {
            // perform metadata uniqueness validation
            ValidatePropertiesAreUnique(listingDetailViewModel.Properties);

            //validate state
            if (ModelState.IsValid)
            {
                try
                {
                    //if ok ... continue with save and return persisted viewmodel
                    var listing = listingDetailViewModel.GetDTOForSave();
                    listing = RESTClient.Instance.Call<Listing, Listing>(listing, listing.ListingNumber,
                                                                          RESTMethod.UpdateListing);
                    listingDetailViewModel = new ListingDetailViewModel(listing);
                    ViewData["SaveResult"] = "BOOYA!";
                }
                catch (Exception e)
                {
                    ViewData["SaveResult"] = "Negatory Fool";
                    //handle error stuff in here ... or something
                    ModelState.AddModelError("SavingError", e.Message);
                    throw;
                }
            }
            else
            {
                foreach (string key in ModelState.Keys)
                {
                    for (int i = 0; i < ModelState[key].Errors.Count; i++)
                    {
                        ModelError err = ModelState[key].Errors[i];
                    }
                }            
            }

            //ViewData["SaveResult"] = !string.IsNullOrEmpty(ViewData["SaveResult"].ToString()) ?? string.Empty;

            // a save will assign a new id, so return view to indicate result
            return RedirectToAction("ViewDetail", new {id = listingDetailViewModel.Id});
        }


        public PartialViewResult AddMetadata()
        {
            return PartialView("_MetadataPartial", new Property()); // string viewname, object model
        }

        public ActionResult ViewXml(int id )
        {
            var cacheEntry = RESTClient.Instance.Call<CacheEntry>(id, RESTMethod.GetCacheEntry);
            var cacheXmlDataViewModel = new CacheXmlDataViewModel
                                            {
                                                CacheXml = cacheEntry.CacheData,
                                                ListingNumber = cacheEntry.ListingNumber
                                            };

            return View(cacheXmlDataViewModel);
        }


        private ListingCriteriaPaged CreatePagedCriteria(ListingSearchViewModel lsvm, string sort, string sortDir)
        {
            var pagedCriteria = new ListingCriteriaPaged
            {
                Division = lsvm.ListingCriteria.Division,
                ListingNumber = lsvm.ListingCriteria.ListingNumber,
                Account = lsvm.ListingCriteria.Account,
                Ascending = sortDir == "ASC",
                Date = lsvm.ListingCriteria.Date,
                Source = lsvm.ListingCriteria.Source,
                Title = lsvm.ListingCriteria.Title,
                Sort = sort,
                Page = lsvm.PageIndex,
                PageSize = lsvm.PageSize
            };

            return pagedCriteria;
        }

        private void GetCachedDropDowns(ListingSearchViewModel lsvm)
        {
            if (HttpRuntime.Cache != null)
            {
                if (HttpRuntime.Cache["Divisions"] == null)
                {
                    lsvm.Divisions = RESTClient.Instance.Call<List<Division>>(RESTMethod.GetDivisions);
                    HttpRuntime.Cache.Insert("Divisions", lsvm.Divisions, null, DateTime.Now.AddMinutes(30),
                                             Cache.NoSlidingExpiration);
                }
                else
                {
                    lsvm.Divisions = (List<Division>) HttpRuntime.Cache["Divisions"];
                }
                if (HttpRuntime.Cache["Sources"] == null)
                {
                    lsvm.Sources = RESTClient.Instance.Call<List<SourceSystemLookup>>(RESTMethod.GetSourceSystems);
                    HttpRuntime.Cache.Insert("Sources", lsvm.Sources, null, DateTime.Now.AddMinutes(30),
                                             Cache.NoSlidingExpiration);
                }
                else
                {
                    lsvm.Sources = (List<SourceSystemLookup>) HttpRuntime.Cache["Sources"];
                }
            }
        }
        
        private void GetListingDetailViewModelLookups(ListingDetailViewModel listingDetailViewModel)
        {
            // get the lookup information
            var publications = RESTClient.Instance.Call<List<Publication>>(RESTMethod.GetPublications);
            ViewData["PublicationLookups"] = publications;
            Session["PublicationLookups"] = publications;

            var statuses = RESTClient.Instance.Call<List<ListingStatusLookup>>(RESTMethod.GetListingStatuses);
            ViewData["StatusLookups"] = statuses;
            Session["StatusLookups"] = statuses;

            // populate lookup variables
            listingDetailViewModel.Publications = publications;
            listingDetailViewModel.Statuses = statuses;
        }

        private void ValidatePropertiesAreUnique(List<Property> properties)
        {
            //check for null
            if (properties != null)
            {
                // get a list of properties grouped by code with a count > 1
                var dupPropertyCodes = properties.GroupBy(i => i.Code)
                    .Where(g => g.Count() > 1)
                    .Select(g => g.Key);

                foreach (var d in dupPropertyCodes)
                    ModelState.AddModelError("ValidationError", "Listing Metadata item (" + d + ") is not unique");
            }
        }

        private List<ListingSummary> ListingSearch(ListingCriteria lc)
        {
            // perform the search.  This will return the specified list of ListingSummary objects.
            List<ListingSummary> result = null;

            result = RESTClient.Instance.Call<ListingCriteria, List<ListingSummary>>
                (lc, RESTMethod.GetListingSummariesUsingCriteria);

            return result;
        }

        private Enumerations.SortDirection GetSortDirection(string sortDirection)
        {
            if (sortDirection != null)
            {
                if (sortDirection.Equals("DESC", StringComparison.OrdinalIgnoreCase) ||
                    sortDirection.Equals("DESCENDING", StringComparison.OrdinalIgnoreCase))
                {
                    return Enumerations.SortDirection.Descending;
                }
            }
            return Enumerations.SortDirection.Ascending;
        }

        private void IdentifySortColumnAndDirection(ListingSearchViewModel lsvm, string sort)
        {
            if (lsvm.SortColumn == sort)
                lsvm.SortDirection = lsvm.SortDirection == "ASC" ? "DESC" : "ASC";
            else
                lsvm.SortDirection = "ASC";

            lsvm.SortColumn = sort;
        }

    }
}
