﻿using System.Web.Mvc;

namespace Postmedia.ListingRepository.Web.Controllers
{
    public class ErrorController : Controller
    {
        // (optional) Redirect to home when /Error is navigated to directly  
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        public ActionResult NotFound()
        {
            ViewData["Title"] = "The page you requested was not found";
            return View();
        }
    }
}
