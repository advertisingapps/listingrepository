﻿using System.Configuration;
using NUnit.Framework;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.EMail;

namespace Postmedia.ListingRepository.Tests.Unit.Core
{
    [TestFixture]
    public class EmailServiceTests
    {
        private string recipients;
        private string recipientsFromConfig;
        private string singleRecipient;
        private string account;
        private string accountFromConfig;
        
        [SetUp]
        public void Setup()
        {
            recipients = "alevine@obsglobal.com, aalevine@postmedia.com, aaronlevine@gmail.com";
            recipientsFromConfig = ConfigurationManager.AppSettings[ServiceSettings.Recipients.ToString()];
            singleRecipient = "alevine@obsglobal.com";
            account = "aalevine@postmedia.com";
            accountFromConfig = ConfigurationManager.AppSettings[ServiceSettings.EmailAccount.ToString()];

        }

        [Test]
        public void EmailService_can_send_an_email_when_server_name_is_provided()
        {
            var message = new System.Net.Mail.MailMessage("aalevine@postmedia.com",
                                                          "aalevine@postmedia.com")
                              {
                                  Subject = "EmailService_can_send_an_email_when_server_name_is_provided"
                              };
            MailService.SendNotificationEmail(message);
        }

        [Test]
        public void EmailService_can_create_a_MailMessage_using_provided_string_values()
        {
            var message = MailService.CreateMessage(singleRecipient, account, "", "");
            Assert.That(message, Is.Not.Null);
        }

        [Test]
        public void EmailService_can_create_a_MailMessage_using_app_config_values()
        {
           
            var message = MailService.CreateMessage(recipientsFromConfig, accountFromConfig, "", "");
            var firstAddy = recipientsFromConfig.Split(',')[0];

            Assert.That(message, Is.Not.Null);
            Assert.That(message.To[0].Address, Is.Not.Null.And.Not.Empty);
            Assert.That(message.From.Address, Is.Not.Null.And.Not.Empty);
        }

        [Test]
        public void EmailService_can_create_a_MailMessage_with_multiple_CC_recipients()
        {
            var message = MailService.CreateMessage(recipients, account, "", "");

            Assert.That(message, Is.Not.Null);
            Assert.That(message.To, Is.Not.Null.And.Not.Empty);
            Assert.That(message.CC.Count, Is.EqualTo(recipients.Split(',').Length -1));
            
        }

        [Test]
        public void EmailService_can_create_a_MailMessage_with_multiple_CC_recipients_using_app_config_values()
        {
            var message = MailService.CreateMessage(recipientsFromConfig, account, "", "");

            Assert.That(message, Is.Not.Null);
            Assert.That(message.To, Is.Not.Null.And.Not.Empty);
            Assert.That(message.CC.Count, Is.EqualTo(recipientsFromConfig.Split(',').Length - 1));

        }
    }
}
