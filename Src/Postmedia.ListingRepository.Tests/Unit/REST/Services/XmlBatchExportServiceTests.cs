﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.FileIO;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Repositories;
using Postmedia.ListingRepository.REST.Services.BatchExport;
using Rhino.Mocks;

namespace Postmedia.ListingRepository.Tests.Unit.REST.Services
{
    [TestFixture]
    public class XmlBatchExportServiceTests
    {
        private List<CacheEntry> _cacheEntries;
        private const string TestingOutputLocation = @"C:\Testing\Export\";
        private const string TestingOutputFileLocation = @"C:\Testing\Export\TestOutput.xml";
            
        [TestFixtureSetUp]
        public void Setup()
            {
                _cacheEntries = new List<CacheEntry>
                                   {
                                       new CacheEntry
                                           {
                                               Id = 4,
                                               CacheData = @"<AD><UniqueId>VAN365497</UniqueId><Type /><PostDate>01/07/2012</PostDate><Email>noreply@postmedia.com</Email><FirstName1 /><LastName1 /><Announcement>
				Garcia &amp; Sanderson&lt;br /&gt;Philip &amp; Marsha Sanderson and Patricia &amp; Jose Garcia are happy &lt;br /&gt;to announce the marriage of their children, Angela &amp; Jeffery.&lt;br /&gt;CONGRATULATIONS!  

			</Announcement></AD>",
                                               StartDate = DateTime.Parse("2012-01-07 00:00:00.000"),
                                               ExpireDate = DateTime.Parse("2012-01-07 00:00:00.000"),
                                               FeedName = "Celebrating",
                                               ListingNumber = "VAN365497",
                                               LastModifiedDate = DateTime.Parse("2012-01-11 11:01:16.523")
                                           }
                                   };
            File.Create(TestingOutputFileLocation);
            }

        [Test]
        public void XmlBatchExportService_returns_summary_with_zero_summary_count_when_filteredItems_is_null()
        {
            //Arrange
            var stubXslTransformService = MockRepository.GenerateStub<IXslTransformService>();
            var stubBatchLogRepository = MockRepository.GenerateStub<IBatchLogRepository<BatchLog>>();
            var stubDivisionRepository = MockRepository.GenerateStub<IDivisionRepository<Division>>();
            var stubFileManager = MockRepository.GenerateStub<IFileManager>();
          
            //Act
            var service = new XmlBatchExportService(stubXslTransformService, stubBatchLogRepository, stubDivisionRepository, stubFileManager);
            var exportSummary = service.ExportBatch(ExportTarget.Celebrating, null);

            //Assert
            Assert.That(exportSummary.SummaryCount == 0);
        }











        [Test]
        public void XmlBatchExportService_returns_an_export_summary_when_filteredItems_is_not_null()
        {
            //Arrange
            var stubXslTransformService = MockRepository.GenerateStub<IXslTransformService>();
            var stubBatchLogRepository = MockRepository.GenerateStub<IBatchLogRepository<BatchLog>>();
            var stubDivisionRepository = MockRepository.GenerateStub<IDivisionRepository<Division>>();
            var stubFileManager = MockRepository.GenerateStub<IFileManager>();
            stubXslTransformService.ExportDir = TestingOutputLocation;
            stubXslTransformService.Stub(x => x.Transform(null, ExportTarget.Celebrating, DateTime.Now, null)).IgnoreArguments().Return(TestingOutputFileLocation);
            stubBatchLogRepository.Stub(x => x.FetchAll()).Return(new List<BatchLog>());
            stubBatchLogRepository.Stub(x => x.SaveOrUpdate(null)).IgnoreArguments();
            stubDivisionRepository.Stub(x => x.FetchByCode("VAN")).Return(new Division { Name = "Vancouver", Code = "VAN" });
            stubFileManager.Stub(x => x.CopyFiles(null, null)).IgnoreArguments();

            //Act
            var service = new XmlBatchExportService(stubXslTransformService, stubBatchLogRepository, stubDivisionRepository, stubFileManager);
            var exportSummary = service.ExportBatch(ExportTarget.Celebrating, _cacheEntries);

            //Assert
            Assert.That(exportSummary, Is.Not.Null);
            Assert.That(exportSummary.SummaryCount, Is.GreaterThan(0));
        }















        [Test]
        [Ignore("Assert condition has not been written yet")]
        public void XmlBatchExportService_produces_a_file_with_the_same_number_of_items_as_were_provided_when_filtered_items_collection_is_not_empty()
        {
            //XmlBatchExportService produces a file with the same number of items 
            //as were provided when filtered items collection is not empty

            //Arrange
            var stubXslTransformService = MockRepository.GenerateStub<IXslTransformService>();
            var stubBatchLogRepository = MockRepository.GenerateStub<IBatchLogRepository<BatchLog>>();
            var stubFileManager = MockRepository.GenerateStub<IFileManager>();
            stubXslTransformService.ExportDir = TestingOutputLocation;
            stubXslTransformService.Stub(x => x.Transform(null, ExportTarget.Celebrating, DateTime.Now, null)).IgnoreArguments().Return(TestingOutputFileLocation);
            stubBatchLogRepository.Stub(x => x.FetchAll()).Return(new List<BatchLog>());
            stubBatchLogRepository.Stub(x => x.SaveOrUpdate(null)).IgnoreArguments();
            stubFileManager.Stub(x => x.CopyFiles(null, null)).IgnoreArguments();

            //Act
            var service = new XmlBatchExportService(stubXslTransformService, stubBatchLogRepository, null, stubFileManager);
            // we don't care about the summaries 
            service.ExportBatch(ExportTarget.Celebrating, _cacheEntries);

            //Assert
            //locate XDOC and count nodes == cacheentries.count()
        }
    }
}
