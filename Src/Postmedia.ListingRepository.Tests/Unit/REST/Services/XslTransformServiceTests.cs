﻿using System;
using System.Configuration;
using System.IO;
using System.Xml;
using NUnit.Framework;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.FileIO;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Services.BatchExport;
using Rhino.Mocks;

namespace Postmedia.ListingRepository.Tests.Unit.REST.Services
{
    [TestFixture]
    public class XslTransformServiceTests
    {
        private XmlDocument _docToTransform;
        private string _exportFolder;

        [TestFixtureSetUp]
        public void Setup()
        {
            _exportFolder = ConfigurationManager.AppSettings["ExportDir"];

            _docToTransform = new XmlDocument();
            _docToTransform.LoadXml(
                @"<AD><UniqueId>VAN365497</UniqueId><Type /><PostDate>01/07/2012</PostDate><Email>noreply@postmedia.com</Email><FirstName1 /><LastName1 /><Announcement>
				Garcia &amp; Sanderson&lt;br /&gt;Philip &amp; Marsha Sanderson and Patricia &amp; Jose Garcia are happy &lt;br /&gt;to announce the marriage of their children, Angela &amp; Jeffery.&lt;br /&gt;CONGRATULATIONS!  

			</Announcement></AD>");
        }

        private string SetupExportDirectory(ExportTarget target)
        {
            var exportDir = _exportFolder + target.ToString();
            if (!Directory.Exists(exportDir))
            {
                Directory.CreateDirectory(exportDir);
            }

            string[] filePaths = Directory.GetFiles(exportDir);
            foreach (string filePath in filePaths)
                File.Delete(filePath);

            return exportDir + @"\";
        }
       

        [Test]
        [Ignore("Move to integration folder")]
        public void XslService_Call_To_Transform_Will_Produce_Output_XML_In_ExportFolder()
        {
            //Arrange - Delete all pre-existing files
            var exportDir = SetupExportDirectory(ExportTarget.Celebrating);
            var fileManager = MockRepository.GenerateStub<IFileManager>();
            var division = new Division { Code = "VAN", Name = "Vancouver"} ;

            //Act - perform the transformation
            var service = new XslTransformService(fileManager, exportDir);
            service.Transform(_docToTransform, ExportTarget.Celebrating, DateTime.Now, division);

            //Assert - that we have a file that was exported
            var files = Directory.GetFiles(exportDir);
            Assert.That(files.Length > 0);
        }

        [Test]
        [Ignore("Move to integration folder")]
        public void XslService_Call_To_Transform_With_XsltName_CDATATransform_Will_Produce_Output_XML_With_CDATA_Included()
        {
            //Arrange
            var exportDir = SetupExportDirectory(ExportTarget.Celebrating);
            var mockFileManager = MockRepository.GenerateStub<IFileManager>();
            //TODO: mock out file Manager calls
            var division = new Division { Code = "VAN", Name = "Vancouver" };

            
            //Act
            var service = new XslTransformService(mockFileManager);
            service.Transform(_docToTransform, ExportTarget.Celebrating, DateTime.Now, division);

            //Assert
            //Assert - that we have a file that was exported
            var files = Directory.GetFiles(exportDir);
            if(files.Length > 0)
            {
                Assert.That(File.Exists(files[0]));
                var fileReader = new StreamReader(files[0]);
                var fileText = fileReader.ReadToEnd();
                Assert.That(fileText.Contains("CDATA"));
            }
        }
    }
}
