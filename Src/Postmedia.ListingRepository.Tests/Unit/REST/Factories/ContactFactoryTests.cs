﻿using System;
using NUnit.Framework;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Factories;

namespace Postmedia.ListingRepository.Tests.Unit.REST.Factories
{
    [TestFixture]
    public class ContactFactoryTests : IFactoryTests
    {
        private ContactFactory Factory { get; set; }

        public void Setup()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void When_create_is_called_with_a_null_source_a_null_object_is_returned()
        {
            //Arrange
            var factory = new ContactFactory();
            
            //Act
            var contact = factory.Create(null);

            //Assert
            Assert.That(contact, Is.Null);
        }

        [Test]
        public void When_create_is_called_with_a_null_dest_and_null_source_a_null_object_is_returned()
        {
            //Arrange
            var factory = new AdvertiserFactory();

            //Act
            var advertiser = factory.Create(null, null);

            //Assert
            Assert.That(advertiser, Is.Null);
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new ContactFactory();

            var source = new DTO.Contact
            {
                Email = "source@email.com",
                FirstName = "sourceFName",
                LastName = "sourceLName",
                Phone = "2045555555"
            };

            //Act
            var dest = factory.Create(source, null);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.Email, Is.EqualTo(source.Email));
            Assert.That(dest.FirstName, Is.EqualTo(source.FirstName));
            Assert.That(dest.LastName, Is.EqualTo(source.LastName));
            Assert.That(dest.Phone, Is.EqualTo(source.Phone));
            
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new ContactFactory();

            var source = new DTO.Contact
            {
                Email = "source@email.com",
                FirstName = "sourceFName",
                LastName = "sourceLName",
                Phone = "2045555555"
            };

            //Act
            var dest = factory.Create(source, new Contact());

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.Email, Is.EqualTo(source.Email));
            Assert.That(dest.FirstName, Is.EqualTo(source.FirstName));
            Assert.That(dest.LastName, Is.EqualTo(source.LastName));
            Assert.That(dest.Phone, Is.EqualTo(source.Phone));
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new ContactFactory();

            var source = new DTO.Contact
            {
                Email = "source@email.com",
                FirstName = "sourceFName",
                LastName = "sourceLName",
                Phone = "2045555555"
            };

            var dest = new Contact
                           {
                               Email = "dest@email.com",
                               FirstName = "destFName",
                               LastName = "destLName",
                               Phone = "2044444444"
                           };

            //Act
            dest = factory.Create(source, dest);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.Email, Is.EqualTo(source.Email));
            Assert.That(dest.FirstName, Is.EqualTo(source.FirstName));
            Assert.That(dest.LastName, Is.EqualTo(source.LastName));
            Assert.That(dest.Phone, Is.EqualTo(source.Phone));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new ContactFactory();

            var source = new DTO.Contact();

            //Act
            var dest = factory.Create(source, null);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.Email, Is.EqualTo(source.Email));
            Assert.That(dest.FirstName, Is.EqualTo(source.FirstName));
            Assert.That(dest.LastName, Is.EqualTo(source.LastName));
            Assert.That(dest.Phone, Is.EqualTo(source.Phone));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_an_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new ContactFactory();

            var source = new DTO.Contact();

            //Act
            var dest = factory.Create(source, new Contact());

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.Email, Is.EqualTo(source.Email));
            Assert.That(dest.FirstName, Is.EqualTo(source.FirstName));
            Assert.That(dest.LastName, Is.EqualTo(source.LastName));
            Assert.That(dest.Phone, Is.EqualTo(source.Phone));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_a_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new ContactFactory();

            var source = new DTO.Contact();
            var dest = new Contact
                           {
                               Email = "dest@email.com",
                               FirstName = "destFName",
                               LastName = "destLName",
                               Phone = "2044444444"
                           };

            //Act
            dest = factory.Create(source, dest);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.Email, Is.EqualTo(source.Email));
            Assert.That(dest.FirstName, Is.EqualTo(source.FirstName));
            Assert.That(dest.LastName, Is.EqualTo(source.LastName));
            Assert.That(dest.Phone, Is.EqualTo(source.Phone));
        }

    }
}
