﻿using System;
using NUnit.Framework;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Factories;
using Postmedia.ListingRepository.REST.Repositories;
using Rhino.Mocks;

namespace Postmedia.ListingRepository.Tests.Unit.REST.Factories
{
    [TestFixture]
    public class AdvertiserFactoryTests : IFactoryTests
    {
        private AdvertiserFactory Factory { get; set; }

        public void Setup()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void When_create_is_called_with_a_null_source_a_null_object_is_returned()
        {
            //Arrange
            var factory = new AdvertiserFactory();
            
            //Act
            var advertiser = factory.Create(null);

            //Assert
            Assert.That(advertiser, Is.Null);
        }

        [Test]
        public void When_create_is_called_with_a_null_dest_and_null_source_a_null_object_is_returned()
        {
            //Arrange
            var factory = new AdvertiserFactory();

            //Act
            var advertiser = factory.Create(null, null);

            //Assert
            Assert.That(advertiser, Is.Null);
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubContactFactory = MockRepository.GenerateStub<IContactFactory>();
            var stubAddressFactory = MockRepository.GenerateStub<IAddressFactory>();
            var factory = new AdvertiserFactory(stubContactFactory, stubAddressFactory);
            stubContactFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);
            stubAddressFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);

            var source = new DTO.Advertiser
                                       {
                                           CompanyName = "Company Name",
                                           Contact = null,
                                           Address = null,
                                           Name = "Advertiser Name",
                                           Url = "http://testurl.com"
                                       };

            //Act
            var dest = factory.Create(source, null);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.CompanyName, Is.EqualTo(source.CompanyName));
            Assert.That(dest.Name, Is.EqualTo(source.Name));
            Assert.That(dest.Url, Is.EqualTo(source.Url));
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new AdvertiserFactory();
            var source = new DTO.Advertiser
            {
                CompanyName = "Company Name",
                Contact = null,
                Address = null,
                Name = "Advertiser Name",
                Url = "http://testurl.com"
            };

            //Act
            var dest = factory.Create(source, new Advertiser());

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.CompanyName, Is.EqualTo(source.CompanyName));
            Assert.That(dest.Contact, Is.EqualTo(source.Contact));
            Assert.That(dest.Address, Is.EqualTo(source.Address));
            Assert.That(dest.Name, Is.EqualTo(source.Name));
            Assert.That(dest.Url, Is.EqualTo(source.Url));
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new AdvertiserFactory();
            var source = new DTO.Advertiser
            {
                CompanyName = "Company Name",
                Contact = null,
                Address = null,
                Name = "Advertiser Name",
                Url = "http://testurl.com"
            };
            var dest = new Advertiser()
                           {
                               CompanyName = "Other Company Name",
                               Contact = null,
                               Address = null,
                               Name = "Other  Name",
                               Url = "http://newurl.com"
                           };

            //Act
            

            dest = factory.Create(source, dest);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.CompanyName, Is.EqualTo(source.CompanyName));
            Assert.That(dest.Contact, Is.EqualTo(source.Contact));
            Assert.That(dest.Address, Is.EqualTo(source.Address));
            Assert.That(dest.Name, Is.EqualTo(source.Name));
            Assert.That(dest.Url, Is.EqualTo(source.Url));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubContactFactory = MockRepository.GenerateStub<IContactFactory>();
            var stubAddressFactory = MockRepository.GenerateStub<IAddressFactory>();
            stubContactFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);
            stubAddressFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);

            var factory = new AdvertiserFactory(stubContactFactory, stubAddressFactory);            
            var source = new DTO.Advertiser();
            
            //Act
            var dest = factory.Create(source, null);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.CompanyName, Is.EqualTo(source.CompanyName));
            Assert.That(dest.Contact, Is.EqualTo(source.Contact));
            Assert.That(dest.Address, Is.EqualTo(source.Address));
            Assert.That(dest.Name, Is.EqualTo(source.Name));
            Assert.That(dest.Url, Is.EqualTo(source.Url));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_an_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubContactFactory = MockRepository.GenerateStub<IContactFactory>();
            var stubAddressFactory = MockRepository.GenerateStub<IAddressFactory>();
            stubContactFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);
            stubAddressFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);

            var factory = new AdvertiserFactory(stubContactFactory, stubAddressFactory);
            var source = new DTO.Advertiser();
            var dest = new Advertiser();

            //Act
            dest = factory.Create(source, dest);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.CompanyName, Is.EqualTo(source.CompanyName));
            Assert.That(dest.Contact, Is.EqualTo(source.Contact));
            Assert.That(dest.Address, Is.EqualTo(source.Address));
            Assert.That(dest.Name, Is.EqualTo(source.Name));
            Assert.That(dest.Url, Is.EqualTo(source.Url));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_a_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubContactFactory = MockRepository.GenerateStub<IContactFactory>();
            var stubAddressFactory = MockRepository.GenerateStub<IAddressFactory>();
            stubContactFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);
            stubAddressFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);

            var factory = new AdvertiserFactory(stubContactFactory, stubAddressFactory);
            var source = new DTO.Advertiser();
            var dest = new Advertiser
                           {
                               CompanyName = "Other Company Name",
                               Name = "Other  Name",
                               Url = "http://newurl.com"
                           };

            //Act
            dest = factory.Create(source, dest);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.CompanyName, Is.EqualTo(source.CompanyName));
            Assert.That(dest.Name, Is.EqualTo(source.Name));
            Assert.That(dest.Url, Is.EqualTo(source.Url));
        }

    }
}
