﻿using NUnit.Framework;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Factories;
using Postmedia.ListingRepository.REST.Repositories;
using Rhino.Mocks;

namespace Postmedia.ListingRepository.Tests.Unit.REST.Factories
{
    [TestFixture]
    public class AdBookingFactoryTests : IFactoryTests
    {
        private AdBookingFactory Factory { get; set; }
        
        public void Setup()
        {
            //setup requirements for tests
        }

        [Test]
        public void When_create_is_called_with_a_null_source_a_null_object_is_returned()
        {
            //Arrange
            var repo = MockRepository.GenerateStub<IDivisionRepository<Division>>();
            var factory = new AdBookingFactory(repo);
            repo.Stub(x => x.FetchByCode(null)).IgnoreArguments().Return(new Division());

            //Act
            var adbooking = factory.Create(null);

            //Assert
            Assert.That(adbooking, Is.Null);
        }

        [Test]
        public void When_create_is_called_with_a_null_dest_and_null_source_a_null_object_is_returned()
        {
            //Arrange
            var repo = MockRepository.GenerateStub<IDivisionRepository<Division>>();
            var factory = new AdBookingFactory(repo);
            repo.Stub(x => x.FetchByCode(null)).IgnoreArguments().Return(new Division());

            //Act
            var adbooking = factory.Create(null, null);

            //Assert
            Assert.That(adbooking, Is.Null);
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var repo = MockRepository.GenerateStub<IDivisionRepository<Division>>();
            var factory = new AdBookingFactory(repo);
            repo.Stub(x => x.FetchByCode(null)).IgnoreArguments().Return(null);

            var source = new DTO.AdBooking
                              {
                                  AccountNumber = "1234",
                                  AdOrderNumber = "5678",
                                  AgencyAccountNumber = "91011",
                                  ExternalAccountNumber = "121314",
                                  ExternalAdOrderNumber = "151617",
                                  Division = null,
                                  Id = 0
                              };

            //Act
            var adbooking = factory.Create(source, null);

            //Assert
            Assert.That(adbooking.AccountNumber, Is.EqualTo(source.AccountNumber));
            Assert.That(adbooking.AdOrderNumber, Is.EqualTo(source.AdOrderNumber));
            Assert.That(adbooking.AgencyAccountNumber, Is.EqualTo(source.AgencyAccountNumber));
            Assert.That(adbooking.ExternalAccountNumber, Is.EqualTo(source.ExternalAccountNumber));
            Assert.That(adbooking.ExternalAdOrderNumber, Is.EqualTo(source.ExternalAdOrderNumber));
            Assert.That(adbooking.Division, Is.EqualTo(source.Division));
            
        }
              
        [Test]
        public void When_create_is_called_with_a_populated_source_and_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var repo = MockRepository.GenerateStub<IDivisionRepository<Division>>();
            var factory = new AdBookingFactory(repo);
            repo.Stub(x => x.FetchByCode(null)).IgnoreArguments().Return(new Division());

            var booking = new DTO.AdBooking
            {
                AccountNumber = "1234",
                AdOrderNumber = "5678",
                AgencyAccountNumber = "91011",
                ExternalAccountNumber = "121314",
                ExternalAdOrderNumber = "151617",
                Division = null
            };

            //Act
            var adbooking = factory.Create(booking, new AdBooking());

            //Assert
            Assert.That(adbooking.AccountNumber, Is.EqualTo(booking.AccountNumber));
            Assert.That(adbooking.AdOrderNumber, Is.EqualTo(booking.AdOrderNumber));
            Assert.That(adbooking.AgencyAccountNumber, Is.EqualTo(booking.AgencyAccountNumber));
            Assert.That(adbooking.ExternalAccountNumber, Is.EqualTo(booking.ExternalAccountNumber));
            Assert.That(adbooking.ExternalAdOrderNumber, Is.EqualTo(booking.ExternalAdOrderNumber));
            Assert.That(adbooking.Division, Is.EqualTo(booking.Division));
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var repo = MockRepository.GenerateStub<IDivisionRepository<Division>>();
            var factory = new AdBookingFactory(repo);
            repo.Stub(x => x.FetchByCode(null)).IgnoreArguments().Return(new Division());

            var source = new DTO.AdBooking
            {
                AccountNumber = "1234",
                AdOrderNumber = "5678",
                AgencyAccountNumber = "91011",
                ExternalAccountNumber = "121314",
                ExternalAdOrderNumber = "151617",
                Division = null
            };

            var dest = new AdBooking
                           {
                               AccountNumber = "0123",
                               AdOrderNumber = "4567",
                               AgencyAccountNumber = "8910",
                               ExternalAccountNumber = "1112",
                               ExternalAdOrderNumber = "1314",
                               Division = null
                           };

            //Act
            var adbooking = factory.Create(source, dest);

            //Assert
            Assert.That(adbooking.AccountNumber, Is.EqualTo(dest.AccountNumber));
            Assert.That(adbooking.AdOrderNumber, Is.EqualTo(dest.AdOrderNumber));
            Assert.That(adbooking.AgencyAccountNumber, Is.EqualTo(dest.AgencyAccountNumber));
            Assert.That(adbooking.ExternalAccountNumber, Is.EqualTo(dest.ExternalAccountNumber));
            Assert.That(adbooking.ExternalAdOrderNumber, Is.EqualTo(dest.ExternalAdOrderNumber));
            Assert.That(adbooking.Division, Is.EqualTo(dest.Division));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var repo = MockRepository.GenerateStub<IDivisionRepository<Division>>();
            var factory = new AdBookingFactory(repo);
            repo.Stub(x => x.FetchByCode(null)).IgnoreArguments().Return(new Division());

            var booking = new DTO.AdBooking();

            //Act
            var adbooking = factory.Create(booking, null);

            //Assert
            Assert.That(adbooking.AccountNumber, Is.EqualTo(booking.AccountNumber));
            Assert.That(adbooking.AdOrderNumber, Is.EqualTo(booking.AdOrderNumber));
            Assert.That(adbooking.AgencyAccountNumber, Is.EqualTo(booking.AgencyAccountNumber));
            Assert.That(adbooking.ExternalAccountNumber, Is.EqualTo(booking.ExternalAccountNumber));
            Assert.That(adbooking.ExternalAdOrderNumber, Is.EqualTo(booking.ExternalAdOrderNumber));
            Assert.That(adbooking.Division, Is.EqualTo(booking.Division));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_an_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var repo = MockRepository.GenerateStub<IDivisionRepository<Division>>();
            var factory = new AdBookingFactory(repo);
            repo.Stub(x => x.FetchByCode(null)).IgnoreArguments().Return(new Division());

            var booking = new DTO.AdBooking();

            //Act
            var adbooking = factory.Create(booking, new AdBooking());

            //Assert
            Assert.That(adbooking.AccountNumber, Is.EqualTo(booking.AccountNumber));
            Assert.That(adbooking.AdOrderNumber, Is.EqualTo(booking.AdOrderNumber));
            Assert.That(adbooking.AgencyAccountNumber, Is.EqualTo(booking.AgencyAccountNumber));
            Assert.That(adbooking.ExternalAccountNumber, Is.EqualTo(booking.ExternalAccountNumber));
            Assert.That(adbooking.ExternalAdOrderNumber, Is.EqualTo(booking.ExternalAdOrderNumber));
            Assert.That(adbooking.Division, Is.EqualTo(booking.Division));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_a_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var repo = MockRepository.GenerateStub<IDivisionRepository<Division>>();
            var factory = new AdBookingFactory(repo);
            repo.Stub(x => x.FetchByCode(null)).IgnoreArguments().Return(new Division());

            var booking = new DTO.AdBooking();
            var dest = new AdBooking
                           {
                               AccountNumber = "0123",
                               AdOrderNumber = "4567",
                               AgencyAccountNumber = "8910",
                               ExternalAccountNumber = "1112",
                               ExternalAdOrderNumber = "1314",
                               Division = null
                           };

            //Act
            var adbooking = factory.Create(booking, dest);

            //Assert
            Assert.That(adbooking.AccountNumber, Is.EqualTo(booking.AccountNumber));
            Assert.That(adbooking.AdOrderNumber, Is.EqualTo(booking.AdOrderNumber));
            Assert.That(adbooking.AgencyAccountNumber, Is.EqualTo(booking.AgencyAccountNumber));
            Assert.That(adbooking.ExternalAccountNumber, Is.EqualTo(booking.ExternalAccountNumber));
            Assert.That(adbooking.ExternalAdOrderNumber, Is.EqualTo(booking.ExternalAdOrderNumber));
            Assert.That(adbooking.Division, Is.EqualTo(booking.Division));
        }

    }
}
