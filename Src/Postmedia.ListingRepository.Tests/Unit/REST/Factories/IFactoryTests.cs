using NUnit.Framework;

namespace Postmedia.ListingRepository.Tests.Unit.REST.Factories
{
    public interface IFactoryTests
    {
        void Setup();

        [Test]
        void When_create_is_called_with_a_null_source_a_null_object_is_returned();

        [Test]
        void When_create_is_called_with_a_null_dest_and_null_source_a_null_object_is_returned();

        [Test]
        void When_create_is_called_with_a_populated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source();

        [Test]
        void When_create_is_called_with_a_populated_source_and_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source();

        [Test]
        void When_create_is_called_with_a_populated_source_and_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source();

        [Test]
        void When_create_is_called_with_an_unpopulated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source();

        [Test]
        void When_create_is_called_with_an_unpopulated_source_and_an_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source();

        [Test]
        void When_create_is_called_with_an_unpopulated_source_and_a_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source();

    }
}