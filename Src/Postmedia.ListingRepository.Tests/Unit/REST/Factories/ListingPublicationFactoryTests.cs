﻿using System;
using NUnit.Framework;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Factories;
using Postmedia.ListingRepository.REST.Repositories;
using Rhino.Mocks;

namespace Postmedia.ListingRepository.Tests.Unit.REST.Factories
{
    [TestFixture]
    public class ListingPublicationFactoryTests : IFactoryTests
    {
        public void Setup()
        {
            throw new System.NotImplementedException();
        }

        [Test]
        public void When_create_is_called_with_a_null_source_a_null_object_is_returned()
        {
            //Arrange
            var stubUpsellRepo = MockRepository.GenerateStub<IUpsellRepository<Upsell>>();
            var stubListingPublicationRepo = MockRepository.GenerateStub<IListingPublicationRepository<ListingPublication>>();
            var stubPublicationRepo = MockRepository.GenerateStub<IPublicationRepository<Publication>>();
            var stubCategoryRepo = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            var factory = new ListingPublicationFactory(stubListingPublicationRepo, stubUpsellRepo, stubPublicationRepo, stubCategoryRepo);

            //Act
            var dest = factory.Create(null);

            //Assert
            Assert.That(dest, Is.Null);
        }

        [Test]
        public void When_create_is_called_with_a_null_dest_and_null_source_a_null_object_is_returned()
        {
            //Arrange
            var stubUpsellRepo = MockRepository.GenerateStub<IUpsellRepository<Upsell>>();
            var stubListingPublicationRepo = MockRepository.GenerateStub<IListingPublicationRepository<ListingPublication>>();
            var stubPublicationRepo = MockRepository.GenerateStub<IPublicationRepository<Publication>>();
            var stubCategoryRepo = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            var factory = new ListingPublicationFactory(stubListingPublicationRepo, stubUpsellRepo, stubPublicationRepo, stubCategoryRepo);

            //Act
            var dest = factory.Create(null, null);

            //Assert
            Assert.That(dest, Is.Null);
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubUpsellRepo = MockRepository.GenerateStub<IUpsellRepository<Upsell>>();
            var stubListingPublicationRepo = MockRepository.GenerateStub<IListingPublicationRepository<ListingPublication>>();
            var stubPublicationRepo = MockRepository.GenerateStub<IPublicationRepository<Publication>>();
            var stubCategoryRepo = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            var factory = new ListingPublicationFactory(stubListingPublicationRepo, stubUpsellRepo, stubPublicationRepo, stubCategoryRepo);

            var source = new DTO.ListingPublication
                             {
                                 ExpireDate = DateTime.Now,
                                 StartDate = DateTime.Now,
                                 Status = "sourcestatus"
                             };

            //Act
            var dest = factory.Create(source, null);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.ExpireDate, Is.EqualTo(source.ExpireDate));
            Assert.That(dest.StartDate, Is.EqualTo(source.StartDate));
            Assert.That(dest.Status, Is.EqualTo(source.Status));

        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubUpsellRepo = MockRepository.GenerateStub<IUpsellRepository<Upsell>>();
            var stubListingPublicationRepo = MockRepository.GenerateStub<IListingPublicationRepository<ListingPublication>>();
            var stubPublicationRepo = MockRepository.GenerateStub<IPublicationRepository<Publication>>();
            var stubCategoryRepo = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            var factory = new ListingPublicationFactory(stubListingPublicationRepo, stubUpsellRepo, stubPublicationRepo, stubCategoryRepo);

            var source = new DTO.ListingPublication
            {
                ExpireDate = DateTime.Now,
                StartDate = DateTime.Now,
                Status = "sourcestatus"
            };

            var dest = new ListingPublication
            {
                ExpireDate = DateTime.Now.AddDays(1),
                StartDate = DateTime.Now.AddDays(1),
                Status = "deststatus"
            };

            //Act
            dest = factory.Create(source, dest);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.ExpireDate, Is.EqualTo(source.ExpireDate));
            Assert.That(dest.StartDate, Is.EqualTo(source.StartDate));
            Assert.That(dest.Status, Is.EqualTo(source.Status));
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubUpsellRepo = MockRepository.GenerateStub<IUpsellRepository<Upsell>>();
            var stubListingPublicationRepo = MockRepository.GenerateStub<IListingPublicationRepository<ListingPublication>>();
            var stubPublicationRepo = MockRepository.GenerateStub<IPublicationRepository<Publication>>();
            var stubCategoryRepo = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            var factory = new ListingPublicationFactory(stubListingPublicationRepo, stubUpsellRepo, stubPublicationRepo, stubCategoryRepo);

            var source = new DTO.ListingPublication
            {
                ExpireDate = DateTime.Now,
                StartDate = DateTime.Now,
                Status = "sourcestatus"
            };

            var dest = new ListingPublication();

            //Act
            dest = factory.Create(source, dest);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.ExpireDate, Is.EqualTo(source.ExpireDate));
            Assert.That(dest.StartDate, Is.EqualTo(source.StartDate));
            Assert.That(dest.Status, Is.EqualTo(source.Status));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubUpsellRepo = MockRepository.GenerateStub<IUpsellRepository<Upsell>>();
            var stubListingPublicationRepo = MockRepository.GenerateStub<IListingPublicationRepository<ListingPublication>>();
            var stubPublicationRepo = MockRepository.GenerateStub<IPublicationRepository<Publication>>();
            var stubCategoryRepo = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            var factory = new ListingPublicationFactory(stubListingPublicationRepo, stubUpsellRepo, stubPublicationRepo, stubCategoryRepo);

            var source = new DTO.ListingPublication();
            
            //Act
            var dest = factory.Create(source, null);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.ExpireDate, Is.EqualTo(source.ExpireDate));
            Assert.That(dest.StartDate, Is.EqualTo(source.StartDate));
            Assert.That(dest.Status, Is.EqualTo(source.Status));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_an_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubUpsellRepo = MockRepository.GenerateStub<IUpsellRepository<Upsell>>();
            var stubListingPublicationRepo = MockRepository.GenerateStub<IListingPublicationRepository<ListingPublication>>();
            var stubPublicationRepo = MockRepository.GenerateStub<IPublicationRepository<Publication>>();
            var stubCategoryRepo = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            var factory = new ListingPublicationFactory(stubListingPublicationRepo, stubUpsellRepo, stubPublicationRepo, stubCategoryRepo);

            var source = new DTO.ListingPublication();

            //Act
            var dest = factory.Create(source, new ListingPublication());

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.ExpireDate, Is.EqualTo(source.ExpireDate));
            Assert.That(dest.StartDate, Is.EqualTo(source.StartDate));
            Assert.That(dest.Status, Is.EqualTo(source.Status));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_a_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubUpsellRepo = MockRepository.GenerateStub<IUpsellRepository<Upsell>>();
            var stubListingPublicationRepo = MockRepository.GenerateStub<IListingPublicationRepository<ListingPublication>>();
            var stubPublicationRepo = MockRepository.GenerateStub<IPublicationRepository<Publication>>();
            var stubCategoryRepo = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            var factory = new ListingPublicationFactory(stubListingPublicationRepo, stubUpsellRepo, stubPublicationRepo, stubCategoryRepo);

            var source = new DTO.ListingPublication();
            var dest = new ListingPublication
            {
                ExpireDate = DateTime.Now.AddDays(1),
                StartDate = DateTime.Now.AddDays(1),
                Status = "deststatus"
            };
            //Act
            dest = factory.Create(source, dest);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.ExpireDate, Is.EqualTo(source.ExpireDate));
            Assert.That(dest.StartDate, Is.EqualTo(source.StartDate));
            Assert.That(dest.Status, Is.EqualTo(source.Status));
        }

    }
}
