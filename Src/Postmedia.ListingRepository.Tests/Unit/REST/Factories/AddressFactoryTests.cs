﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Factories;

namespace Postmedia.ListingRepository.Tests.Unit.REST.Factories
{
    [TestFixture]
    public class AddressFactoryTests : IFactoryTests
    {
        private AddressFactory Factory { get; set; }

        public void Setup()
        {
            throw new NotImplementedException();
        }

        [Test]
        public void When_create_is_called_with_a_null_source_a_null_object_is_returned()
        {
            //Arrange
            var factory = new AddressFactory();
            
            //Act
            var address = factory.Create(null);

            //Assert
            Assert.That(address, Is.Null);
        }

        [Test]
        public void When_create_is_called_with_a_null_dest_and_null_source_a_null_object_is_returned()
        {
            //Arrange
            var factory = new AddressFactory();

            //Act
            var address = factory.Create(null, null);

            //Assert
            Assert.That(address, Is.Null);
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new AddressFactory();
            var source = new DTO.Address
                             {
                                 City = "sourceCity",
                                 Country = "sourceCountry",
                                 Neighbourhood = "sourceNeighborhood",
                                 PostalCode = "s0rc3p",
                                 Province = "sourceProv",
                                 Region = "sourceReg", 
                                 AddressLines = new List<DTO.AddressLine>
                                                    {
                                                        new DTO.AddressLine
                                                            {
                                                                LineText = "TestText",
                                                                Number = 1
                                                            }
                                                    }
                             };

            //Act
            var dest= factory.Create(source, null);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.City, Is.EqualTo(source.City));
            Assert.That(dest.Country, Is.EqualTo(source.Country));
            Assert.That(dest.Neighbourhood, Is.EqualTo(source.Neighbourhood));
            Assert.That(dest.PostalCode, Is.EqualTo(source.PostalCode));
            Assert.That(dest.Province, Is.EqualTo(source.Province));
            Assert.That(dest.Region, Is.EqualTo(source.Region));
            Assert.That(dest.AddressLines.Count, Is.EqualTo(source.AddressLines.Count));
            for (int i = 0; i < source.AddressLines.Count; i++)
            {
                Assert.That(source.AddressLines[i].LineText, Is.EqualTo(dest.AddressLines[i].LineText));
                Assert.That(source.AddressLines[i].Number, Is.EqualTo(dest.AddressLines[i].LineNumber));
            }
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new AddressFactory();
            var source = new DTO.Address
            {
                City = "sourceCity",
                Country = "sourceCountry",
                Neighbourhood = "sourceNeighborhood",
                PostalCode = "s0rc3p",
                Province = "sourceProv",
                Region = "sourceReg",
                AddressLines = new List<DTO.AddressLine>
                                                    {
                                                        new DTO.AddressLine
                                                            {
                                                                LineText = "TestText",
                                                                Number = 1
                                                            }
                                                    }
            };

            //Act
            var dest = factory.Create(source, new Address());

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.City, Is.EqualTo(source.City));
            Assert.That(dest.Country, Is.EqualTo(source.Country));
            Assert.That(dest.Neighbourhood, Is.EqualTo(source.Neighbourhood));
            Assert.That(dest.PostalCode, Is.EqualTo(source.PostalCode));
            Assert.That(dest.Province, Is.EqualTo(source.Province));
            Assert.That(dest.Region, Is.EqualTo(source.Region));
            Assert.That(dest.AddressLines.Count, Is.EqualTo(source.AddressLines.Count));
            for (int i = 0; i < source.AddressLines.Count; i++)
            {
                Assert.That(source.AddressLines[i].LineText, Is.EqualTo(dest.AddressLines[i].LineText));
                Assert.That(source.AddressLines[i].Number, Is.EqualTo(dest.AddressLines[i].LineNumber));
            }
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new AddressFactory();
            var source = new DTO.Address
                             {
                                 City = "sourceCity",
                                 Country = "sourceCountry",
                                 Neighbourhood = "sourceNeighborhood",
                                 PostalCode = "s0rc3p",
                                 Province = "sourceProv",
                                 Region = "sourceReg",
                                 AddressLines = new List<DTO.AddressLine>
                                                    {
                                                        new DTO.AddressLine
                                                            {
                                                                LineText = "TestText",
                                                                Number = 1
                                                            }
                                                    }
                             };
            var dest = new Address
                           {
                               City = "destCity",
                               Country = "destCountry",
                               Neighbourhood = "destNeighborhood",
                               PostalCode = "d3s9s7",
                               Province = "destProv",
                               Region = "destReg"
                           };

            //Act
            dest = factory.Create(source, dest);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.City, Is.EqualTo(source.City));
            Assert.That(dest.Country, Is.EqualTo(source.Country));
            Assert.That(dest.Neighbourhood, Is.EqualTo(source.Neighbourhood));
            Assert.That(dest.PostalCode, Is.EqualTo(source.PostalCode));
            Assert.That(dest.Province, Is.EqualTo(source.Province));
            Assert.That(dest.Region, Is.EqualTo(source.Region));
            Assert.That(dest.AddressLines.Count, Is.EqualTo(source.AddressLines.Count));
            for (int i = 0; i < source.AddressLines.Count; i++)
            {
                Assert.That(source.AddressLines[i].LineText, Is.EqualTo(dest.AddressLines[i].LineText));
                Assert.That(source.AddressLines[i].Number, Is.EqualTo(dest.AddressLines[i].LineNumber));
            }
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new AddressFactory();
            var source = new DTO.Address();

            var dest = new Address
            {
                City = "destCity",
                Country = "destCountry",
                Neighbourhood = "destNeighborhood",
                PostalCode = "d3s9s7",
                Province = "destProv",
                Region = "destReg"
            };

            //Act
            dest = factory.Create(source, dest);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.City, Is.EqualTo(source.City));
            Assert.That(dest.Country, Is.EqualTo(source.Country));
            Assert.That(dest.Neighbourhood, Is.EqualTo(source.Neighbourhood));
            Assert.That(dest.PostalCode, Is.EqualTo(source.PostalCode));
            Assert.That(dest.Province, Is.EqualTo(source.Province));
            Assert.That(dest.Region, Is.EqualTo(source.Region));
         
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_an_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new AddressFactory();
            var source = new DTO.Address();

            //Act
            var dest = factory.Create(source, new Address());

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.City, Is.EqualTo(source.City));
            Assert.That(dest.Country, Is.EqualTo(source.Country));
            Assert.That(dest.Neighbourhood, Is.EqualTo(source.Neighbourhood));
            Assert.That(dest.PostalCode, Is.EqualTo(source.PostalCode));
            Assert.That(dest.Province, Is.EqualTo(source.Province));
            Assert.That(dest.Region, Is.EqualTo(source.Region));
          
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_a_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var factory = new AddressFactory();
            var source = new DTO.Address();

            //Act
            var dest = factory.Create(source, null);

            //Assert
            Assert.That(dest, Is.Not.Null);
            Assert.That(dest.City, Is.EqualTo(source.City));
            Assert.That(dest.Country, Is.EqualTo(source.Country));
            Assert.That(dest.Neighbourhood, Is.EqualTo(source.Neighbourhood));
            Assert.That(dest.PostalCode, Is.EqualTo(source.PostalCode));
            Assert.That(dest.Province, Is.EqualTo(source.Province));
            Assert.That(dest.Region, Is.EqualTo(source.Region));
            
        }
     
    }
}
