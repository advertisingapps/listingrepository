﻿using AutoMapper;
using NUnit.Framework;
using Postmedia.ListingRepository.REST.Mapping;

namespace Postmedia.ListingRepository.Tests.Unit.REST.Mapping
{
    [TestFixture]
    public class AutomapperConfigurationTests
    {
        [Test]
        public void TestAutomapperConfiguration()
        {
            AutomapperConfiguration.Config();
            Mapper.AssertConfigurationIsValid();
        }
    }
}
