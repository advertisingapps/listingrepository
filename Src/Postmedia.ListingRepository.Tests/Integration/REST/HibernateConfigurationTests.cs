﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.Tests.Integration.REST
{
    [TestFixture]
    public class HibernateConfigurationTests
    {

        [Test]
        public void NHibernate_configuration_can_generate_schema()
        {
            Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008
                              .ConnectionString(
                                  c => c.FromConnectionStringWithKey("DbConn"))
                              .ShowSql())
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Listing>())
                .ExposeConfiguration(cfg => new SchemaExport(cfg).Create(true,false))
                .BuildSessionFactory();
        }
    }
}
