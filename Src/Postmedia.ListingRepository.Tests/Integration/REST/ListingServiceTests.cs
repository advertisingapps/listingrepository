﻿using NUnit.Framework;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Mapping;
using Postmedia.ListingRepository.REST.Repositories;
using Postmedia.ListingRepository.REST.Services;
using Rhino.Mocks;

namespace Postmedia.ListingRepository.Tests.Integration.REST
{
    [TestFixture]
    public class ListingServiceTests
    {


        [SetUp]
        public void Setup()
        {
            AutomapperConfiguration.Config();
        }

        [Test]
        public void When_GetListingById_is_called_ListingRepository_will_call_ListingRepository_FetchById()
        {
            //Arrange
            var repository = MockRepository.GenerateStub<IListingRepository<Listing>>();
            var service = new ListingService(repository, null, null, null, null);
            repository.Expect(x => x.FetchById(5)).Return(new Listing {Id = 5});

            //Act
            service.GetListingById(5);

            //Assert
            repository.AssertWasCalled(x=>x.FetchById(5));
        }
    }
}
