﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;
using Postmedia.ListingRepository.Core.Translators;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.REST.Mapping;
using Postmedia.ListingRepository.REST.Repositories;
using Postmedia.ListingRepository.REST.Services;
using AdBooking = Postmedia.ListingRepository.REST.Entities.AdBooking;

namespace Postmedia.ListingRepository.Tests.Integration.Sonic
{
    [TestFixture]
    public class SonicServiceTests
    {
        private ISessionFactory factory { get; set; }

        private void ConfigureNHibernate()
        {
            factory = Fluently.Configure()
                 .Database(MsSqlConfiguration.MsSql2008
                               .ConnectionString(
                                   c => c.FromConnectionStringWithKey("DbConn"))
                               .ShowSql())
                 .Mappings(m => m.FluentMappings.AddFromAssemblyOf<AdBooking>())
                 .ExposeConfiguration(cfg => new SchemaExport(cfg).Create(false, false))
                 .BuildSessionFactory();
        }
        
        private void ConfigureAutomapper()
        {
            AutomapperConfiguration.Config();
        }

        [TestFixtureSetUp]
        public void Setup()
        {
            ConfigureNHibernate();
            ConfigureAutomapper();
        }

        [Test]
        public void When_Connect_is_called_with_valid_credentials_a_connection_is_opened_successfully()
        {
            var service = new SonicService();
            var host = "wpgesbd1";
            var port = 2506;
            var username  = "Administrator";
            var password = "Administrator";
            bool success = service.Connect(host, port, username, password);

            Assert.That(success, Is.True);
        }

        [Test]
        public void When_publish_is_called_the_topic_post_occurs_with_no_exceptions()
        {
            var service = new SonicService();
            var host = "wpgesbd1";
            var port = 2506;
            var username = "Administrator";
            var password = "Administrator";
            var success = service.Connect(host, port, username, password);
            if (success)
            {
                service.PublishToTopic("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "cw.global.Exit");
                service.Disconnect();
                service.Dispose();
            }

            Assert.That(success, Is.True);
        }

        [Test]
        public void When_publish_is_called_with_a_new_listing_object_the_post_occurs_with_no_exceptions()
        {
            var service = new SonicService();
            var host = "wpgesbd1";
            var port = 2506;
            var username = "Administrator";
            var password = "Administrator";
            var success = service.Connect(host, port, username, password);

            var listing = new Listing();

            var listingxml = StreamTranslator<Listing>.DtoToXml(listing);
            if (success)
            {
                service.PublishToTopic(listingxml, "cw.global.Exit");
                service.Disconnect();
                service.Dispose();
            }

            Assert.That(success, Is.True);
        }

        [Test]
        public void When_publish_is_called_with_a_retrieved_listing_object_the_post_occurs_with_no_exceptions()
        {
            var sonic = new SonicService();

            var session = factory.OpenSession();
            var sessionStorage = new IglooCoder.Commons.WcfNhibernate.Testing.InMemorySessionStorage(session);
            var listingRepo = new ListingRepository.REST.Repositories.ListingRepository(sessionStorage);
            var divRepo = new DivisionRepository();

            var listingService = new ListingService(listingRepo, divRepo, null, null, null);
            
            var listing = listingService.GetListingById(1);

            var host = "wpgesbd1";
            var port = 2506;
            var username = "Administrator";
            var password = "Administrator";
            var success = sonic.Connect(host, port, username, password);

            var listingxml = StreamTranslator<Listing>.DtoToXml(listing);
            if (success)
            {
                sonic.PublishToTopic(listingxml, "cw.global.Exit");
                sonic.Disconnect();
                sonic.Dispose();
            }

            Assert.That(success, Is.True);
        }

    }
}
