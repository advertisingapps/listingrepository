﻿using AutoMapper;
using NUnit.Framework;

using Postmedia.ListingRepository.Services.Mapping;

namespace Postmedia.ListingRepository.Tests.Services
{
    [TestFixture]
    public class AutomapperConfigurationTests
    {
        [Test]
        public void TestAutomapperConfiguration()
        {
            AutomapperConfiguration.Config();
            Mapper.AssertConfigurationIsValid();
        }
    }
}
