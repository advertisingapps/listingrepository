﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Security.Principal;
using log4net;

namespace Postmedia.ListingRepository.Core.FileIO
{
    public class FileManager : IFileManager
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void PrepareExportDirectory(string path, bool removeAllFiles)
        {
            if (!string.IsNullOrEmpty(path))
            {
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                if (removeAllFiles)
                {
                    var files = Directory.GetFiles(path);
                    if (files.Length > 0)
                    {
                        //directory is not empty ... clear it before writing
                        foreach (var file in files)
                        {
                            File.Delete(file);
                        }
                    }
                }
            }
            else
            {
                throw new ArgumentException("Path parameter cannot be null when creating or cleaning directories. Please check your configuration");
            }
        }
        public void CleanDirectory(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                if (Directory.Exists(path))
                {
                    var files = Directory.GetFiles(path);
                    if (files.Length > 0)
                    {
                        //directory is not empty ... clear it before writing
                        foreach (var file in files)
                        {
                            File.Delete(file);
                        }
                    }
                }
            }
            else
            {
                throw new ArgumentException("Path parameter cannot be null when creating or cleaning directories. Please check your configuration");
            }
        }
        public bool ValidateImportDirectory(string path, bool hasFiles)
        {
            bool isValid = (Directory.Exists(path));
            isValid = isValid && (Directory.GetFiles(path).Length > 0);
            return isValid;
        }


        public void CopyFiles(string sourcePath, string destinationPath)
        {
            CopyFiles(sourcePath, destinationPath, false);
        }
        public void CopyFiles(string sourcePath, string destinationPath, bool overwrite)
        {
            try
            {
                Log.Debug("Copying files... - CopyFiles()");
                File.Copy(sourcePath, destinationPath, overwrite);
                Log.Debug("Files Copied - CopyFiles()");
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }
    }
}
