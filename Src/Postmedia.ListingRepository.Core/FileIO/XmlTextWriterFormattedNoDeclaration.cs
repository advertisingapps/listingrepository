﻿namespace Postmedia.ListingRepository.Core.FileIO
{
    public class XmlTextWriterFormattedNoDeclaration : System.Xml.XmlTextWriter
    {
        public XmlTextWriterFormattedNoDeclaration(System.IO.TextWriter w):base(w)
        {
        }

        public override void WriteStartDocument()
        {
            //supress this from being written
        }
    }
}
