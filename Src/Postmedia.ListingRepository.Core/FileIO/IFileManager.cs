namespace Postmedia.ListingRepository.Core.FileIO
{
    public interface IFileManager
    {
        void PrepareExportDirectory(string path, bool removeAllFiles);
        bool ValidateImportDirectory(string path, bool hasFiles);
        void CleanDirectory(string path);
        void CopyFiles(string sourcePath, string destinationPath);
        void CopyFiles(string sourcePath, string destinationPath, bool overwrite);
    }
}