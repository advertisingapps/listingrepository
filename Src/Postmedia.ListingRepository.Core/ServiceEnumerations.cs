﻿using Postmedia.ListingRepository.Core.EnumExtensions.Attributes;

namespace Postmedia.ListingRepository.Core
{
    public enum ExportTarget
    {
        [TransformUsing("CDATAWorking")]
        Working,
        [TransformUsing("CDATACelebrating")]
        Celebrating,
        [TransformUsing("CDATARemembering")]
        Remembering,
        [TransformUsing("CDATAAdPerfect")]
        AdPerfect
    }

    public enum ServiceSettings
    {
        DbConn,
        ServiceTimeout,
        CacheExportDir,
        CacheExportLocalDir,
        GeneraDropDownExportDir,
        GeneraDropDownExportDirOodle,
        GeneraDropDownExportDirYMM,
        GeneraDropDownExportLocalDir,
        YearMakeModelImportDir,
        OodleXmlUrl,
        Recipients,
        EmailAccount,
        EsbTopic,
        EsbHost,
        EsbPort,
        EsbUsername,
        EsbPassword,
        WorkingExportFileFormat,
        CelebratingExportFileFormat,
        RemeberingExportFileFormat,
        AdPerfectExportFileFormat,
        CacheExportTestingModeEnabled,
        CacheExportTestingModeReplacementEmail,
        DefaultWorkingEmailAccount,
        ServiceEnvironment,
        QueryTimeout,
        AccountLogoRepository,
        DefaulWorkopolisEmailAccount,
        DefaultSasQrCodeEmailAccount,
        DefaultRegQrCodeEmailAccount,
        RetryAllowed,
        RetryDelay
    }

    public enum WebAppSettings
    {
        PageSize,
        ServiceEndpoint,
    }

    public enum OodleExportCategories
    {
        [OodleRootName("Cars & Vehicles")]
        Cars,
        [OodleRootName("Real Estate")]
        RealEstateSales,
        [OodleRootName("Property For Rent")]
        RealEstateRentals,
        [OodleRootName("Pets")]
        Pets,
        [OodleRootName("Jobs")]
        Jobs,
        [OodleRootName("Items for Sale")]
        ItemsForSale,
        [OodleRootName("Tickets")]
        Tickets,
        OodleExportCatetgoryCount
    }

    public enum YMMImportValue
    {
        Make,
        Model,
        Year,
        Trim,
        Count
    }

    public enum SamsWorkingReportEnum
    {
        ListingId,
        Division,
        ListingNumber,
        AdOrderNumber,
        AdvertiserName,
        Title,
        Category,
        StartDate,
        ExpireDate,
        AdLength,
        FeedName
    }

    public enum SamsWorkingNoFeedbackReportEnum
    {
        ListingId,
        Division,
        ListingNumber,
        AdOrderNumber,
        AdvertiserName,
        Title,
        Category,
        StartDate,
        ExpireDate,
        AdLength,
        FeedName
    }

    //used for determing source of update
    public enum UpdateFrom
    {
        None,
        ESB,
        ListingRepository
    }

    //used in AdminModificationsFactory
    public enum ModType
    {
        None, 
        Update,
        Delete,
        Insert
    }

    public enum ListingEventType
    {
        NONE,
        CREATE,
        UPDATE
    }
}