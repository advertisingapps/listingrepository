﻿using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;
using Postmedia.ListingRepository.Core.FileIO;

namespace Postmedia.ListingRepository.Core.Translators
{
    public class StreamTranslator<T> where T : class
    {
        //public static T StreamToDTO(Stream content)
        //{
        //    T retVal = null;
        //    if (content != null)
        //    {
        //        var xmlSerializer = new XmlSerializer(typeof (T));
        //        retVal = xmlSerializer.Deserialize(content) as T;
        //    }
        //    return retVal;
        //}

        //public static T StreamElementToDTO(Stream content, string elementName)
        //{
        //    T retVal = null;
        //    if (content != null)
        //    {
        //        var streamReader = new StreamReader(content);
        //        string streamText = streamReader.ReadToEnd();

        //        var document = new XmlDocument();
        //        document.LoadXml(streamText);

        //        //Get the listing from the listing body element
        //        var element = document.DocumentElement;
        //        if (element != null)
        //        {
        //            var node = element.SelectSingleNode("//" + elementName);

        //            if (node != null)
        //            {
        //                //Assuming doc is an XML document containing a serialized object and objType is a System.Type set to the type of the object.
        //                var reader = new XmlNodeReader(node);
        //                var serializer = new XmlSerializer(typeof (T));

        //                // Then you just need to cast obj into whatever type it is eg:
        //                retVal = serializer.Deserialize(reader) as T;
        //            }
        //        }
        //    }
        //    return retVal;
        //}

        public static T XMLToDTO(string xmlString, string elementName)
        {
            T retVal = null;
            if (xmlString != null)
            {
                var document = new XmlDocument();
                document.LoadXml(xmlString);

                //Get the listing from the listing body element
                var element = document.DocumentElement;
                if (element != null)
                {
                    var node = element.SelectSingleNode("//" + elementName);

                    if (node != null)
                    {
                        //Assuming doc is an XML document containing a serialized object and objType is a System.Type set to the type of the object.
                        var reader = new XmlNodeReader(node);
                        var serializer = new XmlSerializer(typeof(T));

                        // Then you just need to cast obj into whatever type it is eg:
                        retVal = serializer.Deserialize(reader) as T;
                    }
                }
            }
            return retVal;
        }
        
        //public static Stream DTOToStream(T dto)
        //{
        //    MemoryStream retVal = null;
        //    if (dto != null)
        //    {
        //        retVal = new MemoryStream();
        //        var writer = new StreamWriter(retVal);
        //        var xmlSerializer = new XmlSerializer(typeof (T));

        //        var namespaces = new XmlSerializerNamespaces();
        //        namespaces.Add(string.Empty, string.Empty);

        //        xmlSerializer.Serialize(writer, dto, namespaces);
        //        retVal.Seek(0, SeekOrigin.Begin);
        //    }
        //    return retVal;
        //}

        //public static string DTOToXMLString(T dto)
        //{
        //    string retVal = null;
        //    var dtoStream = DTOToStream(dto);
        //    if (dtoStream != null)
        //    {
        //        var reader = new StreamReader(dtoStream);
        //        retVal = reader.ReadToEnd(); 
        //    }
        //    return retVal;
        //}
        
        public static string DtoToXml(T dto)
        {
            string inputXML;
            //TODO: Determine which serializer to user
            //var xmlSerializer = new XmlSerializer(typeof (T));
            //var serializer = new DataContractSerializer(typeof(T));
            
            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add("", "");
            var serializer = new XmlSerializer(typeof(T), "");

            using (var backing = new StringWriter())
            {
                //using (var writer = new XmlTextWriter(backing))
                using (var writer = new XmlTextWriterFormattedNoDeclaration(backing))
                {
                    serializer.Serialize(writer, dto, namespaces);
                    //serializer.WriteObject(writer, dto);
                    inputXML = backing.ToString();
                }
            }
            return inputXML;
        }
        public static byte[] DtoToByteXml(T dto)
        {
            var xmlString = DtoToXml(dto);
            var dtoBytes = System.Text.Encoding.UTF8.GetBytes(xmlString);
            return dtoBytes;
        }
        public static T StreamToDto(Stream content)
        {
            T dto = null;
            if (content != null)
            {
                using (var internalReader = new StreamReader(content))
                {
                    using (var reader = new XmlTextReader(internalReader))
                    {
                        // create a datacontractserializer based on our GenericType
                        var xs = new XmlSerializer(typeof (T));
                        //var xs = new DataContractSerializer(typeof(T));

                        //deserialize the object to our response object
                        dto = xs.Deserialize(reader) as T;
                        //dto = xs.ReadObject(reader) as T;
                    }
                }
            }
            return dto;
        }
        
    }
}