﻿using System;
using System.Configuration;
using System.Threading;
using log4net;

namespace Postmedia.ListingRepository.Core.Wrapper
{
    public static class Retry
    {
        
        private static readonly int RetryAllowed = int.Parse(ConfigurationManager.AppSettings[ServiceSettings.RetryAllowed.ToString()]);
        private static readonly int RetryDelay = int.Parse(ConfigurationManager.AppSettings[ServiceSettings.RetryDelay.ToString()]);

        public static T Do<T>(Func<T> action, ILog log)
        {
            return Do(action, RetryDelay, RetryAllowed, log); 
        }

        public static T Do<T>(Func<T> action, int retryDelay, int retryAllowed, ILog log)
        {
            int retryCount = 1;
            do
            {
                try
                {
                    log.Debug("Attempt:" + retryCount);
                    return action();
                }
                catch (Exception ex)
                {
                    Interlocked.Increment(ref retryCount);
                    Thread.Sleep(retryDelay);
                    if (retryCount <= retryAllowed)
                    {
                        log.ErrorFormat(action.GetType() + ": The following problem has occurred: {0}" + "\r\n Attempting to retry!", ex);
                    }
                    else
                    {
                        throw;
                    }
                }
            } while (retryCount <= retryAllowed);
            return default(T);
        }
    }
}
