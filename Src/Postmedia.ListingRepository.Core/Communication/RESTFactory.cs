﻿using System.Collections.Generic;
using System.Net;
using Postmedia.ListingRepository.Core.EnumExtensions.RESTMethodExtensions;

namespace Postmedia.ListingRepository.Core.Communication
{
    public static class RESTFactory
    {
        public static WebRequest CreateRequest(string url)
        {
            var request = WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/xml";

            return request;
        }

        public static WebRequest CreateRequest(string baseUrl, RESTMethod method)
        {
            var request = WebRequest.Create(baseUrl + method.GetMethod());

            request.Method = method.GetWebMethod();
            request.ContentType = "application/xml";
            
            return request;
        }

        public static WebRequest CreateRequest(string baseUrl, RESTMethod method, int timeout)
        {
            var request = WebRequest.Create(baseUrl + method.GetMethod());
            
            request.Method = method.GetWebMethod();
            request.ContentType = "application/xml";

            request.Timeout = timeout*60000;
            
            return request;
        }

        public static WebRequest CreateRequest(string baseUrl, int id, RESTMethod method)
        {
            var request = WebRequest.Create(baseUrl + method.GetMethod() + "/" + id);

            request.Method = method.GetWebMethod();
            request.ContentType = "application/xml";

            return request;
        }

        public static WebRequest CreateRequest(string baseUrl, string id, RESTMethod method)
        {
            var request = WebRequest.Create(baseUrl + method.GetMethod() + "/" + id);

            request.Method = method.GetWebMethod();
            request.ContentType = "application/xml";

            return request;
        }

        public static WebRequest CreateRequest(string baseUrl, IDictionary<string, string> keyValuePairs, RESTMethod method)
        {
            var queryString = CreateQueryStringFromDictionary(keyValuePairs);
            var request = WebRequest.Create(baseUrl + method.GetMethod() + queryString);

            request.Method = method.GetWebMethod();
            request.ContentType = "application/xml";

            return request;
        }

        public static string CreateQueryStringFromDictionary(IDictionary<string, string> dictionary)
        {
            var queryString = string.Empty;
            if (dictionary.Count > 0)
            {
                var keyIndex = 0;
                foreach (var keyValuePair in dictionary)
                {
                    queryString += keyIndex == 0 ? "?" : "&";
                    queryString += keyValuePair.Key;
                    queryString += "=" + keyValuePair.Value;
                    keyIndex++;
                }
            }
            return queryString;
        }
    }
}
