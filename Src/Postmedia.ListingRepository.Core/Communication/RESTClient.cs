﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml;
using Microsoft.Http;
using Postmedia.ListingRepository.Core.EnumExtensions.RESTMethodExtensions;
using Postmedia.ListingRepository.Core.Translators;
using log4net;


namespace Postmedia.ListingRepository.Core.Communication
{
    public class RESTClient 
    {
        private RESTClient() {}
        private static RESTClient _instance;
        private static void ConfigureLogger()
        {
            //get the only embedded resource and convert it to a memory stream
            var stream =
                Assembly.GetCallingAssembly().GetManifestResourceStream("App.config");
            //Assembly.GetCallingAssembly().GetManifestResourceNames()[0]);

            //pass the core specific configuration to log4net
            log4net.Config.XmlConfigurator.Configure(stream);

            //create a new instance of the logger to be used by the singleton
            Logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        }
        private static ILog Logger { get; set; }

        public static string BaseUrl { get; set; }   

        public static RESTClient Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new RESTClient();
                    ConfigureLogger();
                }
                return _instance;
            }
        }

        /// <summary>
        /// Executes a method where the success / failure is all we care about
        /// should be used for batch processes / etc. on the server where output
        /// metrics would be recorded in the database / etc.
        /// </summary>
        /// <param name="method">RESTApi enum</param>
        /// <returns>Boolean representing success / failure of the execution</returns>
        public bool Execute(RESTMethod method)
        {
            var executionSuccessful = false;
            try
            {
                using (var client = new HttpClient(BaseUrl))
                {
                    int timeout ;
                    int.TryParse(ConfigurationManager.AppSettings[ServiceSettings.ServiceTimeout.ToString()], out timeout);
                    client.TransportSettings.ConnectionTimeout = new TimeSpan(0, timeout, 0);

                    using (var response = client.Get(method.GetMethod()))
                    {
                        switch (response.StatusCode)
                        {
                            case HttpStatusCode.OK:
                                executionSuccessful = true;
                                break;
                            case HttpStatusCode.NotFound:
                                throw new EndpointNotFoundException("Could not find a service endpoint at " + BaseUrl + method.GetMethod());
                        }
                        
                    }
                }
            }
            catch (Exception e)
            {
                var message = "An Error Occured: "
                              + Environment.NewLine
                              + Environment.NewLine
                              + e.Message
                              + Environment.NewLine
                              + Environment.NewLine
                              + e.StackTrace
                              + Environment.NewLine
                              + Environment.NewLine
                              + (e.InnerException != null ? (e.InnerException.Message) : string.Empty);
                Logger.Error(message + " --> " + this.GetType());
                throw;
            }
            return executionSuccessful;
        }
    
        /// <summary>
        /// Executes a method where the success / failure is contained within the 
        /// TOutputType object returned
        /// </summary>
        /// <typeparam name="TOutputType">The return object TYPE (should be of type DTO.XXX - eg: DTO.Listing)</typeparam>
        /// <param name="method">RESTApi enum</param>
        /// <returns>Instance of TOutputType</returns>
        public TOutputType Call<TOutputType>(RESTMethod method) where TOutputType : class 
        {
            TOutputType responseObj;
            try
            {
                var request = RESTFactory.CreateRequest(BaseUrl, method, 10);
                var response = GetData(request);

                var responseStream = HandleResponse(response);
                responseObj = StreamTranslator<TOutputType>.StreamToDto(responseStream);
            }
            catch (Exception e)
            {
                var message = "An Error Occured: "
                              + Environment.NewLine + e.Message
                              + Environment.NewLine+ e.StackTrace
                              + Environment.NewLine
                              + (e.InnerException != null ? (e.InnerException.Message) : string.Empty);
                Logger.Error(message + " --> " + GetType());
                throw;
            }

            return responseObj;
        }

        /// <summary>
        /// Executes any method against an ID of a particular entity
        /// Should be used for Deletes and FetchById calls
        /// </summary>
        /// <typeparam name="TOutputType">The return object TYPE (should be of type DTO.XXX - eg: DTO.Listing)</typeparam>
        /// <param name="id">The integer Id of the object to perform "method" on</param>
        /// <param name="method">RESTApi enum</param>
        /// <returns>Instance of DTO.XXX</returns>
        public TOutputType Call<TOutputType>(int id, RESTMethod method) 
            where TOutputType : class
        {
            return Call<TOutputType>(id.ToString(), method);
        }

        /// <summary>
        /// Executes any method against an ID of a particular entity
        /// Should be used for Deletes and FetchById calls
        /// </summary>
        /// <typeparam name="TOutputType">The return object TYPE (should be of type DTO.XXX - eg: DTO.Listing)</typeparam>
        /// <param name="id">The integer Id of the object to perform "method" on</param>
        /// <param name="method">RESTApi enum</param>
        /// <returns>Instance of DTO.XXX</returns>
        public TOutputType Call<TOutputType>(string id, RESTMethod method)
            where TOutputType : class
        {
            string url = method.GetMethod() + "/" + id;
            return Call<TOutputType>(url);
        }







        /// <summary>
        /// Internal - used to send a customized URL to the send data method
        /// </summary>
        /// <typeparam name="TOutputType"></typeparam>
        /// <param name="url"></param>
        /// <returns></returns>
        private TOutputType Call<TOutputType>(string url) 
            where TOutputType : class
        {
            TOutputType responseObj;

            try
            {
                var request = RESTFactory.CreateRequest(BaseUrl + url);
                var response = GetData(request);

                var responseStream = HandleResponse(response);
                responseObj = StreamTranslator<TOutputType>.StreamToDto(responseStream);
            }
            catch (Exception e)
            {
                var message = "An Error Occured: "
                              + Environment.NewLine + e.Message
                              + Environment.NewLine + e.StackTrace
                              + Environment.NewLine +
                              (e.InnerException != null ? (e.InnerException.Message) : string.Empty);
                Logger.Error(message + " --> " + GetType());
                throw;
            }

            return responseObj;
        }








        /// <summary>
        /// Executes any method against an unspecified number of query string values
        /// of a particular entity. This could be used for Deletes or
        /// Fetch type calls if you have the ID value already stored in a dictionary 
        /// </summary>
        /// <typeparam name="TOutputType">The return object TYPE (should be of type Model.type - eg: Listing)</typeparam>
        /// <param name="input"> A dictionary of values to add to the querystring</param>
        /// <param name="method">RESTApi enum</param>
        /// <returns>Instance of DTO.XXX</returns>
        public TOutputType Call<TOutputType>(IDictionary<string, string> input, RESTMethod method) 
            where TOutputType : class
        {
            TOutputType responseObj;
            try
            {
                var client = new WebClient { BaseAddress = BaseUrl };
                var url = method.GetMethod() + RESTFactory.CreateQueryStringFromDictionary(input);
                var responseString = client.DownloadString(url);

                using (var internalReader = new StringReader(responseString))
                {
                    using (var reader = new XmlTextReader(internalReader))
                    {
                        var xs = new DataContractSerializer(typeof(TOutputType));
                        responseObj = xs.ReadObject(reader) as TOutputType;
                    }
                }
            }
            catch (Exception e)
            {
                var message = "An Error Occured: "
                              + Environment.NewLine + e.Message
                              + Environment.NewLine + e.StackTrace 
                              + Environment.NewLine + (e.InnerException != null ? (e.InnerException.Message) : string.Empty);
                Logger.Error(message + " --> " + GetType());
                throw;
            }

            return responseObj;
        }

        /// <summary>
        /// Used to POST data to the service and provide the response
        /// </summary>
        /// <typeparam name="TInputType">The input TYPE (should be typeof(DTO.XXX) - eg: DTO.Criteria / etc.)</typeparam>
        /// <typeparam name="TOutputType">The return TYPE (should be of type typeof(DTO.XXX) - eg: DTO.Listing)</typeparam>
        /// <param name="input">The input object (An instance of ListingCriteria - the actual object)</param>
        /// <param name="method">RESTApi enum</param>
        /// <returns>The output object (An instance of type TOutputType)</returns>
        public TOutputType Call<TInputType, TOutputType>(TInputType input, RESTMethod method)
            where TInputType : class
            where TOutputType : class
        {
            TOutputType responseObj;

            try
            {
                var request = RESTFactory.CreateRequest(BaseUrl, method);
                var inputXML = StreamTranslator<TInputType>.DtoToXml(input);

                var response = SendData(request, inputXML);
                var responseStream = HandleResponse(response);

                responseObj = StreamTranslator<TOutputType>.StreamToDto(responseStream);
            }
            catch (Exception e)
            {
                var message = "An Error Occured: "
                              + Environment.NewLine + e.Message
                              + Environment.NewLine + e.StackTrace
                              + Environment.NewLine +
                              (e.InnerException != null ? (e.InnerException.Message) : string.Empty);
                Logger.Error(message + " --> " + GetType());
                throw;
            }

            return responseObj;
        }

        /// <summary>
        /// Used to POST data to the service for updates and provide the response (updated object)
        /// </summary>
        /// <typeparam name="TInputType">The input TYPE (should be typeof(DTO.XXX) - eg: DTO.Criteria / etc.)</typeparam>
        /// <typeparam name="TOutputType">The return TYPE (should be of type typeof(DTO.XXX) - eg: DTO.Listing)</typeparam>
        /// <param name="input">The input object (An instance of ListingCriteria - the actual object)</param>
        /// <param name="id">The ID to be used as part of the lookup for the object being updated (ie: ListingID)</param>
        /// <param name="method">RESTApi enum</param>
        /// <returns>The output object (An instance of type TOutputType)</returns>
        public TOutputType Call<TOutputType, TInputType>(TInputType input, int id, RESTMethod method)
            where TInputType : class
            where TOutputType : class
        {

            TOutputType responseObj;
            try
            {
                var request = RESTFactory.CreateRequest(BaseUrl, id, method);
                var inputXML = StreamTranslator<TInputType>.DtoToXml(input);

                var response = SendData(request, inputXML);
                var responseStream = HandleResponse(response);

                responseObj = StreamTranslator<TOutputType>.StreamToDto(responseStream);
            }
            catch (Exception e)
            {
                var message = "An Error Occured: "
                              + Environment.NewLine + e.Message
                              + Environment.NewLine + e.StackTrace
                              + Environment.NewLine + (e.InnerException != null ? (e.InnerException.Message) : string.Empty);
                Logger.Error(message + " --> " + this.GetType());
                throw;
            }
            return responseObj;
        }

        /// <summary>
        /// Used to POST data to the service for updates and provide the response (updated object)
        /// </summary>
        /// <typeparam name="TInputType">The input TYPE (should be typeof(DTO.XXX) - eg: DTO.Criteria / etc.)</typeparam>
        /// <typeparam name="TOutputType">The return TYPE (should be of type typeof(DTO.XXX) - eg: DTO.Listing)</typeparam>
        /// <param name="input">The input object (An instance of ListingCriteria - the actual object)</param>
        /// <param name="id">The string value to be used as part of the lookup for the object being updated (ie - ListingNumber)</param>
        /// <param name="method">RESTApi enum</param>
        /// <returns>The output object (An instance of type TOutputType)</returns>
        public TOutputType Call<TOutputType, TInputType>(TInputType input, string id, RESTMethod method)
            where TInputType : class
            where TOutputType : class
        {

            TOutputType responseObj;
            try
            {
                var request = RESTFactory.CreateRequest(BaseUrl, id, method);
                var inputXML = StreamTranslator<TInputType>.DtoToXml(input);

                var response = SendData(request, inputXML);
                var responseStream = HandleResponse(response);

                responseObj = StreamTranslator<TOutputType>.StreamToDto(responseStream);
            }
            catch (Exception e)
            {
                var message = "An Error Occured: "
                              + Environment.NewLine + e.Message
                              + Environment.NewLine + e.StackTrace
                              + Environment.NewLine + (e.InnerException != null ? (e.InnerException.Message) : string.Empty);
                Logger.Error(message + " --> " + this.GetType());
                throw;
            }
            return responseObj;
        }

        /// <summary>
        /// Used to POST data to the service for updates and provide the response (updated object)
        /// </summary>
        /// <typeparam name="TInputType">The input TYPE (should be typeof(DTO.XXX) - eg: DTO.Criteria / etc.)</typeparam>
        /// <typeparam name="TOutputType">The return TYPE (should be of type typeof(DTO.XXX) - eg: DTO.Listing)</typeparam>
        /// <param name="input">The input object (An instance of ListingCriteria - the actual object)</param>
        /// <param name="values">A dictionary containing the ID (and other query string values) 
        ///                      to be used as part of the lookup for the object being updated</param>
        /// <param name="method">RESTApi enum</param>
        /// <returns>The output object (An instance of type TOutputType)</returns>
        public TOutputType Call<TOutputType, TInputType>(TInputType input, IDictionary<string, string> values, RESTMethod method)
            where TInputType : class
            where TOutputType : class
        {
            TOutputType responseObj;

            try
            {
                var request = RESTFactory.CreateRequest(BaseUrl, values, method);
                var inputXML = StreamTranslator<TInputType>.DtoToXml(input);

                var response = SendData(request, inputXML);
                var responseStream = HandleResponse(response);

                responseObj = StreamTranslator<TOutputType>.StreamToDto(responseStream);
            }
            catch (Exception e)
            {
                var message = "An Error Occured: "
                              + Environment.NewLine + e.Message
                              + Environment.NewLine + e.StackTrace
                              + Environment.NewLine + (e.InnerException != null ? (e.InnerException.Message) : string.Empty);
                Logger.Error(message + " --> " + this.GetType());
                throw;
            }
            return responseObj;
        }


        private HttpWebResponse SendData(WebRequest request, string inputXml)
        {
            using (var sw = new StreamWriter(request.GetRequestStream()))
            {
                Logger.Debug("Writing request xml to stream ");
                sw.Write(inputXml);
            }

            var response = request.GetResponse() as HttpWebResponse;
            return response;
        }
        private HttpWebResponse GetData(WebRequest request)
        {
            Logger.Debug("Getting response from request");
            var response = request.GetResponse() as HttpWebResponse;
            return response;
        }

        private Stream HandleResponse(HttpWebResponse response)
        {
            Stream responseStream = null;
            if (CheckForSuccessfulResponse(response))
            {
                var webResponseStream = response.GetResponseStream();
                if (webResponseStream != null)
                {
                    responseStream= new MemoryStream();
                    // copy the bytes to our local stream 
                    // so we can close the response object
                    webResponseStream.CopyTo(responseStream);
                    //move the stream position to the start
                    responseStream.Position = 0;
                }
                response.Close();
            }
            else
            {
                HandleUnSuccessfulResponse(response);
            }
            return responseStream;
        }
        private void HandleUnSuccessfulResponse(HttpWebResponse response)
        {
            if (response != null)
            {
                // if the response is empty
                if(response.ContentLength == 0)
                {
                    throw new Exception("Response was empty - returned from webservice at: " + response.ResponseUri);
                }

                //check the response code
                switch (response.StatusCode) {}

                //close the response
                response.Close();
            }
            else
            {
                throw new WebException("Did not receive a response from the WebService located at: " + response.ResponseUri);
            }
        }
        private bool CheckForSuccessfulResponse(HttpWebResponse response)
        {
            var success = false;
            if(response != null)
            {
                if (((response.StatusCode == HttpStatusCode.OK) || (response.StatusCode == HttpStatusCode.Created)) 
                    && (response.ContentLength > 0))
                {
                    success = true;
                }
            }
            return success;
        }
    }
}
