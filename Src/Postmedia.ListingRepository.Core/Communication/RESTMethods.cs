﻿using Postmedia.ListingRepository.Core.EnumExtensions.Attributes;

namespace Postmedia.ListingRepository.Core.Communication
{
    public enum RESTMethod
    {
        // lookups
        [ServiceCall(UriTemplate = "lookup/divisions", Method = "GET")]
        GetDivisions,

        //[ServiceCall(UriTemplate = "lookup/categories", Method = "GET")]
        //GetCategories,

        [ServiceCall(UriTemplate = "lookup/publications", Method = "GET")]
        GetPublications,

        [ServiceCall(UriTemplate = "lookup/sources", Method = "GET")]
        GetSourceSystems,

        [ServiceCall(UriTemplate = "lookup/statuses", Method = "GET")]
        GetListingStatuses,

        [ServiceCall(UriTemplate = "lookup/upsellvalues", Method = "GET")]
        GetUpsellValues,


        [ServiceCall(UriTemplate = "metadata/updatecategories", Method = "GET")]
        UpdateCategories,

        [ServiceCall(UriTemplate = "metadata/updateymm", Method = "GET")]
        UpdateYearMakeModel,

        [ServiceCall(UriTemplate = "listingsummaries", Method = "GET")]
        GetAllListingSummaries,

        [ServiceCall(UriTemplate = "listings/search", Method = "POST")]
        GetListingSummariesUsingCriteria,

        [ServiceCall(UriTemplate = "listings/pagedsearch", Method = "POST")]
        GetPagedListingSummariesUsingCriteria,

        [ServiceCall(UriTemplate = "listings", Method = "GET")]
        GetListingById,

        [ServiceCall(UriTemplate = "listings", Method = "GET")]
        GetListingByNumber,
        
        [ServiceCall(UriTemplate = "listings", Method = "PUT")]
        UpdateListing,

        [ServiceCall(UriTemplate = "listings", Method = "POST")]
        CreateListing,
        
        [ServiceCall(UriTemplate = "listings", Method = "POST")]
        DeleteListing,

        [ServiceCall(UriTemplate = "exportcache", Method = "PUT")]
        ExportOutBoundCache,

        [ServiceCall(UriTemplate = "listings/likenumber", Method = "GET")]
        GetListingLikeNumber,

        // reporting methods
        [ServiceCall(UriTemplate = "reports/workingsams", Method = "POST")]
        GetWorkingSamsReport,

        [ServiceCall(UriTemplate = "reports/workingsamsnofeedback", Method = "POST")]
        GetWorkingSamsNoFeedbackReport,

        [ServiceCall(UriTemplate = "reports/qrcodeurl", Method = "POST")]
        GetQRCodeUrlReport,
        
        [ServiceCall(UriTemplate = "reports/workingupsells", Method = "POST")]
        GetWorkingUpsellReport,

        [ServiceCall(UriTemplate = "cache/search", Method = "GET")]
        GetCacheEntriesFromListingNumber,

        [ServiceCall(UriTemplate = "cache/refresh", Method = "GET")]
        RefreshCacheForListingNumber,

        [ServiceCall(UriTemplate = "cache", Method = "GET")]
        GetCacheEntry,

        [ServiceCall(UriTemplate = "reports/generallistingpublication", Method = "POST")]
        GetListingPubReportUsingCriteria,

        [ServiceCall(UriTemplate = "reports/getgeneralfailedlisting", Method = "POST")]
        GetGeneralFailedListing,

        [ServiceCall(UriTemplate = "reports/getdetailedupsellreport", Method = "POST")]
        GetDetailedUpsellReport
    }
}
