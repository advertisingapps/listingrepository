﻿using System;
using Postmedia.ListingRepository.Core.EnumExtensions.Attributes;

namespace Postmedia.ListingRepository.Core.EnumExtensions.OodleExportCategoriesExtensions
{
    public static class OodleExportCategoriesExtensions
    {
        public static string GetOodleRootName(this Enum value)
        {
            var type = value.GetType();
            var fieldInfo = type.GetField(value.ToString());

            // Get the stringvalue attributes
            var attributes = fieldInfo.GetCustomAttributes(
                typeof(OodleRootNameAttribute), false) as OodleRootNameAttribute[];

            // Return the first if there was a match.
            string retVal = null;
            if (attributes != null)
                retVal = attributes.Length > 0 ? attributes[0].RootName : null;

            return retVal;
        }
    }
}
