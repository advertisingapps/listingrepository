﻿using System;
using Postmedia.ListingRepository.Core.EnumExtensions.Attributes;


namespace Postmedia.ListingRepository.Core.EnumExtensions.RESTMethodExtensions
{
    public static class RESTMethodExtensions
    {
        public static string GetMethod(this Enum value)
        {
            var type = value.GetType();
            var fieldInfo = type.GetField(value.ToString());

            // Get the stringvalue attributes
            var attributes = fieldInfo.GetCustomAttributes(
                typeof(ServiceCallAttribute), false) as ServiceCallAttribute[];

            // Return the first if there was a match.
            string retVal = null;
            if (attributes != null)
                retVal = attributes.Length > 0 ? attributes[0].UriTemplate : null;

            return retVal;
        }
       
        public static string GetWebMethod(this Enum value)
        {
            var type = value.GetType();
            var fieldInfo = type.GetField(value.ToString());

            // Get the stringvalue attributes
            var attributes = fieldInfo.GetCustomAttributes(
                typeof(ServiceCallAttribute), false) as ServiceCallAttribute[];

            // Return the first if there was a match.
            string retVal = null;
            if (attributes != null)
                retVal = attributes.Length > 0 ? attributes[0].Method : null;

            return retVal;
        }

        public static Type InputType(this Enum value)
        {
            var type = value.GetType();
            var fieldInfo = type.GetField(value.ToString());

            // Get the stringvalue attributes
            var attributes = fieldInfo.GetCustomAttributes(
                typeof(ServiceCallAttribute), false) as ServiceCallAttribute[];

            // Return the first if there was a match.
            Type retVal = null;
            if (attributes != null)
                retVal = attributes.Length > 0 ? attributes[0].InputType : null;

            return retVal;
        }

        public static Type OutputType(this Enum value)
        {
            var type = value.GetType();
            var fieldInfo = type.GetField(value.ToString());

            // Get the stringvalue attributes
            var attributes = fieldInfo.GetCustomAttributes(
                typeof(ServiceCallAttribute), false) as ServiceCallAttribute[];

            // Return the first if there was a match.
            Type retVal = null;
            if (attributes != null)
                retVal = attributes.Length > 0 ? attributes[0].InputType : null;

            return retVal;
        }
    }
}
