﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Postmedia.ListingRepository.Core.EnumExtensions.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class InputTypeAttribute : Attribute
    {
        public Type InputType { get; protected set; }
        public InputTypeAttribute(Type value)
        {
            InputType = value;
        }
    }
}
