﻿using System;

namespace Postmedia.ListingRepository.Core.EnumExtensions.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ServiceCallAttribute : Attribute
    {
        private string _uriTemplate;
        private string _method;
        private Type _inputType;
        private Type _outputType;

        public virtual string UriTemplate { get { return _uriTemplate; } set { _uriTemplate = value; } }
        public virtual string Method { get { return _method; } set { _method = value; } }
        public virtual Type InputType { get { return _inputType; } set { _inputType = value; } }
        public virtual Type OutputType { get { return _outputType; } set { _outputType = value; } }

        public ServiceCallAttribute()
        {
            _uriTemplate = string.Empty;
            _method = string.Empty;
            _inputType = null;
            _outputType = null;
        }
    }
}