﻿using System;

namespace Postmedia.ListingRepository.Core.EnumExtensions.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ExportTransferFolderAttribute : Attribute
    {
        public string ExportTransferFolder { get; protected set; }
        public ExportTransferFolderAttribute(string value)
        {
            ExportTransferFolder = value;
        }
    }
}