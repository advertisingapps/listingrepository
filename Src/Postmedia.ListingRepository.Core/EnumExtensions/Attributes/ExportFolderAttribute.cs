﻿using System;

namespace Postmedia.ListingRepository.Core.EnumExtensions.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ExportFolderAttribute : Attribute
    {
        public string ExportFolder { get; protected set; }
        public ExportFolderAttribute(string value)
        {
            ExportFolder = value;
        }
    }
}