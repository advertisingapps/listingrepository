using System;

namespace Postmedia.ListingRepository.Core.EnumExtensions.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class OodleRootNameAttribute : Attribute
    {
        public string RootName { get; protected set; }
        public OodleRootNameAttribute(string value)
        {
            RootName = value;
        }
    }
}