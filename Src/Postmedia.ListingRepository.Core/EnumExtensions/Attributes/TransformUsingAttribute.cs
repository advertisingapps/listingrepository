using System;

namespace Postmedia.ListingRepository.Core.EnumExtensions.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class TransformUsingAttribute : Attribute
    {
        public string XsltName { get; protected set; }
        public TransformUsingAttribute(string value)
        {
            XsltName = value;
        }
    }
}