﻿using System;

namespace Postmedia.ListingRepository.Core.EnumExtensions.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class OutputTypeAttribute : Attribute
    {
        public Type OutputType { get; protected set; }
        public OutputTypeAttribute (Type value)
        {
            OutputType = value;
        }
    }
}
