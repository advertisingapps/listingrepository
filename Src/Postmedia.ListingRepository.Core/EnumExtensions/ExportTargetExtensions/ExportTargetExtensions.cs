﻿using System;
using Postmedia.ListingRepository.Core.EnumExtensions.Attributes;

namespace Postmedia.ListingRepository.Core.EnumExtensions.ExportTargetExtensions
{
    public static class ExportTargetExtensions
    {
        public static string GetXsltResourceName(this Enum value)
        {
            var type = value.GetType();
            var fieldInfo = type.GetField(value.ToString());

            // Get the stringvalue attributes
            var attributes = fieldInfo.GetCustomAttributes(
                typeof(TransformUsingAttribute), false) as TransformUsingAttribute[];

            // Return the first if there was a match.
            string retVal = null;
            if (attributes != null)
                retVal = attributes.Length > 0 ? attributes[0].XsltName : null;

            return retVal;
        }
    }
}
