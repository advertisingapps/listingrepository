﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using log4net;

namespace Postmedia.ListingRepository.Core.EMail
{
    public static class MailService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Sends the provided message using the app / web config specified mail server
        /// </summary>
        /// <param name="message">The Email message to send</param>
        public static void SendNotificationEmail(MailMessage message)
        {
            try
            {
                using (var client = new SmtpClient("smtp.postmedia.com", 25))
                {
                    client.Send(message);
                }
            }
            catch (SmtpException smtpException)
            {
                // handle exception here
                Log.Info("SMTPError sending email message", smtpException);
            }
            catch (Exception ex)
            {
                // handle exception here
                Log.Info("Error sending email message", ex);
            }
        }

        public static MailMessage CreateMessage(string to, string from, string body, string subject)
        {
            return CreateMessage(to, from, body, subject, false);
        }

        /// <summary>
        /// Used to create a MailMessage object from string variables
        /// </summary>
        /// <param name="to">Comma separated list of string email addresses</param>
        /// <param name="from">string representing the sender's email addresses</param>
        /// <param name="body">Simple text for email body</param>
        /// <param name="subject">Simple text for email subject</param>
        /// <param name="isBodyHtml">Boolean flag for HTML contents</param>
        /// <returns></returns>
        public static MailMessage CreateMessage(string to, string from, string body, string subject, bool isBodyHtml)
        {
            MailAddress toAddress = null;

            var ccAddresses = new MailAddressCollection();
            var splitAddresses = to.Split(',');
            int ccAddressCount = 0;
            foreach (var address in splitAddresses)
            {
                if (ccAddressCount == 0)
                    toAddress = new MailAddress(address);
                else
                    ccAddresses.Add(new MailAddress(address));
                
                ccAddressCount++;
            }

            var fromAddress = new MailAddress(from);

            var message = new MailMessage(fromAddress, toAddress)
                              {
                                  Subject = subject,
                                  Body = body
                              };

            message.ReplyToList.Add(fromAddress);
            message.IsBodyHtml = isBodyHtml;

            foreach (var address in ccAddresses)
            {
                message.CC.Add(address);
            }
            return message;
        }
    }
}
