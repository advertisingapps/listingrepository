﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Postmedia.ListingRepository.DTO
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class OodleImportSummary
    {
        [XmlElement(ElementName = "numberOfHeaders")]
        [DataMember(Name = "numberOfHeader")]
        public int numberOfHeaders { get; set; }
        [XmlElement(ElementName = "numberOfItems")]
        [DataMember(Name = "numberOfItems")]
        public int numberOfItems { get; set; }
        [XmlElement(ElementName = "importUrl")]
        [DataMember(Name = "importUrl")]
        public string importUrl { get; set; }
    }
}
