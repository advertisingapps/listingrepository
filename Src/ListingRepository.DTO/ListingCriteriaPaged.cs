﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Postmedia.ListingRepository.DTO
{
    [Serializable]
    [DataContract(Namespace="")]
    public class ListingCriteriaPaged
    {
        [XmlElement]
        public string Division { get; set; }
        [XmlElement]
        public string ListingNumber { get; set; }
        [XmlElement]
        public string Title { get; set; }
        [XmlElement]
        public string Source { get; set; }
        [XmlElement]
        public string Account { get; set; }
        [XmlElement]
        public DateTime? Date { get; set; }

        [XmlElement]
        public int PageSize { get; set; }
        [XmlElement]
        public int Page { get; set; }
        [XmlElement]
        public string Sort { get; set; }
        [XmlElement]
        public bool Ascending { get; set; }
    }
}
