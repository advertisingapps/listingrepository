﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Postmedia.ListingRepository.DTO.Reports.Working
{
    [DataContract(Namespace = "")]
    [Serializable]
    public class GeneralFailedListingEvent
    {
        [DataMember]
        [XmlElement]
        public string EventId { get; set; }
        [DataMember]
        [XmlElement]
        public string FeedName { get; set; }
        [DataMember]
        [XmlElement]
        public string ListingNumber { get; set; }
        [DataMember]
        [XmlElement]
        public string EventTime { get; set; }
        [DataMember]
        [XmlElement]
        public string StartDate { get; set; }
        [DataMember]
        [XmlElement]
        public string ExpireDate { get; set; }
        [DataMember]
        [XmlElement]
        public string Status { get; set; }
        [DataMember]
        [XmlElement]
        public string EventType { get; set; }
        [DataMember]
        [XmlElement]
        public string EventDescription { get; set; }
    }
}
