﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO.Reports.Working
{
    [Serializable]
    public class UpsellCriteria : ReportEmailBase
    {
        [XmlElement]
        public string Division { get; set; }
        [XmlElement]
        public DateTime? StartDate { get; set; }
        [XmlElement]
        public DateTime? EndDate { get; set; }
    }
}
