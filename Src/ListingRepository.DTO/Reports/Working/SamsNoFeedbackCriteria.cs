﻿using System;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO.Reports.Working
{
    [Serializable]
    public class SamsNoFeedbackCriteria : ReportEmailBase
    {
        [XmlElement]
        public string Division { get; set; }
        [XmlElement]
        public DateTime? Date { get; set; }
    }
}
