﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO.Reports.Working
{
    [Serializable]
    public class TravidiaNoFeedbackCriteria : ReportEmailBase
    {
        [XmlElement]
        public string Division { get; set; }
        [XmlElement]
        public DateTime? Date { get; set; }
    }
}