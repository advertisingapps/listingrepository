namespace Postmedia.ListingRepository.DTO.Reports.Working
{
    public class UpsellCount    
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }
}