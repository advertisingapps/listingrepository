﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Postmedia.ListingRepository.DTO.Reports.Working
{
    [Serializable]
    public class UpsellResult
    {
        [DataMember]
        [XmlElement]
        public string Division { get; set; }

        [DataMember]
        [XmlElement]
        public List<UpsellCount> UpsellCounts { get; set; } 

    }
}
