﻿
using System;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO.Reports.Working
{
    [Serializable]
    public class GeneralFailedListingEventCriteria : ReportEmailBase
    {
        [XmlElement]
        public DateTime? RunDate { get; set; }
    }
}
