﻿using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO.Reports.Working
{
    public class DigitalDisplayResult : ReportWorkingResultsBase
    {
        public string Publication { get; set; }
    }
}
