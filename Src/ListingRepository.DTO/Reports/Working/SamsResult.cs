﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;


namespace Postmedia.ListingRepository.DTO.Reports.Working
{
	public class SamsResult
	{
        [DataMember]
        [XmlElement]
        public int ListingId { get; set; }

        [DataMember]
        [XmlElement]
        public string Division { get; set; }

        [DataMember]
        [XmlElement]
        public string ListingNumber { get; set; }

        [DataMember]
        [XmlElement]
        public string AdOrderNumber { get; set; }

        [DataMember]
        [XmlElement]
        public string AdvertiserName { get; set; }

        [DataMember]
        [XmlElement]
        public string Title { get; set; }

        [DataMember]
        [XmlElement]
        public string Category { get; set; }

        [DataMember]
        [XmlElement]
        public DateTime StartDate { get; set; }

        [DataMember]
        [XmlElement]
        public DateTime ExpireDate { get; set; }

        [DataMember]
        [XmlElement]
        public int AdLength { get; set; }

        [DataMember]
        [XmlElement]
        public string FeedName { get; set; }
    }	
}
