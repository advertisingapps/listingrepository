﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO.Reports.Working
{
    public class QRCodeUrlResult : ReportWorkingResultsBase
    {
        public string Publication { get; set; }
    }
}
