﻿using System;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO.Reports
{
    [Serializable]
    public class DetailedUpsellCriteria : ReportEmailBase
    {
        [XmlElement]
        public DateTime? StartDate1 { get; set; }
        [XmlElement]
        public DateTime? StartDate2 { get; set; }
        [XmlElement]
        public DateTime? ExpireDate1 { get; set; }
        [XmlElement]
        public DateTime? ExpireDate2 { get; set; }
        [XmlElement]
        public string UpsellCode { get; set; }
        [XmlElement]
        public string ListingPubStatus { get; set; }
        [XmlElement]
        public string DivisionCode { get; set; }
        [XmlElement]
        public string PublicationCode { get; set; }
        [XmlElement]
        public string ListingSource { get; set; }
        [XmlElement]
        public long? PublishingDays { get; set; }
    }
}
