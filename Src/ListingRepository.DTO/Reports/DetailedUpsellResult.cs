﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Postmedia.ListingRepository.DTO.Reports
{
    [Serializable]
    public class DetailedUpsellResult
    {
        [DataMember]
        [XmlElement]
        public long ListingId { get; set; }

        [DataMember]
        [XmlElement]
        public long UpsellId { get; set; }
        
        [DataMember]
        [XmlElement]
        public string Source { get; set; }

        [DataMember]
        [XmlElement]
        public string DivisionName { get; set; }
        
        [DataMember]
        [XmlElement]
        public string ListingNumber { get; set; }

        [DataMember]
        [XmlElement]
        public string PublicationName { get; set; }
        
        [DataMember]
        [XmlElement]
        public  string Title { get; set; }

        [DataMember]
        [XmlElement]
        public DateTime? StartDate { get; set; }

        [DataMember]
        [XmlElement]
        public DateTime? ExpireDate { get; set; }

        [DataMember]
        [XmlElement]
        public string Status { get; set; }

        [DataMember]
        [XmlElement]
        public string UpsellName  { get; set; }

        [DataMember]
        [XmlElement]
        public long PublishingDays { get; set; }
    }
}
