﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;


namespace Postmedia.ListingRepository.DTO
{
    [DataContract(Namespace = "")]
    [Serializable]
    public class ListingPublication : DTOBase
    {
        [DataMember]
        [XmlElement(DataType = "date")]
        public DateTime? StartDate { get; set; }

        [DataMember]
        [XmlElement(DataType = "date")]
        public DateTime? ExpireDate { get; set; }

        [DataMember]
        [XmlElement]
        public string Status { get; set; }

        [DataMember(Name = "Categories")]
        [XmlElement(ElementName = "Category")]
        public List<Category> Categories { get; set; }

        [DataMember(Name="Upsells", IsRequired = false)]
        [XmlElement(ElementName = "Upsell")]
        public List<Upsell> Upsells { get; set; }

        [DataMember]
        [XmlElement]
        public Publication Publication { get; set; }
    }
}
