﻿namespace Postmedia.ListingRepository.DTO
{
    public class ExportSummary
    {
        public int SummaryCount { get; set; }
        public bool DailyExport { get; set; }
        public string FeedName { get; set; }
        public long BatchId { get; set; }
    }
}
