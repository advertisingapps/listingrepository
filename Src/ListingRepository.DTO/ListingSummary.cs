﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [DataContract(Namespace = "")]
    [Serializable]
    public class ListingSummary : DTOBase
    {
        [DataMember]
        [XmlElement]
        public string Division { get; set; }

        [DisplayName("Listing #")]
        [DataMember]
        [XmlElement]
        public string ListingNumber { get; set; }
        [DataMember]
        [XmlElement]
        public string Title { get; set; }
        [DataMember]
        [XmlElement]
        public string Source { get; set; }
        [DataMember]
        [XmlElement]
        public string Account { get; set; }

        [DisplayName("Start Date")]
        [DataMember]
        [XmlElement]
        public DateTime StartDate { get; set; }

        [DisplayName("Expiry Date")]
        [DataMember]
        [XmlElement]
        public DateTime ExpireDate { get; set; }

        [DataMember]
        [XmlElement]
        public string Category { get; set; }

    }
}
