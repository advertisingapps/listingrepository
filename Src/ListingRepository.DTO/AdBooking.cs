using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [DataContract(Namespace = "")]
    [Serializable]
    public class AdBooking : DTOBase
    {
        [DataMember]
        [XmlElement]
        public string AccountNumber { get; set; }
        [DataMember]
        [XmlElement]
        public string AdOrderNumber { get; set; }
        [DataMember]
        [XmlElement]
        public string AgencyAccountNumber { get; set; }
        [DataMember]
        [XmlElement]
        public string ExternalAccountNumber { get; set; }
        [DataMember]
        [XmlElement]
        public string ExternalAdOrderNumber { get; set; }
        [DataMember]
        [XmlElement]
        public Division Division { get; set; }
    }
}