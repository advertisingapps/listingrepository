﻿using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    public class Media : DTOBase
    {
        public string Url { get; set; }
        public string MimeType { get; set; }
        public string Role { get; set; }
    }
}
