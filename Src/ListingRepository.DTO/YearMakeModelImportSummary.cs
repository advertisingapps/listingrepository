﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Postmedia.ListingRepository.DTO
{
    [Serializable]
    [DataContract(Namespace = "")]
    public class YearMakeModelImportSummary
    {
        [DataMember]
        [XmlElement]
        public int TotalNumberOfRecords { get; set; }
        [DataMember]
        [XmlElement]
        public int NumberOfRecordsImported { get; set; }
        [DataMember]
        [XmlElement]
        public string ImportSourceLocation { get; set; }
        [DataMember]
        [XmlElement]
        public int YearCount { get; set; }
        [DataMember]
        [XmlElement]
        public int MakeCount { get; set; }
        [DataMember]
        [XmlElement]
        public int ModelCount { get; set; }
        [DataMember]
        [XmlElement]
        public bool Success { get; set; }
        [DataMember]
        [XmlElement]
        public string FailureReason { get; set; }

        public string FailureReasonDisplay 
        {
            get 
            {
                return !Success ? "Failure Reason: " + FailureReason : string.Empty; 
            }
        }
    }
}
