﻿using System;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [Serializable]
    public class ListingPubCriteria : ReportEmailBase
    {
        [XmlElement]
        public string Division { get; set; }
        [XmlElement]
        public string AccountNumber { get; set; }
        [XmlElement]
        public string ListingNumber { get; set; }
        [XmlElement]
        public string ListingSource { get; set; }
        [XmlElement]
        public string ListingCaption { get; set; }
        [XmlElement]
        public string ListingText { get; set; }
        [XmlElement]
        public string AdOrderNumber { get; set; }
        [XmlElement]
        public DateTime? ListingPubDate { get; set; }
        [XmlElement]
        public string ListingPubStatus { get; set; }
        [XmlElement]
        public string ListingPubCategory { get; set; }
        [XmlElement]
        public string PublicationName { get; set; }
        [XmlElement]
        public string FeedName { get; set; }
        [XmlElement]
        public string UpsellName { get; set; }
        [XmlElement]
        public string UpsellCode { get; set; }
        [XmlElement]
        public string PropertyCode { get; set; }
        [XmlElement]
        public string PropertyValue { get; set; }

        [XmlElement]
        public DateTime? ListingPubStartDate { get; set; }
        [XmlElement]
        public DateTime? ListingPubExpireDate { get; set; }
    }
}
