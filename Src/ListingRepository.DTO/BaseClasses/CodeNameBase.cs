using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Postmedia.ListingRepository.DTO.BaseClasses
{
    [DataContract(Namespace="")]
    [Serializable]
    public abstract class CodeNameBase : DTOBase
    {
        [DataMember]
        [XmlElement]
        public string Code { get; set; }
        [DataMember]
        [XmlElement]
        public string Name { get; set; }
    }
}