﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Postmedia.ListingRepository.DTO.BaseClasses
{
    public abstract class ReportWorkingResultsBase
    {
        [DataMember]
        [XmlElement]
        public string Division { get; set; }

        [DataMember]
        [XmlElement]
        public string ListingNumber { get; set; }
        
        [DataMember]
        [XmlElement]
        public string Title { get; set; }

        [DataMember]
        [XmlElement]
        public string Source { get; set; }

        [DataMember]
        [XmlElement]
        public DateTime StartDate { get; set; }

        [DataMember]
        [XmlElement]
        public DateTime ExpireDate { get; set; }

        [DataMember]
        [XmlElement]
        public string AdOrderNumber { get; set; }

        [DataMember]
        [XmlElement]
        public string ExternalAccount { get; set; }

        [DataMember]
        [XmlElement]
        public string Account { get; set; }

        [DataMember]
        [XmlElement]
        public string AdvertiserName { get; set; }

        [DataMember]
        [XmlElement]
        public string CompanyName { get; set; }

        [DataMember]
        [XmlElement]
        public string Category { get; set; }

        [DataMember]
        [XmlElement]
        public string Description { get; set; }

        public int AdLength
        {
            get 
            {
                int retVal = 0;
                if (!String.IsNullOrEmpty(Description))
                { 
                    retVal = Description.Length; 
                }
                return retVal;
            }
        }

        [DataMember(IsRequired = false)]
        [XmlElement(ElementName = "Media")]
        public List<Media> MediaItems { get; set; }
    
    }
}
