using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Postmedia.ListingRepository.DTO.BaseClasses
{
    [DataContract(Namespace="")]
    [Serializable]
    public abstract class LookupBase : DTOBase
    {
        [DataMember]
        [XmlElement]
        public string Value { get; set; }
        [DataMember]
        [XmlElement]
        public string SortSequence { get; set; }
    }
}