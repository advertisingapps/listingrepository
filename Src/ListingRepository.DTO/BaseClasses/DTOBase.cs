﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Postmedia.ListingRepository.DTO.BaseClasses
{
    [DataContract(Namespace = "")]
    [Serializable]
    public abstract class DTOBase
    {
        [DataMember(IsRequired = false)]
        [XmlElement]
        public long Id { get; set; }
    }
}
