﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Postmedia.ListingRepository.DTO.BaseClasses
{
    public abstract class ReportEmailBase
    {
        [DataMember]
        [XmlElement]
        public bool EmailResults { get; set; }

        [DataMember]
        [XmlElement]
        public string EmailAddress { get; set; }
    }
}
