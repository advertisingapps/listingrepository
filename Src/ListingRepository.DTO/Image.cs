using System.Runtime.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [DataContract(Namespace="")]
    public class Image : DTOBase
    {
        [DataMember]
        public string Url { get; set; }
    }
}