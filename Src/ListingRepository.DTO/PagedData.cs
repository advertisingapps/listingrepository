﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.DTO
{
    public class PagedData<TDto>
    {
        public int Page { get; set; }
        public string Sort { get; set; }
        public int RowCount { get; set; }
        public Enumerations.SortDirection SortDirection { get; set; }
        
        public List<TDto> Items { get; set; }
    }
}
