﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [DataContract(Namespace = "")]
    [Serializable]
    public class Publication : CodeNameBase
    {
        [DataMember(IsRequired = false)]
        [XmlElement(ElementName = "Category")]
        public List<Category> Categories { get; set; }
        
    }
}
