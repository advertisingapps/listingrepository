﻿using System;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    public class CacheEntry : DTOBase
    {
        public virtual string FeedName { get; set; }
        public virtual string ListingNumber { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? ExpireDate { get; set; }
        public virtual DateTime? LastModifiedDate { get; set; }
        public virtual string CacheData { get; set; }
    }
}
