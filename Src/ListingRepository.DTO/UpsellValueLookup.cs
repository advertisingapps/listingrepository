﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [DataContract(Namespace = "")]
    [Serializable]
    public class UpsellValueLookup : LookupBase
    {
        [DataMember]
        [XmlElement]
        public string Code { get; set; }
    }
}
