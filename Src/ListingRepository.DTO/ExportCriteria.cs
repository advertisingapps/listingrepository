﻿using System;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace Postmedia.ListingRepository.DTO
{
    [Serializable]
    public class ExportCriteria
    {
        [XmlElement]
        public string FeedName { get; set; }
        
        [XmlElement]
        public DateTime? LastRunDate { get; set; }
        public bool ShouldSerializeLastRunDate()
        {
            return LastRunDate.HasValue;
        }
        
        [XmlElement]
        public DateTime? RunDate { get; set; }
        public bool ShouldSerializeRunDate()
        {
            return RunDate.HasValue;
        }
        
        [XmlElement]
        public bool DailyExport { get; set; }
    }
}
