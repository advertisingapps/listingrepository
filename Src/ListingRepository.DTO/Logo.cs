using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [DataContract(Namespace="")]
    [Serializable]
    public class Logo : DTOBase
    {
        [DataMember]
        [XmlElement]
        public string Url { get; set; }
    }
}