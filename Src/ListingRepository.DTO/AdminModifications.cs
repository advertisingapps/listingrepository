﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [DataContract(Namespace = "")]
    [Serializable]
    public class AdminModifications:DTOBase
    {
        //[DataMember]
        //[XmlElement]
        //public int Id { get; set; }
        [DataMember]
        [XmlElement]
        public string ListingNumber { get; set; }
        [DataMember]
        [XmlElement]
        public string Field { get; set; }
        [DataMember]
        [XmlElement]
        public DateTime DateModified { get; set; }
        [DataMember]
        [XmlElement]
        public string  ModType{ get; set; }
    }
}
