﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [DataContract(Namespace = "")]
    [Serializable]
    public class ListingEvent: DTOBase
    {
        [DataMember]
        [XmlElement]
        public string ListingNumber { get; set; }
        [DataMember]
        [XmlElement]
        public string FeedName { get; set; }
        [DataMember]
        [XmlElement]
        public string EventDescription { get; set; }
        [DataMember]
        [XmlElement]
        public string EventType { get; set; }
        [DataMember]
        [XmlElement]
        public int RowsAffected { get; set; }
        [DataMember]
        [XmlElement]
        public DateTime EventTime { get; set; }
    }
}
