﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.DTO
{
    public class ListingSummaryPagedData
    {
        public int Page { get; set; }
        public string Sort { get; set; }
        public Enumerations.SortDirection SortDirection { get; set; }
        public int RowCount { get; set; }

        public IEnumerable<ListingSummary> Items { get; set; }
    }
}
