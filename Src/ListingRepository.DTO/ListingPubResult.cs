﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Postmedia.ListingRepository.DTO
{
    [Serializable]
    public class ListingPubResult
    {
        [DataMember]
        [XmlElement]
        public long Id { get; set; }

        [DataMember]
        [XmlElement]
        public long ListingPublicationId { get; set; }

        [DataMember]
        [XmlElement]
        public string Division { get; set; }

        [DataMember]
        [XmlElement]
        public string AccountNumber { get; set; }
        
        [DataMember]
        [XmlElement]
        public string ListingNumber { get; set; }

        [DataMember]
        [XmlElement]
        public string AdOrderNumber { get; set; }

        [DataMember]
        [XmlElement]
        public string FeedName { get; set; }

        [DataMember]
        [XmlElement]
        public string Publication { get; set; }

        [DataMember]
        [XmlElement]
        public DateTime StartDate { get; set; }

        [DataMember]
        [XmlElement]
        public DateTime ExpireDate { get; set; }

        [DataMember]
        [XmlElement]
        public string Status { get; set; }

        [DataMember]
        [XmlElement]
        public string Category { get; set; }

        [DataMember]
        [XmlElement]
        public string Title { get; set; }

        [DataMember]
        [XmlElement]
        public string Source { get; set; }
    }
}
