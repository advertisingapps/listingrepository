using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [DataContract(Namespace = "")]
    [Serializable]
    public class AddressLine : DTOBase
    {
        [DataMember]
        [XmlElement]
        public int AddressId { get; set; }
        [XmlAttribute("LineNumber")]
        [DataMember]
        public int Number { get; set; }
        [XmlText()]
        [DataMember]
        public string LineText { get; set; }
    }
}