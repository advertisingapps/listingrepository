﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [DataContract( Namespace = "")]
    [Serializable]
    public class Listing : DTOBase
    {
        [DataMember]
        [XmlElement(ElementName = "ListingNumber")]
        public string ListingNumber { get; set; }
        [DataMember]
        [XmlElement(ElementName = "Source")]
        public string Source { get; set; }
        [DataMember]
        [XmlElement(ElementName = "AdType")]
        public string AdType { get; set; }
        [DataMember]
        [XmlElement(ElementName = "Title")]
        public string Title { get; set; }
        [DataMember]
        [XmlElement(ElementName = "Description")]
        public string Description { get; set; }
        [DataMember(IsRequired = false)]
        [XmlElement(ElementName = "Updater")]
        public string Updater { get; set; }

        [DataMember(IsRequired = false)]
        [XmlElement]
        public AdBooking AdBooking { get; set; }

        [DataMember(IsRequired = false)]
        [XmlElement]
        public Advertiser Advertiser { get; set; }

        [DataMember(IsRequired = false)]
        [XmlElement(ElementName = "Media")]
        public List<Media> MediaItems { get; set; }

        [DataMember(IsRequired = false)]
        [XmlElement(ElementName = "Property")]
        public List<Property> Properties { get; set; }

        [DataMember(IsRequired = false, Name = "ListingPublications")]
        [XmlElement(ElementName = "ListingPublication")]
        public List<ListingPublication> ListingPublications { get; set; }

        [DataMember(IsRequired = false, Name = "CacheEntries")]
        [XmlElement(ElementName = "CacheEntry")]
        public List<CacheEntry> CacheEntries { get; set; }

        [DataMember(IsRequired = false, Name = "HasAdminLock")]
        public bool HasAdminLock { get; set; }
    }
}
