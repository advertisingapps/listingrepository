using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [DataContract(Namespace = "")]
    [Serializable]
    public class Advertiser : DTOBase
    {
        [DataMember]
        [XmlElement]
        public string Name { get; set; }
        [DataMember]
        [XmlElement]
        public string CompanyName { get; set; }
        [DataMember]
        [XmlElement]
        public string Url { get; set; }

        [XmlElement("Address")]
        [DataMember]
        public Address Address { get; set; }
        [XmlElement("Contact")]
        [DataMember]
        public Contact Contact { get; set; }

        [XmlElement]
        [DataMember]
        public string ExternalId { get; set; }
    }
}