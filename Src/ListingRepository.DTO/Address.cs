using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [DataContract(Namespace="")]
    [Serializable]
    public class Address : DTOBase
    {
        [DataMember]
        [XmlElement(ElementName="AddressLine")]
        public List<AddressLine>  AddressLines { get; set; }
        [DataMember]
        [XmlElement]
        public string City { get; set; }
        [DataMember]
        [XmlElement]
        public string Province { get; set; }
        [DataMember]
        [XmlElement]
        public string PostalCode { get; set; }
        [DataMember]
        [XmlElement]
        public string Country { get; set; }
        [DataMember]
        [XmlElement]
        public string Neighbourhood { get; set; }
        [DataMember]
        [XmlElement]
        public string Region { get; set; }
    }
}