using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Postmedia.ListingRepository.DTO.BaseClasses;

namespace Postmedia.ListingRepository.DTO
{
    [DataContract(Namespace="")]
    [Serializable]
    public class Contact : DTOBase
    {
        [DataMember]
        [XmlElement]
        public string FirstName { get; set; }
        [DataMember]
        [XmlElement]
        public string LastName { get; set; }
        [DataMember]
        [XmlElement]
        public string Email { get; set; }
        [DataMember]
        [XmlElement]
        public string Phone { get; set; }
    }
}