﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class AdBookingMap : ClassMap<AdBooking>
    {
        public AdBookingMap()
        {
            Table("AdBooking");

            Id(x => x.Id, "AdBookingID")
                .GeneratedBy
                .Native();

            Map(x => x.AccountNumber);
            Map(x => x.AdOrderNumber);
            Map(x => x.AgencyAccountNumber);
            Map(x => x.ExternalAccountNumber);
            Map(x => x.ExternalAdOrderNumber);

            References(x => x.Division)
                .Column("DivisionID")
                .Cascade.All();

        }
    }
}