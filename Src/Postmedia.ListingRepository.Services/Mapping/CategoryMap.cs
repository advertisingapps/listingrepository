﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class CategoryMap : ClassMap<Category>
    {
        public CategoryMap()
        {
            Table("Category");
                
            Id(x => x.Id, "CategoryID")
                .GeneratedBy
                .Native();

            References(x => x.Publication)
                .Column("PublicationID");

            HasManyToMany(x => x.ListingPublications)
                .Table("JoinListingPublicationToCategory")
                .ChildKeyColumn("ListingPublicationID")
                .ParentKeyColumn("CategoryID")
                .Cascade.SaveUpdate();
        }
    }
}