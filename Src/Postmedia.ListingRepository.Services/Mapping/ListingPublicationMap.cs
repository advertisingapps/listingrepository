﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class ListingPublicationMap : ClassMap<ListingPublication>
    {
        public ListingPublicationMap()
        {
            Table("ListingPublication");
            Id(x => x.Id, "ListingPublicationID")
                .GeneratedBy
                .Native();

            Map(x => x.ExpireDate);
            Map(x => x.StartDate);
            Map(x => x.Status);

            References(x => x.Listing)
                .Column("ListingID");

            References(x => x.Publication)
                .Column("PublicationID");
                

            //HasMany(x => x.Upsells)
            //    .Cascade.AllDeleteOrphan()
            //    .KeyColumn("ListingPublicationID");

            //HasManyToMany(x => x.Categories)
            //    .Table("JoinListingPublicationToCategory")
            //    .ParentKeyColumn("ListingPublicationID")
            //    .ChildKeyColumn("CategoryID")
            //    .Cascade.SaveUpdate();
        }
    }
}