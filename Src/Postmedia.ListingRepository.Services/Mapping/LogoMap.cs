﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class LogoMap : ClassMap<Logo>
    {
        public LogoMap()
        {
            Table("Logo");

            Id(x => x.Id, "LogoID")
                .GeneratedBy
                .Native();

            Map(x => x.Url);

            References(x => x.Listing)
                .Column("ListingID");

        }
    }
}