﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class ListingMap : ClassMap<Listing>
    {
        public ListingMap()
        {
            Table("Listing");

            Id(x => x.Id, "ListingID")
                .GeneratedBy
                .Native();

            Map(x => x.ListingNumber);
            Map(x => x.Source);
            Map(x => x.Title);
            Map(x => x.Description);
            Map(x => x.AdType);

            //References(x => x.AdBooking)
            //    .Column("AdBookingID")
            //    .Nullable()
            //    .Cascade.All();

            References(x => x.Advertiser)
                .Column("AdvertiserID")
                .Nullable()
                .Cascade.All();

            //HasMany(x => x.Images)
            //    .Cascade.AllDeleteOrphan()
            //    .KeyNullable()
            //    .KeyColumn("ListingID");

            //HasMany(x => x.Logos)
            //    .Cascade.AllDeleteOrphan()
            //    .KeyNullable()
            //    .KeyColumn("ListingID");

            //HasMany(x => x.Properties)
            //  .Cascade.AllDeleteOrphan()
            //  .KeyNullable()
            //  .KeyColumn("ListingID");

            HasMany(x => x.ListingPublications)
                .KeyColumn("ListingID")
                .Cascade.AllDeleteOrphan();

          
        }
    }
}