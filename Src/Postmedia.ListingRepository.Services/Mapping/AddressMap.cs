﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class AddressMap : ClassMap<Address>
    {
        public AddressMap()
        {
            Table("Address");

            Id(x => x.Id, "AddressID")
                .GeneratedBy
                .Native();

            Map(x => x.City);
            Map(x => x.Country);
            Map(x => x.Neighbourhood);
            Map(x => x.PostalCode);
            Map(x => x.Province);
            Map(x => x.Region);

            References(x => x.Advertiser)
                .Column("AdvertiserID")
                .Not.Nullable();

            HasMany(x => x.AddressLines)
                .Cascade.AllDeleteOrphan()
                .KeyColumn("AddressID");
        }
    }
}