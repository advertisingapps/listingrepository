﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class BatchLogItemMap: ClassMap<BatchLogItem>
    {
        public BatchLogItemMap()
        {
            Table("BatchLogItem");

            Id(x => x.Id, "BatchLogItemID")
                .GeneratedBy
                .Native();

            Map(x => x.ListingNumber);
            Map(x => x.OutboundCacheID);

            References(x => x.Batch)
                .Column("BatchLogID");
        }
    }
}