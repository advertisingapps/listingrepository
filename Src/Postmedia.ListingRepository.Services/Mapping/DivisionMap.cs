﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class DivisionMap : ClassMap<Division>
    {
        public DivisionMap()
        {
            Table("Division");

            Id(x => x.Id, "DivisionID")
                .GeneratedBy
                .Native();

            Map(x => x.Code);
            Map(x => x.Name);

        }
    }
}