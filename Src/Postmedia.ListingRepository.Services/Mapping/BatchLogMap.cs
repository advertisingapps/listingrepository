﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class BatchLogMap: ClassMap<BatchLog>
    {
        public BatchLogMap()
        {
            Table("BatchLog");

            Id(x => x.Id, "BatchLogID")
                .GeneratedBy
                .Native();

            Map(x => x.FeedName);
            Map(x => x.TimeStamp);

            HasMany(x => x.LogItems)
              .Cascade.AllDeleteOrphan()
              .KeyColumn("BatchLogID");

        }
    }
}