﻿using System.Linq;
using AutoMapper;

using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public static class AutomapperConfiguration
    {
        public static void Config()
        {

            //Model ===> DTO
            Mapper.CreateMap<Model.Listing, ListingSummary>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.AdBooking != null ? src.AdBooking.AccountNumber : null))
                .ForMember(dest => dest.Division, opt => opt.MapFrom(src => src.AdBooking != null ? src.AdBooking.Division.Name : null))
                .ForMember(dest => dest.ExpireDate
                           , opt => opt.MapFrom(
                               src => src.ListingPublications.Max(r => r.ExpireDate)))
                .ForMember(dest => dest.ListingNumber, opt => opt.MapFrom(src => src.ListingNumber))
                .ForMember(dest => dest.Source, opt => opt.MapFrom(src => src.Source))
                .ForMember(dest => dest.StartDate
                           , opt => opt.MapFrom(
                               src => src.ListingPublications.Min(r => r.StartDate)))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title));

            Mapper.CreateMap<Model.Listing, Listing>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.AdBooking, opt => opt.MapFrom(src => src.AdBooking ?? null))
                .ForMember(dest => dest.AdType, opt => opt.MapFrom(src => src.AdType))
                .ForMember(dest => dest.Advertiser, opt => opt.MapFrom(src => src.Advertiser ?? null))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                //.ForMember(dest => dest.Images, opt => opt.MapFrom(src => src.Images ?? null))
                //.ForMember(dest => dest.Logos, opt => opt.MapFrom(src => src.Logos ?? null))
                .ForMember(dest => dest.ListingNumber, opt => opt.MapFrom(src => src.ListingNumber))
                //.ForMember(dest => dest.ListingPublications, opt => opt.MapFrom(src => src.ListingPublications ?? null))
                //.ForMember(dest => dest.Properties, opt => opt.MapFrom(src => src.Properties ?? null))
                .ForMember(dest => dest.Source, opt => opt.MapFrom(src => src.Source))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title));

            Mapper.CreateMap<Model.ListingPublication, ListingPublication>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Categories, opt => opt.Ignore())
                //.ForMember(dest => dest.Categories, opt => opt.MapFrom(src => src.Categories ?? null))
                .ForMember(dest => dest.ExpireDate, opt => opt.MapFrom(src => src.ExpireDate))
                .ForMember(dest => dest.Publication, opt => opt.MapFrom(src => src.Publication ?? null))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status ?? null));
                //.ForMember(dest => dest.Upsells, opt => opt.MapFrom(src => src.Upsells?? null));

            Mapper.CreateMap<Model.Publication, Publication>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.AspControl, opt => opt.MapFrom(src => src.ASPControl))
                .ForMember(dest => dest.Categories, opt => opt.MapFrom(src => src.Categories))
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

            Mapper.CreateMap<Model.Advertiser, Advertiser>()
              .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
              .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address ?? null))
              .ForMember(dest => dest.Contact, opt => opt.MapFrom(src => src.Contact ?? null));

            Mapper.CreateMap<Model.AdBooking, AdBooking>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Division, opt => opt.MapFrom(src => src.Division))
                .ForMember(dest => dest.AccountNumber, opt => opt.MapFrom(src => src.AccountNumber))
                .ForMember(dest => dest.AdOrderNumber, opt => opt.MapFrom(src => src.AdOrderNumber));

            Mapper.CreateMap<Model.Contact, Contact>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone));

            Mapper.CreateMap<Model.Address, Address>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.AddressLines, opt => opt.MapFrom(src => src.AddressLines ?? null))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.Country))
                .ForMember(dest => dest.PostalCode, opt => opt.MapFrom(src => src.PostalCode))
                .ForMember(dest => dest.Province, opt => opt.MapFrom(src => src.Province));

            Mapper.CreateMap<Model.AddressLine, AddressLine>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.LineText, opt => opt.MapFrom(src => src.LineText))
                .ForMember(dest => dest.Number, opt => opt.MapFrom(src => src.LineNumber));

            Mapper.CreateMap<Model.Division, Division>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));
            Mapper.CreateMap<Model.Category, Category>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));
            Mapper.CreateMap<Model.Upsell, Upsell>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));
            Mapper.CreateMap<Model.Property, Property>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Value)); 
            Mapper.CreateMap<Model.Image, Image>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Url, opt => opt.MapFrom(src => src.Url));
            Mapper.CreateMap<Model.Logo, Logo>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Url, opt => opt.MapFrom(src => src.Url)); 
            Mapper.CreateMap<Model.ASPControl, ASPControl>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

        }
    }
} 