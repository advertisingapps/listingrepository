﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class YearMakeModelMap : ClassMap<YearMakeModel>
    {
        public YearMakeModelMap()
        {
            Table("YearMakeModel");

            Id(x => x.Id, "YearMakeModelID")
                .GeneratedBy
                .Native();

            Map(x => x.Year);
            Map(x => x.Make);
            Map(x => x.Model);
            Map(x => x.AdPerfectID);
        }
    }
}