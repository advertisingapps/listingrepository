﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class AddressLineMap : ClassMap<AddressLine>
    {
        public AddressLineMap()
        {
            Table("AddressLines");

            Id(x => x.Id, "AddressLineID")
                .GeneratedBy
                .Native();

            Map(x => x.LineNumber);
            Map(x => x.LineText);

            References(x => x.Address)
                .Column("AddressID")
                .Nullable();
        }
    }
}