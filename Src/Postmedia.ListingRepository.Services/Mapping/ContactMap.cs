﻿using FluentNHibernate;
using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class ContactMap : ClassMap<Contact>
    {
        public ContactMap()
        {
            Table("Contact");

            Id(x => x.Id, "ContactID")
                .GeneratedBy
                .Native();

            Map(x => x.Email);
            Map(x => x.FirstName);
            Map(x => x.LastName);
            Map(x => x.Phone);

            References(x => x.Advertiser)
                .Column("AdvertiserID")
                .Not.Nullable();
        }
    }
}