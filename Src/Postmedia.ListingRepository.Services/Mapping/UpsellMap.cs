﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class UpsellMap : ClassMap<Upsell>
    {
        public UpsellMap()
        {
            Table("Upsell");

            Id(x => x.Id, "UpsellID")
                .GeneratedBy
                .Native();

            Map(x => x.Code);
            Map(x => x.Name);
        }
    }
}