﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public class ImageMap : ClassMap<Image>
    {
        public ImageMap()
        {
            Table("Image");

            Id(x => x.Id, "ImageID")
                .GeneratedBy
                .Native();

            Map(x => x.Url);

            References(x => x.Listing)
                .Column("ListingID");
        }

    }
}