﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Web;
using IglooCoder.Commons.WcfNhibernate;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.Translators;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.Services.Services;
using log4net;


namespace Postmedia.ListingRepository.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [NHibernateContext]
    public class ListingRepositoryService : IListingRepositoryService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public Stream UpdateCategories()
        {
            Log.Debug("Entered - UpdateCategories");

            var service = new MetadataService();

            Log.Debug("Calling MetadataService.UpdateCategories - UpdateCategories");

            var doc = service.UpdateCategories();

            Log.Debug("Exiting - UpdateCategories");
            return null;
        }

        public Stream UpdateYearMakeModel()
        {
            Log.Debug("Entered - UpdateYearMakeModel");

            var service = new MetadataService();

            Log.Debug("Calling MetadataService.UpdateYearMakeModel - UpdateYearMakeModel");

            service.UpdateYearMakeModel();

            Log.Debug("Exiting - UpdateYearMakeModel");
            return null;
        }

        public Stream GetDivisions()
        {
            Log.Debug("Entered - GetListingSummaries");

            var service = new MetadataService();

            Log.Debug("Calling MetadataService inside GetDivisions");

            var divisions = service.GetDivisions();
            Stream divisionsStream = null;
            if (divisions.Count > 0)
            {
                Log.Debug("Converting List<Divisions> to stream");
                divisionsStream = StreamTranslator<List<Division>>.DTOToStream(divisions);
            }
            else
            {
                WebOperationContext context = WebOperationContext.Current;
                if (context != null)
                    context.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
            }

            Log.Debug("Exiting - GetDivisions");
            return divisionsStream;
        }

        public Stream GetListingSummaries()
        {
            Log.Debug("Entered - GetListingSummaries");

            var service = new ListingService();

            Log.Debug("Calling Service inside");

            var summaries = service.GetAllListingSummaries();
            Stream summaryStream = null;
            if (summaries.Count > 0)
            {
                Log.Debug("Converting List<ListingSummary> to stream");
                summaryStream = StreamTranslator<List<ListingSummary>>.DTOToStream(summaries);
            }
            else
            {
                WebOperationContext context = WebOperationContext.Current;
                if (context != null)
                    context.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
            }

            Log.Debug("Exiting - GetListingSummaries");
            return summaryStream;
        }

        public Stream GetListingSummariesUsingCriteria(Stream content)
        {
            Log.Debug("Entered - GetListingSummariesUsingCriteria");

            var service = new ListingService();

            Log.Debug("Converting stream to ListingCriteria");

            var criteria = StreamTranslator<ListingCriteria>.StreamToDTO(content);

            //TODO:validate criteria
            // validation

            Log.Debug("Calling service.GetLisingSummariesUsingCriteria");

            //perform query based on criteria
            var summaries = service.GetListingSummariesUsingCriteria(criteria);
            Stream summaryStream = null;
            if (summaries.Count > 0)
            {
                Log.Debug("Converting List<ListingSummary> to Stream");
                //return the results of the executed query
                summaryStream = StreamTranslator<List<ListingSummary>>.DTOToStream(summaries);
            }
            else
            {
                WebOperationContext context = WebOperationContext.Current;
                if (context != null)
                    context.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
            }

            Log.Debug("Exiting - GetListingSummariesUsingCriteria");
            return summaryStream;
        }

        public Stream DeleteListing(string listingId)
        {
            throw new NotImplementedException();
        }

        public Stream SaveListing(Stream content)
        {
            var session = new WcfSessionStorage();
            var transaction = session.GetSession().BeginTransaction();

            Log.Debug("Entered - SaveListing");

            Log.Debug("Converting stream to Listing - SaveListing");
            var listing = StreamTranslator<Listing>.StreamElementToDTO(content, "Listing");

            Log.Debug("Creating ListingService");
            var service = new ListingService();
            Log.Debug("Calling Service.SaveOrUpdateListing(listing) - SaveListing");
            service.SaveOrUpdateListing(listing);

            Stream retVal = null;
            if (listing != null)
            {
                Log.Debug("Converting Listing to stream - SaveListing");
                retVal = StreamTranslator<Listing>.DTOToStream(listing);
            }

            Log.Debug("committing the transaction");
            transaction.Commit();

            Log.Debug("Exiting - SaveListing");
            return retVal;
        }

        public Stream RetrieveListing(string listingId)
        {
            Log.Debug("Entered - RetrieveListing");
            Stream retVal = null;
            var service = new ListingService();

            Log.Debug("Parsing listingId");

            int id;
            bool isInteger = int.TryParse(listingId, out id);
            Listing listing = null;
            if (isInteger)
            {
                Log.Debug("Calling service.GetListing - Using ID(" + id + ")");
                listing = service.GetListingById(id);
            }
            else
            {
                Log.Debug("Calling service.GetListing - Using ListingNumber(" + id + ")");
                listing = service.GetListingByNumber(listingId);
            }


            if (listing != null)
            {
                Log.Debug("Converting Listing to stream - RetrieveListing");
                retVal = StreamTranslator<Listing>.DTOToStream(listing); 
            }
            else
            {
                WebOperationContext context = WebOperationContext.Current;
                if (context != null)
                    context.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
            }
            
            Log.Debug("Exiting - RetrieveListing");
            return retVal;
        }

        public Stream ExportOutBoundCache(Stream exportCriteria)
        {
            Log.Debug("Entered - ExportWorkingOutBoundCache");

            var criteria = StreamTranslator<ExportCriteria>.StreamElementToDTO(exportCriteria, "ExportCriteria");

            var service = new ListingService();

            Log.Debug("Calling ListingService inside ExportWorkingOutBoundCache");

            ExportTarget target = GetTargetFromFeedName(criteria.FeedName);

            var exportSummary = service.ExportBatch(target, criteria);
            Stream exportSummaryStream = null;
            if (exportSummary != null)
            {
                Log.Debug("Converting exportSummary to stream - ExportWorkingOutBoundCache");
                exportSummaryStream = StreamTranslator<ExportSummary>.DTOToStream(exportSummary);
            }
            else
            {
                WebOperationContext context = WebOperationContext.Current;
                if (context != null)
                    context.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
            }

            Log.Debug("Exiting - ExportWorkingOutBoundCache");
            return exportSummaryStream;
        }

        private ExportTarget GetTargetFromFeedName(string feedName)
        {
            ExportTarget target;
            switch (feedName.ToLower())
            {
                case "celebrating" :
                    target = ExportTarget.Celebrating;
                    break;
                case "working" :
                    target = ExportTarget.Working;
                    break;
                case "remembering" :
                    target = ExportTarget.Remembering;
                    break;
                default : 
                    target = ExportTarget.AdPerfect;
                    break;
            }
            return target;
        }

        
    }
}

