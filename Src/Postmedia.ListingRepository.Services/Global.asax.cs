﻿using System;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Tool.hbm2ddl;
using Postmedia.ListingRepository.Services.Mapping;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            AutomapperConfiguration.Config();

            NHibernateFactory.Initialize(
                Fluently.Configure()
                    .Database(MsSqlConfiguration.MsSql2008
                                  .ConnectionString(
                                      c => c.FromConnectionStringWithKey("DBConn"))
                                  .ShowSql())
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Listing>())
                    .ExposeConfiguration(cfg => new SchemaExport(cfg).Create(false, false))
                    .BuildSessionFactory()
                );

            HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }


    }
}