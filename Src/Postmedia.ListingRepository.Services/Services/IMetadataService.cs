﻿using System.Collections.Generic;
using System.Xml;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Services.Services
{
    public interface IMetadataService
    {
        List<Division> GetDivisions();
        OodleImportSummary UpdateCategories();
        YearMakeModelImportSummary UpdateYearMakeModel();
    }
}
