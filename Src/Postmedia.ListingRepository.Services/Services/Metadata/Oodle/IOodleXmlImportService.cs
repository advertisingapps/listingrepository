﻿using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Services.Services.Metadata.Oodle
{
    public interface IOodleXmlImportService
    {
        OodleImportSummary ExportOodleRootNodesToXmlConfigurations();
    }
}