using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Xml;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.EnumExtensions;
using Postmedia.ListingRepository.Core.FileIO;
using Postmedia.ListingRepository.DTO;
using log4net;

namespace Postmedia.ListingRepository.Services.Services.Metadata.Oodle
{
    public class OodleXmlImportService : IOodleXmlImportService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private XmlDocument ImportCategories(string importUrl)
        {
            var doc = new XmlDocument();
            doc.Load(importUrl);
            return doc;
        }

        public OodleImportSummary ExportOodleRootNodesToXmlConfigurations()
        {
            OodleImportSummary summary = null;

            var oodleUrl = ConfigurationManager.AppSettings[AppSettings.OodleXmlUrl.ToString()];
            var oodleCategoryList = ImportCategories(oodleUrl);
            
            if((oodleCategoryList != null) && (oodleCategoryList.DocumentElement != null))
            {
                //export the category root nodes to an oodleCategoryExportFile
                var exportDir = ConfigurationManager.AppSettings[AppSettings.GeneraDropDownExportDir.ToString()];
                DirectoryManager.PrepareExportDirectory(exportDir, true);

                var itemCount = 0;
                var headerCount = 0;

                for (int i = 0; i < (int) OodleExportCategories.OodleExportCatetgoryCount; i++)
                {
                    headerCount++;

                    var cat = (OodleExportCategories) Enum.ToObject(typeof (OodleExportCategories), i);

                    var rootNode = (from cNode in oodleCategoryList.DocumentElement.ChildNodes.Cast<XmlNode>()
                                    where cNode.Attributes["name"].Value == cat.GetOodleRootName()
                                    select cNode).FirstOrDefault();

                    if (rootNode != null)
                    {
                        itemCount += rootNode.ChildNodes.Count;

                        var exportDoc = new XmlDocument();
                        exportDoc.AppendChild(exportDoc.CreateXmlDeclaration("1.0", "UTF-8", null));
                        exportDoc.AppendChild(exportDoc.ImportNode(rootNode, true));

                        exportDoc.Save(exportDir + cat.ToString() + ".xml");
                    }
                }

                summary = new OodleImportSummary
                              {
                                  importUrl = oodleUrl, 
                                  numberOfHeaders = headerCount, 
                                  numberOfItems = itemCount
                              };
            }

            return summary;
        }
    }
}