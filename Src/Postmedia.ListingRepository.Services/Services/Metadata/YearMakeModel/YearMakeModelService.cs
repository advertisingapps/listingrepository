using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Xml;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.FileIO;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.Services.Repositories;
using log4net;

namespace Postmedia.ListingRepository.Services.Services.Metadata.YearMakeModel
{
    public class YearMakeModelService : IYearMakeModelService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IYearMakeModelRepository<Model.YearMakeModel> YearMakeModelRepository { get; set; }

        public YearMakeModelService() : this(new YearMakeModelRepository()) {}
        public YearMakeModelService(IYearMakeModelRepository<Model.YearMakeModel> yearMakeModelRepository)
        {
            YearMakeModelRepository = yearMakeModelRepository;
        }

        public YearMakeModelImportSummary ImportYearMakeModels()
        {
            YearMakeModelImportSummary summary = null;
            var years = new Dictionary<string, int>();
            var makes = new Dictionary<string, int>();
            var models = new Dictionary<string, int>();
            
            //clear the previously saved YMM import records
            YearMakeModelRepository.DeleteImportedRecords();

            var importPath = ConfigurationManager.AppSettings[AppSettings.YearMakeModelImportDir.ToString()];
            if (DirectoryManager.ValidateImportDirectory(importPath, true))
            {
                var filePath = Directory.GetFiles(importPath)[0];
                var yearMakeModelStrings = File.ReadAllLines(filePath);

                if (yearMakeModelStrings.Length >= 1)
                {
                    var yearMakeModelString = yearMakeModelStrings[0];
                    var splitYmm = yearMakeModelString.Split(',');

                    if (ValidateFileFormat(splitYmm))
                    {
                        var numberImported = 0;
                        for (var index = 1; index < yearMakeModelStrings.Length; index++)
                        {
                            yearMakeModelString = yearMakeModelStrings[index];
                            splitYmm = yearMakeModelString.Split(',');

                            var ymm = new Model.YearMakeModel
                                          {
                                              AdPerfectID = int.Parse(splitYmm[(int)YMMImportValue.AdPerfectId]),
                                              Year = splitYmm[(int)YMMImportValue.Year].Trim('"'),
                                              Make = splitYmm[(int)YMMImportValue.Make].Trim('"'),
                                              Model = splitYmm[(int)YMMImportValue.Model].Trim('"')
                                          };

                            IncrementDictionaryCount(years, ymm.Year);
                            IncrementDictionaryCount(makes, ymm.Make);
                            IncrementDictionaryCount(models, ymm.Model);

                            YearMakeModelRepository.SaveOrUpdate(ymm);
                            numberImported++;
                        }

                        summary = new YearMakeModelImportSummary
                                      {
                                          TotalNumberOfRecords = yearMakeModelStrings.Length,
                                          NumberOfRecordsImported = numberImported,
                                          YearCount = years.Count,
                                          MakeCount = makes.Count,
                                          ModelCount = models.Count,
                                          ImportSourceLocation = importPath,
                                          Success = true
                                      };
                    }
                    else
                    {
                        //TODO: invalid file format - file format is different than expected
                        //TODO: output message to log
                        //TODO: inform via email regarding failure to import the      
                        summary = new YearMakeModelImportSummary
                                      {
                                          Success = false,
                                          FailureReason = "Import file format invalid"
                                      };
                    }
                }
                else
                {
                    //TODO: Not enough records
                    summary = new YearMakeModelImportSummary
                                  {
                                      Success = false, 
                                      FailureReason = "Import file did not contain any records"
                                  };
                }
            }
            
            return summary;
        }

        public bool ExportYearMakeModelsToXml()
        {
            //TODO: Export the YMM to xml for import to Genera
            var generaYMM = new XmlDocument();
            var declaration = generaYMM.CreateXmlDeclaration("1.0", "UTF-8", null);
            generaYMM.AppendChild(declaration);

            var root = generaYMM.CreateElement("YearMakeModels");
            generaYMM.AppendChild(root);

            var ymms = YearMakeModelRepository.FetchAll();
            foreach (var yearMakeModel in ymms)
            {
                var ymm = generaYMM.CreateElement("YearMakeModel");
                ymm.SetAttribute("Year", yearMakeModel.Year);
                ymm.SetAttribute("Make", yearMakeModel.Make);
                ymm.SetAttribute("Model", yearMakeModel.Model);

                root.AppendChild(ymm);
            }

            var exportFolder = ConfigurationManager.AppSettings[AppSettings.GeneraDropDownExportDir.ToString()];
            generaYMM.Save(exportFolder + "YearMakeModels.xml");

            return true;

        }

        private bool ValidateFileFormat(string[] splitYmm)
        {
            return splitYmm.Length == (int)YMMImportValue.Count;
        }

        private void IncrementDictionaryCount(IDictionary<string, int> dict, string key)
        {
            //update counts
            if (!dict.ContainsKey(key))
                dict.Add(key, 1);
            else
                dict[key]++;
        }
    }
}