﻿using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Services.Services.Metadata.YearMakeModel
{
    public interface IYearMakeModelService
    {
        YearMakeModelImportSummary ImportYearMakeModels();
        bool ExportYearMakeModelsToXml();
    }
}
