﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Xsl;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.EnumExtensions;
using Postmedia.ListingRepository.Core.FileIO;
using Postmedia.ListingRepository.Services.XSLT;

namespace Postmedia.ListingRepository.Services.Services.BatchExport
{
    public class XslTransformService : IXslTransformService
    {
        public void Transform(XmlDocument doc, ExportTarget target, XsltName xslt)
        {
            // apply the transform
            XslCompiledTransform xslTransform = null;
            var xsltResourceStream = GetXsltStream(xslt);
            if (xsltResourceStream != null)
            {
                xslTransform = new XslCompiledTransform();
                var reader = new XmlTextReader(xsltResourceStream);
                xslTransform.Load(reader);
            }
            if (xslTransform != null)
            {
                var outputDirectory = CreateOutputDirectoryName(target);
                DirectoryManager.PrepareExportDirectory(outputDirectory, false);

                // = get value from configuration
                var writer = XmlWriter.Create(CreateOutputFileName(target)
                    , xslTransform.OutputSettings);

                //output the cache results to a file
                xslTransform.Transform(doc, null, writer);
                writer.Flush();
                writer.Close();
            }
        }

        private Stream GetXsltStream(XsltName xslt)
        {
            string xsltResourceName = Assembly.GetExecutingAssembly().GetName().Name + ".XSLT." + xslt.ToString() + ".xslt";
            return Assembly.GetExecutingAssembly().GetManifestResourceStream(xsltResourceName);
        }

        private string CreateOutputDirectoryName(ExportTarget target)
        {
            var directoryName = ConfigurationManager.AppSettings[AppSettings.CacheExportDir.ToString()]
                                + target.ToString() + @"\";
            return directoryName;
        }

        private string CreateOutputFileName(ExportTarget target)
        {
            var fileName = CreateOutputDirectoryName(target)
                   + target.ToString()
                   + " " + DateTime.Now.ToString("yyyy-MM-ddThhmmss")
                   + ".xml";
            return fileName;
        }
    }
}