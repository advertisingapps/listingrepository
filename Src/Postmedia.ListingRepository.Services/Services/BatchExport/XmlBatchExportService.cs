using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.Services.Model;
using Postmedia.ListingRepository.Services.Repositories;
using Postmedia.ListingRepository.Services.XSLT;
using log4net;
using CacheEntry = Postmedia.ListingRepository.Services.Model.CacheEntry;

namespace Postmedia.ListingRepository.Services.Services.BatchExport
{
    public class XmlBatchExportService : IXmlBatchExportService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IXslTransformService XslService { get; set; }
        private IBatchLogRepository<BatchLog> BatchLogRepository{ get; set; }

        public XmlBatchExportService()
            : this(new XslTransformService(), new BatchLogRepository()) {}

        public XmlBatchExportService(IXslTransformService xslService, IBatchLogRepository<Model.BatchLog> batchLogRepository)
        {
            XslService = xslService;
            BatchLogRepository = batchLogRepository;
        }

        
        public ExportSummary ExportBatch(ExportTarget target
            , List<CacheEntry> filteredCacheEntries)
        {
            Log.Debug("Entering - ExportBatch");
            ExportSummary summary = null;
            if (filteredCacheEntries != null)
            {
                Log.Debug("Exporting the file - ExportBatch");
                if (filteredCacheEntries.Count > 0)
                {
                    ExportFile(filteredCacheEntries, target);

                    Log.Debug("Create batch log - ExportBatch");
                    BatchLog batchLog = CreateExportLog(filteredCacheEntries, target);
                    
                    //move the exported file to the FTP file location
                    //
                    
                    BatchLogRepository.SaveOrUpdate(batchLog);
                    summary = new ExportSummary
                    {
                        SummaryCount = filteredCacheEntries.Count(),
                        BatchId = batchLog.Id
                    };
                }

                Log.Debug("Create export summary - ExportBatch");
            }
            
            return summary;
        }


        private BatchLog CreateExportLog(List<CacheEntry> filteredCacheEntries, ExportTarget target)
        {
            var logItems = CreateLogItems(filteredCacheEntries);

            var batchLog = new BatchLog
            {
                FeedName = target.ToString(),
                TimeStamp = DateTime.Now
            };

            foreach (var item in logItems)
            {
                batchLog.AddBatchItem(item);
            }

            return batchLog;
        }
        private void ExportFile(List<CacheEntry> filteredCacheEntries, ExportTarget target)
        {
            Log.Debug("Entering - ExportFile");
            var exportDoc = new XmlDocument();
            var xmlString = CreateXmlToTranslate(filteredCacheEntries);
            if (!string.IsNullOrEmpty(xmlString))
            {
                exportDoc.LoadXml(xmlString);

                Log.Debug("Performing the Transform - ExportBatch");
                XslService.Transform(exportDoc, target, XsltName.CDATATransform);

                Log.Debug("Moving the file to the export location - ExportBatch");
                //TODO : Add the file transfer to the export location
            }
        }
        private string CreateXmlToTranslate(List<CacheEntry> filteredCacheEntries)
        {
            var xmlString = string.Empty;
            if ((filteredCacheEntries != null) && (filteredCacheEntries.Count > 0))
            {
                var stringBuilder = new StringBuilder();
                string rootNodePlural = GetNodePluralization(filteredCacheEntries[0].CacheData);

                Log.Debug("Checking for root node pluralization - ExportBatch");
                if (!string.IsNullOrEmpty(rootNodePlural))
                {
                    Log.Debug("Iterating through the filtered Items to create XMLDoc - ExportBatch");
                    //Add the cache data to the document
                    foreach (var filteredCacheEntry in filteredCacheEntries)
                    {
                        if (stringBuilder.Length == 0)
                        {
                            Log.Debug("Adding pluralized root node - ExportBatch");
                            stringBuilder.AppendLine(rootNodePlural);
                        }
                        stringBuilder.AppendLine(filteredCacheEntry.CacheData);
                    }
                    Log.Debug("Finished adding " + filteredCacheEntries.Count + "items - ExportBatch");

                    Log.Debug("Closing Root node - ExportBatch");
                    stringBuilder.Append("</" + rootNodePlural.Substring(1));
                    Log.Debug("Loading the constructed XMLDoc- ExportBatch");
                    xmlString = stringBuilder.ToString();
                }
            }

            return xmlString;
        }
        private string GetNodePluralization(string cacheData)
        {
            string pluralNode = string.Empty;
            if (!string.IsNullOrEmpty(cacheData))
            {
                pluralNode = cacheData.Substring(0, cacheData.IndexOf(">")) + "S>";
                pluralNode = pluralNode.ToUpper();
            }
            return pluralNode;
        }
        private List<BatchLogItem> CreateLogItems(List<CacheEntry> filteredCacheEntries)
        {
            var logItems = new List<BatchLogItem>();
            if(filteredCacheEntries != null)
            {
                foreach (var entry in filteredCacheEntries)
                {
                    logItems.Add(new BatchLogItem
                    {
                        ListingNumber = entry.ListingNumber,
                        OutboundCacheID = entry.Id,
                    });
                }
            }
            return logItems;
        }
    }
}