﻿using System.Collections.Generic;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Services.Services.BatchExport
{
    public interface IXmlBatchExportService
    {
        ExportSummary ExportBatch(ExportTarget target
                                , List<Model.CacheEntry> filteredCacheEntries);
    }
}
