using System.Xml;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Services.XSLT;

namespace Postmedia.ListingRepository.Services.Services.BatchExport
{
    public interface IXslTransformService
    {
        void Transform(XmlDocument doc, ExportTarget target, XsltName Xslt);
    }
}