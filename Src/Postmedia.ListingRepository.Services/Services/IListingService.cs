﻿using System.Collections.Generic;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Services.Services
{
    public interface IListingService
    {
        void SaveOrUpdateListing(Listing listing);
        Listing GetListingById(int listingId);
        Listing GetListingByNumber(string listingId);
        
        List<ListingSummary> GetAllListingSummaries();
        List<ListingSummary> GetListingSummariesUsingCriteria(ListingCriteria criteria);

        ExportSummary ExportBatch(ExportTarget target, ExportCriteria criteria);
    }
}
