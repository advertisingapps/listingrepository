﻿using System.Linq;
using Postmedia.ListingRepository.DTO;
using Listing = Postmedia.ListingRepository.Services.Model.Listing;

namespace Postmedia.ListingRepository.Services.Services.CriteriaFilters
{
    public static class ListingCriteriaFilter
    {
        public static IQueryable<Listing> On(IQueryable<Listing> listings, ListingCriteria criteria)
        {
            if (!string.IsNullOrEmpty(criteria.Account))
            {
                listings = listings.Where(listing => listing.AdBooking.AccountNumber.Contains(criteria.Account));
            }
            if (criteria.Date != null)
            {
                listings = listings.Where(listing => listing.ListingPublications.All(lp => lp.ExpireDate <= criteria.Date));
            }
            if (!string.IsNullOrEmpty(criteria.Division))
            {
                //listings = listings.Where(listing => listing.AdBooking.Division.Name.Contains(criteria.Division));
            }
            if (!string.IsNullOrEmpty(criteria.ListingNumber))
            {
                listings = listings.Where(listing => listing.ListingNumber.Contains(criteria.ListingNumber));
            }
            if (!string.IsNullOrEmpty(criteria.Source))
            {
                listings = listings.Where(listing => listing.Source.Contains(criteria.Source));
            }
            if (!string.IsNullOrEmpty(criteria.Title))
            {
                listings = listings.Where(listing => listing.Title.Contains(criteria.Title));
            }
            return listings;
        }

    }
}