﻿using System.Linq;
using Postmedia.ListingRepository.DTO;
using CacheEntry = Postmedia.ListingRepository.Services.Model.CacheEntry;

namespace Postmedia.ListingRepository.Services.Services.CriteriaFilters
{
    public class ExportCriteriaFilter : IExportCriteriaFilter
    {
        public static IQueryable<CacheEntry> On(IQueryable<CacheEntry> cacheEntries, ExportCriteria criteria)
        {
            if ((cacheEntries != null) && (criteria != null))
            {
                if (!string.IsNullOrEmpty(criteria.FeedName))
                {
                    cacheEntries = cacheEntries.Where(entry => entry.FeedName.ToLower() == criteria.FeedName.ToLower());
                }

                if(criteria.LastRunDate != null)
                {
                    cacheEntries = cacheEntries.Where(entry =>
                                                      entry.LastModifiedDate >= criteria.LastRunDate);
                }

                if(criteria.RunDate != null)
                {
                    cacheEntries =
                        cacheEntries.Where(entry => 
                            criteria.RunDate >= entry.StartDate 
                            && criteria.RunDate <= entry.ExpireDate);
                }
            }
            return cacheEntries;
        }
    }
}