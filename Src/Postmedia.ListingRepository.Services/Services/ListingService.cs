﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.Services.Model;
using Postmedia.ListingRepository.Services.Repositories;
using Postmedia.ListingRepository.Services.Services.BatchExport;
using log4net;
using Listing = Postmedia.ListingRepository.DTO.Listing;


namespace Postmedia.ListingRepository.Services.Services
{
    public class ListingService : IListingService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        private IListingRepository<Model.Listing> ListingRepository { get; set; }
        private IDivisionRepository<Model.Division> DivisionRepository { get; set; }
        private ICacheEntryRepository<Model.CacheEntry> OutboundCacheRepository { get; set; }
        private IBatchLogRepository<Model.BatchLog> BatchLogRepository { get; set; }
        private IXmlBatchExportService XmlBatchExportService { get; set; }
        
        
        public ListingService() : this(new Repositories.ListingRepository(), new DivisionRepository(), new CacheEntryRepository(), new BatchLogRepository(), new XmlBatchExportService()) { }
        public ListingService(IListingRepository<Model.Listing> listingRepository, 
            IDivisionRepository<Model.Division> divisionRepository,
            ICacheEntryRepository<Model.CacheEntry> cacheEntryRepository,
            IBatchLogRepository<Model.BatchLog> batchLogRepository,
            IXmlBatchExportService xmlBatchExportService)
        {
            DivisionRepository = divisionRepository;
            ListingRepository = listingRepository;
            OutboundCacheRepository = cacheEntryRepository;
            BatchLogRepository = batchLogRepository;
            XmlBatchExportService = xmlBatchExportService;
        }
        

        public void SaveOrUpdateListing(Listing listing)
        {
            try
            {
                Log.Debug("Entering - SaveOrUpdateListing");

                Log.Debug("Retrieving saved Listing - SaveOrUpdateListing");
                //var rep = new PublicationRepository();
                //var myPub = rep.FetchByCode("37");
                
                //Model.Listing listingToSave = listingRepository.FetchByNumber(listing.ListingNumber);
                //listingToSave.Description = "Updated By Don and Aaron";
                //var modelListing = new ListingFactory().Create(retListing, listing);

                //Log.Debug("Calling Repository.SaveOrUpdate() - SaveOrUpdateListing");
                //listingRepository.SaveOrUpdate(listingToSave); 


                //Log.Debug("Creating Repositories");
                var repo = new AdBookingRepository();
                var divRepo = new DivisionRepository();

                Log.Debug("Getting AdBooking (252)");
                var adBooking = repo.FetchById(309);

                //adBooking.Division = null;
                Log.Debug("Getting Montreal Division");
                var newDiv = divRepo.FetchByCode("MON");
                Log.Debug("Montreal Division ID - " + newDiv.Id);
                Log.Debug("AdBooking.Division.ID - " + adBooking.Division.Id);

                Log.Debug("Setting AdBooking.Division To Retrieved Montreal Division");
                adBooking.Division = newDiv;

                Log.Debug("Saving ... ");
                repo.SaveAdBooking(adBooking);

            }
            catch (Exception ex)
            {
                string message = ex.Message;
                throw;
            }
        }

        public Listing GetListingById(int listingId)
        {
            try
            {
                Log.Debug("Entering - " + this.GetType());
                Log.Debug("Calling Repository.GetListingById(" + listingId + ") - " + this.GetType());
                var modelListing = ListingRepository.FetchById(listingId);

                Log.Debug("Mapping Entities.Listing --> Listing" + this.GetType());
                var listing = Mapper.Map<Model.Listing, Listing>(modelListing);

                Log.Debug("Exiting" + this.GetType());
                return listing;
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                throw;
            }
           
        }

        public Listing GetListingByNumber(string listingNumber)
        {
            try
            {
                Log.Debug("Entering - " + this.GetType());
                Log.Debug("Calling Repository.GetListingByNumber(" + listingNumber + ") - " + this.GetType());
                var modelListing = ListingRepository.FetchByNumber(listingNumber);

                Log.Debug("Mapping Entities.Listing --> Listing" + this.GetType());
                var listing = Mapper.Map<Model.Listing, Listing>(modelListing);

                Log.Debug("Exiting" + this.GetType());
                return listing;
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                throw;
            }

        }

        public List<ListingSummary> GetAllListingSummaries()
        {
            Log.Debug("Entering - GetListingSummaries");
            Log.Debug("Calling NHRepository.GetAllListingsForSummary() ");
            
            var listings = ListingRepository.FetchAll();
           
            Log.Debug("Creating List<ListingSummary> - ");
            var summaries = new List<ListingSummary>();
            
            foreach (var listing in listings)
            {
                summaries.Add(Mapper.Map<Model.Listing, ListingSummary>(listing));    
            }

            Log.Debug("Exiting - GetListingSummaries");
            return summaries;
        }
        
        public List<ListingSummary> GetListingSummariesUsingCriteria(ListingCriteria criteria)
        {
            Log.Debug("Entering - GetListingSummariesUsingCriteria" );
            Log.Debug("Calling Repository.GetAllListingsForSummaryUsingCriteria() ");
            
            //get DetachedCriteria from criteria object
            var listings = ListingRepository.GetListingsUsingCriteria(criteria).ToList();

            Log.Debug("Creating List<ListingSummary>");

            var summaries = listings.Select(Mapper.Map<Model.Listing, ListingSummary>).ToList();
            
            Log.Debug("Exiting - GetListingSummariesUsingCriteria");
            return summaries;
        }

        public ExportSummary ExportBatch(ExportTarget target, ExportCriteria criteria)
        {
            Log.Debug("Entered - ExportBatch");

            Log.Debug("Deriving Date Critieria - ExportBatch");
            DeriveDateCriteriaForFilter(criteria);

            Log.Debug("Calling outboundCacheRepository.FetchUsingCriteria() - ExportBatch");
            var filteredCacheEntries = OutboundCacheRepository.FetchUsingCriteria(criteria).ToList();
            
            Log.Debug("Calling Export Service to export the XML - ExportBatch");
            var summary = XmlBatchExportService.ExportBatch(target, filteredCacheEntries);
            summary.DailyExport = criteria.DailyExport;
            summary.FeedName = criteria.FeedName;

            Log.Debug("Exiting - ExportBatch");
            return summary;
        }

        private void DeriveDateCriteriaForFilter(ExportCriteria criteria)
        {
            Log.Debug("Entering - DeriveDateCriteriaForFilter");
            if (criteria != null)
            {
                Log.Debug("Calling BatchLogRepository.FetchAll() Where - DeriveDateCriteriaForFilter");
                //get the last batch runtime for the selected ING
                if (criteria.DailyExport == false)
                {
                    var lastBatchRuntime = (from batch in BatchLogRepository.FetchAll()
                                            select batch).OrderByDescending(x => x.TimeStamp).FirstOrDefault();

                    Log.Debug("Setting the last rundate - ");
                    if (lastBatchRuntime != null)
                        criteria.LastRunDate = lastBatchRuntime.TimeStamp ?? null;
                }
                else
                {
                    criteria.LastRunDate = null;
                }
                Log.Debug("Setting the rundate - DeriveDateCriteriaForFilter");
                //ensure datetime in criteria is populated
                criteria.RunDate = criteria.RunDate ?? DateTime.Now;
            }
        }

    }
}