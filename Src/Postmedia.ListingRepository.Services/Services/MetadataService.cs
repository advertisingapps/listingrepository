﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Xml;
using AutoMapper;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.FileIO;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.Services.Repositories;
using Postmedia.ListingRepository.Services.Services.Metadata.Oodle;
using Postmedia.ListingRepository.Services.Services.Metadata.YearMakeModel;
using log4net;
using Division = Postmedia.ListingRepository.Services.Model.Division;

namespace Postmedia.ListingRepository.Services.Services
{
    public class MetadataService : IMetadataService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IDivisionRepository<Division> DivisionRepo { get; set; }
        private IOodleXmlImportService OodleXmlImportService { get; set; }
        private IYearMakeModelService YearMakeModelService { get; set; }

        public MetadataService()
            : this(new DivisionRepository(), new OodleXmlImportService(), new YearMakeModelService())
        {
        }

        public MetadataService(IDivisionRepository<Division> divisionRepository
                               , IOodleXmlImportService oodleXmlImportService
                               , IYearMakeModelService yearMakeModelService)
        {
            DivisionRepo = divisionRepository;
            OodleXmlImportService = oodleXmlImportService;
            YearMakeModelService = yearMakeModelService;
        }

        public List<DTO.Division> GetDivisions()
        {
            try
            {
                Log.Debug("Entering - GetDivisions()");
                Log.Debug("Calling Repository.GetDivisions() - GetDivisions()");
                var modelDivisions = DivisionRepo.FetchAll();

                var divisions = modelDivisions.Select(Mapper.Map<Division, DTO.Division>).ToList();

                Log.Debug("Exiting GetDivisions()");
                return divisions;
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                throw;
            }

        }

        public OodleImportSummary UpdateCategories()
        {
            var summary = OodleXmlImportService.ExportOodleRootNodesToXmlConfigurations();
            return summary;
        }

        public YearMakeModelImportSummary UpdateYearMakeModel()
        {
            var summary = YearMakeModelService.ImportYearMakeModels();
            summary = InterpretExportResults(YearMakeModelService.ExportYearMakeModelsToXml(), summary);
            return summary;
        }

        public YearMakeModelImportSummary InterpretExportResults(bool success, YearMakeModelImportSummary summary)
        {
            if (!summary.Success)
            {
                summary.FailureReason +=
                    "The export process to genera configs failed. Please check the logs for more details";
                summary.Success = false;
            }

            return summary;
        }
    }
}