﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Services.Mapping
{
    public static class AutomapperConfiguration
    {
        public static void Config()
        {
            //EF to DTO
            Mapper.CreateMap<Entities.Listing, ListingSummary>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ListingID))
                .ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.AdBooking.AccountNumber))
                .ForMember(dest => dest.Division, opt => opt.MapFrom(src => src.AdBooking.Division.Name))
                .ForMember(dest => dest.ExpireDate
                           , opt => opt.MapFrom(
                               src => src.ListingPublications.Max(r => r.ExpireDate)))
                .ForMember(dest => dest.ListingNumber, opt => opt.MapFrom(src => src.ListingNumber))
                .ForMember(dest => dest.Source, opt => opt.MapFrom(src => src.Source))
                .ForMember(dest => dest.StartDate
                           , opt => opt.MapFrom(
                               src => src.ListingPublications.Min(r => r.StartDate)))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title));

            Mapper.CreateMap<Entities.Listing, Listing>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ListingID))
                .ForMember(dest => dest.AdBooking, opt => opt.MapFrom(src => src.AdBooking))
                .ForMember(dest => dest.AdType, opt => opt.MapFrom(src => src.AdType))
                .ForMember(dest => dest.Advertiser, opt => opt.MapFrom(src => src.Advertiser))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.Images, opt => opt.MapFrom(src => src.Images))
                .ForMember(dest => dest.Logos, opt => opt.MapFrom(src => src.Logos))
                .ForMember(dest => dest.ListingNumber, opt => opt.MapFrom(src => src.ListingNumber))
                .ForMember(dest => dest.ListingPublications, opt => opt.MapFrom(src => src.ListingPublications))
                .ForMember(dest => dest.Properties, opt => opt.MapFrom(src => src.Properties))
                .ForMember(dest => dest.Source, opt => opt.MapFrom(src => src.Source))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title));

            Mapper.CreateMap<Entities.ListingPublication, ListingPublication>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ListingPublicationID))
                .ForMember(dest => dest.Categories, opt => opt.MapFrom(src => src.Categories))
                .ForMember(dest => dest.ExpireDate, opt => opt.MapFrom(src => src.ExpireDate))
                .ForMember(dest => dest.Publication, opt => opt.MapFrom(src => src.Publication))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Upsells, opt => opt.MapFrom(src => src.Upsells));
            
            Mapper.CreateMap<Entities.Publication, Publication>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.PublicationID))
                .ForMember(dest => dest.AspControl, opt => opt.MapFrom(src => src.ASPControl))
                .ForMember(dest => dest.Categories, opt => opt.MapFrom(src => src.Categories))
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

            Mapper.CreateMap<Entities.Advertiser, Advertiser>()
              .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.AdvertiserID))
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
              .ForMember(dest => dest.Addresses, opt => opt.MapFrom(src => src.Addresses))
              .ForMember(dest => dest.Contacts, opt => opt.MapFrom(src => src.Contacts));

            Mapper.CreateMap<Entities.AdBooking, AdBooking>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.DivisionID))
                .ForMember(dest => dest.Division, opt => opt.MapFrom(src => src.Division))
                .ForMember(dest => dest.AccountNumber, opt => opt.MapFrom(src => src.AccountNumber))
                .ForMember(dest => dest.AdOrderNumber, opt => opt.MapFrom(src => src.AdOrderNumber));

            Mapper.CreateMap<Entities.Contact, Contact>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ContactID))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone));

            Mapper.CreateMap<Entities.Address, Address>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.AddressID))
                .ForMember(dest => dest.AddressLines, opt => opt.MapFrom(src => src.AddressLines))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.Country))
                .ForMember(dest => dest.PostalCode, opt => opt.MapFrom(src => src.PostalCode))
                .ForMember(dest => dest.Province, opt => opt.MapFrom(src => src.Province));

            Mapper.CreateMap<Entities.AddressLine, AddressLine>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.AddressLineID))
                .ForMember(dest => dest.LineText, opt => opt.MapFrom(src => src.LIneText))
                .ForMember(dest => dest.Number, opt => opt.MapFrom(src => src.LineNumber));

            Mapper.CreateMap<Entities.Division, Division>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.DivisionID));

            Mapper.CreateMap<Entities.Category, Category>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.CategoryID));

            Mapper.CreateMap<Entities.Upsell, Upsell>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UpsellID));

            Mapper.CreateMap<Entities.Property, Property>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.PropertyID));

            Mapper.CreateMap<Entities.Image, Image>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ImageID));

            Mapper.CreateMap<Entities.Logo, Logo>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.LogoID));
                
            Mapper.CreateMap<Entities.ASPControl, ASPControl>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ASPControlID));

            #region DTO to EF (not necessary?)
            ////DTO to EF
            //Mapper.CreateMap<Listing, Entities.Listing>()
            //   .ForMember(dest => dest.AdBooking, opt => opt.MapFrom(src => src.AdBooking))
            //   .ForMember(dest => dest.AdType, opt => opt.MapFrom(src => src.AdType))
            //   .ForMember(dest => dest.Advertiser, opt => opt.MapFrom(src => src.Advertiser))
            //   .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
            //   .ForMember(dest => dest.Images, opt => opt.MapFrom(src => src.Images))
            //   .ForMember(dest => dest.ListingNumber, opt => opt.MapFrom(src => src.ListingNumber))
            //   .ForMember(dest => dest.ListingPublications, opt => opt.MapFrom(src => src.ListingPublications))
            //   .ForMember(dest => dest.Properties, opt => opt.MapFrom(src => src.Properties))
            //   .ForMember(dest => dest.Source, opt => opt.MapFrom(src => src.Source))
            //   .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title));

            //Mapper.CreateMap<Publication, Entities.Publication>()
            //    .ForMember(dest => dest.PublicationID, opt => opt.MapFrom(src => src.Id))
            //    .ForMember(dest => dest.ASPControl, opt => opt.MapFrom(src => src.AspControl))
            //    .ForMember(dest => dest.Categories, opt => opt.MapFrom(src => src.Categories))
            //    .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
            //    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

            //Mapper.CreateMap<Advertiser, Entities.Advertiser>()
            //  .ForMember(dest => dest.AdvertiserID, opt => opt.MapFrom(src => src.Id))
            //  .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            //  .ForMember(dest => dest.Addresses, opt => opt.MapFrom(src => src.Addresses))
            //  .ForMember(dest => dest.Contacts, opt => opt.MapFrom(src => src.Contacts));

            //Mapper.CreateMap<AddressLine, Entities.AddressLine>()
                
            //    .ForMember(dest => dest.LIneText, opt => opt.MapFrom(src => src.LineText))
            //    .ForMember(dest => dest.LineNumber, opt => opt.MapFrom(src => src.Number));

            //Mapper.CreateMap<ListingPublication, Entities.ListingPublication>()
            //    .ForMember(dest => dest.ListingPublicationID, opt => opt.Ignore())
            //    .ForMember(dest => dest.ListingID, opt => opt.Ignore())
            //    .ForMember(dest => dest.Listing, opt => opt.Ignore())
            //    .ForMember(dest => dest.ListingReference, opt => opt.Ignore())
            //    .ForMember(dest => dest.PublicationReference, opt => opt.Ignore())
            //    .ForMember(dest => dest.EntityKey, opt => opt.Ignore())
            //    .ForMember(dest => dest.Categories, opt => opt.MapFrom(src => src.Categories))
            //    .ForMember(dest => dest.ExpireDate, opt => opt.MapFrom(src => src.ExpireDate))
            //    .ForMember(dest => dest.Publication, opt => opt.MapFrom(src => src.Publication))
            //    .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
            //    .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
            //    .ForMember(dest => dest.Upsells, opt => opt.MapFrom(src => src.Upsells));

            //Mapper.CreateMap<AdBooking, Entities.AdBooking>()
            //    .ForMember(dest => dest.ExternalAccountNumber, opt => opt.Ignore())
            //    .ForMember(dest => dest.AgencyAccountNumber, opt => opt.Ignore())
            //    .ForMember(dest => dest.ExternalAdOrderNumber, opt => opt.Ignore())
            //    .ForMember(dest => dest.AdBookingID, opt => opt.Ignore())
            //    .ForMember(dest => dest.DivisionID, opt => opt.Ignore())
            //    .ForMember(dest => dest.DivisionReference, opt => opt.Ignore())
            //    .ForMember(dest => dest.Listings, opt => opt.Ignore())
            //    .ForMember(dest => dest.EntityKey, opt => opt.Ignore());



            //Mapper.CreateMap<Division, Entities.Division>()
            //    .ForMember(dest => dest.DivisionID, opt => opt.Ignore())
            //    .ForMember(dest => dest.AdBookings, opt => opt.Ignore())
            //    .ForMember(dest => dest.AdicioCareercasts, opt => opt.Ignore())
            //    .ForMember(dest => dest.Classifications, opt => opt.Ignore())
            //    .ForMember(dest => dest.DivisionID, opt => opt.Ignore())
            //    .ForMember(dest => dest.SAMSOptionRateCodes, opt => opt.Ignore())
            //    .ForMember(dest => dest.SAMSProductCodes, opt => opt.Ignore())
            //    .ForMember(dest => dest.EntityKey, opt => opt.Ignore());
      
            //Mapper.CreateMap<Category, Entities.Category>();
            //Mapper.CreateMap<Upsell, Entities.Upsell>();
            //Mapper.CreateMap<Property, Entities.Property>();
            //Mapper.CreateMap<Contact, Entities.Contact>();
            //Mapper.CreateMap<Address, Entities.Address>();
            //Mapper.CreateMap<Image, Entities.Image>();
            //Mapper.CreateMap<ASPControl, Entities.ASPControl>();
            #endregion

        }
    }
}