﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public interface IYearMakeModelRepository<T>
    {
        IEnumerable<T> FetchAll();
        T FetchById(long id);
        void DeleteImportedRecords();
        void SaveOrUpdate(T listing);
    }
}