﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public interface IListingPublicationRepository<T>
    {
        IEnumerable<T> FetchAll();
        T FetchById(long id);
        void SaveOrUpdate(T listing);
    }
}
