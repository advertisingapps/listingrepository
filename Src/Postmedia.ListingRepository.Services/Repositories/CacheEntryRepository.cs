﻿using System.Collections.Generic;
using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.Services.Services.CriteriaFilters;
using CacheEntry = Postmedia.ListingRepository.Services.Model.CacheEntry;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public class CacheEntryRepository : BaseNHibernateRepository<CacheEntry>, ICacheEntryRepository<CacheEntry>
    {
        public CacheEntryRepository() : base(new WcfSessionStorage())  {}

        public IEnumerable<CacheEntry> FetchUsingCriteria(ExportCriteria criteria)
        {
            var cacheEntries = from entry in Session.Query<CacheEntry>()
                               select entry;

            var result = ExportCriteriaFilter.On(cacheEntries, criteria);
            return result;
        }
    }
}