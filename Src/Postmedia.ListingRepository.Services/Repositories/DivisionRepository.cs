﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public class DivisionRepository : BaseNHibernateRepository<Division>, IDivisionRepository<Division>
    {
        public DivisionRepository() : base(new WcfSessionStorage())
        {

        }

        public Division FetchByCode(string code)
        {
            using (Session.BeginTransaction())
            {
                var division = (from div in Session.Query<Division>()
                                    where div.Code == code
                                    select div).FirstOrDefault();

                return division;
            }
        }
    }
}