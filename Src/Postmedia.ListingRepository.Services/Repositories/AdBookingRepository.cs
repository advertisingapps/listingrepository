﻿using IglooCoder.Commons.WcfNhibernate;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public class AdBookingRepository : BaseNHibernateRepository<AdBooking>, IAdBookingRepository<AdBooking>
    {
        public AdBookingRepository()
            : base(new WcfSessionStorage())
        {

        }

        public void SaveAdBooking(AdBooking booking)
        {
            Session.SaveOrUpdate(booking);
        }
    }
}