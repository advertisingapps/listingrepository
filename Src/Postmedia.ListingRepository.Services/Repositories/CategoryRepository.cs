﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public class CategoryRepository : BaseNHibernateRepository<Category>, ICategoryRepository<Category>
    {
        public CategoryRepository(): base(new WcfSessionStorage())
        {

        }

        public Category FetchByCode(string code)
        {
            using (Session.BeginTransaction())
            {
                var category = (from div in Session.Query<Category>()
                                    where div.Code == code
                                    select div).FirstOrDefault();

                return category;
            }
        }
    }
}