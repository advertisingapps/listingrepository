﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public class PublicationRepository : BaseNHibernateRepository<Publication>, IPublicationRepository<Publication>
    {
        public PublicationRepository()
            : base(new WcfSessionStorage())
        {

        }

        public Publication FetchByCode(string code)
        {
            using (Session.BeginTransaction())
            {
                var division = (from div in Session.Query<Publication>()
                                    where div.Code == code
                                    select div).FirstOrDefault();

                return division;
            }
        }
    }
}
    
    