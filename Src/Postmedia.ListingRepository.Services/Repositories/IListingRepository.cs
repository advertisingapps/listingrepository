﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public interface IListingRepository<T>
    {
        IEnumerable<T> FetchAll();
        T FetchById(long id);
        T FetchByNumber(string listingNumber);
        void SaveOrUpdate(T listing);
        
        IEnumerable<T> GetListingsUsingCriteria(DTO.ListingCriteria criteria);
    }
}
