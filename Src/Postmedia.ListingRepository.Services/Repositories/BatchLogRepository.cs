﻿using System.Collections.Generic;
using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public class BatchLogRepository : BaseNHibernateRepository<BatchLog>, IBatchLogRepository<BatchLog>
    {
        public BatchLogRepository() : base(new WcfSessionStorage())
        {

        }

        public List<BatchLogItem> FetchBatchLogItems(int batchID)
        {
            using (Session.BeginTransaction())
            {
                List<BatchLogItem> items = null;
                var batchLog = (from log in Session.Query<BatchLog>()
                                    where log.Id == batchID
                                    select log).FirstOrDefault();
                if(batchLog != null)
                {
                    items = batchLog.LogItems.ToList();
                }

                return items;

            }
        }
    }

    
}