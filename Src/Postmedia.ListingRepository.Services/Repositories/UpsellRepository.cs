﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public class UpsellRepository : BaseNHibernateRepository<Upsell>, IUpsellRepository<Upsell>
    {
        public UpsellRepository() : base(new WcfSessionStorage())
        {

        }

        public Upsell FetchByCode(string code)
        {
            using (Session.BeginTransaction())
            {
                var division = (from div in Session.Query<Upsell>()
                                    where div.Code == code
                                    select div).FirstOrDefault();

                return division;
            }
        }
    }
}