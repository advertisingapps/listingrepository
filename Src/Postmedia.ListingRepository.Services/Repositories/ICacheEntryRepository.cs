﻿using System.Collections.Generic;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public interface ICacheEntryRepository<T>
    {
        IEnumerable<T> FetchAll();
        IEnumerable<T> FetchUsingCriteria(ExportCriteria criteria);
        T FetchById(long id);
        void SaveOrUpdate(T listing);
    }
}