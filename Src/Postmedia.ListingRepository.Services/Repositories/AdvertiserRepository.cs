﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public class AdvertiserRepository : BaseNHibernateRepository<Advertiser>, IAdvertiserRepository<Advertiser>
    {
        public AdvertiserRepository()
            : base(new WcfSessionStorage())
        {

        }
    }
}