﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public class YearMakeModelRepository : BaseNHibernateRepository<YearMakeModel>, IYearMakeModelRepository<YearMakeModel>
    {
        public YearMakeModelRepository()
            : base(new WcfSessionStorage())
        {

        }

        public void DeleteImportedRecords()
        {
            Session.Delete("from YearMakeModel");
        }
    }
}