﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using System.Collections.Generic;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.Services.Services.CriteriaFilters;
using Listing = Postmedia.ListingRepository.Services.Model.Listing;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public class ListingRepository : BaseNHibernateRepository<Listing>, IListingRepository<Listing>
    {
        public ListingRepository() : this(new WcfSessionStorage())
        {
        }

        public ListingRepository(ISessionStorage sessionStorage) : base(sessionStorage)
        {
        }


        public Listing FetchByNumber(string listingNumber)
        {
            var foundListing = (from listing in Session.Query<Listing>()
                                where listing.ListingNumber == listingNumber
                                select listing).FirstOrDefault();

            return foundListing;
        }

        public IEnumerable<Listing> GetListingsUsingCriteria(ListingCriteria criteria)
        {
            var listings = from listing in Session.Query<Listing>()
                           select listing;

            var result = ListingCriteriaFilter.On(listings, criteria);
            return result;
        }
    }
}

       