﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.Repositories
{
    public class ListingPublicationRepository : BaseNHibernateRepository<ListingPublication>, IListingPublicationRepository<ListingPublication>
    {
        public ListingPublicationRepository()
            : base(new WcfSessionStorage())
        {

        }

        
    }
}