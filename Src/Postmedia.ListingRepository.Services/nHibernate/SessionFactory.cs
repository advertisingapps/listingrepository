﻿using System;
using System.IO;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using Postmedia.ListingRepository.Services.Model;

namespace Postmedia.ListingRepository.Services.nHibernate
{
    public static class SessionFactory
    {
        public static ISessionFactory CreateSessionFactory()
        {
            try
            {
                return Fluently.Configure()
                    .Database(MsSqlConfiguration.MsSql2008
                                  .ConnectionString(
                                      c => c.FromConnectionStringWithKey("DBConn"))
                                      .ShowSql())
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Listing>())
                    .BuildSessionFactory();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private static void BuildSchema(Configuration config)
        {
            // this NHibernate tool takes a configuration (with mapping info in)
            // and exports a database schema from it
            new SchemaExport(config).Create(false, true);
        }
    }
}