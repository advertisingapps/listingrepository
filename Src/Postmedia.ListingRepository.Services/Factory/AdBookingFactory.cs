﻿using Postmedia.ListingRepository.Services.Model;
using Postmedia.ListingRepository.Services.Repositories;

namespace Postmedia.ListingRepository.Services.Factory
{
    public class AdBookingFactory
    {
        private IAdBookingRepository<AdBooking> _adBookingRepository = null;
        private IDivisionRepository<Division> _divisionRepository = null;

        public AdBookingFactory(IAdBookingRepository<AdBooking> adBookingRepository
            , IDivisionRepository<Division> divisionRepository )
        {
            _adBookingRepository = adBookingRepository;
            _divisionRepository = divisionRepository;
        }

        public AdBookingFactory()
            : this(new AdBookingRepository()
            , new DivisionRepository())
        {
        }

        public AdBooking  Create(DTO.AdBooking source)
        {
            var dest = new AdBooking();

            if (source != null)
            {
                if (source.Id > 0)
                {
                    dest = _adBookingRepository.FetchById(source.Id);
                }
                dest = Create(source, dest);
            }

            return dest;
        }

        public AdBooking Create(DTO.AdBooking source , AdBooking dest)
        {
            if ((dest != null) && (source != null))
            {
                dest = PerformMapping(source, dest);
            }
            return dest;
        }

        public AdBooking PerformMapping(DTO.AdBooking source, AdBooking dest )
        {
            if(source != null)
            {
                dest.AccountNumber = source.AccountNumber;
                dest.AdOrderNumber = source.AdOrderNumber;
                dest.AgencyAccountNumber = source.AgencyAccountNumber;
                dest.ExternalAccountNumber = source.ExternalAccountNumber;
                dest.ExternalAdOrderNumber = source.ExternalAdOrderNumber;

                if (source.Division != null)
                {
                    //dest.Division = _divisionRepository.FetchByCode(source.Division.Code);
                }
            }

            return dest;
        }
    }
}