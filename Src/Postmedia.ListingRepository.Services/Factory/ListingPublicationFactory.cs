﻿using System.Collections.Generic;
using System.Linq;
using Postmedia.ListingRepository.Services.Model;
using Postmedia.ListingRepository.Services.Repositories;

namespace Postmedia.ListingRepository.Services.Factory
{
    public class ListingPublicationFactory
    {
        private IListingPublicationRepository<ListingPublication> _listingPublicationRepository = null;
        private IUpsellRepository<Upsell> _upsellRepository = null;
        private IPublicationRepository<Publication> _publicationRepository = null;

        public ListingPublicationFactory(IListingPublicationRepository<ListingPublication> listingPublicationRepository
            ,IUpsellRepository<Upsell> upsellRepository
            ,IPublicationRepository<Publication> publicationRepository)
        {
            _listingPublicationRepository = listingPublicationRepository;
            _upsellRepository = upsellRepository;
            _publicationRepository = publicationRepository;
        }

        public ListingPublicationFactory(): this(
            new ListingPublicationRepository()
            , new UpsellRepository()
            , new PublicationRepository())
        {
        }

       public ListingPublication Create(DTO.ListingPublication source)
        {
            var dest = new ListingPublication();

            if (source != null)
            {
                if(source.Id > 0)
                {
                    dest = _listingPublicationRepository.FetchById(source.Id);
                }
                dest = Create(dest, source);
            }

            return dest;
        }

        public ListingPublication Create(ListingPublication dest, DTO.ListingPublication source)
        {
            if ((dest != null) && (source != null))
            {
                dest = PerformMapping(source, dest);
            }
            return dest;
        }
        
        public ListingPublication PerformMapping(DTO.ListingPublication source, ListingPublication dest)
        {
            dest.ExpireDate = source.ExpireDate;
            dest.StartDate = source.StartDate;
            dest.Status = source.Status;

            if (source.Upsells != null)
            {
                foreach (var upsell in source.Upsells)
                {
                    dest.AddUpsell(_upsellRepository.FetchByCode(upsell.Code));
                }
            }

            if (source.Publication != null)
            {
                dest.Publication = _publicationRepository.FetchByCode(source.Publication.Code);
            }

            //if(source.Categories != null)
            //{
            //    foreach (var category in source.Categories)
            //    {
            //        dest.AddCategory(CategoryRepository.FetchByCode(category.Code));
            //    }
            //}

            return dest;
        }


    }
}