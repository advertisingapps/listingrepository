﻿using Postmedia.ListingRepository.Services.Model;
using Postmedia.ListingRepository.Services.Repositories;

namespace Postmedia.ListingRepository.Services.Factory
{
    public class AdvertiserFactory
    {
        private IAdvertiserRepository<Advertiser> _advertiserRepository = null;

        public AdvertiserFactory(IAdvertiserRepository<Advertiser> advertiserRepository)
        {
            _advertiserRepository= advertiserRepository;
        }

        public AdvertiserFactory()
            : this(new AdvertiserRepository())
        {
        }


        public Advertiser Create(DTO.Advertiser source)
        {
            var dest = new Advertiser();

            if (source != null)
            {
                if (source.Id > 0)
                {
                    dest = _advertiserRepository.FetchById(source.Id);
                }
                dest = Create(source, dest);
            }

            return dest;
        }

        public Advertiser Create(DTO.Advertiser source , Advertiser dest)
        {
            if ((dest != null) && (source != null))
            {
                dest = PerformMapping(source, dest);
            }
            return dest;
        }

        public Advertiser PerformMapping(DTO.Advertiser source, Advertiser dest)
        {

            if (source.Contact != null)
            {
                dest = new Advertiser();

                dest.Contact = new Contact()
                                   {
                                       Advertiser = dest,
                                       Email = source.Contact.Email,
                                       FirstName = source.Contact.FirstName,
                                       LastName = source.Contact.LastName,
                                       Phone = source.Contact.Phone
                                   };
            }

            if (source.Address != null)
            {
                dest = dest ?? new Advertiser();

                dest.Address = new Address()
                                   {
                                       Advertiser = dest,
                                       City = source.Address.City,
                                       Country = source.Address.Country,
                                       Id = source.Address.Id,
                                       Neighbourhood = source.Address.Neighbourhood,
                                       PostalCode = source.Address.PostalCode,
                                       Province = source.Address.Province,
                                       Region = source.Address.Region
                                   };

                if ((source.Address.AddressLines != null) && (source.Address.AddressLines.Length > 0))
                {
                    foreach (var addressLine in source.Address.AddressLines)
                    {
                        var line = new AddressLine()
                                       {
                                           Address = dest.Address
                                           , Id = addressLine.Id
                                           , LineText = addressLine.LineText
                                           , LineNumber = addressLine.Number
                                       };
                        dest.Address.AddAddressLine(line);
                    }
                }
            }


            return dest;
        }
    }
}