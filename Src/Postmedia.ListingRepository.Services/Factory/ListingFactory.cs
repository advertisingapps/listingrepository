﻿using System.Collections.Generic;
using System.Linq;
using Postmedia.ListingRepository.Services.Model;
using Postmedia.ListingRepository.Services.Repositories;

namespace Postmedia.ListingRepository.Services.Factory
{
    public class ListingFactory
    {
        private ListingPublicationFactory _listingPublicationFactory = null;
        private AdvertiserFactory _advertiserFactory = null;
        private AdBookingFactory _adBookingFactory = null;

        public ListingFactory(ListingPublicationFactory listingPublicationFactory
            ,AdvertiserFactory advertiserFactory
            ,AdBookingFactory adBookingFactory)
        {
            _adBookingFactory = adBookingFactory;
            _advertiserFactory = advertiserFactory;
            _listingPublicationFactory = listingPublicationFactory;
        }

        public ListingFactory() : this(new ListingPublicationFactory()
            , new AdvertiserFactory()
            , new AdBookingFactory()) {}

        
        public Listing Create(Listing dest, DTO.Listing source)
        {   
            if ((dest !=null) && (source != null))
            {
                dest = PerformMapping(source, dest);
            }
            return dest;
        }

        private Listing PerformMapping(DTO.Listing source, Listing dest)
        {
            //Copy base properties
            dest.AdType = source.AdType;
            dest.Description = source.Description;
            dest.ListingNumber = source.ListingNumber;
            dest.Source = source.Source;
            dest.Title = source.Title;

            ////Create / update the listing publications
            //foreach (var listingPublication in source.ListingPublications)
            //{
            //    var lp = _listingPublicationFactory.Create(listingPublication);
            //    if(lp.Id == 0)
            //    {
            //        dest.AddListingPublication(lp);
            //    }
            //}
        
            
            //// On Update 
            //// remove any listingPublications which 
            //// are not present in the provided list.
            //if (dest.Id != 0)
            //{
            //    var listingPubsToRemove = new List<ListingPublication>();
            //    foreach (ListingPublication listingPublication in dest.ListingPublications)
            //    {
            //        if (listingPublication.Id != 0)
            //        {
            //            bool foundInSource = (from lp in source.ListingPublications
            //                                  where lp.Id == listingPublication.Id
            //                                  select lp).Any();

            //            if (!foundInSource) 
            //                listingPubsToRemove.Add(listingPublication);
            //        }
            //    }

            //    foreach (var listingPublication in listingPubsToRemove)
            //    {
            //        dest.ListingPublications.Remove(listingPublication);
            //    }
            //}

            ////Create / Update the AdBooking
            //dest.AdBooking = _adBookingFactory.Create(source.AdBooking);

            ////Create / Update the Advertiser
            //dest.Advertiser = _advertiserFactory.Create(source.Advertiser);

            //foreach (DTO.Property prop in source.Properties)
            //{
            //    var property = new Property {Id = prop.Id, Code = prop.Code, Value = prop.Value};
            //    dest.AddProperty(property);
            //}



            /* 
             * 
             * Will be changed to Media Values (uncomment with upcoming database fix)
             * 
             * foreach (DTO.Logo logo in source.Logos)
             * {
             *      var l = new Logo { Id = logo.Id, Url = logo.Url };
             *      dest.AddLogo(l);
             * }
             * 
             * foreach (DTO.Image img in source.Images)
             * {
             *      var image = new Image { Id = img.Id, Url = img.Url };
             *      dest.AddImage(image);
             * }
             * 
             */

            return dest;

        }
    }
}