﻿using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Postmedia.ListingRepository.Services
{
    [ServiceContract]
    public interface IListingRepositoryService
    {
        [OperationContract]
        [WebGet(UriTemplate = "/metadata/updatecategories")]
        Stream UpdateCategories();

        [OperationContract]
        [WebGet(UriTemplate = "/metadata/updateymm")]
        Stream UpdateYearMakeModel();

        [OperationContract]
        [WebGet(UriTemplate = "/divisions")]
        Stream GetDivisions();

        [OperationContract]
        [WebGet( UriTemplate = "/listingsummaries")]
        Stream GetListingSummaries();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/listingsummaries")]
        Stream GetListingSummariesUsingCriteria(Stream content);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/listings", RequestFormat = WebMessageFormat.Xml)]
        Stream SaveListing(Stream content);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/listings/{listingId}")]
        Stream DeleteListing(string listingId);

        [OperationContract]
        [WebGet(UriTemplate = "/listings/{listingId}")]
        Stream RetrieveListing(string listingId);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "exportcache")]
        Stream ExportOutBoundCache(Stream exportCriteria);
    }
}
