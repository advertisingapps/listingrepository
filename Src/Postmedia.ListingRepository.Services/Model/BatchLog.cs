﻿using System;
using System.Collections.Generic;
using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.Services.Model
{
    public class BatchLog : BaseDomain
    {
        public virtual DateTime? TimeStamp { get; set; }
        public virtual string FeedName { get; set; }
        public virtual IList<BatchLogItem> LogItems { get; set; }

        public BatchLog()
        {
            LogItems = new List<BatchLogItem>();
        }

        public virtual void AddBatchItem(BatchLogItem item)
        {
            item.Batch = this;
            LogItems.Add(item);
        }
    }
}