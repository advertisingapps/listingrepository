﻿using Postmedia.ListingRepository.Services.Model.BaseClasses;

namespace Postmedia.ListingRepository.Services.Model
{
    public class Property : CodeValueBase
    {
        public virtual Listing Listing { get; set; }
    }
}