﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.Services.Model
{
    public class AddressLine : BaseDomain
    {
        public virtual int LineNumber { get; set; }
        public virtual string LineText { get; set; }
        //public virtual long AddressId { get; set; }

        public virtual Address Address { get; set; }
    }
}