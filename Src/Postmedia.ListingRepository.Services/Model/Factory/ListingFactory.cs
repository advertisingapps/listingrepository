﻿using System.Linq;

namespace Postmedia.ListingRepository.Services.Model.Factory
{
    public class ListingFactory
    {
        private ListingPublicationFactory _listingPublicationFactory = null;
        private AdvertiserFactory _advertiserFactory = null;
        private AdBookingFactory _adBookingFactory = null;

        public ListingFactory(ListingPublicationFactory listingPublicationFactory,
            AdvertiserFactory advertiserFactory,
            AdBookingFactory adBookingFactory)
        {
            _adBookingFactory = adBookingFactory;
            _advertiserFactory = advertiserFactory;
            _listingPublicationFactory = listingPublicationFactory;
        }

        public ListingFactory() : this(new ListingPublicationFactory()
            , new AdvertiserFactory()
            , new AdBookingFactory()) {}

        public Listing Create(DTO.Listing listing)
        {
            Listing retVal = null;
            if(listing != null)
            {
                retVal = new Listing();
                retVal.AdType = listing.AdType;
                retVal.Description = listing.Description;
                retVal.ListingNumber = listing.ListingNumber;
                retVal.Source = listing.Source;
                retVal.Title = listing.Title;
                retVal.Id = listing.Id;

                foreach (var lp in listing.ListingPublications.Select(
                    listingPublication => _listingPublicationFactory.Create(listingPublication)))
                {
                    retVal.AddListingPublication(lp);
                }

                retVal.AdBooking = _adBookingFactory.Create(listing.AdBooking);

                foreach (var l in listing.Logos.Select(logo => new Logo{ Id = logo.Id, Url = logo.Url }))
                {
                    retVal.AddLogo(l);
                }

                foreach (var image in listing.Images.Select(img => new Image{ Id = img.Id, Url = img.Url}))
                {
                    retVal.AddImage(image);
                }

                foreach (var property in listing.Properties.Select(prop => new Property{ Id = prop.Id, Code = prop.Code, Value = prop.Value }))
                {
                    retVal.AddProperty(property);
                }

                retVal.Advertiser = _advertiserFactory.Create(listing.Advertiser);
            }
            return retVal;
        }
    }
}