﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Postmedia.ListingRepository.Services.Model.Factory
{
    public class ListingPublicationFactory
    {
        public ListingPublication Create(DTO.ListingPublication listingPublication)
        {
            ListingPublication retVal = null;
            if(listingPublication != null)
            {
                retVal = new ListingPublication()
                             {
                                 ExpireDate = listingPublication.ExpireDate,
                                 Id = listingPublication.Id,
                                 StartDate = listingPublication.StartDate,
                                 Status = listingPublication.Status
                             };

                if (listingPublication.Upsells != null)
                {
                    foreach (var lpUpSell in listingPublication.Upsells.Select(
                        upsell => new Upsell()
                        {
                            Code = upsell.Code,
                            Name = upsell.Name,
                            Id = upsell.Id
                        }))
                    {
                        retVal.AddUpsell(lpUpSell);
                    }
                }
            }

            return retVal;
        }
    }
}