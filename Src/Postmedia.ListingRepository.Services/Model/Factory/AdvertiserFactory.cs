﻿

using System.Linq;

namespace Postmedia.ListingRepository.Services.Model.Factory
{
    public class AdvertiserFactory
    {
        public Advertiser Create(DTO.Advertiser advertiser)
        {
            Advertiser retVal = null;

            if (advertiser != null)
            {
                if (advertiser.Contact != null)
                {
                    retVal = new Advertiser();

                    retVal.Contact = new Contact()
                                         {
                                             Advertiser = retVal,
                                             Email = advertiser.Contact.Email,
                                             FirstName = advertiser.Contact.FirstName,
                                             LastName = advertiser.Contact.LastName,
                                             Phone = advertiser.Contact.Phone
                                         };
                }
                if (advertiser.Address != null)
                {
                    retVal = retVal ?? new Advertiser();

                    retVal.Address = new Address()
                                         {
                                             Advertiser = retVal,
                                             City = advertiser.Address.City,
                                             Country = advertiser.Address.Country,
                                             Id = advertiser.Address.Id,
                                             Neighbourhood = advertiser.Address.Neighbourhood,
                                             PostalCode = advertiser.Address.PostalCode,
                                             Province = advertiser.Address.Province,
                                             Region = advertiser.Address.Region
                                         };

                    if ((advertiser.Address.AddressLines != null) && (advertiser.Address.AddressLines.Count > 0))
                    {
                        foreach (var line in advertiser.Address.AddressLines.Select(addressLine =>
                                                                                    new AddressLine()
                                                                                        {
                                                                                            Address = retVal.Address,
                                                                                            Id = addressLine.Id,
                                                                                            LineText =
                                                                                                addressLine.LineText,
                                                                                            LineNumber =
                                                                                                addressLine.Number
                                                                                        }))
                        {
                            retVal.Address.AddAddressLine(line);
                        }
                    }
                }
            }

            return retVal;
        }
    }
}