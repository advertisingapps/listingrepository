﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Postmedia.ListingRepository.Services.Model.Factory
{
    public class AdBookingFactory
    {
        public AdBooking Create(DTO.AdBooking adBooking)
        {
            AdBooking retVal = null;

            if(adBooking != null)
            {
                retVal = new AdBooking()
                             {
                                 AccountNumber = adBooking.AccountNumber,
                                 AdOrderNumber = adBooking.AdOrderNumber,
                                 AgencyAccountNumber = adBooking.AgencyAccountNumber,
                                 ExternalAccountNumber = adBooking.ExternalAccountNumber,
                                 ExternalAdOrderNumber = adBooking.ExternalAdOrderNumber
                             };

                if (adBooking.Division != null)
                {
                    retVal = new AdBooking();
                    retVal.Division = new Division()
                                          {
                                              Code = adBooking.Division.Code,
                                              Name = adBooking.Division.Name,
                                              Id = adBooking.Division.Id
                                          };
                }

            
            }

            return retVal;
        }
    }
}