﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.Services.Model
{
    public class Logo : BaseDomain
    {
        public virtual string Url { get; set; }
        public virtual Listing Listing { get; set; }
    }
}