﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.Services.Model
{
    public class BatchExportSummary : BaseDomain
    {
        public virtual int NumberOfRecords { get; set; }
    }
}