﻿using System.Collections.Generic;
using Postmedia.ListingRepository.Services.Model.BaseClasses;

namespace Postmedia.ListingRepository.Services.Model
{
    public class Publication : CodeNameBase
    {
        public virtual ASPControl ASPControl { get; set; }
        public virtual IList<Category> Categories { get; set; }

        public Publication()
        {
            Categories = new List<Category>();
        }
    }
}
