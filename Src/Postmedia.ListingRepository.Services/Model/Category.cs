﻿using System.Collections.Generic;
using Postmedia.ListingRepository.Services.Model.BaseClasses;

namespace Postmedia.ListingRepository.Services.Model
{
    public class Category : CodeNameBase
    {
        public virtual Publication Publication { get; set; }
        public virtual IList<ListingPublication> ListingPublications { get; set; }
    }
}