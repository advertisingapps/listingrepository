﻿using Postmedia.ListingRepository.Services.Model.BaseClasses;

namespace Postmedia.ListingRepository.Services.Model
{
    public class Upsell : CodeNameBase
    {
        public virtual ListingPublication ListingPublication { get; set; }
    }
}