﻿using System.Collections.Generic;
using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.Services.Model
{
    public class Advertiser : BaseDomain
    {
        public virtual string Name { get; set; }
        public virtual string CompanyName { get; set; }
        public virtual string Url { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual Address Address { get; set; }
    }
}