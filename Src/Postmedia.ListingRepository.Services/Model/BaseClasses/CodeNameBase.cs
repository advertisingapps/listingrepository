﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.Services.Model.BaseClasses
{
    public abstract class CodeNameBase : BaseDomain
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }

    
    }
}