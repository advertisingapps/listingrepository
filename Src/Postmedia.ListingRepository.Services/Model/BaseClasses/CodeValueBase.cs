﻿
using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.Services.Model.BaseClasses
{
    public abstract class CodeValueBase : BaseDomain
    {
        public virtual string Code { get; set; }
        public virtual string Value{ get; set; }
    }
}