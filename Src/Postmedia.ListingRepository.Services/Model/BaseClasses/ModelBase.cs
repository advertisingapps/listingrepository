﻿
namespace Postmedia.ListingRepository.Services.Model.BaseClasses
{
    public abstract class ModelBase
    {
        public virtual int Id { get; set; }
    }
}