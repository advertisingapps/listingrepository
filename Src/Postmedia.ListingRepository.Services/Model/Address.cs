﻿using System.Collections.Generic;
using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.Services.Model
{
    public class Address : BaseDomain
    {
        public virtual string City { get; set; }
        public virtual string Province { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string Country { get; set; }
        public virtual string Neighbourhood { get; set; }
        public virtual string Region { get; set; }

        public virtual Advertiser Advertiser { get; set; }
        public virtual IList<AddressLine> AddressLines { get; set; }

        public Address()
        {
            AddressLines = new List<AddressLine>();
        }

        public virtual void AddAddressLine(AddressLine addressLine)
        {
            addressLine.Address = this;
            AddressLines.Add(addressLine);
        }

    }
}