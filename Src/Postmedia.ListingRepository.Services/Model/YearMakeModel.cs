﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.Services.Model
{
    public class YearMakeModel : BaseDomain
    {
        public virtual int? AdPerfectID { get; set; }
        public virtual string Year { get; set; }
        public virtual string Make { get; set; }
        public virtual string Model { get; set; }
    }
}