﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.Services.Model
{
    public class BatchLogItem : BaseDomain
    {
        public virtual long OutboundCacheID { get; set; }
        public virtual string ListingNumber { get; set; }

        public virtual BatchLog Batch { get; set; }
    }
}