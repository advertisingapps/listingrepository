﻿using System.Collections.Generic;
using System.Linq;
using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.Services.Model
{
    public class Listing : BaseDomain
    {
        public virtual string ListingNumber { get; set; }
        public virtual string Description { get; set; }
        public virtual string AdType { get; set; }
        public virtual string Title { get; set; }
        public virtual string Source { get; set; }

        public virtual AdBooking AdBooking { get; set; }
        public virtual Advertiser Advertiser { get; set; }

        public virtual IList<Image> Images { get; set; }
        public virtual IList<Logo> Logos { get; set; }
        public virtual IList<ListingPublication> ListingPublications { get; set; }
        public virtual IList<Property> Properties { get; set; }
       
        public Listing()
        {
            ListingPublications = new List<ListingPublication>();
            Properties = new List<Property>();
            Logos = new List<Logo>();
            Images = new List<Image>();
        }

        public virtual void AddListingPublication(ListingPublication listingPublication)
        {
            listingPublication.Listing = this;
            ListingPublications.Add(listingPublication);
        }
    

        public virtual void AddImage(Image image)
        {
            image.Listing = this;
            Images.Add(image);
        }

        public virtual void AddLogo(Logo logo)
        {
            logo.Listing = this;
            Logos.Add(logo);
        }

        public virtual void AddProperty(Property property)
        {
            property.Listing = this;
            Properties.Add(property);
        }
    }
}
