﻿$("#addPropertyItem").click(function () {
    $.ajax({
        url: this.href,
        cache: false,
        success: function (html) { $("#propertyMetadata").append(html); }
    });
    return false;
});

$("a.deletePropertyRow").live("click", function () {
    $(this).parents("div.propertyRow:first").remove();
    return false;
});
