﻿using System.Collections.Generic;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Web.ViewModels
{
    public class CacheEntrySearchViewModel
    {
                // empty constructor
        public CacheEntrySearchViewModel() 
        {
            CacheEntries = new List<CacheEntry>();
        }

        // constructor that accepts a listing
        public CacheEntrySearchViewModel(List<CacheEntry> entries)
        {
            CacheEntries = entries;
        }

        public List<CacheEntry> CacheEntries { get; set; }
        public string ListingNumber { get; set; }

    }

}