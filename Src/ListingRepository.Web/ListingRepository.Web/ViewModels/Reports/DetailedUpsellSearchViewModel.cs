﻿using System.Collections.Generic;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports
{
    public class DetailedUpsellSearchViewModel
    {
        public DetailedUpsellCriteriaViewModel DetailedUpsellCriteria { get; set; }
        public List<DetailedUpsellDetailViewModel> DetailedUpsellResults { get; set; }

        // lookups
        public List<Division> Divisions { get; set; }
        public List<SourceSystemLookup> Sources { get; set; }
        public List<Publication> Publications { get; set; }
        public List<UpsellValueLookup> UpsellValues { get; set; }
        public List<ListingStatusLookup> Status { get; set; }
    }
}