﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class DigitalDisplaySearchViewModel
    {
        public DigitalDisplayCriteriaViewModel DigitalDisplayCriteria { get; set; }
        public List<DigitalDisplayDetailViewModel> DigitalDisplayResults { get; set; }

        // lookups
        public List<Division> Divisions { get; set; }
    }
}