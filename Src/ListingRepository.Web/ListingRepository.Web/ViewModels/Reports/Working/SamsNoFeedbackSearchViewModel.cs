﻿using System.Collections.Generic;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class SamsNoFeedbackSearchViewModel
    {
        public SamsNoFeedbackCriteriaViewModel SamsNoFeedbackCriteria { get; set; }
        public List<SamsNoFeedbackDetailViewModel> SamsNoFeedbackResults { get; set; }

        // lookups
        public List<Division> Divisions { get; set; }
    }
}
