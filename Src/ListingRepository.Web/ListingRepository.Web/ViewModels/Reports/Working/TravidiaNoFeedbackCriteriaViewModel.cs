﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class TravidiaNoFeedbackCriteriaViewModel
    {
        private TravidiaNoFeedbackCriteria Criteria { get; set; }

        // empty constructor
        public TravidiaNoFeedbackCriteriaViewModel()
        {
            Criteria = new TravidiaNoFeedbackCriteria();
        }

        // constructor that accepts ReportCriteria
        public TravidiaNoFeedbackCriteriaViewModel(TravidiaNoFeedbackCriteria criteria)
        {
            if (criteria == null)
            {
                Criteria = new TravidiaNoFeedbackCriteria();
            }
            else
                Criteria = criteria;
        }
        
        public string Division
        {
            get
            {
                return Criteria.Division;
            }
            set
            {
                Criteria.Division = value;
            }
        }

        [DisplayName("Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? Date
        {
            get { return Criteria.Date; }
            set { Criteria.Date = value; }
        }

        [DisplayName("Email Results")]
        public bool EmailResults
        {
            get { return Criteria.EmailResults; }
            set { Criteria.EmailResults = value; }
        }

        [DisplayName("Email Address")]
        public string EmailAddress
        {
            get { return Criteria.EmailAddress; }
            set { Criteria.EmailAddress = value; }
        }

        public TravidiaNoFeedbackCriteria getDTOForReport()
        {
            return Criteria;
        }
    }
}