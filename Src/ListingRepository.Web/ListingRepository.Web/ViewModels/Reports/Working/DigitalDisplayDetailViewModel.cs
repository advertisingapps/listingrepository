﻿using System;
using System.ComponentModel;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class DigitalDisplayDetailViewModel
    {
        // empty constructor (required?)
        public DigitalDisplayDetailViewModel() 
        {
            DigitalDisplayReportDetail = new DigitalDisplayResult();
        }

        //Create constructor that accepts a DigitalDisplayResult DTO object
        public DigitalDisplayDetailViewModel(DigitalDisplayResult rptResult)
        {
            if (rptResult == null)
            {
                DigitalDisplayReportDetail = new DigitalDisplayResult(); 
            }
            else
                DigitalDisplayReportDetail = rptResult;
        }

        private DigitalDisplayResult DigitalDisplayReportDetail { get; set; }

        [DisplayName("Division")]
        public string DivisionName 
        { 
            get { return DigitalDisplayReportDetail.Division; }
        }

        [DisplayName("Publication")]
        public string PublicationName
        {
            get { return DigitalDisplayReportDetail.Publication; }
        }

        [DisplayName("Listing Number")]
        public string ListingNumber
        {
            get { return DigitalDisplayReportDetail.ListingNumber; }
        }

        [DisplayName("Account #")]
        public string Account
        {
            get { return DigitalDisplayReportDetail.Account; }
        }

        public string Advertiser
        {
            get { return DigitalDisplayReportDetail.AdvertiserName; }
        }

        public string Title
        {
            get {
                string retval = string.Empty;

                //title is not null
                if (!string.IsNullOrEmpty(DigitalDisplayReportDetail.Title))
                {
                    if (DigitalDisplayReportDetail.Title.Length > 50)
                    {
                        retval = DigitalDisplayReportDetail.Title.Substring(0, 50) + "...";
                    }
                    else
                    {
                        retval = DigitalDisplayReportDetail.Title;
                    }
                }
                return retval;
            }         
        }

        public string Category {
            get { return DigitalDisplayReportDetail.Category; }
        }

        public string StartDate
        {
            get { return DigitalDisplayReportDetail.StartDate.ToString("yyyy/MM/dd"); }
        }

        public string ExpireDate
        {
            get { return DigitalDisplayReportDetail.ExpireDate.ToString("yyyy/MM/dd"); }
        }

        [DisplayName("Ad Length")]
        public int AdLength
        {
            get { return DigitalDisplayReportDetail.AdLength; }
        }

        [DisplayName("Display URL")]
        public string DisplayURL
        {
            get
            {
                return getDisplayURL;
            }
        }

        public string AdLengthStyle
        {
            get
            {
                string retVal = "numeric";
                if (DigitalDisplayReportDetail.AdLength == 0)
                {
                    retVal += " redtext";
                }
                return retVal;
            }
        }

        public string DisplayURLStyle
        {
            get
            {
                string retval = string.Empty;
                if (getDisplayURL == "N/A")
                {
                    retval = "redtext";
                }
                return retval;
            }
        }

        private string getDisplayURL
        {
            get
            {
                string retval = "N/A";
                if ((DigitalDisplayReportDetail.MediaItems != null) && (DigitalDisplayReportDetail.MediaItems.Count > 0))
                {
                    foreach (var item in DigitalDisplayReportDetail.MediaItems)
                    {
                        if ((item.Role == "displayUrl") && (!String.IsNullOrEmpty(item.Url)))
                        {
                            retval = item.Url;
                        }
                    }
                }
                return retval;
            }
        }
    }
}