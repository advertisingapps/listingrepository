﻿using System.ComponentModel;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class TravidiaNoFeedbackDetailViewModel
    {
                // empty constructor (required?)
        public TravidiaNoFeedbackDetailViewModel() 
        {
            TravidiaNoFeedbackReport = new TravidiaNoFeedbackResult();
        }

        //Create constructor that accepts a SAMSResult DTO object
        public TravidiaNoFeedbackDetailViewModel(TravidiaNoFeedbackResult rptResult)
        {
            if (rptResult == null)
            {
                TravidiaNoFeedbackReport = new TravidiaNoFeedbackResult();
            }
            else
                TravidiaNoFeedbackReport = rptResult;
        }

        private TravidiaNoFeedbackResult TravidiaNoFeedbackReport { get; set; }

        public string Division { 
            get { return TravidiaNoFeedbackReport.Division; }
        }

        [DisplayName("Listing Number")]
        public string ListingNumber
        {
            get { return TravidiaNoFeedbackReport.ListingNumber; }
        }

        [DisplayName("Account #")]
        public string Account
        {
            get { return TravidiaNoFeedbackReport.Account; }
        }

        public string Advertiser
        {
            get { return TravidiaNoFeedbackReport.AdvertiserName; }
        }

        public string Title
        {
            get
            {
                string retval = string.Empty;

                //title is not null
                if (!string.IsNullOrEmpty(TravidiaNoFeedbackReport.Title))
                {
                    if (TravidiaNoFeedbackReport.Title.Length > 50)
                    {
                        retval = TravidiaNoFeedbackReport.Title.Substring(0, 50) + "...";
                    }
                    else
                    {
                        retval = TravidiaNoFeedbackReport.Title;
                    }
                }
                return retval;
            }         
        }

        public string Category {
            get { return TravidiaNoFeedbackReport.Category; }
        }

        public string StartDate
        {
            get { return TravidiaNoFeedbackReport.StartDate.ToString("yyyy/MM/dd"); }
        }

        public string ExpireDate
        {
            get { return TravidiaNoFeedbackReport.ExpireDate.ToString("yyyy/MM/dd"); }
        }

        [DisplayName("Ad Length")]
        public int AdLength
        {
            get { return TravidiaNoFeedbackReport.AdLength; }
        }

        public string AdLengthStyle
        {
            get
            {
                string retVal = "numeric";
                if (TravidiaNoFeedbackReport.AdLength == 0)
                {
                    retVal = "numericzero";
                }
                return retVal;
            }
        }

        [DisplayName("Display URL")]
        public string DisplayURL
        {
            get {
                string retval = string.Empty;

                if ((TravidiaNoFeedbackReport.MediaItems != null) && (TravidiaNoFeedbackReport.MediaItems.Count > 0))
                {
                    foreach (var item in TravidiaNoFeedbackReport.MediaItems)
                    {
                        if (item.Role == "displayUrl")
                        {
                            retval = item.Url ?? string.Empty;
                        }
                    }
                }
                return retval;
            }
        }
    }
}