﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class TravidiaNoFeedbackSearchViewModel
    {
        public TravidiaNoFeedbackCriteriaViewModel TravidiaNoFeedbackCriteria { get; set; }
        public List<TravidiaNoFeedbackDetailViewModel> TravidiaNoFeedbackResults { get; set; }

        // lookups
        public List<Division> Divisions { get; set; }

        //
        public long ResultsCount {
            get 
            {
                long retVal = 0;
                if (TravidiaNoFeedbackResults != null)
                {
                    retVal = TravidiaNoFeedbackResults.Count;
                }
                return retVal;
            }
        }
    }
}