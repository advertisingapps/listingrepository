﻿using System.ComponentModel;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class SamsDetailViewModel
    {
        // empty constructor (required?)
        public SamsDetailViewModel() 
        {
            SamsReport = new SamsResult();
        }

        //Create constructor that accepts a SAMSResult DTO object
        public SamsDetailViewModel(SamsResult rptResult)
        {
            if (rptResult == null)
            {
                SamsReport = new SamsResult();
            }
            else
                SamsReport = rptResult;
        }

        private SamsResult SamsReport { get; set; }

        public string Division { 
            get { return SamsReport.Division; }
        }

        [DisplayName("Listing Number")]
        public string ListingNumber
        {
            get { return SamsReport.ListingNumber; }
        }

        [DisplayName("Account")]
        public string Advertiser
        {
            get { return SamsReport.AdvertiserName; }
        }

        public string Title
        {
            get { return SamsReport.Title; }         
        }

        public string Category {
            get { return SamsReport.Category; }
        }

        public string StartDate
        {
            get { return SamsReport.StartDate.ToString("yyyy/MM/dd"); }
        }

        public string ExpireDate
        {
            get { return SamsReport.ExpireDate.ToString("yyyy/MM/dd"); }
        }

        [DisplayName("Ad Length")]
        public int AdLength
        {
            get { return SamsReport.AdLength; }
        }

        public string AdLengthStyle
        {
            get 
            { 
                string retVal = "numeric";
                if (SamsReport.AdLength == 0)
                {
                    retVal += " redtext";
                }
                return retVal;
            }
        }

        public string FeedName
        {
            get { return SamsReport.FeedName; }
        }

        public string FeedNameStyle
        {
            get
            {
                string retVal = "";
                if (SamsReport.FeedName == "N/A")
                {
                    retVal = "redtext";
                }
                return retVal;
            }
        }

        public string SamsAdNumber
        {
            get { return SamsReport.AdOrderNumber; }
        }
    }
}