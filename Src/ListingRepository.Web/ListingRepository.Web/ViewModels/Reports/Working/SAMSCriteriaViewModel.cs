﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Postmedia.ListingRepository.DTO.Reports.Working;


namespace Postmedia.ListingRepository.Web.ViewModels.Reports.Working
{
    public class SAMSCriteriaViewModel
    {
        private SamsCriteria Criteria { get; set; }

        // empty constructor
        public SAMSCriteriaViewModel()
        {
            Criteria = new SamsCriteria();
        }

        // constructor that accepts ReportCriteria
        public SAMSCriteriaViewModel(SamsCriteria criteria)
        {
            if (criteria == null)
            {
                Criteria = new SamsCriteria();
            }
            else
                Criteria = criteria;
        }

        
        public string Division
        {
            get { return Criteria.Division; }
            set { Criteria.Division = value; }
        }

        [DisplayName("Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? Date
        {
            get { return Criteria.Date; }
            set { Criteria.Date = value; }
        }

        [DisplayName("Email Results")]
        public bool EmailResults
        {
            get { return Criteria.EmailResults; }
            set { Criteria.EmailResults = value; }
        }

        [DisplayName("Email Address")]
        public string EmailAddress
        {
            get { return Criteria.EmailAddress; }
            set { Criteria.EmailAddress = value; }
        }
    
        public SamsCriteria getDTOForReport(){
            return Criteria;
        }
    }
}