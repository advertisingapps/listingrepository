﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Postmedia.ListingRepository.DTO.Reports;

namespace Postmedia.ListingRepository.Web.ViewModels.Reports
{
    public class DetailedUpsellCriteriaViewModel
    {
        private DetailedUpsellCriteria Criteria { get; set; }
        
        public DetailedUpsellCriteriaViewModel()
        {
            Criteria = new DetailedUpsellCriteria();
            PublishDayFilter = null;
        }

        public DetailedUpsellCriteriaViewModel(DetailedUpsellCriteria criteria)
        {
            Criteria = criteria ?? new DetailedUpsellCriteria();
        }

        [DataType(DataType.Date)]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate1
        {
            get { return Criteria.StartDate1; }
            set { Criteria.StartDate1 = value; }
        }

        [DataType(DataType.Date)]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate2
        {
            get { return Criteria.StartDate2; }
            set { Criteria.StartDate2 = value; }
        }

        [DataType(DataType.Date)]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? ExpireDate1
        {
            get { return Criteria.ExpireDate1; }
            set { Criteria.ExpireDate1 = value; }
        }

        [DataType(DataType.Date)]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? ExpireDate2
        {
            get { return Criteria.ExpireDate2; }
            set { Criteria.ExpireDate2 = value; }
        }

        [DisplayName("Upsell")]
        [DataType(DataType.Text)]
        public string UpsellCode
        {
            get { return Criteria.UpsellCode; }
            set { Criteria.UpsellCode = value; }

        }

        [DisplayName("Status")]
        [DataType(DataType.Text)]
        public string ListingPubStatus
        {
            get { return Criteria.ListingPubStatus; }
            set { Criteria.ListingPubStatus = value; }
        }

        [DisplayName("Division")]
        [DataType(DataType.Text)]
        public String DivisionCode
        {
            get { return Criteria.DivisionCode; }
            set { Criteria.DivisionCode = value; }
        }

        [DisplayName("Publication")]
        [DataType(DataType.Text)]
        public String PublicationCode
        {
            get { return Criteria.PublicationCode; }
            set { Criteria.PublicationCode = value; }

        }

        [DisplayName("Email Results")]
        public bool EmailResults
        {
            get { return Criteria.EmailResults; }
            set { Criteria.EmailResults = value; }
        }

        [DisplayName("Email Address")]
        public string EmailAddress
        {
            get { return Criteria.EmailAddress; }
            set { Criteria.EmailAddress = value; }
        }

        [DisplayName("Publishing Day Filter")]
        public long? PublishDayFilter {
            get { return Criteria.PublishingDays; }
            set { Criteria.PublishingDays = value; }
        }

        [DisplayName("Listing Source")]
        public string ListingSource { 
            get { return Criteria.ListingSource; }
            set { Criteria.ListingSource = value; }
        }

        
        public DetailedUpsellCriteria getDTOForReport()
        {
            return Criteria;
        }

    }
}