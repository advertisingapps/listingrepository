﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Postmedia.ListingRepository.DTO;
using System.ComponentModel;



namespace Postmedia.ListingRepository.Web.ViewModels
{
    public class ListingSummaryViewModel
    {
        public ListingSummary ListingSummary { get; internal set; }
        
        public ListingSummaryViewModel()
        {
            ListingSummary = new ListingSummary();
        }

        public ListingSummaryViewModel(ListingSummary summaries)
        {
            if (summaries == null)
            {
                ListingSummary = new ListingSummary();
            }
            else
                ListingSummary = summaries;
        }

        public long Id
        {
            get { return ListingSummary.Id; }
        }

        public string Division
        {
            get { return ListingSummary.Division; }
        }

        [DisplayName("Listing #")]
        [StringLength(25)]
        public string ListingNumber
        {
            get { return ListingSummary.ListingNumber; }
        }

        [StringLength(60)]
        public string Title
        {
            get
            {
                string retval = string.Empty;

                //title is not null
                if (!string.IsNullOrEmpty(ListingSummary.Title))
                {
                    if (ListingSummary.Title.Length > 50)
                    {
                        retval = ListingSummary.Title.Substring(0, 50) + "...";
                    }
                    else
                    {
                        retval = ListingSummary.Title;
                    }
                }
                return retval;
            }
        }
        
        [StringLength(25)]
        public string Source
        {
            get { return ListingSummary.Source; }
        }
        
        public string Account
        {
            get { return ListingSummary.Account; }
        }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate
        {
            get { return ListingSummary.StartDate; }
        }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? ExpireDate
        {
            get { return ListingSummary.ExpireDate; }
        }

        public string StartDateString 
        {
            get { return ListingSummary.StartDate.ToString("yyyy/MM/dd"); }
        }

        public string ExpireDateString
        {
            get { return ListingSummary.ExpireDate.ToString("yyyy/MM/dd"); }
        }

        public string Category 
        {
            get 
            { 
                string retVal = "--Category Unavailable--";

                if (!string.IsNullOrEmpty(ListingSummary.Category))
                {
                    //return the first value only (see note above)
                    retVal = ListingSummary.Category;
                }
                return retVal;
            }
        }
    }
}