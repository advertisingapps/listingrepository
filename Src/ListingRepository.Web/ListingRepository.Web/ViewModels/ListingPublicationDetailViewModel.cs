﻿using System.ComponentModel;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Web.ViewModels
{
    public class ListingPublicationDetailViewModel
    {
        // empty constructor (required?)
        public ListingPublicationDetailViewModel() 
        {
            ListingPublicationReportDetail = new ListingPubResult();
        }
        // overloaded constructor
        public ListingPublicationDetailViewModel(ListingPubResult rptResult)
        {
            if (rptResult == null)
            {
                ListingPublicationReportDetail = new ListingPubResult(); 
            }
            else
                ListingPublicationReportDetail = rptResult;
        }

        private ListingPubResult ListingPublicationReportDetail { get; set; }

        public long Id
        {
            get { return ListingPublicationReportDetail.Id; }
        }

        [DisplayName("Division")]
        public string DivisionName 
        { 
            get { return ListingPublicationReportDetail.Division; }
        }

        [DisplayName("Account #")]
        public string Account
        {
            get { return ListingPublicationReportDetail.AccountNumber; }
        }

        [DisplayName("Listing Number")]
        public string ListingNumber
        {
            get { return ListingPublicationReportDetail.ListingNumber; }
        }

        [DisplayName("Publication")]
        public string PublicationName
        {
            get { return ListingPublicationReportDetail.Publication; }
        }

        public string AdOrderNumber
        {
            get { return ListingPublicationReportDetail.AdOrderNumber; }
        }            

        public string FeedName
        {
            get { return ListingPublicationReportDetail.FeedName; }
        }            

        public string ListingPublicationStatus
        {
            get { return ListingPublicationReportDetail.Status; }
        }

        public string ListingSource
        {
            get { return ListingPublicationReportDetail.Source; }
        }

        public string Title
        {
            get 
            {
                string retval = string.Empty;

                //title is not null
                if (!string.IsNullOrEmpty(ListingPublicationReportDetail.Title))
                {
                    if (ListingPublicationReportDetail.Title.Length > 50)
                    {
                        retval = ListingPublicationReportDetail.Title.Substring(0, 50) + "...";
                    }
                    else
                    {
                        retval = ListingPublicationReportDetail.Title;
                    }
                }
                return retval;
            }
        }

        public string Category {
            get 
            {
                string retVal = "--Category Unavailable--";

                if (!string.IsNullOrEmpty(ListingPublicationReportDetail.Category))
                {
                    retVal = ListingPublicationReportDetail.Category;
                }
                return retVal;
            }
        }

        public string StartDate
        {
            get { return ListingPublicationReportDetail.StartDate.ToString("yyyy/MM/dd"); }
        }

        public string ExpireDate
        {
            get { return ListingPublicationReportDetail.ExpireDate.ToString("yyyy/MM/dd"); }
        }

    }
}