﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Postmedia.ListingRepository.DTO;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Postmedia.ListingRepository.Web.Controllers.ModelBinders;
using Postmedia.ListingRepository.Core;


namespace Postmedia.ListingRepository.Web.ViewModels
{
    [ModelBinder(typeof(ListingDetailViewModelBinder))]
    public class ListingDetailViewModel
    {
        // empty constructor
        public ListingDetailViewModel() 
        {
            Listing = new Listing();
            ListingMediaViewModels = new List<ListingMediaViewModel>();
        }

        // constructor that accepts a listing
        public ListingDetailViewModel(Listing listing)
        {
            Listing = listing ?? new Listing();

            if (listing != null)
            {
                if ((listing.ListingPublications != null) && (ListingPubViewModels.Count == 0))
                {
                    foreach (var listPub in Listing.ListingPublications)
                    {
                        ListingPubViewModels.Add(new ListingPublicationViewModel(listPub));
                    }
                }
                else
                {
                    ListingPubViewModels = new List<ListingPublicationViewModel>();
                }

                ListingMediaViewModels = new List<ListingMediaViewModel>();
                if (listing.MediaItems != null)
                {
                    foreach (var listMedia in Listing.MediaItems)
                    {
                        ListingMediaViewModels.Add(new ListingMediaViewModel(listMedia, Listing.Source));
                    }
                }

                if (listing.CacheEntries != null)
                {
                    foreach (var cacheEntry in Listing.CacheEntries)
                    {
                        CacheEntryViewModels.Add(new ListingCacheEntryViewModel(cacheEntry));
                    }
                }
            }
        }

        public Listing Listing { get; set; }

        // Listing Details Information
        [Required]
        public int Id
        {
            get { return (int)Listing.Id; }
        }

        [Required]
        [DisplayName("Listing #")]
        public string ListingNumber
        {
            get { return Listing.ListingNumber ?? string.Empty; }
        }

        [Required]
        [DisplayName("Division")]
        public string ListingDivision
        {
            get
            {
                string retval = string.Empty;

                if ((Listing.AdBooking != null) && (Listing.AdBooking.Division != null))
                {
                    retval = Listing.AdBooking.Division.Name ?? string.Empty; 
                }
                return retval;
            }
        }

        [Required]
        [DisplayName("Source")]
        public string ListingSource 
        {
            get { return Listing.Source ?? string.Empty; }
        }

        [Required]
        [DisplayName("Ad Type")]
        public string ListingAdType
        {
            get { return Listing.AdType ?? string.Empty; }
        }

        [Required]
        [DisplayName("Account")]
        public string AccountNumber
        {
            get
            {
                string retval = string.Empty;

                if (Listing.AdBooking != null)
                {
                    retval = Listing.AdBooking.AccountNumber ?? string.Empty; 
                }
                return retval;
            }
        }

        [Required]
        [DisplayName("Ad Order #")]
        public string AdOrderNumber
        {
            get
            {
                string retval = string.Empty;

                if (Listing.AdBooking != null)
                {
                    retval = Listing.AdBooking.AdOrderNumber ?? string.Empty;
                }
                return retval;
            }
        }

        [DisplayName("Agency Account #")]
        public string AgencyAccountNumber
        {
            get
            {
                string retval = string.Empty;

                if (Listing.AdBooking != null)
                {
                    retval = Listing.AdBooking.AgencyAccountNumber ?? string.Empty;
                }
                return retval;
            }
        }

        [DisplayName("External Account #")]
        public string ExternalAccountNumber
        {
            get
            {
                string retval = string.Empty;

                if (Listing.AdBooking != null)
                {
                    retval = Listing.AdBooking.ExternalAccountNumber ?? string.Empty;
                }
                return retval;
            }
        }
        
        [DisplayName("External Ad Order #")]
        public string ExternalAdOrderNumber
        {
            get
            {
                string retval = string.Empty;

                if (Listing.AdBooking != null)
                {
                    retval = Listing.AdBooking.ExternalAdOrderNumber ?? string.Empty;
                }
                return retval;
            }
        }

        [Required]
        [DisplayName("Customer Name")]
        public string AdvertiserName
        {
            get
            {
                string retval = string.Empty;

                if (Listing.Advertiser != null)
                {
                    retval = Listing.Advertiser.Name ?? string.Empty;
                }
                return retval;
            }
        }

        [Required]
        [DisplayName("Company Name")]
        public string AdvertiserCompanyName
        {
            get
            {
                string retval = string.Empty;

                if (Listing.Advertiser != null)
                {
                    retval = Listing.Advertiser.CompanyName ?? string.Empty;
                }
                return retval;
            }
        }

        [DisplayName("First Name")]
        public string ContactFirstName
        {
            get
            {
                string retval = string.Empty;

                if ((Listing.Advertiser != null) && (Listing.Advertiser.Contact != null))
                {
                    retval = Listing.Advertiser.Contact.FirstName ?? string.Empty;
                }
                return retval;
            }
            set 
            { 
                if(Listing.Advertiser.Contact == null)
                {
                    Listing.Advertiser.Contact = new Contact();
                }
                Listing.Advertiser.Contact.FirstName = value; 
            }
        }

        [DisplayName("Last Name")]
        public string ContactLastName
        {
            get
            {
                string retval = string.Empty;

                if ((Listing.Advertiser != null) && (Listing.Advertiser.Contact != null))
                {
                    retval = Listing.Advertiser.Contact.LastName ?? string.Empty;
                }
                return retval;
            }
            set
            {
                if (Listing.Advertiser.Contact == null)
                {
                    Listing.Advertiser.Contact = new Contact();
                }
                Listing.Advertiser.Contact.LastName = value;
            }

        }

        [RegularExpression(@"(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})",ErrorMessage="Entry for Contact email is not a valid email format")]
        [DisplayName("Email")]
        public string ContactEmail
        {
            get
            {
                string retval = string.Empty;

                if( (Listing.Advertiser != null) && (Listing.Advertiser.Contact != null))
                {
                    retval = Listing.Advertiser.Contact.Email ?? string.Empty;
                }
                return retval;
            }
            set
            {
                if (Listing.Advertiser.Contact == null)
                {
                    Listing.Advertiser.Contact = new Contact();
                }
                Listing.Advertiser.Contact.Email = value;
            }

        }

        [DisplayName("Phone")]
        public string ContactPhone
        {
            get
            {
                string retval = string.Empty;

                if ((Listing.Advertiser != null) && (Listing.Advertiser.Contact != null))
                {
                    retval = Listing.Advertiser.Contact.Phone ?? string.Empty;
                }
                return retval;
            }
            set
            {
                if (Listing.Advertiser.Contact == null)
                {
                    Listing.Advertiser.Contact = new Contact();
                } 
                Listing.Advertiser.Contact.Phone = value;
            }

        }

        // Ad Text information 
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ad Description is required.")]
        [DisplayName("Description")]
        [AllowHtml]
        public string Description
        {
            get
            {
                return Listing.Description ?? string.Empty;
            }
            set
            {
                Listing.Description = value;
            }
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Ad Title is required")]
        [DisplayName("Title")]
        public string Title
        {
            get { return Listing.Title; }
            set { Listing.Title = value; }
        }

        // properties to display ListingPub min and max dates for the detail portion of the form
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Start Date")]
        public DateTime? MinListingPubStartDate
        {
            get { return ListingPublications.Min(i => i.StartDate); }
        }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("End Date")]
        public DateTime? MaxListingPubExpireDate
        {
            get { return ListingPublications.Max(i => i.ExpireDate); }
        }

        public List<Property> Properties
        {
            get { return Listing.Properties; }
            set { Listing.Properties = value; }
        }

        public List<ListingPublication> ListingPublications
        {
            get { return Listing.ListingPublications; }
            set { Listing.ListingPublications = value; }
        }

        // properties used for lookups on the ListingPublication records 
        public List<Publication> Publications { get; set; }
        public List<Category> Categories { get; set; }
        public List<ListingStatusLookup> Statuses { get; set; }

        // properties used when inserting a new Metadata record 
        public string MetaDataName { get; set; }
        public string MetaDataValue { get; set; }
        
        private List<ListingCacheEntryViewModel> _cacheEntryViewModels;
        public List<ListingCacheEntryViewModel> CacheEntryViewModels 
        {
            get { return _cacheEntryViewModels ?? (_cacheEntryViewModels = new List<ListingCacheEntryViewModel>()); }
            set { _cacheEntryViewModels= value; }
        }

        private List<ListingPublicationViewModel> _listingPublicationViewModels;
        public List<ListingPublicationViewModel> ListingPubViewModels 
        {
            get { return _listingPublicationViewModels ?? (_listingPublicationViewModels = new List<ListingPublicationViewModel>()); }
            set { _listingPublicationViewModels = value; }
        }

        private List<ListingMediaViewModel> _listingMediaViewModels;
        public List<ListingMediaViewModel> ListingMediaViewModels
        {
            get { return _listingMediaViewModels ?? (_listingMediaViewModels = new List<ListingMediaViewModel>()); }
            set { _listingMediaViewModels = value; }    
        }
        
        public int listingPubUpsellCount {
            get {
                int count = 0;

                if (ListingPublications.Any())
                {
                    foreach (var listPub in ListingPublications)
                    {
                        if (listPub.Upsells != null)
                        {
                            count = count + listPub.Upsells.Count;
                        }
                    }
                }
                return count;              
            }
        }

        public int listingMediaCount
        {
            get
            {
                int count = 0;
                if(ListingMediaViewModels != null)
                {
                    count = ListingMediaViewModels.Count;
                }
                return count;
            }
        }

        public Listing GetDTOForSave()
        {
            // TODO: need to find a better way to do this, as this approach is really poor.
            // doing this because the viewmodels are not working.  Only the viewmodel contains the 
            // updated values for the editable fields, but the viewmodels are not containing the values
            // for either listingpublication upsells or categories.
            if (Listing.ListingPublications.Count() == ListingPubViewModels.Count())
            {
                for (int i = 0; i < ListingPubViewModels.Count(); i++)
                {
                    // set editable Listingpublication fields if they have changed.  Using the same array
                    // value should work because the viewmodel is loaded in the same order
                    if (Listing.ListingPublications[i].StartDate != ListingPubViewModels[i].StartDate)
                    {
                        Listing.ListingPublications[i].StartDate = ListingPubViewModels[i].StartDate;
                    }
                    if (Listing.ListingPublications[i].ExpireDate != ListingPubViewModels[i].ExpireDate)
                    {
                        Listing.ListingPublications[i].ExpireDate = ListingPubViewModels[i].ExpireDate;
                    }
                    if (Listing.ListingPublications[i].Status != ListingPubViewModels[i].Status)
                    {
                        Listing.ListingPublications[i].Status = ListingPubViewModels[i].Status;
                    }
                }
            }

            Listing.Updater = UpdateFrom.ListingRepository.ToString();

            //pull DTOs from ViewModels and re-insert updated values
//            Listing.ListingPublications = ListingPubViewModels.Select(lpub => lpub.GetListingPublicationDTOFromViewModel()).ToList();
            return Listing;
        }

        [DisplayName("Administrative Lock (Do Not Export On Update)")]
        public bool HasAdminLock
        {
            get
            {
                return Listing.HasAdminLock;
            }
            set { Listing.HasAdminLock = value; }

        }
    }
}
