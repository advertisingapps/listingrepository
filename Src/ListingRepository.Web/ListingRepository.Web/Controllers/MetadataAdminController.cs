﻿using System;
using System.Web.Mvc;
using Postmedia.ListingRepository.Core.Communication;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Web.Controllers
{
    public class MetadataAdminController : Controller
    {
        //
        // GET: /MetadataAdmin/

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult UpdateYmm()
        {
            try
            {
                var summary = RESTClient.Instance.Call<YearMakeModelImportSummary>(RESTMethod.UpdateYearMakeModel);
                return PartialView("_YMMSummaryPartial", summary);
            }
            catch (Exception e)
            {
                var ymmsummary = new YearMakeModelImportSummary
                                     {
                                         NumberOfRecordsImported = 0,
                                         TotalNumberOfRecords = 0,
                                         FailureReason = e.Message,
                                         Success = false
                                     };
                return PartialView("_YMMSummaryPartial", ymmsummary);
            }
        }

        public PartialViewResult ImportOodleCategories()
        {
            try
            {
                var summary = RESTClient.Instance.Call<OodleImportSummary>(RESTMethod.UpdateCategories);
                return PartialView("_OodleSummaryPartial", summary);
            }
            catch (Exception e)
            {
                var summary = new OodleImportSummary
                                  {
                                      importUrl = string.Empty,
                                      numberOfHeaders = 0,
                                      numberOfItems = 0
                                  };
                return PartialView("_OodleSummaryPartial", summary);
            }
        }
    }
}
