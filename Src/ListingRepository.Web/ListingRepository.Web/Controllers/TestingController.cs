﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.Communication;
using Postmedia.ListingRepository.Core.Translators;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.Web.Controllers
{
    public class TestingController : Controller
    {
        public ActionResult Index()
        {
            //TestDeReSerialize();
            TestBatchExport(ExportTarget.AdPerfect.ToString());
            return View();
        }

        public void TestYMMImport()
        {
            var summary = RESTClient.Instance.Call<YearMakeModelImportSummary>(RESTMethod.UpdateYearMakeModel);
            if(summary != null)
            {
                var i = 0;
            }
        }

        public void TestCategoryDocumentCreation()
        {
            RESTClient.Instance.Execute(RESTMethod.UpdateCategories);
        }

        public void TestPost()
        {
            var listing = CreateListingDTO();
            var retVal = RESTClient.Instance.Call<Listing, Listing>(listing, RESTMethod.CreateListing);
            if (retVal != null)
            {
            }
     
        }

        public void TestGet()
        {
            var listing = RESTClient.Instance.Call<Listing>("CAL369433", RESTMethod.GetListingByNumber);
            if(listing != null)
            {
                //woo we're good!
            }
        }

        public void TestGetSave()
        {
            var listing = RESTClient.Instance.Call<Listing>(33, RESTMethod.GetListingById );
            if (listing != null)
            {
                listing.Title = "Updated by Aaron";
                var savedListing = RESTClient.Instance.Call<Listing, Listing>(
                    listing, 
                    33, 
                    RESTMethod.UpdateListing);

                //if(savedListing.Title == listing.Title)
                //{
                //    bool success = true;
                //    if(success)
                //    {

                //    }
                //}
            }
        }

        public void TestCreate()
        {
            var listing = CreateListingDTO();
            if (listing != null)
            {
                var returnedListing = RESTClient
                    .Instance
                    .Call<Listing, Listing>(listing, RESTMethod.CreateListing);
            }
        }

        public void TestDeReSerialize()
        {
            var myListing = CreateListingDTO();
            var xml = StreamTranslator<Listing>.DtoToXml(myListing);
            
            var element = XElement.Parse(xml);
            var result = new MemoryStream();
            var writer = new XmlTextWriter(result, Encoding.UTF8);
            element.WriteTo(writer);
            
            var listingFromStream = StreamTranslator<Listing>.StreamToDto(result);
        }

        public void TestBatchExport(string feedName)
        {
            //RESTClient.Instance.Call<ExportSummary>(RESTMethod.CelebratingCacheExport);   
            var cacheSummary = RESTClient
                    .Instance
                    .Call<ExportCriteria, ExportSummary>(
                    new ExportCriteria{ FeedName = feedName, RunDate = new DateTime(2012, 1,4), DailyExport = false}
                    , RESTMethod.ExportOutBoundCache);
        }

        public List<ListingSummary> TestCriteriaSearch()
        {
            var criteria = new ListingCriteria();
            criteria.Division = "Mont";
            
            //var listingSummaries = RESTClient.Call<ListingCriteria, List<ListingSummary>>(summaries, "listingsummaries");
            //var listingSummaries = RESTClient.Call<List<ListingSummary>>(RESTMethod.GetAllListingSummaries);
            //var listingSummaries = RESTClient.Call<Listing>(RESTMethod.RetrieveListing, "1");

            var listings = RESTClient.Instance.Call
                <ListingCriteria, //in type
                List<ListingSummary>> //out type
                (criteria, RESTMethod.GetListingSummariesUsingCriteria);


            return null;
        }

        public void TestCreateListing()
        {
            var listingDTO = CreateListingDTO();
            var listing = RESTClient.Instance.Call
                <Listing,
                    Listing> //out type
                (listingDTO, RESTMethod.CreateListing);

            // check to see that the listingDTO has the ID value set 
            // (indicating that it was inserted into the DB)

        }

        public Listing CreateListingDTO()
        {
            var myListing = new Listing();

            myListing.ListingNumber = "VAN363593";
            myListing.Source = "NGEN";
            myListing.AdType = "Liner";
            myListing.Title = "Happy 66th Anniversary to you";
            myListing.Description = "Happy 66th Anniversary  to you";

            myListing.AdBooking = new AdBooking();
            myListing.AdBooking.AccountNumber = "T493204";
            myListing.AdBooking.AdOrderNumber = "363593";
            myListing.AdBooking.ExternalAccountNumber = "VAN9@29500";

            myListing.AdBooking.Division = new Division() { Code = "VAN" };

            myListing.Advertiser = new Advertiser
            {
                Name = "TestAddressName"
                , CompanyName = "DatCompany"
                //,
                //Address = new Address
                //    {
                //        AddressLines = new List<AddressLines>()
                //                           {
                //                               new AddressLines() {LineText = "123 Main St.", Number = 0},
                //                               new AddressLines() {LineText = "4th Floor", Number = 1}
                //                           },
                //        City = "Saskatoon", 
                //        Province = "SK" ,
                //        PostalCode = "H0H0H0",
                //        Country = "CA"
                //    }
                //,
                //Contact = new Contact()
                //    {
                //        FirstName = "Test",
                //        LastName = "Customer",
                //        Email = "test.customer@shaw.ca",
                //        Phone = "306-555-1212"
                //    }

            };
                     
            //myListing.Images = new List<Image>()
            //                       {
            //                          new Image() {
            //                                        Url = "http://www.postmedia.com/wp-content/themes/postmedia/common/images/logos/pmn-logo-3.png"
            //                                   }
            //                        };
            //myListing.Properties = new List<Property>()
            //                           {
            //                               new Property()
            //                                   {
            //                                       Code = "CANON-META-NAME",
            //                                       Value = "Some Metadata"
            //                                   }
            //                           };
            //myListing.ListingPublications = new List<ListingPublication>()
            //                                    {
            //                                        new ListingPublication()
            //                                            {
            //                                                Id = 109,
            //                                                StartDate = DateTime.Parse("2011-12-07"),
            //                                                ExpireDate = DateTime.Parse("2011-12-08"),
            //                                                Status = "A",
            //                                                //Categories = new List<Category>() { new Category() { Code = "GMerch", PropertyCode="General Merchandise" }},
            //                                                //Upsells = new List<Upsell>()
            //                                                //              {
            //                                                //                  new Upsell() { Code = "HIGH", PropertyCode = "Highlight"}
            //                                                //              },
            //                                                              Publication = new Publication { Id = 5, Code = "37" }
            //                                            }
            //                                    };
            return myListing;
        }
    }
}
