﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports;
using Postmedia.ListingRepository.DTO.Reports.Working;
using Postmedia.ListingRepository.Web.ViewModels.Reports;
using Postmedia.ListingRepository.Web.ViewModels.Reports.Working;
using Postmedia.ListingRepository.Core.Communication;


namespace Postmedia.ListingRepository.Web.Controllers
{
    public class ReportListController : Controller
    {
        //
        // GET: /ReportList/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /ReportList/WorkingSAMSSearch/
        public ActionResult WorkingSAMSSearch(SAMSSearchViewModel svm)
        {
            // populate the divisions lookup
            var divisions = RESTClient.Instance.Call<List<Division>>(RESTMethod.GetDivisions);
            svm.Divisions = divisions;

            // check if the view model is null (first time hitting page)
            if (svm.SamsCriteria == null)
            {
                // if true then create a new viewmodel
                svm.SamsCriteria = new SAMSCriteriaViewModel(null);

                // set up some default summaries values
                var defaultDate = DateTime.Today;
                svm.SamsCriteria.Date = defaultDate;
            }

            if (ModelState.IsValid)
            {
                var results = RESTClient.Instance.Call<SamsCriteria, List<SamsResult>>(svm.SamsCriteria.getDTOForReport(), RESTMethod.GetWorkingSamsReport);
                svm.SamsResults = new List<SamsDetailViewModel>();
                foreach (var workdetail in results)
                {
                    svm.SamsResults.Add(new SamsDetailViewModel(workdetail));
                }
            }
            return View(svm);
        }


        // GET: /ReportList/WorkingSamsNoFeedbackSearch/
        public ActionResult WorkingSamsNoFeedbackSearch(SamsNoFeedbackSearchViewModel svm)
        {
            // populate the divisions lookup
            var divisions = RESTClient.Instance.Call<List<Division>>(RESTMethod.GetDivisions);
            svm.Divisions = divisions;

            // check if the view model is null (first time hitting page)
            if (svm.SamsNoFeedbackCriteria == null)
            {
                // if true then create a new viewmodel
                svm.SamsNoFeedbackCriteria = new SamsNoFeedbackCriteriaViewModel(null);

                // set up some default summaries values
                var defaultDate = DateTime.Today;
                svm.SamsNoFeedbackCriteria.Date = defaultDate;
            }

            if (ModelState.IsValid)
            {
                var results = RESTClient.Instance.Call<SamsNoFeedbackCriteria, List<SamsNoFeedbackResult>>(svm.SamsNoFeedbackCriteria.getDTOForReport(), RESTMethod.GetWorkingSamsNoFeedbackReport);
                svm.SamsNoFeedbackResults = new List<SamsNoFeedbackDetailViewModel>();
                foreach (var workdetail in results)
                {
                    svm.SamsNoFeedbackResults.Add(new SamsNoFeedbackDetailViewModel(workdetail));
                }
            }
            return View(svm);
        }
        
        
        
        // GET: /ReportList/WorkingDigitalDisplaySearch/
        public ActionResult QRCodeUrlSearch(QRCodeUrlSearchViewModel ddvm)
        {
            // populate the divisions lookup
            var divisions = RESTClient.Instance.Call<List<Division>>(RESTMethod.GetDivisions);
            ddvm.Divisions = divisions;

            // check if the view model is null (first time hitting page)
            if (ddvm.QrCodeUrlCriteria == null)
            {
                // if true then create a new viewmodel
                ddvm.QrCodeUrlCriteria = new QRCodeUrlCriteriaViewModel(null);

                // set up some default summaries values
                var defaultDate = DateTime.Today;
                ddvm.QrCodeUrlCriteria.Date = defaultDate;
            }

            if (ModelState.IsValid)
            {
                var results = RESTClient.Instance.Call<QRCodeUrlCriteria, List<QRCodeUrlResult>>(ddvm.QrCodeUrlCriteria.getDTOForReport(), RESTMethod.GetQRCodeUrlReport);
                ddvm.QrCodeUrlResults = new List<QRCodeUrlDetailViewModel>();
                foreach (var workdetail in results)
                {
                    ddvm.QrCodeUrlResults.Add(new QRCodeUrlDetailViewModel(workdetail));
                }
            }
            return View(ddvm);
        }

        // GET: /ReportList/WorkingUpsellSearch/
        public ActionResult WorkingUpsellSearch(UpsellSearchViewModel uvm)
        {
            // populate the divisions lookup
            var divisions = RESTClient.Instance.Call<List<Division>>(RESTMethod.GetDivisions);
            uvm.Divisions = divisions;

            // check if the view model is null (first time hitting page)
            if (uvm.UpsellCriteria == null)
            {
                // if true then create a new viewmodel
                uvm.UpsellCriteria = new UpsellCriteriaViewModel(null);

                // set up some default summaries values
                uvm.UpsellCriteria.StartDate = DateTime.Today.AddDays(-7);
                uvm.UpsellCriteria.EndDate = DateTime.Today;
            }

            if (ModelState.IsValid)
            {
                var results = RESTClient.Instance.Call<UpsellCriteria, List<UpsellResult>>(uvm.UpsellCriteria.getDTOForReport(), RESTMethod.GetWorkingUpsellReport);
                uvm.UpsellResults = new List<UpsellDetailViewModel>();
                foreach (var upselldetail in results)
                {
                    uvm.UpsellResults.Add(new UpsellDetailViewModel(upselldetail));
                }
            }
            return View(uvm);
        }

        public ActionResult DetailedUpsellSearch(DetailedUpsellSearchViewModel dvm)
        {
            var divisions = RESTClient.Instance.Call<List<Division>>(RESTMethod.GetDivisions);
            var upsellValues = RESTClient.Instance.Call<List<UpsellValueLookup>>(RESTMethod.GetUpsellValues);
            var publications = RESTClient.Instance.Call<List<Publication>>(RESTMethod.GetPublications);
            var status = RESTClient.Instance.Call<List<ListingStatusLookup>>(RESTMethod.GetListingStatuses);
            var sources = RESTClient.Instance.Call<List<SourceSystemLookup>>(RESTMethod.GetSourceSystems);
            
            dvm.Divisions = divisions;
            dvm.Sources = sources;
            dvm.UpsellValues = upsellValues;
            dvm.Publications = publications;
            dvm.Status = status;

            if (dvm.DetailedUpsellCriteria == null)
            {
                dvm.DetailedUpsellCriteria = new DetailedUpsellCriteriaViewModel(null);
                var firstOfTheMonth = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                dvm.DetailedUpsellCriteria.PublicationCode = "40";
                dvm.DetailedUpsellCriteria.UpsellCode = "FEAT";
                dvm.DetailedUpsellCriteria.StartDate1 = firstOfTheMonth;
                dvm.DetailedUpsellCriteria.StartDate2 = firstOfTheMonth.AddMonths(1).AddDays(-1);
            }

            if (ModelState.IsValid)
            {
                var results =
                    RESTClient.Instance.Call<DetailedUpsellCriteria, List<DetailedUpsellResult>>(
                        dvm.DetailedUpsellCriteria.getDTOForReport(), RESTMethod.GetDetailedUpsellReport);
                
                dvm.DetailedUpsellResults = new List<DetailedUpsellDetailViewModel>();

                foreach (var result in results)
                {
                    dvm.DetailedUpsellResults.Add(new DetailedUpsellDetailViewModel(result));
                }

                //if (dvm.DetailedUpsellCriteria.PublishDayFilter != null) dvm.DetailedUpsellResults = dvm.DetailedUpsellResults.FindAll(x => x.PublishingDays == dvm.DetailedUpsellCriteria.PublishDayFilter);
            }

            return View(dvm);
        }        
    }
}
