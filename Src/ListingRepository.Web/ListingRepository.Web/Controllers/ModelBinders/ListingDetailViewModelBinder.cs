﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Web.Infrastructure.DynamicValidationHelper;
using Postmedia.ListingRepository.Core.Communication;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.Web.ViewModels;

namespace Postmedia.ListingRepository.Web.Controllers.ModelBinders
{
    [ValidateInput(false)]
    public class ListingDetailViewModelBinder : DefaultModelBinder
    {
        private const string PropertyPrefix = "pvms";
        private const string ListingPublicationPrefix = "ListingPubViewModels[";

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (controllerContext == null)
            {
                throw new ArgumentNullException("controllerContext");
            }
            if (bindingContext == null)
            {
                throw new ArgumentNullException("bindingContext");
            }

            //UNvalidated form posting
            //var formValues = controllerContext.HttpContext.Request.Form;
            Func<NameValueCollection> formGetter;
            Func<NameValueCollection> queryStringGetter;
            ValidationUtility.GetUnvalidatedCollections(HttpContext.Current, out formGetter, out queryStringGetter);
            var formValues = formGetter();

            //Get the ID value from the form's hidden field
            var value = (int)bindingContext.ValueProvider.GetValue("Id").ConvertTo(typeof(int));


            //retrieve the existing model from the database to preserve 
            //all the data that ISN'T modified by the form
            var listing = RESTClient.Instance.Call<Listing>(value, RESTMethod.GetListingById);
            listing.Properties = RetrieveMetadataFormValues(formValues);

            /* 
             * NOTE: creating the new binding based off a new ListingDetailViewModel
             * and using the retrieved Listing overridden with the form values
             * as the base for the binding 
             * ... only the metadata should be required since the rest "should" 
             * get automagically bound in the base binder (we'll see) 
             */

            if (bindingContext.ModelType == typeof(ListingDetailViewModel))
            {
                var newBindingContext =
                    new ModelBindingContext()
                    {
                        ModelMetadata =
                            ModelMetadataProviders.Current.GetMetadataForType(
                                () => new ListingDetailViewModel()
                                          {
                                              Listing = listing
                                          }, typeof(ListingDetailViewModel)
                            ),
                        ModelState = bindingContext.ModelState,
                        ValueProvider = bindingContext.ValueProvider
                    };

                //we get here and we're happy
                var bindModelResult = base.BindModel(controllerContext, newBindingContext);
                return bindModelResult;
            }
            
            //Should never get here (but its a catchall to prevent crashing)
            return base.BindModel(controllerContext, bindingContext);
        }

        private List<Property> RetrieveMetadataFormValues(NameValueCollection formValues)
        {
            var guids = new List<string>();
            
            foreach (var key in formValues.AllKeys.Where(k=>k.StartsWith(PropertyPrefix)))
            {
                if ((key.Length > 38) && (key.Substring(0, 4) == PropertyPrefix))
                {
                    var guid = key.Substring(4, 38);
                    if (!guids.Contains(guid))
                    {
                        guids.Add(guid);
                    }
                }
            }

            var properties = new List<Property>();
            foreach (var guid in guids)
            {
                var property = new Property();
                var baseString = PropertyPrefix + guid + ".";
                var codeString = baseString + "Code";
                var valueString = baseString + "Value";

                property.Code = formValues[codeString];
                property.Value = formValues[valueString];
                properties.Add(property);
            }
            return properties;
        }
        
    }
}