﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using Postmedia.ListingRepository.Core.Translators;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.BatchFormApplication
{
    public class LrBatchProxy
    {
        public ExportCriteria Criteria { get; set; }
        public ExportSummary Summary { get; set; }
        
        public void ExecuteBatchAsync()
        {
            var url = ConfigurationManager.AppSettings.Get("ServiceEndpoint") + "exportcache";
       
            var rest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
            if (rest != null)
            {
                rest.Method = "PUT";
                rest.BeginGetRequestStream(GetRequestStreamCallBack, rest);
            }
        }

        private void GetRequestStreamCallBack(IAsyncResult ar)
        {
            var request = (HttpWebRequest) ar.AsyncState;
            Stream postStream = request.EndGetRequestStream(ar);

            Criteria = new ExportCriteria
                           {
                               FeedName = "Celebrating", 
                               RunDate = DateTime.Now, 
                               DailyExport = false
                           };
            var streamCriteria = StreamTranslator<ExportCriteria>.DTOToStream(Criteria);
            var readByte = new byte[(int)streamCriteria.Length];
            streamCriteria.Read(readByte, 0, (int)streamCriteria.Length);

            postStream.Write(readByte, 0, readByte.Length);
            postStream.Flush();
            postStream.Close();

            request.BeginGetResponse(BatchAsyncCallBack, request);

        }

        private void BatchAsyncCallBack(IAsyncResult iar)
        {
            var rest = (HttpWebRequest)iar.AsyncState;
            var response = rest.EndGetResponse(iar) as HttpWebResponse;
            if(response!= null)
            {
                var result = StreamTranslator<Listing>.StreamToDTO(response.GetResponseStream());
                Summary = null;
            }
        }

    }
}
