﻿using System;
using System.Configuration;
using System.Net;
using System.Reflection;
using System.Threading;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.EMail;
using Postmedia.ListingRepository.Core.Translators;
using Postmedia.ListingRepository.DTO;
using log4net;

namespace Postmedia.ListingRepository.BatchExport
{
    public class BatchExport
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        static ManualResetEvent _resetEvent = new ManualResetEvent(false);
        private static ExportCriteria Criteria { get; set; }

        private const string WebMethod = "POST";
        private const string ContentType = "application/xml";

        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();

                Log.Info("Begining batch export process");
                Console.WriteLine("Begining batch export process");
                var configurator = new BatchExportConfigurator();
                Log.Info("parsing command line arguments");
                Console.WriteLine("parsing command line arguments");

                //TODO: include 'if' for help file.

                Criteria = configurator.GetExportCriteria(args);

                if (Criteria != null)
                {
                    Log.Info("Criteria parsed successfully executing batch");
                    Console.WriteLine("Criteria parsed successfully executing batch");
                    ExecuteBatchAsync();
                }
            }
            catch (Exception e)
            {
                SendExportWarningEmail(e);
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public static void ExecuteBatchAsync()
        {
            try
            {

                Log.Info("Retrieving service endpoint");
                var url = ConfigurationManager.AppSettings.Get("ServiceEndpoint") ;
                Log.Info("Service endpoint retrieved");
                Console.WriteLine("Service endpoint retrieved");
                var rest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
                Log.Info("Web request created at: " + url);
                Console.WriteLine("Web Request Created at" + url);
                if (rest != null)
                {
                    rest.Method = WebMethod;
                    rest.ContentType = ContentType;
                    Log.Info("Begining request stream");
                    Console.WriteLine("Begining request stream");
                    
                    rest.BeginGetRequestStream(GetRequestStreamCallBack, rest);
                }
                _resetEvent.WaitOne();
            }

            catch (Exception e)
            {
                SendExportWarningEmail(e);
                Console.WriteLine("Error: " + e.Message);
                _resetEvent.Set();
            }
        }

        private static void GetRequestStreamCallBack(IAsyncResult ar)
        {
            try
            {
                Log.Info("Request stream call back entered");
                Console.WriteLine("Request stream call back entered");
                var request = (HttpWebRequest) ar.AsyncState;
                Log.Info("Creating stream from request for writing");
                Console.WriteLine("Creating stream from request for writing");
                var postStream = request.EndGetRequestStream(ar);

                Log.Info("Generating criteria byte array from criteria");
                Console.WriteLine("Generating criteria byte array from criteria");
                var criteriaBytes = StreamTranslator<ExportCriteria>.DtoToByteXml(Criteria);
                Log.Info("Writing the byte array to the request");
                Console.WriteLine("Writing the byte array to the request");
                postStream.Write(criteriaBytes, 0, criteriaBytes.Length);
                postStream.Flush();
                postStream.Close();

                Log.Info("Begining async request to LR Services");
                Console.WriteLine("Begining async request to LR Services");
                request.BeginGetResponse(BatchAsyncCallBack, request);
            }

            catch (Exception e)
            {
                SendExportWarningEmail(e);
                Console.WriteLine("Error: " + e.Message);
                _resetEvent.Set();
            }
        }

        private static void BatchAsyncCallBack(IAsyncResult iar)
        {
            try
            {
                Log.Info("Returned from Async call");
                Console.WriteLine("Returned from Async call");
                var rest = (HttpWebRequest) iar.AsyncState;
                Log.Info("Ending the response");
                Console.WriteLine("Ending the response");
                var response = rest.EndGetResponse(iar) as HttpWebResponse;
                if (response != null)
                {
                    Log.Info("Response is not null --> retrieving Export Summary");
                    Console.WriteLine("Response is not null --> retrieving Export Summary");
                    var summary = StreamTranslator<ExportSummary>.StreamToDto(response.GetResponseStream());

                    // Write summary information to Log? Console?
                    var dailyIncremental = summary.DailyExport ? "daily" : "incremental";
                    Log.Info("Completed running " + dailyIncremental + " batch for " + summary.FeedName);
                    Console.WriteLine("Completed running " + dailyIncremental + " batch for " + summary.FeedName);
                    Log.Info("Number of records exported: " + summary.SummaryCount);
                    Console.WriteLine("Number of records exported: " + summary.SummaryCount);

                    // Send an email if we get a daily summary object with no entries
                    if (summary.DailyExport && (summary.SummaryCount == 0))
                    {
                        Log.Info("Empty daily feed exported - no summary records created");
                        //SendExportWarningEmail(
                        //    new Exception("Daily export was run with no entries exported from outbound cache"));
                    }

                }

                Log.Info("Releasing the 'resource lock' program should exit");
                Console.WriteLine("Releasing the 'resource lock' program should exit");
                _resetEvent.Set();
            }

            catch (Exception e)
            {
                SendExportWarningEmail(e);
                Console.WriteLine("Error: " + e.Message);
                _resetEvent.Set();
            }
        }

        private static void SendExportWarningEmail(Exception e)
        {
            Log.Info("Sending Email");
            var subject = "ExportBatchApplication - Error during batch export";
            var body = "There was an error during the batch export process." + Environment.NewLine
                       + "Error was: " + (e != null ? e.Message : string.Empty) + Environment.NewLine
                       + Environment.NewLine
                       + "Host Machine: " + Environment.MachineName + Environment.NewLine
                       + "RunDate was: " + (Criteria != null ? Criteria.RunDate.ToString() : string.Empty) + Environment.NewLine
                       + "LastRunDate was: " + (Criteria != null ? Criteria.LastRunDate.ToString() : string.Empty) + Environment.NewLine
                       + "FeedName was: " + (Criteria != null ? Criteria.FeedName : string.Empty) + Environment.NewLine
                       + "DailyExport was: " + (Criteria != null ? Criteria.DailyExport.ToString() : string.Empty) + Environment.NewLine;

            var innerExceptionInfo = "Additional Information"
                + Environment.NewLine + "=-=-=-=-=-=-=-=-=-=-=-"+ Environment.NewLine 
                + ((e != null && e.InnerException != null) ? GetInnerExceptionInfo(e) : "-- None --");

            body += innerExceptionInfo;

            var message = MailService.CreateMessage(
                ConfigurationManager.AppSettings[ServiceSettings.Recipients.ToString()],
                ConfigurationManager.AppSettings[ServiceSettings.EmailAccount.ToString()],
                body,
                subject);
            
            MailService.SendNotificationEmail(message);
        }

        private static string GetInnerExceptionInfo(Exception exception)
        {
            var exceptionInfo = string.Empty;
            if (exception != null)
            {
                if (exception.InnerException != null)
                {
                    exceptionInfo += Environment.NewLine +
                        "InnerException" + Environment.NewLine +
                        "Message: " + exception.InnerException.Message + Environment.NewLine +
                        "StackTrace: " + exception.InnerException.StackTrace + Environment.NewLine +
                        GetInnerExceptionInfo(exception.InnerException);
                }
            }
            return exceptionInfo;
        }
    }
}
