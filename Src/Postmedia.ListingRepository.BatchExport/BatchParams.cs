namespace Postmedia.ListingRepository.BatchExport
{
    public enum BatchExportParams
    {
        FeedName, 
        DailyExport,
        RunDate
    }
}