﻿using System;
using System.Configuration;
using System.Net;
using System.Reflection;
using System.Threading;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.EMail;
using Postmedia.ListingRepository.Core.Translators;
using Postmedia.ListingRepository.DTO;
using log4net;

namespace Postmedia.ListingRepository.BatchUpdateYMM
{
    public class BatchUpdateYMM
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private const string WebMethod = "GET";
        private const string ContentType = "application/xml";
        static ManualResetEvent _resetEvent = new ManualResetEvent(false);
        
        static void Main(string[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();

                Log.Info("Begining batch export process");
                Console.WriteLine("Begining batch export process");

                Log.Info("Criteria parsed successfully executing batch");
                Console.WriteLine("Criteria parsed successfully executing batch");
                ExecuteBatchAsync();

            }
            catch (Exception e)
            {
                SendUpdateWarningEmail(e);
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public static void ExecuteBatchAsync()
        {
            try
            {

                Log.Info("Retrieving service endpoint");
                var url = ConfigurationManager.AppSettings.Get("ServiceEndpoint") ;
                Log.Info("Service endpoint retrieved");
                Console.WriteLine("Service endpoint retrieved");
                var rest = HttpWebRequest.Create(new Uri(url)) as HttpWebRequest;
                Log.Info("Web request created at: " + url);
                Console.WriteLine("Web Request Created at " + url);
                if (rest != null)
                {
                    rest.Method = WebMethod;
                    rest.ContentType = ContentType;
                    Log.Info("Begining request stream");
                    Console.WriteLine("Begining request stream");

                    rest.BeginGetResponse(GetResponseCallBack, rest);
                }
                _resetEvent.WaitOne();
            }

            catch (Exception e)
            {
                SendUpdateWarningEmail(e);
                Console.WriteLine("Error: " + e.Message);
                _resetEvent.Set();
            }
        }
        
        private static void GetResponseCallBack(IAsyncResult iar)
        {
            try
            {
                Log.Info("Returned from Async call");
                Console.WriteLine("Returned from Async call");
                var rest = (HttpWebRequest)iar.AsyncState;
                Log.Info("Ending the response");
                Console.WriteLine("Ending the response");
                var response = rest.EndGetResponse(iar) as HttpWebResponse;
                if (response != null)
                {
                    Log.Info("Response is not null --> retrieving update Summary");
                    Console.WriteLine("Response is not null --> retrieving update Summary");
                    var summary = StreamTranslator<YearMakeModelImportSummary>.StreamToDto(response.GetResponseStream());

                    Log.Info("Completed running update - " + (summary.Success ? "Success" : "Failure"));
                    Console.WriteLine("Completed running update - " + (summary.Success ? "Success" : "Failure"));
                    if(!summary.Success)
                    {
                        Log.Info("Error - " + summary.FailureReason);
                        Console.WriteLine("Error - " + summary.FailureReason);
                    }

                    Log.Info("Number of YMM Imported: " + summary.NumberOfRecordsImported);
                    Console.WriteLine("Number of YMM Imported: " + summary.NumberOfRecordsImported);

                    Log.Info("Detailed Breakdown: ");
                    Console.WriteLine("Detailed Breakdown: ");
                    Log.Info("Number of Years: " + summary.YearCount);
                    Console.WriteLine("Number of Years: " + summary.YearCount);
                    Log.Info("Number of Makes: " + summary.MakeCount);
                    Console.WriteLine("Number of Makes: " + summary.MakeCount);
                    Log.Info("Number of Models: " + summary.ModelCount);
                    Console.WriteLine("Number of Models: " + summary.ModelCount);
                }

                Log.Info("Releasing the 'resource lock' program should exit");
                Console.WriteLine("Releasing the 'resource lock' program should exit");
                _resetEvent.Set();
            }

            catch (Exception e)
            {
                SendUpdateWarningEmail(e);
                Console.WriteLine("Error: " + e.Message);
                _resetEvent.Set();
            }
        }

        private static void SendUpdateWarningEmail(Exception e)
        {
            Log.Info("Sending Email");
            var subject = "YMMBatchApplication - Error during auto YMM update";
            var body = "There was an error during the batch update year make model process." + Environment.NewLine
                       + "Host Machine: " + Environment.MachineName + Environment.NewLine
                       + "Error was: " + (e != null ? e.Message : string.Empty) + Environment.NewLine
                       + Environment.NewLine;


            var innerExceptionInfo = "Additional Information"
                + Environment.NewLine + "=-=-=-=-=-=-=-=-=-=-=-" + Environment.NewLine
                + ((e != null && e.InnerException != null) ? GetInnerExceptionInfo(e) : "-- None --");

            body += innerExceptionInfo;

            var message = MailService.CreateMessage(
                ConfigurationManager.AppSettings[ServiceSettings.Recipients.ToString()],
                ConfigurationManager.AppSettings[ServiceSettings.EmailAccount.ToString()],
                body,
                subject);


            MailService.SendNotificationEmail(message);
        }

        private static string GetInnerExceptionInfo(Exception exception)
        {
            var exceptionInfo = string.Empty;
            if (exception != null)
            {
                if (exception.InnerException != null)
                {
                    exceptionInfo += Environment.NewLine +
                        "InnerException" + Environment.NewLine +
                        "Message: " + exception.InnerException.Message + Environment.NewLine +
                        "StackTrace: " + exception.InnerException.StackTrace + Environment.NewLine +
                        GetInnerExceptionInfo(exception.InnerException);
                }
            }
            return exceptionInfo;
        }
    }
}
