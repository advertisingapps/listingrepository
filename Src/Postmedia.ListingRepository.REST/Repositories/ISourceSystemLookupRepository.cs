﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public interface ISourceSystemLookupRepository<T>
    {
        IEnumerable<T> FetchAll();
    }
}