﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class SourceSystemLookupRepository: BaseNHibernateRepository<SourceSystemLookup>, ISourceSystemLookupRepository<SourceSystemLookup>
    {
        public SourceSystemLookupRepository(ISessionStorage session) : base (session)
        {
        }

        public SourceSystemLookupRepository()
            : base(new WcfSessionStorage())
        {

        }
    }
}