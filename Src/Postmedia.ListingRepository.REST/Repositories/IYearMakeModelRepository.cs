﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public interface IYearMakeModelRepository<T>
    {
        IEnumerable<T> FetchAll();
        T FetchById(long id);
        void DeletePreviousData();
        void SaveOrUpdate(T listing);
    }
}