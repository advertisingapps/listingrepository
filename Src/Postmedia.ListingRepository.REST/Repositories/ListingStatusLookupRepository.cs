﻿using System.Linq;
using IglooCoder.Commons.WcfNhibernate;
using NHibernate.Linq;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class ListingStatusLookupRepository: BaseNHibernateRepository<ListingStatusLookup>, IListingStatusLookupRepository<ListingStatusLookup>
    {
        public ListingStatusLookupRepository(ISessionStorage session) : base (session)
        {
        }

        public ListingStatusLookupRepository()
            : base(new WcfSessionStorage())
        {

        }
    }
}