﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public interface IUpsellValueLookupRepository<T>
    {
        IEnumerable<T> FetchAll();
    }
}