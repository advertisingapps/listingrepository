﻿using IglooCoder.Commons.WcfNhibernate;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class AdvertiserRepository : BaseNHibernateRepository<Advertiser>, IAdvertiserRepository<Advertiser>
    {
        public AdvertiserRepository()
            : base(new WcfSessionStorage())
        {

        }
    }
}