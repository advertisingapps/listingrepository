﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public interface IUpsellRepository<T>
    {
        IEnumerable<T> FetchAll();
        T FetchById(long id);
        T FetchByCode(string code);
        void SaveOrUpdate(T listing);
    }
}