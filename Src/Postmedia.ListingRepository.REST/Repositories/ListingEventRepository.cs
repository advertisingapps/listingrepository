﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using IglooCoder.Commons.WcfNhibernate;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports.Working;
using ListingEvent = Postmedia.ListingRepository.REST.Entities.ListingEvent;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class ListingEventRepository : BaseNHibernateRepository<ListingEvent>, IListingEventRepository<ListingEvent>
    {
        public ListingEventRepository()
            : base(new WcfSessionStorage())
        {
        }

        public ListingEventRepository(ISessionStorage sessionStorage)
            : base(sessionStorage)
        {
        }

        public void Save(ListingEvent listingEvent)
        {
            var conn = new SqlConnection() { ConnectionString = ConfigurationManager.ConnectionStrings[0].ConnectionString };
            
            conn.Open();

            // create a command object identifying the stored procedure
            var cmd = new SqlCommand()
            {
                Connection =  conn,
                CommandText = "LRCreateListingEvent",
                CommandTimeout = int.Parse(ConfigurationManager.AppSettings[Core.ServiceSettings.QueryTimeout.ToString()]),
                CommandType = CommandType.StoredProcedure
            };
            
            cmd.Parameters.Add((new SqlParameter("@ListingNumber", listingEvent.ListingNumber)));
            cmd.Parameters.Add((new SqlParameter("@FeedName", listingEvent.FeedName)));
            cmd.Parameters.Add((new SqlParameter("@EventDescription", listingEvent.EventDescription)));
            cmd.Parameters.Add((new SqlParameter("@EventType", listingEvent.EventType)));
            cmd.Parameters.Add((new SqlParameter("@RowsAffected", listingEvent.RowsAffected)));

            cmd.ExecuteNonQuery();

            // clean up connection
            conn.Close();
            conn.Dispose();
        }

        public List<GeneralFailedListingEvent> GetGeneralFailedListing(GeneralFailedListingEventCriteria criteria)
        {
            //var query = Session.CreateSQLQuery("LRWorkopolisBookingsMissingOnline");
            //var results = query.List<object>();
            //return results;

            var conn = new SqlConnection() { ConnectionString = ConfigurationManager.ConnectionStrings[0].ConnectionString };

            conn.Open();

            // create a command object identifying the stored procedure
            var cmd = new SqlCommand()
            {
                Connection = conn,
                CommandText = "LRGetGeneralFailedListing",
                CommandTimeout = int.Parse(ConfigurationManager.AppSettings[Core.ServiceSettings.QueryTimeout.ToString()]),
                CommandType = CommandType.StoredProcedure
            };

            if (!string.IsNullOrEmpty(criteria.RunDate.ToString())) cmd.Parameters.Add(new SqlParameter("@strDateOne", criteria.RunDate.ToString()));

            var results = cmd.ExecuteReader();
            var list = new List<GeneralFailedListingEvent>();

            while (results.Read())
            {
                list.Add(new GeneralFailedListingEvent()
                    {
                        EventId = results["EventId"].ToString(),
                        FeedName = results["FeedName"].ToString(),
                        ListingNumber = results["ListingNumber"].ToString(),
                        EventTime = results["EventTime"].ToString(),
                        StartDate = results["StartDate"].ToString(),
                        ExpireDate = results["ExpireDate"].ToString(),
                        Status = results["Status"].ToString(),
                        EventType = results["EventType"].ToString(),
                        EventDescription = results["EventDescription"].ToString()
                    }
                );
            }

            // clean up connection
            conn.Close();
            conn.Dispose();

            return list;
        }
    }
}