﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public interface IAdminModificationsRepository<T>
    {
        IEnumerable<T> FetchAll();
        IEnumerable<T> FetchAllByNumber(string listingNumber);
        List<T> FetchExisting(List<T> adminModifications);

        void Delete(List<T> adminModifications);
        void SaveOrUpdate(T adminModification);
        void SaveOrUpdate(List<T> adminModification);
    }
}