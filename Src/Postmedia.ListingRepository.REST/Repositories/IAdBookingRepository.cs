﻿using System.Collections.Generic;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public interface IAdBookingRepository<T>
    {
        IEnumerable<T> FetchAll();
        T FetchById(long id);
        void SaveOrUpdate(T listing);
    }
}