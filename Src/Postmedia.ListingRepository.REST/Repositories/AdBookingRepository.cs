﻿using IglooCoder.Commons.WcfNhibernate;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class AdBookingRepository : BaseNHibernateRepository<AdBooking>, IAdBookingRepository<AdBooking>
    {
        public AdBookingRepository()
            : base(new WcfSessionStorage())
        {

        }

        public void SaveAdBooking(AdBooking booking)
        {
            Session.SaveOrUpdate(booking);
        }
    }
}