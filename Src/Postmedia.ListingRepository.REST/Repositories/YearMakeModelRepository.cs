﻿using IglooCoder.Commons.WcfNhibernate;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Repositories
{
    public class YearMakeModelRepository : BaseNHibernateRepository<YearMakeModel>, IYearMakeModelRepository<YearMakeModel>
    {
        public YearMakeModelRepository()
            : base(new WcfSessionStorage())
        {

        }

        public void DeletePreviousData()
        {
            Session.Delete("from YearMakeModel");
        }
    }
}