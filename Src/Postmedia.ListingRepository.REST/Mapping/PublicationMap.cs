﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class PublicationMap : ClassMap<Publication>
    {
        public PublicationMap()
        {
            Table("Publication");

            Id(x => x.Id, "PublicationID")
                .GeneratedBy
                .Native();

            Map(x => x.Code);
            Map(x => x.Name);

        

            //HasMany(x => x.Categories)
            //    .KeyColumn("PublicationID");
        }
    }
}