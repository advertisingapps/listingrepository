﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class BatchLogItemMap: ClassMap<BatchLogItem>
    {
        public BatchLogItemMap()
        {
            Table("BatchLogItem");

            Id(x => x.Id, "BatchLogItemID")
                .GeneratedBy
                .Native();

            Map(x => x.OutboundCacheID);

            References(x => x.Batch)
                .Column("BatchLogID");
        }
    }
}