﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class ListingEventMap: ClassMap<ListingEvent>
    {
        public ListingEventMap()
        {
            Table("ListingEvent");
            
            Id(x => x.Id, "EventId").GeneratedBy.Native();
            Map(x => x.ListingNumber, "ListingNumber");
            Map(x => x.FeedName, "FeedName");
            Map(x => x.EventDescription, "EventDescription");
            Map(x => x.EventType, "EventType");
            Map(x => x.RowsAffected, "RowsAffected");
            Map(x => x.EventTime, "EventTime");
        }
    }
}