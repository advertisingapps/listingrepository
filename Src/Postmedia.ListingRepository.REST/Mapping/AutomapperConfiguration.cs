﻿using System.Linq;
using AutoMapper;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports.Working;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public static class AutomapperConfiguration
    {
        public static void Config()
        {

            // ******** ENTITY TO DTO ********* //
            Mapper.CreateMap<Entities.Listing, ListingSummary>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Account,
                           opt => opt.MapFrom(src => src.AdBooking != null ? src.AdBooking.AccountNumber : null))
                .ForMember(dest => dest.Division,
                           opt => opt.MapFrom(src => src.AdBooking != null ? src.AdBooking.Division.Name : null))
                .ForMember(dest => dest.ExpireDate
                           , opt => opt.MapFrom(
                               src => src.ListingPublications.Max(r => r.ExpireDate)))
                .ForMember(dest => dest.ListingNumber, opt => opt.MapFrom(src => src.ListingNumber))
                .ForMember(dest => dest.Source, opt => opt.MapFrom(src => src.Source))
                .ForMember(dest => dest.StartDate
                           , opt => opt.MapFrom(
                               src => src.ListingPublications.Min(r => r.StartDate)))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.ListingPublications.First().Categories.FirstOrDefault() == null ? 
                    string.Empty : 
                    src.ListingPublications.First().Categories.FirstOrDefault().Name));

            Mapper.CreateMap<Entities.Listing, Listing>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.AdBooking, opt => opt.MapFrom(src => src.AdBooking))
                .ForMember(dest => dest.AdType, opt => opt.MapFrom(src => src.AdType))
                .ForMember(dest => dest.Advertiser, opt => opt.MapFrom(src => src.Advertiser))
                //.ForMember(dest => dest.Advertiser, opt => opt.MapFrom(src => src != null ? src.Advertiser : null))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.MediaItems, opt => opt.MapFrom(src => src.MediaItems))
                .ForMember(dest => dest.ListingNumber, opt => opt.MapFrom(src => src.ListingNumber))
                .ForMember(dest => dest.ListingPublications, opt => opt.MapFrom(src => src.ListingPublications))
                .ForMember(dest => dest.Properties, opt => opt.MapFrom(src => src.Properties))
                .ForMember(dest => dest.Source, opt => opt.MapFrom(src => src.Source))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.HasAdminLock, opt => opt.ResolveUsing<BitResolver>().FromMember(src => src.HasAdminLock));

            Mapper.CreateMap<Entities.ListingPublication, ListingPublication>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Categories, opt => opt.MapFrom(src => src.Categories))
                .ForMember(dest => dest.ExpireDate, opt => opt.MapFrom(src => src.ExpireDate))
                .ForMember(dest => dest.Publication, opt => opt.MapFrom(src => src.Publication))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Upsells, opt => opt.MapFrom(src => src.Upsells));

            Mapper.CreateMap<Entities.Publication, Publication>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Categories, opt => opt.Ignore())
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

            Mapper.CreateMap<Entities.Advertiser, Advertiser>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(dest => dest.Contact, opt => opt.MapFrom(src => src.Contact))
                .ForMember(dest => dest.ExternalId, opt => opt.MapFrom(src => src.ExternalId));

            Mapper.CreateMap<Entities.AdBooking, AdBooking>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Division, opt => opt.MapFrom(src => src.Division))
                .ForMember(dest => dest.AccountNumber, opt => opt.MapFrom(src => src.AccountNumber))
                .ForMember(dest => dest.AdOrderNumber, opt => opt.MapFrom(src => src.AdOrderNumber));

            Mapper.CreateMap<Entities.Contact, Contact>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone));

            Mapper.CreateMap<Entities.Address, Address>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.AddressLines, opt => opt.MapFrom(src => src.AddressLines))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.Country))
                .ForMember(dest => dest.PostalCode, opt => opt.MapFrom(src => src.PostalCode))
                .ForMember(dest => dest.Province, opt => opt.MapFrom(src => src.Province));

            Mapper.CreateMap<Entities.AddressLine, AddressLine>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.LineText, opt => opt.MapFrom(src => src.LineText))
                .ForMember(dest => dest.Number, opt => opt.MapFrom(src => src.LineNumber));

            Mapper.CreateMap<Entities.Division, Division>();
            Mapper.CreateMap<Entities.Category, Category>();
            Mapper.CreateMap<Entities.Upsell, Upsell>();
            Mapper.CreateMap<Entities.Property, Property>();
            Mapper.CreateMap<Entities.Media, Media>();
            Mapper.CreateMap<Entities.SourceSystemLookup, SourceSystemLookup>();
            Mapper.CreateMap<Entities.ListingStatusLookup, ListingStatusLookup>();
            Mapper.CreateMap<Entities.UpsellValueLookup, UpsellValueLookup>();
            Mapper.CreateMap<Entities.CacheEntry, CacheEntry>();

            // report mapping
            Mapper.CreateMap<Entities.Listing, QRCodeUrlResult>()
                .ForMember(dest => dest.Account, opt => opt.MapFrom(src => src.AdBooking.AccountNumber))
                .ForMember(dest => dest.AdvertiserName, opt => opt.MapFrom(src => src.Advertiser.Name))
                .ForMember(dest => dest.AdOrderNumber, opt => opt.MapFrom(src => src.AdBooking.AdOrderNumber))
                .ForMember(dest => dest.Category, opt => opt.MapFrom(src => src.ListingPublications.First().Categories.FirstOrDefault() == null ?
                    string.Empty :
                    src.ListingPublications.First().Categories.FirstOrDefault().Name))
                .ForMember(dest => dest.Division, opt => opt.MapFrom(src => src.AdBooking.Division.Name))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.Publication, opt => opt.MapFrom(src => src.ListingPublications.First().Publication.Name))
                .ForMember(dest => dest.ExpireDate, opt => opt.MapFrom(src => src.ListingPublications.Max(r => r.ExpireDate)))
                .ForMember(dest => dest.ExternalAccount, opt => opt.MapFrom(src => src.AdBooking.ExternalAccountNumber))
                .ForMember(dest => dest.ListingNumber, opt => opt.MapFrom(src => src.ListingNumber))
                .ForMember(dest => dest.Source, opt => opt.MapFrom(src => src.Source))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.ListingPublications.Min(r => r.StartDate)))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.MediaItems, opt => opt.MapFrom(src => src.MediaItems));

            Mapper.CreateMap<Entities.AdminModifications, DTO.AdminModifications>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.ListingNumber, opt => opt.MapFrom(src => src.ListingNumber))
                .ForMember(dest => dest.Field, opt => opt.MapFrom(src => src.Field))
                .ForMember(dest => dest.DateModified, opt => opt.MapFrom(src => src.DateModified));
            
            Mapper.CreateMap<DTO.AdminModifications, Entities.AdminModifications>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.ListingNumber, opt => opt.MapFrom(src => src.ListingNumber))
                .ForMember(dest => dest.Field, opt => opt.MapFrom(src => src.Field))
                .ForMember(dest => dest.DateModified, opt => opt.MapFrom(src => src.DateModified));
        }
    }

    internal class BitResolver: ValueResolver<int, bool>
    {
        protected override bool ResolveCore(int b)
        {
            return (b != 0);
        }
    }
} 