﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class YearMakeModelMap : ClassMap<YearMakeModel>
    {
        public YearMakeModelMap()
        {
            Table("YearMakeModel");

            Id(x => x.Id, "YearMakeModelID")
                .GeneratedBy
                .Native();

            Map(x => x.Year);
            Map(x => x.Make);
            Map(x => x.Model);
            
        }
    }
}