﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class AdminModificationsMap: ClassMap<AdminModifications>
    {
        public AdminModificationsMap()
        {
            Table("AdminModifications");
            Id(x => x.Id, "Id")
                .GeneratedBy
                .Native();

            Map(x => x.ListingNumber,"ListingNumber");
            Map(x => x.Field, "Field");
            Map(x => x.DateModified, "Date");
        }
    }
}