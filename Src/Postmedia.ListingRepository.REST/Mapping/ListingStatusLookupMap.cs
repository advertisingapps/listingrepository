﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class ListingStatusLookupMap : ClassMap<ListingStatusLookup>
    {
        public ListingStatusLookupMap()
        {
            Table("ListingStatusValue");

            Id(x => x.Id, "ListingStatusValueID")
                .GeneratedBy
                .Native();

            Map(x => x.Value, "Status");
            Map(x => x.SortSequence);
        }
    }
}