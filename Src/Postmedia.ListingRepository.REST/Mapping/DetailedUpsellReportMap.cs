﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class DetailedUpsellReportMap : ClassMap<DetailedUpsellReport>
    {
        public DetailedUpsellReportMap()
        {
            ReadOnly();
            CompositeId()
            .KeyProperty(x => x.ListingId)
            .KeyProperty(x => x.UpsellId);
            Map(x => x.Source);
            Map(x => x.DivisionName);
            Map(x => x.ListingNumber);
            Map(x => x.PublicationName);
            Map(x => x.Title);
            Map(x => x.StartDate);
            Map(x => x.ExpireDate);
            Map(x => x.Status);
            Map(x => x.UpsellName);
            Map(x => x.PublishingDays);
        }
    }
}