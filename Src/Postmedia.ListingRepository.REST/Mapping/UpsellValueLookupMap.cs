﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class UpsellValueLookupMap : ClassMap<UpsellValueLookup>
    {
        public UpsellValueLookupMap()
        {
            Table("UpsellValue");

            Id(x => x.Id, "UpsellValueID")
                .GeneratedBy
                .Native();

            Map(x => x.Value, "Description");
            Map(x => x.Code, "Upsell");
            Map(x => x.SortSequence);
        }
    }
}