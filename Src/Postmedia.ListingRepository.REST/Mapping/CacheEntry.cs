﻿using FluentNHibernate.Mapping;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Mapping
{
    public class CacheEntryMap : ClassMap<CacheEntry>
    {
        public CacheEntryMap()
        {
            Table("OutboundCache");

            Id(x => x.Id, "OutboundCacheID")
                .GeneratedBy
                .Native();

            Map(x => x.CacheData);
            Map(x => x.ExpireDate);
            Map(x => x.FeedName);
            Map(x => x.LastModifiedDate); 
            Map(x => x.ListingNumber);
            Map(x => x.StartDate);
        }
    }
}