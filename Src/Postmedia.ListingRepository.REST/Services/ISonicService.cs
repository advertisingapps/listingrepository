﻿using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.REST.Services
{
    public interface ISonicService
    {
        bool Connect();
        bool Connect(string host, int port, string user, string password);
        void PublishToTopic(string xml);
        void PublishToTopic(Listing listing);
        void PublishToTopic(string xml, string topic);
        void Disconnect();
    }
}
