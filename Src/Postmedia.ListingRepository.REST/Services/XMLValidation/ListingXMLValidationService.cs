﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using Postmedia.ListingRepository.Core.Translators;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.REST.Services.XMLValidation
{
    public class ListingXMLValidationService
    {
        public XDocument ValidateListingXML(Listing listing)
        {
            var xmlReaderSettings = CreateXmlReaderSettings();
            var xdoc = LoadXmlWrapper("CanonicalWrapper.xml");
            var listingXml = StreamTranslator<Listing>.DtoToXml(listing);

            var listingNode = (from xmlNodes in xdoc.Descendants("Listing")
                               select xmlNodes).FirstOrDefault();
            if (listingNode != null)
            {
                listingNode.ReplaceWith(XElement.Parse(listingXml));
            }

            //call the validation

            //get a stream to the xml
            var xdocString = xdoc.ToString();
            var stringReader = new StringReader(xdocString);
            //create the reader using the schemas and validation settings created earlier
            var reader = XmlReader.Create(stringReader, xmlReaderSettings);
            //we don't care about the output because we only care if its valid or not...

            while (reader.Read()) {}

            return xdoc;
        }
    

        private XDocument LoadXmlWrapper(string canonicalwrapperXML)
        {
            //get xsd file names from the manifest manifest
            var names = GetType().Assembly.GetManifestResourceNames().Where(x => x.EndsWith(canonicalwrapperXML));
            return names.Select(OnSelector).Select(XDocument.Load).FirstOrDefault();
        }

        private Stream OnSelector(string name)
        {
            return GetType().Assembly.GetManifestResourceStream(name);
        }

        private XmlReaderSettings CreateXmlReaderSettings()
        {
            //initialize our collection of schemas
            var embeddedSchemas = new XmlSchemaSet();
            //create the reader settings object
            var xmlReaderSettings = new XmlReaderSettings
            {
                ValidationType = ValidationType.Schema
            };

            //get xsd file names from the manifest manifest
            var names = GetType().Assembly.GetManifestResourceNames().Where(x => x.EndsWith(".xsd"));

            //get each of our schemas into the schema collection
            foreach (var name in names)
            {
                var stream = GetType().Assembly.GetManifestResourceStream(name);
                if (stream != null)
                {
                    var currentSchema = XmlSchema.Read(stream, ValidationEventHandler);
                    embeddedSchemas.Add(currentSchema);
                }
            }
            xmlReaderSettings.Schemas.Add(embeddedSchemas);

            xmlReaderSettings.ValidationEventHandler += xmlReaderSettings_ValidationEventHandler;
            return xmlReaderSettings;
        }

        private void ValidationEventHandler(object sender, ValidationEventArgs validationEventArgs)
        {
            //TODO: Implement Logging
            throw validationEventArgs.Exception;
        }

        private void xmlReaderSettings_ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            //TODO: Implement Logging
            throw e.Exception;
        }

    }
}