﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using AutoMapper;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports;
using Postmedia.ListingRepository.DTO.Reports.Working;
using Postmedia.ListingRepository.REST.Repositories;

using log4net;
using System.Reflection;
using System.Net;

namespace Postmedia.ListingRepository.REST.Services
{
    public class ReportService : IReportService
    {
        private IListingRepository<Entities.Listing> ListingRepository { get; set; }
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public ReportService() : this(new Repositories.ListingRepository()){ }
        public ReportService(IListingRepository<Entities.Listing> listingRepository) 
        {
            ListingRepository = listingRepository;
        }

        public List<SamsResult> GetWorkingSamsReport(SamsCriteria criteria)
        {
            // call listing repository 
            var samsResults = ListingRepository.GetSamsResultsUsingCriteria(criteria);

            // no need to map results because the method already returns List<SamsResult>
            var retResults = samsResults.ToList();

            // return results
            return retResults;
        }

        public List<SamsNoFeedbackResult> GetWorkingSamsNoFeedbackReport(SamsNoFeedbackCriteria criteria)
        {
            // call the listing repository and return results
            var samsResults = ListingRepository.GetSamsNoFeedbackResultsUsingCriteria(criteria);
            var retResults = samsResults.ToList();
            return retResults;
        }


        public List<QRCodeUrlResult> GetQRCodeUrlReport(QRCodeUrlCriteria criteria)
        {
            // call listing repository 
            var listings = ListingRepository.GetListingsUsingCriteria(criteria);

            // map the listings to the results
            var qrCodeUrlResults = listings.Select(Mapper.Map<Entities.Listing, QRCodeUrlResult>).ToList();

            // return results
            return qrCodeUrlResults;            
        }

        public List<UpsellResult> GetUpsellReport(UpsellCriteria criteria)
        {
            // call listing repository
            var methodResults = ListingRepository.GetWorkingUpsellUsingCritera(criteria);

            // mapping not needed here because we are returning what we want rather than returning a listing DTO that requires mapping.
            var retResults = methodResults.ToList();

            // return results
            return retResults;
        }

        public List<DetailedUpsellResult> GetDetailedUpsellReport(DetailedUpsellCriteria criteria)
        {
            var results = ListingRepository.GetDetailedUpsellUsingCriteria(criteria);
            var retResults = results.ToList();
            return retResults;
        }

        public List<ListingPubResult> GetListingPubReportUsingCriteria(ListingPubCriteria criteria)
        {
            //var listingpubResults = ListingRepository.GetListingPublicationsUsingCritera(criteria);
            var listingpubResults = ListingRepository.NewGetListingPublicationsUsingCritera(criteria);
            
            // no need to map results because the method already returns List<GeneralListingPubResults>
            var retResults = listingpubResults.ToList();

            // return results
            return retResults;
        }

        // methods for converting the passed criteria to a string to be used as a header
        public string BuildCriteriaString(SamsCriteria criteria) 
        {
            string criteriastring = "SAMS Working Report For: " + (criteria.Date.HasValue ? criteria.Date.Value.ToString("yyyy/MM/dd") : string.Empty);
            return criteriastring;
        }

        public string BuildCriteriaString(SamsNoFeedbackCriteria criteria)
        {
            string criteriastring = "SAMS Employment Missed Ads Report For: " + (criteria.Date.HasValue ? criteria.Date.Value.ToString("yyyy/MM/dd") : string.Empty);
            return criteriastring;
        }

        public string BuildCriteriaString(QRCodeUrlCriteria criteria)
        {
            string criteriastring = "QR Code URL Report For: " + (criteria.Division ?? "") + " " + (criteria.Date.HasValue ? criteria.Date.Value.ToString("yyyy/MM/dd") : string.Empty);
            return criteriastring;
        }
        
        public string BuildCriteriaString(UpsellCriteria criteria)
        {
            string criteriastring = "Working Upsell Report "
                + "For: " + (criteria.StartDate.HasValue ? criteria.StartDate.Value.ToString("yyyy/MM/dd") : string.Empty)
                + " To: " + (criteria.EndDate.HasValue ? criteria.EndDate.Value.ToString("yyyy/MM/dd") : string.Empty);
            return criteriastring;
        }

        public string BuildCriteriaString(DetailedUpsellCriteria criteria)
        {
            string criteriastring = "Detailed Upsell Report Sent @ " + DateTime.Now;
                /*+ "For Start Date Between: " + (criteria.StartDate1.HasValue ? criteria.StartDate1.Value.ToString("yyyy/MM/dd") : string.Empty)
                + " To: " + (criteria.StartDate2.HasValue ? criteria.StartDate2.Value.ToString("yyyy/MM/dd") : string.Empty)
                + "For Expire Date Between: " + (criteria.ExpireDate1.HasValue ? criteria.ExpireDate1.Value.ToString("yyyy/MM/dd") : string.Empty)
                + " To: " + (criteria.ExpireDate2.HasValue ? criteria.ExpireDate2.Value.ToString("yyyy/MM/dd") : string.Empty)*/
                ;
            return criteriastring;
        }

        // methods for converting the passed results to an HTML string
        public string FormatReportForHtml(SamsCriteria criteria, List<SamsResult> results)
        {
            string header = BuildCriteriaString(criteria);

            // build the report as an HTML table.  (This will need to be refactored, just getting something out for now)
            string data = "<table cellpadding=\"2\"><tr class=\"header\"><th>Division</th><th>SAMS Ad</th><th>Account</th><th>Title</th><th>Category</th>"
                    + "<th>Start Date</th><th>Expire Date</th><th>Ad Length</th></tr>";

            if (results.Count == 0)
            {
                data = data + "<tr><td colspan=\"8\">--No Records Found--</td></tr>";
            }
            else
            {
                foreach (var item in results)
                {
                    string samsAdNum;
                    // determine the base SAMS ad number from the listing number
                    if (string.IsNullOrEmpty(item.ListingNumber))
                    {
                        samsAdNum = "&nbsp;";
                    }
                    else
                    {
                        samsAdNum = item.ListingNumber.Substring(3);
                        samsAdNum = samsAdNum.Remove(samsAdNum.IndexOf("-"));
                    }

                    // build up datarow
                    string datarow = "<tr><td>" + (item.Division != null ? item.Division : "&nbsp;") + "</td><td>" + samsAdNum
                        + "</td><td>" + (item.AdvertiserName != null ? item.AdvertiserName : "&nbsp;") + "</td><td>" + GetDisplayTitle(item.Title)
                        + "</td><td>" + (item.Category != null ? item.Category : "&nbsp;") + "</td><td>" + (item.StartDate != null ? item.StartDate.ToString("yyyy/MM/dd") : "&nbsp;")
                        + "</td><td>" + (item.ExpireDate != null ? item.ExpireDate.ToString("yyyy/MM/dd") : "&nbsp;")
                        + "</td><td class=\"" + (item.AdLength > 0 ? "numeric" : "numericRed") + "\">"
                        + (item.AdLength != null ? item.AdLength.ToString() : "&nbsp;") + "</td></tr>";

                    // add completed upsellResult to data
                    data = data + datarow;
                }
            }
            // close the table
            data = data + "</table>";

            return BuildHTMLReport(header, data);
        }


        public string FormatReportForHtml(SamsNoFeedbackCriteria criteria, List<SamsNoFeedbackResult> results)
        {
            string header = BuildCriteriaString(criteria);

            // build the report as an HTML table.  (This will need to be refactored, just getting something out for now)
            string data = "<table cellpadding=\"2\"><tr class=\"header\"><th>Division</th><th>SAMS Ad</th><th>Account</th><th>Title</th><th>Category</th>"
                    + "<th>Start Date</th><th>Expire Date</th><th>Ad Length</th></tr>";

            if (results.Count == 0)
            {
                data = data + "<tr><td colspan=\"8\">--No Records Found--</td></tr>";
            }
            else
            {
                foreach (var item in results)
                {
                    string samsAdNum;
                    // determine the base SAMS ad number from the listing number
                    if (string.IsNullOrEmpty(item.ListingNumber))
                    {
                        samsAdNum = "&nbsp;";
                    }
                    else
                    {
                        samsAdNum = item.ListingNumber.Substring(3);
                        samsAdNum = samsAdNum.Remove(samsAdNum.IndexOf("-"));
                    }

                    // build up datarow
                    string datarow = "<tr><td>" + (item.Division != null ? item.Division : "&nbsp;") + "</td><td>" + samsAdNum
                        + "</td><td>" + (item.AdvertiserName != null ? item.AdvertiserName : "&nbsp;") + "</td><td>" + GetDisplayTitle(item.Title)
                        + "</td><td>" + (item.Category != null ? item.Category : "&nbsp;") + "</td><td>" + (item.StartDate != null ? item.StartDate.ToString("yyyy/MM/dd") : "&nbsp;")
                        + "</td><td>" + (item.ExpireDate != null ? item.ExpireDate.ToString("yyyy/MM/dd") : "&nbsp;")
                        + "</td><td class=\"" + (item.AdLength > 0 ? "numeric" : "numericRed") + "\">"
                        + (item.AdLength != null ? item.AdLength.ToString() : "&nbsp;") + "</td></tr>";

                    // add completed upsellResult to data
                    data = data + datarow;
                }
            }
            // close the table
            data = data + "</table>";

            return BuildHTMLReport(header, data);
        }

        public string FormatReportForHtml(QRCodeUrlCriteria criteria, List<QRCodeUrlResult> results)
        {
            string header = BuildCriteriaString(criteria);

            // build the report as an HTML table.  (This will need to be refactored, just getting something out for now)
            string data = "<table cellpadding=\"2\"><tr class=\"header\"><th>Division</th><th>Publication</th><th>Ad Number</th><th>Account</th><th>Title</th>"
                    + "<th>Category</th><th>Start Date</th><th>Expire Date</th><th>Ad Length</th><th>Display URL</th></tr>";

            if (results.Count == 0)
            {
                data = data + "<tr><td colspan=\"9\">--No Records Found--</td></tr>";
            }
            else
            {
                foreach (var item in results)
                {
                    // get the display URL if applicable.  Adding this here because I didn't want to put logic in the DTO. 
                    string displayUrl = "N/A";
                    if ((item.MediaItems != null) && (item.MediaItems.Count > 0))
                    {                       
                        foreach (var media in item.MediaItems)
                        {
                            if ((media.Role == "displayUrl") && (!string.IsNullOrEmpty(media.Url)))
                            {
                                displayUrl = media.Url;
                            }
                        }
                    }

                    // build up datarow
                    string datarow = "<tr><td>" + (item.Division != null ? item.Division : "&nbsp;") + "</td><td>" + (item.Publication != null ? item.Publication : "&nbsp;")
                        + "</td><td>" + (item.ListingNumber != null ? item.ListingNumber : "&nbsp;")
                        + "</td><td>" + (item.AdvertiserName != null ? item.AdvertiserName : "&nbsp;") + "</td><td>" + GetDisplayTitle(item.Title)
                        + "</td><td>" + (item.Category != null ? item.Category : "&nbsp;") + "</td><td>" + (item.StartDate != null ? item.StartDate.ToString("yyyy/MM/dd") : "&nbsp;")
                        + "</td><td>" + (item.ExpireDate != null ? item.ExpireDate.ToString("yyyy/MM/dd") : "&nbsp;")
                        + "</td><td class=\"" + (item.AdLength > 0 ? "numeric" : "numericRed") + "\">" + item.AdLength.ToString() + "</td>"
                        + "</td><td class=\"" + (displayUrl == "N/A" ? "redText" : "") + "\">"
                        + (!string.IsNullOrEmpty(displayUrl) ? displayUrl.ToString() : "&nbsp;") + "</td></tr>";

                    // add completed upsellResult to data
                    data = data + datarow;
                }
            }
            // close the table
            data = data + "</table>";

            return BuildHTMLReport(header, data);
        }

        public string FormatReportForHtml(UpsellCriteria criteria, List<UpsellResult> results)
        {
            string header = BuildCriteriaString(criteria);

            string data = "";

            if (results.Count == 0)
            {
                data = data + "<tr><td>--No Records Found--</td></tr>";
            }
            else
            {
                // build the report as an HTML table.

                // build the header upsellResult
                data = "<table cellpadding=\"2\"><tr class=\"header\"><th>Division</th>";
                var upsellResult = new UpsellResult();
                upsellResult = results[0];
                
                // need to do this because the number of result columns are dynamic
                foreach (var item in upsellResult.UpsellCounts)
	            {
                    data += ("<th>" + item.Name + "</th>");
	            }
                // close the header row                    
                data += "<th>Totals</th></tr>";
                
                // loop though the line items
                foreach (var row in results)
                {
                    int divisionTotalCount = 0;
                    // loop through the upsell result for each line item
                    string datarow = "<tr><td>" + (row.Division != null ? row.Division : "&nbsp;") + "</td>";

                    foreach (var upsell in row.UpsellCounts)
                    {
                        datarow += ("<td class=\"numeric\">" + upsell.Value.ToString() + "</td>");
                        // increment the total counter for the line item
                        divisionTotalCount += (upsell.Value != null ? upsell.Value : 0);
                    }
                    // add the total to the row
                    datarow += ("<td class=\"totals\">" + divisionTotalCount.ToString() + "</td></tr>");
                    data += datarow;
                }

                // close the table
                data = data + "</table>";
            }
            return BuildHTMLReport(header, data);
        }

        public string FormatReportForHtml(DetailedUpsellCriteria criteria, List<DetailedUpsellResult> results)
        {
            string header = BuildCriteriaString(criteria);

            string data = "";

            if (results.Count == 0)
            {
                data = data + "<tr><td>--No Records Found--</td></tr>";
            }
            else
            {
                // build the report as an HTML table.
                data = "<table cellpadding=\"2\"><tr class=\"header\">";

                foreach (var item in typeof(DetailedUpsellResult).GetProperties())
                {
                    data += "<th>" + item.Name + "</th>";
                }

                data += "</tr>";

                // build the header upsellResult
                foreach (var rec in results)
                {
                    data += "<tr>";
                    data += "<td>" + rec.ListingId + "</td>";
                    data += "<td>" + rec.UpsellId + "</td>";
                    data += "<td>" + rec.Source + "</td>";
                    data += "<td>" + rec.DivisionName + "</td>";
                    data += "<td>" + rec.ListingNumber + "</td>";
                    data += "<td>" + rec.PublicationName + "</td>";
                    data += "<td>" + rec.Title + "</td>";
                    data += "<td>" + rec.StartDate + "</td>";
                    data += "<td>" + rec.ExpireDate + "</td>";
                    data += "<td>" + rec.Status + "</td>";
                    data += "<td>" + rec.UpsellName + "</td>";
                    data += "<td>" + rec.PublishingDays + "</td>";
                    data += "</tr>";
                }

                data += "</table>";
            }
            return BuildHTMLReport(header, data);
        }

        private string BuildHTMLReport(string header, string body)
        {
            string htmlReport = "<html><head><title>" + header + "</title>"
                + "<style type=\"text/css\">.header{background-color: #CCCCCC;}.numeric {text-align:right;}.numericRed {text-align:right;color:red;}.redText{color:red;}.totals{font-weight:bold; text-align:right;}</style>"
                + "</head><body><h2>" + header + "</h2>" + body + "</body></html>";
            return htmlReport;        
        }

        private string GetDisplayTitle(string title)
        {
            return (title != null ? (title.Length > 50 ? title.Substring(0, 50) + "..." : title) : "&nbsp;");
        }

        public string BuildCriteriaString(GeneralFailedListingEventCriteria criteria)
        {
            string criteriastring = "Workopolis Failed Listing Event";

            criteriastring +=
                (criteria.RunDate.HasValue)
                ? (" For RunDate " + (criteria.RunDate.Value.ToString("yyyy/MM/dd")))
                : (" For RunDate " + (DateTime.Now.ToString("yyyy/MM/dd")));

            return criteriastring;
        }

        public List<GeneralFailedListingEvent> GetGeneralFailedListing(GeneralFailedListingEventCriteria criteria)
        {
            var repository = new ListingEventRepository();
            var results = repository.GetGeneralFailedListing(criteria);
            return results;
        }

        public string FormatReportForHtml(GeneralFailedListingEventCriteria criteria, List<GeneralFailedListingEvent> results)
        {
            string header = BuildCriteriaString(criteria);

            string data = "";

            if (results.Count == 0)
            {
                data = data + "<tr><td>--No Records Found--</td></tr>";
            }
            else
            {
                // build the report as an HTML table.

                // build the header upsellResult
                data = "<table cellpadding=\"2\" border=1><tr class=\"header\">"
                    + "<th>EventId</th>"
                    + "<th>FeedName</th>"
                    + "<th>ListingNumber</th>"
                    + "<th>EventTime</th>"
                    + "<th>StartDate</th>"
                    + "<th>ExpireDate</th>"
                    + "<th>Status</th>"
                    + "<th>EventType</th>"
                    + "<th>EventDescription</th></tr>";


                // loop though the line items
                foreach (var row in results)
                {
                    data += "<tr>"
                        + "<td>" + row.EventId + "</td>"
                        + "<td>" + row.FeedName + "</td>"
                        + "<td>" + row.ListingNumber + "</td>"
                        + "<td>" + row.EventTime + "</td>"
                        + "<td>" + row.StartDate + "</td>"
                        + "<td>" + row.ExpireDate + "</td>"
                        + "<td>" + row.Status + "</td>"
                        + "<td>" + row.EventType + "</td>"
                        + "<td>" + row.EventDescription + "</td>"
                        + "</tr>";
                }

                // close the table
                data = data + "</table>";
            }
            return BuildHTMLReport(header, data);
        }
        
    }
}