﻿using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.REST.Services
{
    public interface IAdminModificationsService
    {
        void CheckListingForChanges(Listing currentListing, Listing newListing);
        Listing ResolveListingWithMasterChanges(Listing currentListing, Listing newListing);
    }
}
