﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using AutoMapper;
using NHibernate.Mapping;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports.Working;
using Postmedia.ListingRepository.REST.Factories;
using Postmedia.ListingRepository.REST.Repositories;
using log4net;

namespace Postmedia.ListingRepository.REST.Services
{
    public class ListingEventService: IListingEventService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        public void LogEvent(ListingEvent listingEvent)
        {
            try
            {
                var repository = new ListingEventRepository();
                repository.Save(new ListingEventFactory().DtoToEntity(listingEvent));
            }
            catch (Exception e)
            {
                Log.Error("Failed to save ListingEvent", e);
            }
        }
    }
}