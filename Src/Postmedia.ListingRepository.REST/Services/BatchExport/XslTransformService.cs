﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Xsl;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.EnumExtensions.ExportTargetExtensions;
using Postmedia.ListingRepository.Core.FileIO;
using Postmedia.ListingRepository.REST.Entities;
using log4net;


namespace Postmedia.ListingRepository.REST.Services.BatchExport
{
    public class XslTransformService : IXslTransformService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public string ExportDir { get; set; }
        private IFileManager FileManager { get; set; }

        public XslTransformService(IFileManager fileManager)
        {
            FileManager = fileManager;
        }
        
        public XslTransformService(IFileManager fileManager, string exportPath)
        {
            FileManager = fileManager;
            ExportDir = exportPath;
        }

        public string Transform(XmlDocument doc, ExportTarget target, DateTime timeStamp, Division division)
        {
            Log.Info("Entered - Transform()");
            // apply the transform
            XslCompiledTransform xslTransform;
            Log.Info("Getting the XSLTStream using resource: " + target.GetXsltResourceName() + "- Transform()");
            var xsltResourceStream = GetXsltStream(target.GetXsltResourceName());

            if (xsltResourceStream != null)
            {
                Log.Info("Resource stream acquired / not null - Transform()");
                xslTransform = new XslCompiledTransform();
                var reader = new XmlTextReader(xsltResourceStream);
                xslTransform.Load(reader);
                Log.Info("Transform loaded - Transform()");
            }
            else
            {
                throw new IOException("Could not locate the XSL resource stream to reader xslt file");
            }

            Log.Info("Transform not null - Transform()");
            var outputDirectory = ExportDir;
            Log.Info("Preparing output directory - Transform()");
            if (string.IsNullOrEmpty(outputDirectory))
            {
                outputDirectory = CreateOutputDirectoryName(target);
                Log.Info("Output directory will be created at: " + outputDirectory + "- Transform()");
            }

            FileManager.PrepareExportDirectory(outputDirectory, false);
            Log.Info("Output directory created - Transform()");

            // = get value from configuration
            Log.Info("Creating xml writer for output directory - Transform()");
            var transformedFilePath = CreateOutputFileName(outputDirectory, target, timeStamp, division);
            var writer = XmlWriter.Create(transformedFilePath, xslTransform.OutputSettings);

            //output the cache results to a file
            Log.Info("Performing transform - Transform()");
            xslTransform.Transform(doc, null, writer);
            Log.Info("Transform completed flushing writer- Transform()");
            writer.Flush();
            writer.Close();


            Log.Info("Exiting - Transform()");
            return transformedFilePath;
        }

        private Stream GetXsltStream(string transform)
        {
            string xsltResourceName = Assembly.GetExecutingAssembly().GetName().Name + ".XSLT." + transform + ".xslt";
            return Assembly.GetExecutingAssembly().GetManifestResourceStream(xsltResourceName);
        }

        private string CreateOutputDirectoryName(ExportTarget target)
        {
            var directoryName = ConfigurationManager.AppSettings[ServiceSettings.CacheExportLocalDir.ToString()]
                                + target.ToString() + @"\";
            return directoryName;
        }
        
        private string CreateOutputFileName(string path, ExportTarget target, DateTime timeStamp, Division division)
        {
            var targetFileFormat = GetFormatFromTarget(target);
            var fileName = ReplaceFormatTagsWithValues(targetFileFormat, timeStamp, division);
            
            return path + fileName;
        }

        private string ReplaceFormatTagsWithValues(string targetFileFormat, DateTime timeStamp, Division division)
        {
            var formattedFileName = targetFileFormat;
            if (division != null)
            {
                formattedFileName = formattedFileName.Replace("#shortdiv#", division.Code);
                formattedFileName = formattedFileName.Replace("#longdiv#", division.Name.Replace(" ", string.Empty));
            }
            formattedFileName = formattedFileName.Replace("#formatteddatetime#",
                                                          timeStamp.ToString("yyyy-MM-dd'T'hhmmss"));
            formattedFileName = formattedFileName.Replace("#date#", timeStamp.ToString("yyyyMMdd"));
            formattedFileName = formattedFileName.Replace("#time#", timeStamp.ToString("hhmm"));

            return formattedFileName;
        }

        private string GetFormatFromTarget(ExportTarget target)
        {
            string formatString;

            switch (target)
            {
                case ExportTarget.Celebrating:
                    formatString = ConfigurationManager.AppSettings[ServiceSettings.CelebratingExportFileFormat.ToString()];
                    break;
                case ExportTarget.Remembering:
                    formatString = ConfigurationManager.AppSettings[ServiceSettings.RemeberingExportFileFormat.ToString()];
                    break;
                case ExportTarget.Working:
                    formatString = ConfigurationManager.AppSettings[ServiceSettings.WorkingExportFileFormat.ToString()];
                    break;
                default:
                    formatString = ConfigurationManager.AppSettings[ServiceSettings.AdPerfectExportFileFormat.ToString()];
                    break;
            }

            return formatString;
        }
    }
}