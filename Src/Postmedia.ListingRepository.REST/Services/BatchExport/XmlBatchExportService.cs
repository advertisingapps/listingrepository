using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.FileIO;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Repositories;
using log4net;
using CacheEntry = Postmedia.ListingRepository.REST.Entities.CacheEntry;

namespace Postmedia.ListingRepository.REST.Services.BatchExport
{
    public class XmlBatchExportService : IXmlBatchExportService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private const string AdPerfectNodeName = "export";
        private const string AdPerfectNodeWrapper = "<adperfect>";
        private const string TargetPathValue = "#target#";

        private IXslTransformService XslService { get; set; }
        private IBatchLogRepository<BatchLog> BatchLogRepository{ get; set; }
        private IDivisionRepository<Entities.Division> DivisionRepository{ get; set; }
        private IFileManager FileManager { get; set; }

        public XmlBatchExportService()
            : this(new XslTransformService(new FileManager()), new BatchLogRepository(), new DivisionRepository(), new FileManager()) {}

        public XmlBatchExportService(IXslTransformService xslService
            , IBatchLogRepository<BatchLog> batchLogRepository
            , IDivisionRepository<Entities.Division> divisionRepository
            , IFileManager fileManager)
        {
            XslService = xslService;
            BatchLogRepository = batchLogRepository;
            DivisionRepository= divisionRepository;
            FileManager = fileManager;
        }

        public ExportSummary ExportBatch(ExportTarget target
            , List<CacheEntry> filteredCacheEntries)
        {
            Log.Debug("Entering - ExportBatch()");
            var summary = new ExportSummary();

            if (filteredCacheEntries != null)
            {
                if (filteredCacheEntries.Count > 0)
                {
                    // Add Filter the Cache Entries based on the ListingNumber first 3 chars
                    // this "should" be equal to the division code
                    ILookup<string, CacheEntry> groupedCacheEntries = null;
                    Log.Debug("Creating lookups - ExportBatch()");
                    if(target != ExportTarget.AdPerfect)
                    {
                        
                         groupedCacheEntries = filteredCacheEntries.ToLookup(x => x.ListingNumber.Substring(0, 3));
                    }
                    else
                    {
                        groupedCacheEntries = filteredCacheEntries.ToLookup(x => x.FeedName);
                    }
                    Log.Debug("Cache entry grouping successful - ExportBatch()");
                    var timeStamp = DateTime.Now;
                    foreach (var divGroupedCacheEntryList in groupedCacheEntries)
                    {
                        // get division code from our first entry 
                        // and retrieve it for use in ExportFile() call

                        var divCode = divGroupedCacheEntryList.Key;
                        Log.Debug("Retrieving division for code ()" + divCode + " - ExportBatch()");
                        var division = DivisionRepository.FetchByCode(divCode);

                        
                        Log.Debug("Exporting batch " + (division != null ? " for div " + division.Name : string.Empty) + " - ExportBatch()");
                        ExportFile(divGroupedCacheEntryList.ToList(), target, timeStamp, division);
                    }

                    Log.Debug("Create batch log - ExportBatch()");
                    var batchLog = CreateExportLog(filteredCacheEntries, target, timeStamp);

                    BatchLogRepository.SaveOrUpdate(batchLog);
                    summary = new ExportSummary
                                  {
                                      SummaryCount = filteredCacheEntries.Count(),
                                      BatchId = batchLog.Id
                                  };
                    
                    Log.Debug("Create export summary - ExportBatch()");
                }               
            }
            return summary;
        }

        protected BatchLog CreateExportLog(List<CacheEntry> filteredCacheEntries, ExportTarget target, DateTime timeStamp)
        {
            try
            {
                Log.Debug("Entering - CreateExportLog()");
                var logItems = CreateLogItems(filteredCacheEntries);

                Log.Debug("Log items created - CreateExportLog()");
                var batchLog = new BatchLog
                {
                    FeedName = target.ToString(),
                    TimeStamp = timeStamp
                };
                Log.Debug("Batch log created - CreateExportLog()");

                foreach (var item in logItems)
                {
                    batchLog.AddBatchItem(item);
                }
                Log.Debug("Batch log items added to log - CreateExportLog()");
                Log.Debug("Exiting - CreateExportLog()");
                return batchLog;
            }
            catch (Exception ex)
            {
                Log.Error("Error during batch log creation", ex);
                throw;
            }
        }
        private void ExportFile(List<CacheEntry> filteredCacheEntries, ExportTarget target, DateTime timeStamp, Entities.Division division)
        {
            Log.Debug("Entering - ExportFile()");
            try
            {
                var exportDoc = new XmlDocument();
                Log.Debug("Creating XML - ExportFile()");
                var xmlString = CreateXmlToTranslate(filteredCacheEntries);
                if (!String.IsNullOrEmpty(xmlString))
                {
                    exportDoc.LoadXml(xmlString);
                    //var nodeList = exportDoc.GetElementsByTagName("email");
                    //foreach (XmlNode node in nodeList)
                    //{
                    //    node.InnerText = "";
                    //}

                    Log.Debug("Performing the Transform - ExportFile()");
                    var filePath = XslService.Transform(exportDoc, target, timeStamp, division);
                    Log.Debug("Returned from Transform with filePath - " + filePath + " - ExportFile()");
                    if (!string.IsNullOrEmpty(filePath))
                    {
                        Log.Debug("Moving the file to the export location - ExportFile()");
                        
                        var source = filePath;
                        var dest = ConfigurationManager.AppSettings[ServiceSettings.CacheExportDir.ToString()];
                        dest = dest.Replace(TargetPathValue, target.ToString());
                        dest += filePath.Substring(filePath.LastIndexOf(@"\", StringComparison.InvariantCulture) + 1); 

                        Log.Debug("Source file location: " + source + " - ExportFile()");
                        Log.Debug("Destination file location: " + dest + " - ExportFile()");

                        FileManager.CopyFiles(source, dest, true);
                        Log.Debug("File copied - ExportFile");
                    }
                    else
                    {
                        var message = "Filename was not returned from transform service batch export was aborted";
                        throw new FileNotFoundException(message);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error during file export", ex);
                throw;
            }
        }

        private string CreateXmlToTranslate(List<CacheEntry> filteredCacheEntries)
        {
            var xmlString = String.Empty;
            Log.Debug("Entered - CreateXmlToTranslate()");
            if ((filteredCacheEntries != null) && (filteredCacheEntries.Count > 0))
            {
                var stringBuilder = new StringBuilder();
                string rootNodePlural = GetNodePluralization(filteredCacheEntries[0].CacheData);

                Log.Debug("Checking for root node pluralization - CreateXmlToTranslate()");
                if (!String.IsNullOrEmpty(rootNodePlural))
                {
                    Log.Debug("Iterating through the filtered Items to create XMLDoc - CreateXmlToTranslate()");
                    //Add the cache data to the document
                    foreach (var filteredCacheEntry in filteredCacheEntries)
                    {
                        if (stringBuilder.Length == 0)
                        {
                            Log.Debug("Adding pluralized root node - CreateXmlToTranslate()");
                            stringBuilder.AppendLine(rootNodePlural);
                        }
                        stringBuilder.AppendLine(filteredCacheEntry.CacheData);
                    }
                    Log.Debug("Finished adding " + filteredCacheEntries.Count + "items - CreateXmlToTranslate()");

                    Log.Debug("Closing Root node - CreateXmlToTranslate()");
                    stringBuilder.Append("</" + rootNodePlural.Substring(1));
                    Log.Debug("Loading the constructed XMLDoc - CreateXmlToTranslate()");
                    xmlString = stringBuilder.ToString();

                    //added testing email replacement 
                    bool testExportEnabled;
                    var testExportEmail = ConfigurationManager.AppSettings[ServiceSettings.CacheExportTestingModeReplacementEmail.ToString()];
                    if(bool.TryParse(ConfigurationManager.AppSettings[ServiceSettings.CacheExportTestingModeEnabled.ToString()], out testExportEnabled))
                    {
                        if(testExportEnabled)
                        {
                            xmlString = Regex.Replace(xmlString, @"\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b", testExportEmail, RegexOptions.IgnoreCase);
                        }
                    }
                }
            }

            return xmlString;
        }

        private string GetNodePluralization(string cacheData)
        {
            string pluralNode = String.Empty;
            bool adPerfectExport = cacheData.Substring(0, cacheData.IndexOf(" ")).TrimStart('<') == AdPerfectNodeName;
            Log.Debug("adPerfectExport - " + adPerfectExport + " - GetNodePluralization()");
            if (adPerfectExport)
                pluralNode = AdPerfectNodeWrapper;
            else
            {
                if (!String.IsNullOrEmpty(cacheData))
                {
                    pluralNode = cacheData.Substring(0, cacheData.IndexOf(">")) + "S>";
                    pluralNode = pluralNode.ToUpper();
                }
            }
            Log.Debug("PluralNode is: " + pluralNode + "- GetNodePluralization()");
            return pluralNode;
        }
        private List<BatchLogItem> CreateLogItems(List<CacheEntry> filteredCacheEntries)
        {
            var logItems = new List<BatchLogItem>();
            if(filteredCacheEntries != null)
            {
                logItems.AddRange(filteredCacheEntries
                    .Select(entry => new BatchLogItem
                    {
                        OutboundCacheID = entry.Id,
                    }));
            }
            return logItems;
        }
    }
}