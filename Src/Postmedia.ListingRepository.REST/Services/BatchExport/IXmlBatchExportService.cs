﻿using System.Collections.Generic;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.REST.Services.BatchExport
{
    public interface IXmlBatchExportService
    {
        ExportSummary ExportBatch(ExportTarget target
                                , List<Entities.CacheEntry> filteredCacheEntries);
    }
}
