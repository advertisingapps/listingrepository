﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Postmedia.ListingRepository.Core.FileIO;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.REST.Repositories;
using Postmedia.ListingRepository.REST.Services.Metadata.Oodle;
using Postmedia.ListingRepository.REST.Services.Metadata.YearMakeModel;
using log4net;

namespace Postmedia.ListingRepository.REST.Services
{
    public class MetadataService : IMetadataService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);


        private IOodleService OodleService { get; set; }
        private IYearMakeModelService YearMakeModelService { get; set; }

        public MetadataService() : this(new OodleService(new FileManager())
            , new YearMakeModelService())
        {
        }
        public MetadataService(IOodleService oodleService
                               , IYearMakeModelService yearMakeModelService)
        {
            OodleService = oodleService;
            YearMakeModelService = yearMakeModelService;
        }

        public OodleImportSummary UpdateCategories()
        {
            Log.Debug("Entered - UpdateCategories()");
            var summary = OodleService.ExportOodleRootNodesToXmlConfigurations();
            Log.Debug("Exiting - UpdateCategories()");
            return summary;
        }

        public YearMakeModelImportSummary UpdateYearMakeModel()
        {
            var summary = YearMakeModelService.ImportYearMakeModels();
            if(summary.Success)
            {
                var success = YearMakeModelService.ExportYearMakeModelsToXml();
                if(!success)
                {
                    summary.FailureReason += "Export XML to Genera FTP folder failed. Please check the logs for more details";
                    summary.Success = false;
                }
            }
            return summary;
        }
    }
}