﻿using System;
using System.Configuration;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;
using Sonic.Jms;
using ConnectionFactory = Sonic.Jms.Cf.Impl.ConnectionFactory;

namespace Postmedia.ListingRepository.REST.Services
{
    public class SonicService : ISonicService
    {
        Sonic.Jms.Ext.Connection Connection { get; set; }
        bool Disposed { get; set; }
        bool IsConnected { get { return Connection != null; } }

        public bool Connect()
        {
            // Retrieve the connection parameters
            var host = ConfigurationManager.AppSettings[ServiceSettings.EsbHost.ToString()];
            var port = int.Parse(ConfigurationManager.AppSettings[ServiceSettings.EsbPort.ToString()]);
            var user = ConfigurationManager.AppSettings[ServiceSettings.EsbUsername.ToString()];
            var password = ConfigurationManager.AppSettings[ServiceSettings.EsbPassword.ToString()];

            return Connect(host, port, user, password);
        }
        public bool Connect(string host, int port, string user, string password)
        {
            // Create a connection.
            try
            {
                Sonic.Jms.Ext.ConnectionFactory connectionFactory = new ConnectionFactory(string.Format("{0}:{1}", host, port));
                try
                {
                    Connection = (Sonic.Jms.Ext.Connection)connectionFactory.createConnection(user, password);
                }
                finally
                {
                    connectionFactory = null;
                }
            }
            catch (JMSException jmse)
            {
                throw jmse;
            }

            return IsConnected;
        }
     
        public void PublishToTopic(string xml, string topic)
        {
            if (!IsConnected)
                throw new Exception("Publish failed, SonicService is not connected to host."); 

            Connection.start();
            
            var publishSession = (Sonic.Jms.Ext.Session)Connection.createSession(false, Sonic.Jms.Ext.SessionMode.SINGLE_MESSAGE_ACKNOWLEDGE);
            var destination = publishSession.createTopic(topic);
            var messageProducer = publishSession.createProducer(destination);

            var message = (Sonic.Client.Jms.Impl.Ext.XMLMessage)publishSession.createXMLMessage();
            message.setStringProperty("Content-ID", "Listing_XML");
            message.setStringProperty("Content-Type", "text/xml");
            message.setText(xml);
            
            messageProducer.send(message);

            publishSession.close();
            Connection.stop();
        }
        public void PublishToTopic(string xml)
        {
            var topic = ConfigurationManager.AppSettings[ServiceSettings.EsbTopic.ToString()];
            PublishToTopic(xml, topic);
        }
        public void PublishToTopic(Listing listing)
        {
            Connect();
            var listingXML = Core.Translators.StreamTranslator<Listing>.DtoToXml(listing);
            PublishToTopic(listingXML);
            Disconnect();
        }

        public void Disconnect()
        {
            try
            {
                if (null != Connection)
                {
                    Connection.close();
                }
            }
            finally
            {
                Connection = null;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public void Dispose(bool disposing)
        {
            try
            {
                if (!Disposed)
                {
                    if (disposing)
                    {
                    }
                }

                Disconnect();
            }
            finally
            {
                Disposed = true;
            }
        }
        ~SonicService()
        {
            Dispose(false);
        }
    }
}