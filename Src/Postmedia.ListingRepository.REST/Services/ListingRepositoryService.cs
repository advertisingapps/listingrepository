﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using IglooCoder.Commons.WcfNhibernate;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.Wrapper;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.DTO.Reports;
using Postmedia.ListingRepository.DTO.Reports.Working;
using Postmedia.ListingRepository.REST.Services.XMLValidation;
using Postmedia.ListingRepository.Core.EMail;
using log4net;

namespace Postmedia.ListingRepository.REST.Services
{
    // Start the service and browse to 
    // http://<machine_name>:<port>/ListingRepositoryService/help to view the service's generated help page
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [NHibernateContext]
    public class ListingRepositoryService : IListingRepositoryService
    {
        private static readonly Object _ObjectLock = new Object();
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public List<ListingSummary> GetListingSummaries(ListingCriteria criteria)
        {
            IListingService service = new ListingService();
            var listingSummaries = service.GetListingSummariesUsingCriteria(criteria);
            return listingSummaries;
        }

        public PagedData<ListingSummary> GetPagedListingSummaries(ListingCriteriaPaged criteria)
        {
            IListingService service = new ListingService();
            var pagedSummaries = service.GetPagedListingSummariesUsingCriteria(criteria);
            return pagedSummaries;
        }

        public Listing GetListing(string id)
        {
            lock (_ObjectLock)
            {
                try
                {
                    Log.Debug(lockHeader(MethodBase.GetCurrentMethod().Name, true, id));

                    //IListingService service = new ListingService();

                    int listingId;
                    var idIsInt = int.TryParse(id, out listingId);
                    Listing listing = null;

                    listing = !idIsInt ? Retry.Do(() => new ListingService().GetListingByNumber(id), Log) : Retry.Do(() => new ListingService().GetListingById(listingId), Log);

                    if (listing != null)
                        listing.CacheEntries = GetCacheEntries(listing.ListingNumber);

                    if (listing == null)
                    {
                        var ctx = WebOperationContext.Current;
                        ctx.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                    }

                    Log.Debug(lockHeader(MethodBase.GetCurrentMethod().Name, false, id));

                    return listing;
                }
                catch (Exception)
                {
                    NHibernateContext.Current().Rollback();
                    var ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = HttpStatusCode.InternalServerError;
                    throw;
                }
            }
        }
        
        public List<Listing> GetListingsLikeNumber(string id)
        {
            lock (_ObjectLock)
            {
                try
                {
                    Log.Debug(lockHeader(MethodBase.GetCurrentMethod().Name, true, id));
                    System.Threading.Thread.Sleep(2000);

                    //IListingService service = new ListingService();

                    List<Listing> listings = null;

                    listings = Retry.Do(() => new ListingService().GetListingsLikeNumber(id), Log);

                    //if (listing != null)
                    //    listing.CacheEntries = GetCacheEntries(listing.ListingNumber);

                    if ((listings == null) || (listings.Count == 0))
                    {
                        var ctx = WebOperationContext.Current;
                        ctx.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                    }

                    Log.Debug(lockHeader(MethodBase.GetCurrentMethod().Name, false, id));

                    return listings;
                }
                catch (Exception)
                {
                    NHibernateContext.Current().Rollback();
                    var ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = HttpStatusCode.InternalServerError;
                    throw;
                }
            }
        }
        
        public Listing CreateListing(Listing instance)
        {
            try
            {
                IListingService service = new ListingService();
                service.CreateListing(instance);

                //get the stored listing changes
                var retListing = service.GetListingByNumber(instance.ListingNumber);
                return retListing;

            }
            catch (Exception)
            {
                NHibernateContext.Current().Rollback();
                throw;
            }
        }

        public Listing UpdateListing(string id, Listing instance)
        {
            lock (_ObjectLock)
            {
                try
                {
                    Log.Debug(lockHeader(MethodBase.GetCurrentMethod().Name, true, id));

                    Log.Info("Entered physical service layer - REST.UpdateListing()");
                    IListingService service = new ListingService();
                    Log.Info("Calling service update method - REST.UpdateListing()");
                    //validate the instance formatting before update
                    var listing = service.UpdateListing(id, instance);

                    if (listing != null)
                    {
                        listing.CacheEntries = GetCacheEntries(listing.ListingNumber);
                        if (!listing.HasAdminLock)
                        {
                            //Update the topic
                            Log.Info("Update successful, pushing to ESB Topic - REST.UpdateListing()");
                            PublishUpdateToTopic(listing);
                        }
                    }
                    
                    Log.Info("Completed Operation returning to caller - REST.UpdateListing()");

                    var ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = HttpStatusCode.Created;

                    Log.Debug(lockHeader(MethodBase.GetCurrentMethod().Name, false, id));

                    return listing;
                }
                catch (Exception)
                {
                    NHibernateContext.Current().Rollback();
                    var ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = HttpStatusCode.InternalServerError;
                    return null;
                }
            }
        }

        private void PublishUpdateToTopic(Listing listing)
        {
            var validationService = new ListingXMLValidationService();
            var xdoc = validationService.ValidateListingXML(listing);

            if (xdoc != null)
            {
                // publish the valid xml to the topic
                var sonicService = new SonicService();
                sonicService.Connect();
                sonicService.PublishToTopic(xdoc.ToString());
                sonicService.Disconnect();
            }
            else
            {
                throw new WebFaultException(HttpStatusCode.InternalServerError);
            }
        }

        public void DeleteListing(string id)
        {
            try
            {
                var service = new ListingService();
                service.DeleteListing(id);
            }
            catch (Exception)
            {
                NHibernateContext.Current().Rollback();
                var ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.InternalServerError;
                throw;
            }
        }

        //Lookups
        public List<Division> GetDivisions()
        {
            var service = new LookupService();
            var divisions = service.GetDivisions();
            return divisions;
        }

        //public List<Category> GetCategories()
        //{
        //    var service = new LookupService();
        //    var categories = service.GetCategories();
        //    return categories;
        //}

        public List<Publication> GetPublications()
        {
            var service = new LookupService();
            var publications = service.GetPublications();
            return publications;
        }

        public List<SourceSystemLookup> GetSourceSystems()
        {
            var service = new LookupService();
            var sourceSystems = service.GetSourceSystems();
            return sourceSystems;
        }

        public List<ListingStatusLookup> GetListingStatuses()
        {
            var service = new LookupService();
            var listingStatuses = service.GetListingStatuses();
            return listingStatuses;
        }

        public List<UpsellValueLookup> GetUpsellValues()
        {
            var service = new LookupService();
            var upsellValues = service.GetUpsellValues();
            return upsellValues;
        }


        //Batch / Imp/Exp Functions

        public OodleImportSummary UpdateCategories()
        {
            try
            {
                var service = new MetadataService();
                var summary = service.UpdateCategories();
                return summary;
            }
            catch (Exception)
            {
                NHibernateContext.Current().Rollback();
                throw;
            }
        }

        public YearMakeModelImportSummary UpdateYearMakeModels()
        {
            try
            {

                var service = new MetadataService();
                var summary = service.UpdateYearMakeModel();
                return summary;
            }
            catch (Exception)
            {
                NHibernateContext.Current().Rollback();
                throw;
            }
        }

        public ExportSummary BatchExport(ExportCriteria criteria)
        {
            lock (_ObjectLock)
            {
                try
                {

                    Log.Debug(lockHeader(MethodBase.GetCurrentMethod().Name, true, null));
                    var service = new ListingService();
                    var summary = service.ExportListingBatch(criteria);
                    Log.Debug(lockHeader(MethodBase.GetCurrentMethod().Name, false, null));
                    return summary;
                }
                catch (Exception)
                {
                    NHibernateContext.Current().Rollback();
                    throw;
                }
            }
        }

        public List<CacheEntry> GetCacheEntries(string listingNumber)
        {
            var service = new ListingService();
            var summary = service.GetCacheEntries(listingNumber);
            return summary;
        }

        public void RefreshCacheForListingNumber(string listingNumber)
        {
            var service = new ListingService();
            var listing = service.GetListingByNumber(listingNumber);

            if (listing != null)
            {
                PublishUpdateToTopic(listing);
            }
        }

        public CacheEntry GetCacheEntry(string id)
        {
            var service = new ListingService();
            var summary = service.GetCacheEntry(int.Parse(id));
            return summary;
        }

        // Reporting
        public List<SamsResult> GetWorkingSamsReport(SamsCriteria criteria)
        {
            var service = new ReportService();
            var results = service.GetWorkingSamsReport(criteria);

            if (criteria.EmailResults)
            {
                // get message components
                string htmlreport = service.FormatReportForHtml(criteria, results);
                string subject = service.BuildCriteriaString(criteria);
                var sendto = GetReportEmailRecipient(criteria.EmailAddress);

                // call email service
                SendReportEmail(sendto, htmlreport, subject);
            }

            return results;
        }

        public List<DetailedUpsellResult> GetDetailedUpsellReport(DetailedUpsellCriteria criteria)
        {
            var service = new ReportService();
            var results = service.GetDetailedUpsellReport(criteria);

            if(criteria.EmailResults)
            {
                // get message components
                string htmlreport = service.FormatReportForHtml(criteria, results);
                string subject = service.BuildCriteriaString(criteria);
                var sendto = GetReportEmailRecipient(criteria.EmailAddress);

                // call email service
                SendReportEmail(sendto, htmlreport, subject);
            }

            return results;
        } 


        public List<SamsNoFeedbackResult> GetWorkingSamsNoFeedbackReport(SamsNoFeedbackCriteria criteria)
        {
            var service = new ReportService();
            var results = service.GetWorkingSamsNoFeedbackReport(criteria);

            if (criteria.EmailResults)
            {
                // get message components
                string htmlreport = service.FormatReportForHtml(criteria, results);
                string subject = service.BuildCriteriaString(criteria);
                var sendto = GetReportEmailRecipient(criteria.EmailAddress);

                // call email service
                SendReportEmail(sendto, htmlreport, subject);
            }

            return results;
        }


        public List<QRCodeUrlResult> GetQRCodeUrlReport(QRCodeUrlCriteria criteria)
        {
            var service = new ReportService();
            var results = service.GetQRCodeUrlReport(criteria);

            if (criteria.EmailResults)
            {
                // get message components
                string htmlreport = service.FormatReportForHtml(criteria, results);
                string subject = service.BuildCriteriaString(criteria);
                var sendto = GetReportEmailRecipient(criteria.EmailAddress);

                // call email service
                SendReportEmail(sendto, htmlreport, subject);
            }

            return results;
        }

        public List<UpsellResult> GetWorkingUpsellReport(UpsellCriteria criteria)
        {
            var service = new ReportService();
            var results = service.GetUpsellReport(criteria);

            if (criteria.EmailResults)
            {
                // get message components
                string htmlreport = service.FormatReportForHtml(criteria, results);
                string subject = service.BuildCriteriaString(criteria);
                var sendto = GetReportEmailRecipient(criteria.EmailAddress);

                // call email service
                SendReportEmail(sendto, htmlreport, subject);
            }

            return results;
        }

        public List<ListingPubResult> GetListingPubReportUsingCriteria(ListingPubCriteria criteria)
        {
            lock (_ObjectLock)
            {
                Log.Debug(lockHeader(MethodBase.GetCurrentMethod().Name, true, null));
                var service = new ReportService();
                var results = service.GetListingPubReportUsingCriteria(criteria);
                Log.Debug(lockHeader(MethodBase.GetCurrentMethod().Name, false, null));
                return results;
            }
        }

        public List<GeneralFailedListingEvent> GetGeneralFailedListing(GeneralFailedListingEventCriteria criteria)
        {
            var service = new ReportService();
            var results = service.GetGeneralFailedListing(criteria);
            if (criteria.EmailResults)
            {
                // get message components
                string htmlreport = service.FormatReportForHtml(criteria, results);
                string subject = service.BuildCriteriaString(criteria);
                var sendto = GetReportEmailRecipient(criteria.EmailAddress);

                // call email service
                SendReportEmail(sendto, htmlreport, subject);
            }
            return results;
        }

        //Misc
        public void Ping()
        {
            try
            {
                var ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception)
            {
                var ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = HttpStatusCode.InternalServerError;
                throw;
            }
        }

        // report email format.
        private void SendReportEmail(string sendto, string body, string baseSubject)
        {
            Log.Info("Sending Email");

            var sendfrom = ConfigurationManager.AppSettings[
                                ServiceSettings.EmailAccount.ToString()];

            var message = MailService.CreateMessage(sendto,
                sendfrom,
                body,
                GetSubjectLine(baseSubject),
                true);

            MailService.SendNotificationEmail(message);
        }

        // Get the subjectline for the email
        private string GetSubjectLine(string baseSubject)
        {
            var serviceEnviron = ConfigurationManager.AppSettings[ServiceSettings.ServiceEnvironment.ToString()];
            var subjectLine = (!String.IsNullOrEmpty(serviceEnviron) && serviceEnviron != "prod" ?
                                "Environment: " + serviceEnviron + " - " + baseSubject : baseSubject);
            return subjectLine;
        }

        // get the mailto recipient
        private string GetReportEmailRecipient(string recipientEmail)
        {
            var defaultWorkingEmail = ConfigurationManager.AppSettings[
                                ServiceSettings.DefaultWorkingEmailAccount.ToString()];
            //return (recipientEmail != null ? recipientEmail : defaultWorkingEmail);
            return (!String.IsNullOrEmpty(recipientEmail) ? recipientEmail : defaultWorkingEmail);
        }

        private string lockHeader(string type, bool enterLock, string id)
        {
            var s = "------------------------------"
                + (enterLock ? "Entering Lock @ " : "Leaving Lock @ ")
                + type
                + (!string.IsNullOrEmpty(id) ? " for [" + id + "]" : "")
                + "------------------------------";
            return s;
        }
    }
}