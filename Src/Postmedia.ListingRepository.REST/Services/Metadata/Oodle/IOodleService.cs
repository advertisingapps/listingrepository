﻿using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.REST.Services.Metadata.Oodle
{
    public interface IOodleService
    {
        OodleImportSummary ExportOodleRootNodesToXmlConfigurations();
    }
}