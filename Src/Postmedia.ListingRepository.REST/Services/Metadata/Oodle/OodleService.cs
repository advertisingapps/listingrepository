using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.EnumExtensions;
using Postmedia.ListingRepository.Core.EnumExtensions.OodleExportCategoriesExtensions;
using Postmedia.ListingRepository.Core.FileIO;
using Postmedia.ListingRepository.DTO;
using log4net;

namespace Postmedia.ListingRepository.REST.Services.Metadata.Oodle
{
    public class OodleService : IOodleService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IFileManager FileManager { get; set; }

        public OodleService(IFileManager fileManager)
        {
            FileManager = fileManager;
        }


        public OodleImportSummary ExportOodleRootNodesToXmlConfigurations()
        {
            Log.Debug("Entered - ExportOodleRootNodesToXmlConfigurations()");
            OodleImportSummary summary = null;

            Log.Debug("Getting App Settings - ExportOodleRootNodesToXmlConfigurations()");
            var oodleUrl = ConfigurationManager.AppSettings[ServiceSettings.OodleXmlUrl.ToString()];
            Log.Debug("Loading XML from url: " + oodleUrl + " - ExportOodleRootNodesToXmlConfigurations()");
            var oodleCategoryList = XDocument.Load(oodleUrl);
            Log.Debug("Category list loaded - ExportOodleRootNodesToXmlConfigurations()");

            if ((oodleCategoryList.Document != null) && (oodleCategoryList.Descendants().Any()))
            {
                //export the category root nodes to an oodleCategoryExportFile
                Log.Debug("Category list contains document with decendants - ExportOodleRootNodesToXmlConfigurations()");
                var exportDir = ConfigurationManager.AppSettings[ServiceSettings.GeneraDropDownExportLocalDir.ToString()];
                Log.Debug("Preparing export dir - ExportOodleRootNodesToXmlConfigurations()");
                FileManager.PrepareExportDirectory(exportDir, true);

                var itemCount = 0;
                var headerCount = 0;
                Log.Debug("Creating documents - ExportOodleRootNodesToXmlConfigurations()");
                for (int i = 0; i < (int) OodleExportCategories.OodleExportCatetgoryCount; i++)
                {
                    headerCount++;
                    var cat = (OodleExportCategories) Enum.ToObject(typeof (OodleExportCategories), i);

                    var rootElement = (from element in oodleCategoryList.Document.Descendants("root")
                                       where element.Attribute("name").Value == cat.GetOodleRootName()
                                       select element).FirstOrDefault();

                    Log.Debug("Root elements identified - ExportOodleRootNodesToXmlConfigurations()");

                    if (rootElement != null)
                    {
                        itemCount += rootElement.Elements().Count();

                        var exportDoc = new XDocument(
                            new XDeclaration("1.0", "UTF-8", "yes")
                            );
                        Log.Debug("New XDocument created - ExportOodleRootNodesToXmlConfigurations()");
                        
                        var sortedElementList = (from element in rootElement.Elements("category")
                                                 orderby element.Attribute("name").Value
                                                 select element);
                        Log.Debug("Sorted elemnts - ExportOodleRootNodesToXmlConfigurations()");

                        exportDoc.AddFirst(new XElement("root", new XAttribute("name", rootElement.Attribute("name").Value)));
                        exportDoc.Root.Add(sortedElementList);
                        Log.Debug("Added sorted elemnts to root - ExportOodleRootNodesToXmlConfigurations()");
                        var exportFileName = cat.ToString() + ".xml";
                        var exportPath = exportDir + exportFileName;
                        Log.Debug("Saving exportDoc to " + exportPath + " - ExportOodleRootNodesToXmlConfigurations()");
                        exportDoc.Save(exportPath);
                        
                        var fileShareExportDir =
                            ConfigurationManager.AppSettings[
                            ServiceSettings.GeneraDropDownExportDirOodle.ToString()];
                        var sharePath = fileShareExportDir + exportFileName;
                        Log.Debug("Copying exportDoc from " + exportPath + " to " + sharePath + " - ExportOodleRootNodesToXmlConfigurations()");
                        FileManager.CopyFiles(exportPath, sharePath, true);
                        Log.Debug("Copy complete - ExportOodleRootNodesToXmlConfigurations()");
                        
                    }
                }

                Log.Debug("Creating new summary object - ExportOodleRootNodesToXmlConfigurations()");
                summary = new OodleImportSummary
                              {
                                  importUrl = oodleUrl,
                                  numberOfHeaders = headerCount,
                                  numberOfItems = itemCount
                              };
            }

            Log.Debug("Exiting - ExportOodleRootNodesToXmlConfigurations()");
            return summary;
        }
    }
}