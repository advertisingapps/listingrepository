﻿using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.REST.Services.Metadata.YearMakeModel
{
    public interface IYearMakeModelService
    {
        YearMakeModelImportSummary ImportYearMakeModels();
        bool ExportYearMakeModelsToXml();
    }
}
