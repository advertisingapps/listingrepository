using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.Core.EMail;
using Postmedia.ListingRepository.Core.FileIO;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.REST.Repositories;
using log4net;

namespace Postmedia.ListingRepository.REST.Services.Metadata.YearMakeModel
{
    public class YearMakeModelService : IYearMakeModelService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        Dictionary<string, int> _years = new Dictionary<string, int>();
        Dictionary<string, int> _makes = new Dictionary<string, int>();
        Dictionary<string, int> _models = new Dictionary<string, int>();

        private const string EmailSubject = "An Error Occurred during Year Make Model Import";
        private const string NoRecords = "Import file did not contain any records";
        private const string InvalidFormat = "Invalid file format. Could not parse Year Make Model import file.";
        private const string InvalidPath = "Invalid Path. Could not find any files to use for import.";
        private const string YMMFileName = "YearMakeModels.xml";

        protected IFileManager FileManager { get; set; }
        private IYearMakeModelRepository<Entities.YearMakeModel> YearMakeModelRepository { get; set; }

        public YearMakeModelService() : this(new YearMakeModelRepository(), new FileManager()) {}
        public YearMakeModelService(IYearMakeModelRepository<Entities.YearMakeModel> yearMakeModelRepository,
            IFileManager fileManager)
        {
            FileManager = fileManager;
            YearMakeModelRepository = yearMakeModelRepository;
        }

        

        public YearMakeModelImportSummary ImportYearMakeModels()
        {
            Log.Info("Entered ImportYearMakeModels()");
            YearMakeModelImportSummary summary = null;

            

            Log.Info("Deleting previous data - ImportYearMakeModels()");
            YearMakeModelRepository.DeletePreviousData();
            Log.Info("Previous data deleted - ImportYearMakeModels()");

            Log.Info("Determining import path" + " - ImportYearMakeModels()");
            var importPath = ConfigurationManager.AppSettings[ServiceSettings.YearMakeModelImportDir.ToString()];
            Log.Info("Import path is: " + importPath + " - ImportYearMakeModels()");

            if (FileManager.ValidateImportDirectory(importPath, true))
            {
                Log.Info("Import path is valid - ImportYearMakeModels()");
                var importDirFiles = Directory.GetFiles(importPath);
                var filePath = string.Empty;
                foreach (var importDirFile in importDirFiles)
                {
                    if (importDirFile.EndsWith(".csv"))
                    {
                        filePath = importDirFile;
                        break;
                    }
                }

                
                Log.Info("File path is: " + filePath + " - ImportYearMakeModels()");
                var yearMakeModelStrings = File.ReadAllLines(filePath);
                Log.Info("Imported (" + yearMakeModelStrings.Count() + ") YearMakeModels from file - ImportYearMakeModels()");

                summary = ImportRows(yearMakeModelStrings, importPath);
            }
            else
            {
                summary = ReportError(InvalidPath);
            }

            Log.Info("Clean import directory - ImportYearMakeModels()");
            FileManager.CleanDirectory(importPath);
            Log.Info("Exiting ImportYearMakeModels()");
            return summary;
        }
        public bool ExportYearMakeModelsToXml()
        {
            Log.Info("Entered ExportYearMakeModlesToXml()");
            var generaYMM = new XmlDocument();
            Log.Info("Created XML Declaration - ExportYearMakeModlesToXml()");
            var declaration = generaYMM.CreateXmlDeclaration("1.0", "UTF-8", null);
            generaYMM.AppendChild(declaration);

            Log.Info("Creating YearMakeModels Element - ExportYearMakeModlesToXml()");
            var root = generaYMM.CreateElement("YearMakeModels");
            generaYMM.AppendChild(root);

            Log.Info("Fetching YearMakeModels - ExportYearMakeModlesToXml()");
            var ymms = YearMakeModelRepository.FetchAll();

            foreach (var yearMakeModel in ymms)
            {
                var ymm = generaYMM.CreateElement("YearMakeModel");
                ymm.SetAttribute("Year", yearMakeModel.Year.ToString());
                ymm.SetAttribute("Make", yearMakeModel.Make);
                ymm.SetAttribute("Model", yearMakeModel.Model);

                root.AppendChild(ymm);
            }
            Log.Info("Processed (" + root.ChildNodes.Count + ") ymms - - ExportYearMakeModlesToXml()");
            Log.Info("Exporting to XML Complete - ExportYearMakeModlesToXml()");
            var localExportFolder = ConfigurationManager.AppSettings[ServiceSettings.GeneraDropDownExportLocalDir.ToString()];
            var localFilePath = localExportFolder + YMMFileName;

            Log.Info("Cleaning up previous YMM Export File - ExportYearMakeModlesToXml()");
            if(File.Exists(localFilePath))
                File.Delete(localFilePath);

            Log.Info("Saving YMM Export File to local folder - ExportYearMakeModlesToXml()");
            generaYMM.Save(localFilePath);
            
            //Copy to File share
            var exportFolder = ConfigurationManager.AppSettings[ServiceSettings.GeneraDropDownExportDirYMM.ToString()];
            var exportFilePath = exportFolder + YMMFileName;
            Log.Info("Copying to Genera Export Folder - " + exportFilePath + " - ExportYearMakeModlesToXml()");
            FileManager.CopyFiles(localFilePath, exportFilePath, true);
            
            return true;
        }

        private YearMakeModelImportSummary ImportRows(string[] yearMakeModelStrings, string importPath)
        {
            Log.Info("Entering ImportRows()");
            YearMakeModelImportSummary summary;
            Log.Info("Check for rows to import - ImportRows()");
            if (yearMakeModelStrings.Length <= 1)
            {
                summary = ReportError(NoRecords);
                summary.ImportSourceLocation = importPath;
            }
            else
            {
                Log.Info("Rows to import exist - ImportRows()");
                var yearMakeModelString = yearMakeModelStrings[0];
                var splitYMM = yearMakeModelString.Split(',');

                if (!ValidateRowFormat(splitYMM))
                {
                    summary = ReportError(InvalidFormat);
                    summary.ImportSourceLocation = importPath;
                }
                else
                {
                    Log.Info("Rows format is valid - ImportRows()");
                    var numberImported = InsertYmmValues(yearMakeModelStrings);
                    Log.Info("Import completed - creating summary - ImportRows()");
                    summary = CreateSummary(yearMakeModelStrings.Length, numberImported, importPath);
                }
            }

            return summary;
        }
        private YearMakeModelImportSummary ReportError(string message)
        {
            Log.Info(message);

            //inform via email regarding failure to import the      

            var email = MailService.CreateMessage(
                ConfigurationManager.AppSettings[ServiceSettings.Recipients.ToString()],
                ConfigurationManager.AppSettings[ServiceSettings.EmailAccount.ToString()],
                message,
                EmailSubject);

            MailService.SendNotificationEmail(email);

            //create failure summary
            var summary = new YearMakeModelImportSummary
            {
                Success = false,
                FailureReason = message
            };
            return summary;
        }
        private YearMakeModelImportSummary CreateSummary(int numberOfRecords, int numberImported, string path)
        {
            return new YearMakeModelImportSummary
            {
                TotalNumberOfRecords = numberOfRecords,
                NumberOfRecordsImported = numberImported,
                YearCount = _years.Count,
                MakeCount = _makes.Count,
                ModelCount = _models.Count,
                ImportSourceLocation = path,
                Success = true
            };
        }
        
        private int InsertYmmValues(string[] yearMakeModelStrings)
        {
            Log.Info("Entered - InsertYmmValues()");
            var numberInserted = 0;
            Log.Info("Creating dictionary and getting counts - InsertYmmValues()");
            for (int index = 1; index < yearMakeModelStrings.Length; index++)
            {               
                var yearMakeModelString = yearMakeModelStrings[index];
                var splitYMM = yearMakeModelString.Split(',');

                var trimVal = splitYMM[(int)YMMImportValue.Trim].Trim(new[] { '"' });
                if (string.IsNullOrEmpty(trimVal)) continue;
                var ymm = CreateYearMakeModelFromStrings(splitYMM);
                IncrementDictionaryCount(_years, ymm.Year.ToString());
                IncrementDictionaryCount(_makes, ymm.Make);
                IncrementDictionaryCount(_models, ymm.Model);

                YearMakeModelRepository.SaveOrUpdate(ymm);
                numberInserted++;
            }
            Log.Info("Exiting - InsertYmmValues()");
            return numberInserted;
        }
        private Entities.YearMakeModel CreateYearMakeModelFromStrings(string[] splitYmm)
        {
            var ymm = new Entities.YearMakeModel
            {
                //AdPerfectID = int.Parse(splitYmm[(int)YMMImportValue.AdPerfectId]),
                Year = int.Parse(splitYmm[(int)YMMImportValue.Year].Trim('"')),
                Make = splitYmm[(int)YMMImportValue.Make].Trim('"'),
                Model = splitYmm[(int)YMMImportValue.Model].Trim('"')
            };
            return ymm;
        }
        private bool ValidateRowFormat(string[] splitYmm)
        {
            return splitYmm.Length == (int)YMMImportValue.Count;
        }
        private void IncrementDictionaryCount(IDictionary<string, int> dict, string key)
        {
            //update counts
            if (!dict.ContainsKey(key))
                dict.Add(key, 1);
            else
                dict[key]++;
        }

        private void UnZipSourceFile(string source, string destination)
        {
        }
    }
}