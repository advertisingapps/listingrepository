﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Repositories;
using log4net;

namespace Postmedia.ListingRepository.REST.Services
{
    public class LookupService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        private IDivisionRepository<Division> DivisionRepository { get; set; }
        private IListingStatusLookupRepository<ListingStatusLookup> ListingStatusLookupRepository{ get; set; }
        private ISourceSystemLookupRepository<SourceSystemLookup> SourceSystemLookupRepository { get; set; }
        private ICategoryRepository<Category> CategoryRepository { get; set; }
        private IPublicationRepository<Publication> PublicationRepository { get; set; }
        private IUpsellValueLookupRepository<UpsellValueLookup> UpsellValueLookupRepository { get; set; }

        public LookupService() : this(new DivisionRepository()
            , new CategoryRepository()
            , new PublicationRepository()
            , new ListingStatusLookupRepository()
            , new SourceSystemLookupRepository()
            , new UpsellValueLookupRepository()) { }
        public LookupService(IDivisionRepository<Division> divisionRepository
            ,ICategoryRepository<Category> categoryRepository
            ,IPublicationRepository<Publication> publicationRepository
            ,IListingStatusLookupRepository<ListingStatusLookup> listingStatusLookupRepository
            , ISourceSystemLookupRepository<SourceSystemLookup> sourceSystemLookupRepository
            , IUpsellValueLookupRepository<UpsellValueLookup> upsellValueLookupRepository)
        {
            DivisionRepository = divisionRepository;
            CategoryRepository = categoryRepository;
            PublicationRepository = publicationRepository;
            ListingStatusLookupRepository = listingStatusLookupRepository;
            SourceSystemLookupRepository = sourceSystemLookupRepository;
            UpsellValueLookupRepository = upsellValueLookupRepository;
        }

        public List<DTO.Division> GetDivisions()
        {
            try
            {
                Log.Debug("Entering - GetDivisions()");
                Log.Debug("Calling Repository.GetDivisions() - GetDivisions()");
                var modelDivisions = DivisionRepository.FetchAll();

                var divisions = modelDivisions.Select(Mapper.Map<Division, DTO.Division>).ToList();

                Log.Debug("Exiting GetDivisions()");
                return divisions.OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                throw;
            }
        }
        //public List<DTO.Category> GetCategories()
        //{
        //    try
        //    {
        //        Log.Debug("Entering - GetCategories()");
        //        Log.Debug("Calling Repository.GetCategories() - GetCategories()");
        //        var modelCategories = CategoryRepository.FetchAll();

        //        var categories = modelCategories.Select(Mapper.Map<Category, DTO.Category>).ToList();

        //        Log.Debug("Exiting GetCategories()");
        //        return categories.ToList();
        //    }
        //    catch (Exception ex)
        //    {
        //        var message = ex.Message;
        //        throw;
        //    }
        //}
        public List<DTO.Publication> GetPublications()
        {
            try
            {
                Log.Debug("Entering - GetPublications()");
                Log.Debug("Calling Repository.GetPublications() - GetPublications()");
                var modelPublications = PublicationRepository.FetchAll();

                var publications = modelPublications.Select(Mapper.Map<Publication, DTO.Publication>).ToList();

                Log.Debug("Exiting GetPublications()");
                return publications.OrderBy(x => x.Name).ToList();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                throw;
            }
        }
        public List<DTO.ListingStatusLookup> GetListingStatuses()
        {
            try
            {
                Log.Debug("Entering - GetListingStatuses()");
                Log.Debug("Calling Repository.FetchAll() - GetListingStatuses()");
                var modelStatuses = ListingStatusLookupRepository.FetchAll();

                var listingStatuses = modelStatuses.Select(Mapper.Map<ListingStatusLookup, DTO.ListingStatusLookup>).ToList();

                Log.Debug("Exiting GetListingStatuses()");
                return listingStatuses.OrderBy(x => x.Value).ToList();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                throw;
            }
        }
        public List<DTO.SourceSystemLookup> GetSourceSystems()
        {
            try
            {
                Log.Debug("Entering - GetListingStatuses()");
                Log.Debug("Calling Repository.FetchAll() - GetListingStatuses()");
                var modelSources = SourceSystemLookupRepository.FetchAll();

                var sourceSystemLookups = modelSources.Select(Mapper.Map<SourceSystemLookup, DTO.SourceSystemLookup>).ToList();

                Log.Debug("Exiting SourceSystemLookup()");
                return sourceSystemLookups.OrderBy(x => x.Value).ToList();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                throw;
            }
        }

        public List<DTO.UpsellValueLookup> GetUpsellValues()
        {
            try
            {
                Log.Debug("Entering - GetUpsellValues()");
                Log.Debug("Calling Repository.FetchAll() - GetUpsellValues()");
                var modelUpsellValues = UpsellValueLookupRepository.FetchAll();

                var upsellValueLookups = modelUpsellValues.Select(Mapper.Map<UpsellValueLookup, DTO.UpsellValueLookup>).ToList();

                Log.Debug("Exiting SourceSystemLookup()");
                return upsellValueLookups.OrderBy(x => x.Value).ToList();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                throw;
            }
        }
    }
}