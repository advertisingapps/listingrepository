﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.REST.Services
{
    public interface IListingEventService
    {
        void LogEvent(ListingEvent eEvent);
    }
}