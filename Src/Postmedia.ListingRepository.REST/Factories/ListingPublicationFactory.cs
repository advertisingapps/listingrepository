﻿using System.Linq;
using System.Reflection;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Repositories;
using log4net;

namespace Postmedia.ListingRepository.REST.Factories
{
    public class ListingPublicationFactory : IListingPublicationFactory
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private IListingPublicationRepository<ListingPublication> ListingPublicationRepository { get; set; }
        private IUpsellRepository<Upsell> UpsellRepository { get; set; }
        private IPublicationRepository<Publication> PublicationRepository { get; set; }
        private ICategoryRepository<Category> CategoryRepository { get; set; }

        public ListingPublicationFactory(IListingPublicationRepository<ListingPublication> listingPublicationRepository
            ,IUpsellRepository<Upsell> upsellRepository
            ,IPublicationRepository<Publication> publicationRepository
            , ICategoryRepository<Category> categoryRepository)
        {
            ListingPublicationRepository = listingPublicationRepository;
            UpsellRepository = upsellRepository;
            PublicationRepository = publicationRepository;
            CategoryRepository = categoryRepository;
        }

        public ListingPublicationFactory(): this(
            new ListingPublicationRepository()
            , new UpsellRepository()
            , new PublicationRepository()
            , new CategoryRepository())
        {
        }

       public ListingPublication Create(DTO.ListingPublication dto)
       {
           ListingPublication entity = null;

            if (dto != null)
            {
                entity = Create(dto, entity);
            }

            return entity;
        }

        public ListingPublication Create(DTO.ListingPublication dto, ListingPublication entity)
        {
            if (dto != null)
            {
                if(entity == null)
                    entity = new ListingPublication();

                entity = PerformMapping(dto, entity);
            }
            return entity;
        }
        
        private ListingPublication PerformMapping(DTO.ListingPublication dto, ListingPublication entity)
        {
            entity.ExpireDate = dto.ExpireDate;
            entity.StartDate = dto.StartDate;
            entity.Status = dto.Status;

            int maxUpsellCount = entity.Upsells.Count;
            for (int index = 0; index < maxUpsellCount; index++)
            {
                entity.Upsells.RemoveAt(0);
            }

            if ((dto.Upsells != null) && (dto.Upsells.Any()))
            {
                foreach (var upsell in dto.Upsells)
                {
                    var us = new Upsell
                                 {
                                     Name = upsell.Name,
                                     Code = upsell.Code
                                 };
                    entity.AddUpsell(us);
                }
            }



            int maxCategoryCount = entity.Categories.Count;
            for (int index = 0; index < maxCategoryCount; index++)
            {
                entity.Categories.RemoveAt(0);
            }

            if ((dto.Categories!= null) && (dto.Categories.Any()))
            {
                foreach (var category in dto.Categories)
                {
                    var cat = new Category()
                    {
                        Name = category.Name,
                        Code = category.Code
                    };
                    entity.AddCategory(cat);
                }
            }

            if (dto.Publication != null)
            {
                entity.Publication = PublicationRepository.FetchByCode(dto.Publication.Code);
            }
            
            return entity;
        }


    }
}