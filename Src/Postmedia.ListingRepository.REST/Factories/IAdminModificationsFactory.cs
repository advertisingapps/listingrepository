﻿using System.Collections.Generic;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.REST.Factories
{
    public interface IAdminModificationsFactory
    {
        List<AdminModifications> FindChanges(string listingNumber, Listing currentListing, Listing newListing);

        Listing ResolveAdminMasterFields(List<Entities.AdminModifications> adMods, Listing currentListing,
                                         Listing newListing);
    }
}