﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Factories
{
    public class AddressFactory : IAddressFactory
    {
        public Address Create(DTO.Address dto)
        {
            Address dest = null;
            if (dto != null)
            {
                dest = Create(dto, dest);
            }

            return dest;
        }

        public Address Create(DTO.Address dto, Address entity)
        {
            if (dto != null)
            {
                if (entity == null)
                    entity = new Address();

                entity = PerformMapping(dto, entity);
            }
            return entity;
        }

        private Address PerformMapping(DTO.Address dto, Address entity)
        {
            if (dto != null)
            {
                entity.City = dto.City;
                entity.Country = dto.Country;
                entity.Neighbourhood = dto.Neighbourhood;
                entity.PostalCode = dto.PostalCode;
                entity.Province = dto.Province;
                entity.Region = dto.Region;

                int maxCount = entity.AddressLines.Count;
                for (int index = 0; index < maxCount; index++)
                {
                    entity.AddressLines.RemoveAt(0);
                }

                if ((dto.AddressLines != null) && (dto.AddressLines.Any()))
                {
                    foreach (var addressLine in dto.AddressLines)
                    {
                        var line = new AddressLine()
                                       {
                                           LineText = addressLine.LineText,
                                           LineNumber = addressLine.Number
                                       };
                        entity.AddAddressLine(line);
                    }
                }
            }
            return entity;
        }
    }
}