﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.REST.Factories
{
    public interface IListingEventFactory
    {
        ListingEvent GetListingEvent(string listingNumber, ListingEventType type);
        Entities.ListingEvent DtoToEntity(DTO.ListingEvent dto);
    }
}
