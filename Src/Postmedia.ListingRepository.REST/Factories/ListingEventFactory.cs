﻿using System;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;
using Postmedia.ListingRepository.REST.Services;

namespace Postmedia.ListingRepository.REST.Factories
{
    public class ListingEventFactory: IListingEventFactory
    {
        public ListingEvent GetListingEvent(string listingNumber, ListingEventType type)
        {
            try
            {
                var listingEvent = new ListingEvent()
                {
                    ListingNumber = listingNumber,
                    FeedName = "LRWebService",
                    EventType = type.ToString()
                };

                return listingEvent;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Entities.ListingEvent DtoToEntity(DTO.ListingEvent dto)
        {
            var dest = new Entities.ListingEvent()
                {
                    ListingNumber = dto.ListingNumber,
                    FeedName = dto.FeedName,
                    EventDescription = dto.EventDescription,
                    EventType = dto.EventType,
                    RowsAffected = dto.RowsAffected
                };

            return dest;
        }
    }
}