using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Factories
{
    public interface IAddressFactory
    {
        Address Create(DTO.Address dto);
        Address Create(DTO.Address dto, Address entity);
    }
}