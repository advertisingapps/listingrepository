using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Factories
{
    public interface IListingPublicationFactory
    {
        ListingPublication Create(DTO.ListingPublication dto);
        ListingPublication Create(DTO.ListingPublication dto, ListingPublication entity);
    }
}