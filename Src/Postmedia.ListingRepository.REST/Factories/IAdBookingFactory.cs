﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Postmedia.ListingRepository.REST.Entities;

namespace Postmedia.ListingRepository.REST.Factories
{
    public interface IAdBookingFactory
    {
        AdBooking Create(DTO.AdBooking source);
        AdBooking Create(DTO.AdBooking source, AdBooking dest);
    }
}
