﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class Media : BaseDomain
    {
        public virtual string Url { get; set; }
        public virtual string MimeType { get; set; }
        public virtual string Role { get; set; }

        public virtual Listing Listing { get; set; }
    }
}