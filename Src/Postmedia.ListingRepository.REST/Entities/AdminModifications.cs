﻿using System;
using System.Collections.Generic;
using IglooCoder.Commons.WcfNhibernate;


namespace Postmedia.ListingRepository.REST.Entities
{
    public class AdminModifications: BaseDomain
    {
        public virtual string ListingNumber { get; set; }
        public virtual string Field { get; set; }
        public virtual DateTime DateModified { get; set; }
    }
}