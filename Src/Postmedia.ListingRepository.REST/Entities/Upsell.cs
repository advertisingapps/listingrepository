﻿using Postmedia.ListingRepository.REST.Entities.BaseClasses;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class Upsell : CodeNameBase
    {
        public virtual ListingPublication ListingPublication { get; set; }
    }
}