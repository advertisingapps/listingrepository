﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class BatchLogItem : BaseDomain
    {
        public virtual long OutboundCacheID { get; set; }
        public virtual BatchLog Batch { get; set; }
    }
}