﻿using System;
using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class CacheEntry : BaseDomain
    {
        public virtual string FeedName { get; set; }
        public virtual string ListingNumber { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? ExpireDate { get; set; }
        public virtual DateTime? LastModifiedDate { get; set; }
        public virtual string CacheData { get; set; }
    }
    
}