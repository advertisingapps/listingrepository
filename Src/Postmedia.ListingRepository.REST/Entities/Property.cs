﻿using Postmedia.ListingRepository.REST.Entities.BaseClasses;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class Property : CodeValueBase
    {
        public virtual Listing Listing { get; set; }
    }
}