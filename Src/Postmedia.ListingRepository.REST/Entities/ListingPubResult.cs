﻿using IglooCoder.Commons.WcfNhibernate;
using System;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class ListingPubResult : BaseDomain
    {
        public virtual long ListingId { get; set; }
        public virtual long ListingPublicationId { get; set; }
        public virtual string Division { get; set; }
        public virtual string AccountNumber { get; set; }
        public virtual string ListingNumber { get; set; }
        public virtual string AdOrderNumber { get; set; }
        public virtual string FeedName { get; set; }
        public virtual string Publication { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime ExpireDate { get; set; }
        public virtual string Status { get; set; }
        public virtual string Category { get; set; }
        public virtual string Caption { get; set; }
        public virtual string Source { get; set; }
    }
}