﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class BatchExportSummary : BaseDomain
    {
        public virtual int NumberOfRecords { get; set; }
    }
}