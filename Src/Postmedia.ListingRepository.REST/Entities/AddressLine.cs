﻿using IglooCoder.Commons.WcfNhibernate;

namespace Postmedia.ListingRepository.REST.Entities
{
    public class AddressLine : BaseDomain
    {
        public virtual int LineNumber { get; set; }
        public virtual string LineText { get; set; }

        public virtual Address Address { get; set; }
    }
}