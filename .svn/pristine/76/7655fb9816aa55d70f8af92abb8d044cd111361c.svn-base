﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using Postmedia.ListingRepository.Core;
using Postmedia.ListingRepository.DTO;

namespace Postmedia.ListingRepository.REST.Factories
{
    public class AdminModificationsFactory: IAdminModificationsFactory
    {
        public Listing ResolveAdminMasterFields(List<Entities.AdminModifications> adMods, Listing currentListing, Listing newListing)
        {
            if(adMods.FindAll(x => x.Field == "Listing.Description").Count > 0)
            {
                newListing.Description = currentListing.Description;
            }

            foreach (var adMod in adMods.FindAll(x => x.Field.StartsWith("Property")))
            {
                var propInNewListing = newListing.Properties.Find(x => x.Code == adMod.Field.Replace("Property.", ""));
                if (propInNewListing != null)
                {
                    newListing.Properties.Remove(propInNewListing);
                }

                var propInOldListing = currentListing.Properties.Find(x => x.Code == adMod.Field.Replace("Property.", ""));
                if (propInOldListing != null)
                {
                    newListing.Properties.Add(propInOldListing);
                }
            }

            return newListing;
        }

        public List<AdminModifications> FindChanges(string listingNumber, Listing currentListing, Listing newListing)
        {
            var adMods = new List<AdminModifications>();

            var descMod = CompareString(currentListing.Description, newListing.Description);
            if (descMod != ModType.None)
            adMods.Add(new AdminModifications()
            {
                ListingNumber = listingNumber,
                Field = "Listing.Description",
                DateModified = DateTime.Now,
                ModType = descMod.ToString()
            });
            
            foreach (var currentProperty in currentListing.Properties)
            {
                var newProperty = newListing.Properties.Find(x => x.Code == currentProperty.Code);

                var modType = ComparePropertyModType(currentProperty, newProperty);
                
                if(modType != ModType.None)
                {
                    adMods.Add(new AdminModifications()
                    {
                        ListingNumber = listingNumber,
                        Field = "Property." + currentProperty.Code,
                        DateModified = DateTime.Now,
                        ModType = modType.ToString()
                    });
                }
            }

            return adMods;
        }
        
        private ModType CompareString(string currentString, string newString)
        {
            if (newString == null) return ModType.Delete;

            return (currentString == newString) ? ModType.None : ModType.Update;
        }

        private ModType ComparePropertyModType(Property currentProp, Property newProp)
        {
            if (newProp == null) return ModType.Delete;

            return (currentProp.Code == newProp.Code && currentProp.Value == newProp.Value) ? ModType.None : ModType.Update;
        }
    }
}