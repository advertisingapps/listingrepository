﻿using System;
using NUnit.Framework;
using Postmedia.ListingRepository.REST.Entities;
using Postmedia.ListingRepository.REST.Factories;
using Postmedia.ListingRepository.REST.Repositories;
using Rhino.Mocks;

namespace Postmedia.ListingRepository.Tests.Unit.REST.Factories
{
    [TestFixture]
    public class ListingFactoryTests : IFactoryTests
    {
        public void Setup()
        {
            throw new NotImplementedException();
        }

        public Listing CreateEntityListing()
        {
            return new Listing
                       {
                           AdType = "destType",
                           Description = "destDesc",
                           ListingNumber = "destNum",
                           Source = "destSource",
                           Title = "destTitle"
                       };
        }

        public DTO.Listing CreateDTOListing()
        {
            return new DTO.Listing
                       {
                           AdType = "sourceType",
                           Description = "sourceDesc",
                           ListingNumber = "sourceNum",
                           Source = "sourceSource",
                           Title = "sourceTitle"
                       };
        }

        [Test]
        public void When_create_is_called_with_a_null_source_a_null_object_is_returned()
        {
            //Arrange
            var stubAdvertiserFactory = MockRepository.GenerateStub<IAdvertiserFactory>();
            var stubAdBookingFactory = MockRepository.GenerateStub<IAdBookingFactory>();
            var stubListingPublicationFactory = MockRepository.GenerateStub<IListingPublicationFactory>();
            var categoryRepository = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            var factory = new ListingFactory(stubListingPublicationFactory, stubAdvertiserFactory, stubAdBookingFactory, categoryRepository);

            //Act
            var dest = factory.Create(null);

            //Assert
            Assert.That(dest, Is.Null);

        }

        [Test]
        public void When_create_is_called_with_a_null_dest_and_null_source_a_null_object_is_returned()
        {
            //Arrange
            var stubAdvertiserFactory = MockRepository.GenerateStub<IAdvertiserFactory>();
            var stubAdBookingFactory = MockRepository.GenerateStub<IAdBookingFactory>();
            var stubListingPublicationFactory = MockRepository.GenerateStub<IListingPublicationFactory>();
            var categoryRepository = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            var factory = new ListingFactory(stubListingPublicationFactory, stubAdvertiserFactory, stubAdBookingFactory, categoryRepository);

            //Act
            var dest = factory.Create(null, null);

            //Assert
            Assert.That(dest, Is.Null);

        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubAdvertiserFactory = MockRepository.GenerateStub<IAdvertiserFactory>();
            var stubAdBookingFactory = MockRepository.GenerateStub<IAdBookingFactory>();
            var stubListingPublicationFactory = MockRepository.GenerateStub<IListingPublicationFactory>();
            var categoryRepository = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            stubListingPublicationFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);
            stubAdBookingFactory.Stub(x => x.Create(null, null)).IgnoreArguments().Return(null);
            stubAdvertiserFactory.Stub(x => x.Create(null, null)).IgnoreArguments().Return(null);
            var factory = new ListingFactory(stubListingPublicationFactory, stubAdvertiserFactory, stubAdBookingFactory, categoryRepository);

            var source = CreateDTOListing();

            //Act
            var dest = factory.Create(source, null);

            //Assert
            Assert.That(source, Is.Not.Null);
            Assert.That(source.AdType, Is.EqualTo(dest.AdType));
            Assert.That(source.Description, Is.EqualTo(dest.Description));
            Assert.That(source.ListingNumber, Is.EqualTo(dest.ListingNumber));
            Assert.That(source.Source, Is.EqualTo(dest.Source));
            Assert.That(source.Title, Is.EqualTo(dest.Title));
        }
        
        [Test]
        public void When_create_is_called_with_a_populated_source_and_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubAdvertiserFactory = MockRepository.GenerateStub<IAdvertiserFactory>();
            var stubAdBookingFactory = MockRepository.GenerateStub<IAdBookingFactory>();
            var stubListingPublicationFactory = MockRepository.GenerateStub<IListingPublicationFactory>();
            var categoryRepository = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            stubListingPublicationFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);
            stubAdBookingFactory.Stub(x => x.Create(null, null)).IgnoreArguments().Return(null);
            stubAdvertiserFactory.Stub(x => x.Create(null, null)).IgnoreArguments().Return(null);
            var factory = new ListingFactory(stubListingPublicationFactory, stubAdvertiserFactory, stubAdBookingFactory, categoryRepository);

            var source = CreateDTOListing();

            //Act
            var dest = factory.Create(source, new Listing());

            //Assert
            Assert.That(source, Is.Not.Null);
            Assert.That(source.AdType, Is.EqualTo(dest.AdType));
            Assert.That(source.Description, Is.EqualTo(dest.Description));
            Assert.That(source.ListingNumber, Is.EqualTo(dest.ListingNumber));
            Assert.That(source.Source, Is.EqualTo(dest.Source));
            Assert.That(source.Title, Is.EqualTo(dest.Title));
        }

        [Test]
        public void When_create_is_called_with_a_populated_source_and_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubAdvertiserFactory = MockRepository.GenerateStub<IAdvertiserFactory>();
            var stubAdBookingFactory = MockRepository.GenerateStub<IAdBookingFactory>();
            var stubListingPublicationFactory = MockRepository.GenerateStub<IListingPublicationFactory>();
            var categoryRepository = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            stubListingPublicationFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);
            stubAdBookingFactory.Stub(x => x.Create(null, null)).IgnoreArguments().Return(null);
            stubAdvertiserFactory.Stub(x => x.Create(null, null)).IgnoreArguments().Return(null);
            var factory = new ListingFactory(stubListingPublicationFactory, stubAdvertiserFactory, stubAdBookingFactory, categoryRepository);

            var source = CreateDTOListing();

            var dest = CreateEntityListing();

            //Act
             dest = factory.Create(source, dest);

            //Assert
            Assert.That(source, Is.Not.Null);
            Assert.That(source.AdType, Is.EqualTo(dest.AdType));
            Assert.That(source.Description, Is.EqualTo(dest.Description));
            Assert.That(source.ListingNumber, Is.EqualTo(dest.ListingNumber));
            Assert.That(source.Source, Is.EqualTo(dest.Source));
            Assert.That(source.Title, Is.EqualTo(dest.Title));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_null_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubAdvertiserFactory = MockRepository.GenerateStub<IAdvertiserFactory>();
            var stubAdBookingFactory = MockRepository.GenerateStub<IAdBookingFactory>();
            var stubListingPublicationFactory = MockRepository.GenerateStub<IListingPublicationFactory>();
            var categoryRepository = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            stubListingPublicationFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);
            stubAdBookingFactory.Stub(x => x.Create(null, null)).IgnoreArguments().Return(null);
            stubAdvertiserFactory.Stub(x => x.Create(null, null)).IgnoreArguments().Return(null);
            var factory = new ListingFactory(stubListingPublicationFactory, stubAdvertiserFactory, stubAdBookingFactory, categoryRepository);

            var source = new DTO.Listing();

            //Act
            var dest = factory.Create(source, null);

            //Assert
            Assert.That(source, Is.Not.Null);
            Assert.That(source.AdType, Is.EqualTo(dest.AdType));
            Assert.That(source.Description, Is.EqualTo(dest.Description));
            Assert.That(source.ListingNumber, Is.EqualTo(dest.ListingNumber));
            Assert.That(source.Source, Is.EqualTo(dest.Source));
            Assert.That(source.Title, Is.EqualTo(dest.Title));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_an_unpopulated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubAdvertiserFactory = MockRepository.GenerateStub<IAdvertiserFactory>();
            var stubAdBookingFactory = MockRepository.GenerateStub<IAdBookingFactory>();
            var stubListingPublicationFactory = MockRepository.GenerateStub<IListingPublicationFactory>();
            var categoryRepository = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            stubListingPublicationFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);
            stubAdBookingFactory.Stub(x => x.Create(null, null)).IgnoreArguments().Return(null);
            stubAdvertiserFactory.Stub(x => x.Create(null, null)).IgnoreArguments().Return(null);
            var factory = new ListingFactory(stubListingPublicationFactory, stubAdvertiserFactory, stubAdBookingFactory, categoryRepository);

            var source = new DTO.Listing();

            var dest = new Listing();

            //Act
            dest = factory.Create(source, dest);

            //Assert
            Assert.That(source, Is.Not.Null);
            Assert.That(source.AdType, Is.EqualTo(dest.AdType));
            Assert.That(source.Description, Is.EqualTo(dest.Description));
            Assert.That(source.ListingNumber, Is.EqualTo(dest.ListingNumber));
            Assert.That(source.Source, Is.EqualTo(dest.Source));
            Assert.That(source.Title, Is.EqualTo(dest.Title));
        }

        [Test]
        public void When_create_is_called_with_an_unpopulated_source_and_a_populated_dest_an_entity_is_returned_with_the_same_values_as_the_source()
        {
            //Arrange
            var stubAdvertiserFactory = MockRepository.GenerateStub<IAdvertiserFactory>();
            var stubAdBookingFactory = MockRepository.GenerateStub<IAdBookingFactory>();
            var stubListingPublicationFactory = MockRepository.GenerateStub<IListingPublicationFactory>();
            var categoryRepository = MockRepository.GenerateStub<ICategoryRepository<Category>>();
            stubListingPublicationFactory.Stub(x => x.Create(null)).IgnoreArguments().Return(null);
            stubAdBookingFactory.Stub(x => x.Create(null, null)).IgnoreArguments().Return(null);
            stubAdvertiserFactory.Stub(x => x.Create(null, null)).IgnoreArguments().Return(null);
            var factory = new ListingFactory(stubListingPublicationFactory, stubAdvertiserFactory, stubAdBookingFactory, categoryRepository);

            var source = CreateDTOListing();

            var dest = CreateEntityListing();

            //Act
            dest = factory.Create(source, dest);

            //Assert
            Assert.That(source, Is.Not.Null);
            Assert.That(source.AdType, Is.EqualTo(dest.AdType));
            Assert.That(source.Description, Is.EqualTo(dest.Description));
            Assert.That(source.ListingNumber, Is.EqualTo(dest.ListingNumber));
            Assert.That(source.Source, Is.EqualTo(dest.Source));
            Assert.That(source.Title, Is.EqualTo(dest.Title));
        }
    }
}
